﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-left">
                <ul>
                    <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                    <li>Error</li>
                </ul>
            </div>
        </div>
    </div>
</div>
    <section class="error">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>404</h2>
                <img src="images/oops.png" alt="Error"><p>The Page you requested was not found!</p>
                <a href="index.html" class="button">Back to Home</a>
            </div>
        </div>
    </div>
</section>
</asp:Content>

