if (typeof SITE_URL === 'undefined') SITE_URL = 'http://localhost/mypsm/';

// https://github.com/Pixabay/jQuery-autoComplete/ Update:14 March 2016 
jQuery(function(){!function(e){e.fn.autoC=function(t){var o=e.extend({},e.fn.autoC.defaults,t);return"string"==typeof t?(this.each(function(){var o=e(this);"destroy"==t&&(e(window).off("resize.autoC",o.updateSC),o.off("blur.autoC focus.autoC keydown.autoC keyup.autoC"),o.data("autoC")?o.attr("autoC",o.data("autoC")):o.removeAttr("autoC"),e(o.data("sc")).remove(),o.removeData("sc").removeData("autoC"))}),this):this.each(function(){function t(e){var t=s.val();if(s.cache[t]=e,e.length&&t.length>=o.minChars){for(var a="",c=0;c<e.length;c++)a+=o.renderItem(e[c],t);s.sc.html(a),s.updateSC(0)}else s.sc.hide()}var s=e(this);s.sc=e('<div class="autoC-ss '+o.menuClass+'"></div>'),s.data("sc",s.sc).data("autoC",s.attr("autoC")),s.attr("autoC","off"),s.cache={},s.last_val="",s.updateSC=function(t,o){if(s.sc.css({top:s.offset().top+s.outerHeight(),left:s.offset().left,width:s.outerWidth()}),!t&&(s.sc.show(),s.sc.maxHeight||(s.sc.maxHeight=parseInt(s.sc.css("max-height"))),s.sc.suggestionHeight||(s.sc.suggestionHeight=e(".autoC-s",s.sc).first().outerHeight()),s.sc.suggestionHeight))if(o){var a=s.sc.scrollTop(),c=o.offset().top-s.sc.offset().top;c+s.sc.suggestionHeight-s.sc.maxHeight>0?s.sc.scrollTop(c+s.sc.suggestionHeight+a-s.sc.maxHeight):0>c&&s.sc.scrollTop(c+a)}else s.sc.scrollTop(0)},e(window).on("resize.autoC",s.updateSC),s.sc.appendTo("body"),s.sc.on("mouseleave",".autoC-s",function(){e(".autoC-s.s").removeClass("s")}),s.sc.on("mouseenter",".autoC-s",function(){e(".autoC-s.s").removeClass("s"),e(this).addClass("s")}),s.sc.on("mousedown click",".autoC-s",function(t){var a=e(this),c=a.data("val");return(c||a.hasClass("autoC-s"))&&(s.val(c),o.onSelect(t,c,a),s.sc.hide()),!1}),s.on("blur.autoC",function(){try{over_sb=e(".autoC-ss:hover").length}catch(t){over_sb=0}over_sb?s.is(":focus")||setTimeout(function(){s.focus()},20):(s.last_val=s.val(),s.sc.hide(),setTimeout(function(){s.sc.hide()},350))}),o.minChars||s.on("focus.autoC",function(){s.last_val="\n",s.trigger("keyup.autoC")}),s.on("keydown.autoC",function(t){if((40==t.which||38==t.which)&&s.sc.html()){var a,c=e(".autoC-s.s",s.sc);return c.length?(a=40==t.which?c.next(".autoC-s"):c.prev(".autoC-s"),a.length?(c.removeClass("s"),s.val(a.addClass("s").data("val"))):(c.removeClass("s"),s.val(s.last_val),a=0)):(a=40==t.which?e(".autoC-s",s.sc).first():e(".autoC-s",s.sc).last(),s.val(a.addClass("s").data("val"))),s.updateSC(0,a),!1}if(27==t.which)s.val(s.last_val).sc.hide();else if(13==t.which||9==t.which){var c=e(".autoC-s.s",s.sc);c.length&&s.sc.is(":visible")&&(o.onSelect(t,c.data("val"),c),setTimeout(function(){s.sc.hide()},20))}}),s.on("keyup.autoC",function(a){if(!~e.inArray(a.which,[13,27,35,36,37,38,39,40])){var c=s.val();if(c.length>=o.minChars){if(c!=s.last_val){if(s.last_val=c,clearTimeout(s.timer),o.cache){if(c in s.cache)return void t(s.cache[c]);for(var l=1;l<c.length-o.minChars;l++){var i=c.slice(0,c.length-l);if(i in s.cache&&!s.cache[i].length)return void t([])}}s.timer=setTimeout(function(){o.source(c,t)},o.delay)}}else s.last_val=c,s.sc.hide()}})})},e.fn.autoC.defaults={source:0,minChars:3,delay:150,cache:1,menuClass:"",renderItem:function(e,t){t=t.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&");var o=new RegExp("("+t.split(" ").join("|")+")","gi");return'<div class="autoC-s" data-val="'+e+'">'+e.replace(o,"<b>$1</b>")+"</div>"},onSelect:function(e,t,o){}}}(jQuery);});

// DOM ready, take it away
jQuery(function ($) {'use strict';

	window.scrollTo(0, 1);
		//$('#mob-menu li.has-submenu').click(function(e){
		//	var $this = $(this); 
		//	$this.toggleClass('show').children('ul.submenu').slideToggle();
		//	$this.siblings('.has-submenu').removeClass('show').children('ul.submenu').slideUp();
		//	return false;
		//});

	
	//input_quantity_btns();
	

	//$('.input-search input:not(.bind)').each(function() {
	//	var s = $(this);
	// 	s.addClass('bind').autoC({minChars: 2,cache:false,delay:50,menuClass:'myt-autoS ',
	//		source: function(term, response){
	//			term = term.toLowerCase().replace(/[^A-Z0-9 -]+/ig, " ").trim();
	//			 console.log(term);
	//			$.get("data/productlist.json?q=" + term, function(data){
	//			  //console.log(data);
	//		      response(data);
	//		    });		
	//		},
	//		renderItem: function (item, search){
	//				return '<div class="autoC-s" data-path="'+item[1]+'" >'+item[0]+'</div>';
	//		},
	//		onSelect: function(e, term, item){
	//			if( item.data('path') ){window.location.href = SITE_URL + item.data('path');}
	//		}
	//	});
	//});//AUTO C


});//JQUERY



function openModal(name){
	if ( jQuery('#'+name).length < 1 ) 	 return;
	jQuery('body').addClass('overlay');
	jQuery('#'+name).addClass('show');
}
function closeModal(name){
	jQuery('body').removeClass('overlay');
	jQuery('.myt-modal.show').removeClass('show');
	if(typeof name !== 'undefined') jQuery('#'+name).removeClass('show');
}

// Change Quantity during click on Plus-Minus buttons
//function input_quantity_btns(){
//	jQuery('div.input-quantity button.minus:not(.bind),div.input-quantity button.plus:not(.bind)').click( function() {
//		// Get current quantity values
//		var qty = jQuery( this ).closest( 'div.input-quantity' ).find( 'input.qty' );
//		var val = parseFloat(qty.val());
//		var max = parseFloat(qty.attr( 'max' )) || 99999;
//		var min = parseFloat(qty.attr( 'min' )) || 0;
//		var step = parseFloat(qty.attr( 'step' )) || 1;

//		if ( jQuery( this ).is( '.plus' ) ) 	val += step;
//		else 						   			val -= step;
//		if ( max < val ) 	    				val  = max;
//		else if (  min > val )  				val  = min;

//		qty.val( val);
	  		  	
//  	}).addClass('bind'); 	
//}



