﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ForgotPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string text = File.ReadAllText(Server.MapPath("/User/JS/ChangePassword.js"));
        Page.Header.Controls.Add(
             new LiteralControl(
                "<script id='checksdrivRequiredField'>" + text + "</script>"
            ));
    }
}