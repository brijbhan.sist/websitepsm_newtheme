﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="ThankYou.aspx.cs" Inherits="ThankYou" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <main role="main" class="main">
            <div>
                <div id="cartcontainer">
                    <div class="container">
                        <div class="breadcrumbs">
                            <span><a href="#">Home</a> &gt; </span>
                            <span>Thank you</span>
                        </div>
                    </div>
                    <div id="cart-page" class="clear-div">
                        <div class="container cart-div">
                            <div class="cart-product-div thankyou-div">
                                <span class="cart-title"><strong>Thank You</strong></span>
                                <div class="order-content">Your order has been placed and is being processed. Your order reference number is <b id="bOrderNo"></b>. You will receive an email with the order details.</div>
                                <div class="cart-products-loaded" id="tblCart" style="background: white;">
                                </div>
                                <!-- .cart-products-loaded -->

                                <div class="cart-collaterals">
                                    <div class="cart_totals">
                                        <h2>Cart totals</h2>
                                        <div id="priceSection"></div>
                                    </div>
                                </div>
                                <div class="continue-shopping">
                                    <a href="#">
                                        <button type="button" onclick="location.href = '/home'" class="button white">Continue Shopping</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
</asp:Content>

