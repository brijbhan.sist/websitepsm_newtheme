﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Cart.aspx.cs" Inherits="Cart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <style>
       table thead th{
           text-align:center !important;
       }
       .product-price
       {
           text-align:center !important;
       }
       .product-subtotal
       {
           text-align:center !important;
       }
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="subtotalamount" />
    <main role="main" class="main">
        <div>
            <div id="cartcontainer">
                <div class="container">
                    <div class="breadcrumbs">
                        <span><a href="/">Home</a> &gt; </span>
                        <span>Cart</span>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--======== /Breadcrumb ===========-->
    <!--======= Cart ==============-->

    <div id="cart-page" class="clear-div">
        <div class="container cart-div">
            <div class="cart-product-div">
                <span class="cart-title"><strong>Cart</strong></span>
                <div class="mainsection">
                    <div class="cart-products-loaded" id="tblCart">                        
                    </div>
                    <!-- .cart-products-loaded -->
                </div>

                <div class="cart-collaterals">
                    <div class="cart_totals">
                        <h2>Cart totals</h2>

                        <table cellspacing="0" class="order_table">
                            <tr class="cart-subtotal">
                                <th>Sub Total</th>
                                <td data-title="Subtotal">
                                    <span>
                                        <bdi>$<span id="spSubTotal"></span></bdi>
                                    </span>
                                </td>
                            </tr>
                            <tr class="order-total">
                                <th>Total</th>
                                <td data-title="Total">
                                    <strong>
                                        <span>
                                            <bdi>$<span id="spTotalAmount"></span></bdi>
                                        </span>
                                    </strong>
                                </td>
                            </tr>
                        </table>

                        <div class="proceed-to-checkout">
                            <a href="#" id="btncheckout"  onclick="checkount()" class="checkout-button button">Proceed to checkout</a>
                        </div>
                    </div>
                </div>
                <div class="continue-shopping">
                    <a href="/" class="button white">
                        Continue Shopping</a>
                </div>
            </div>
        </div>
    </div>
    <center>    
 <div id="EmptyTable" class="clear-div" style='display:none''>
                    <img src="images/noresult.png" /><br />
                    <h3>Your cart is empty.</h3>
    </div>
       </center>

</asp:Content>

