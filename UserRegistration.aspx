﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="UserRegistration.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="/themes/css/registration.css" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="clear-div"></div>
    <main role="main" class="main">
        <div>
            <div id="registercontainer">
                <div class="container">
                    <div class="breadcrumbs">
                        <span><a href="/">Home</a> &gt; </span>
                        <span>Registration</span>
                    </div>
                </div>
                <div id="register-page">
                    <div class="container" id="userinformation">
                        <span class="register-title"><strong>Registration</strong></span>
                        <div class="register-div">
                            <div class="register-form" >
                                <div class="form-row">
                                    <input type="text" id="txtFirstName" maxlength="50" name="CompanyName" placeholder="First Name" class="req"/>
                                </div>
                                <div class="form-row">
                                    <input type="text" id="txtLastName" maxlength="50" name="Name" placeholder="Last Name" />
                                </div>
                                <div class="form-row">
                                    <input type="text" maxlength="10" name="Phone" id="txtMobile" placeholder="Mobile Ex-9999999999"  onkeypress='return isNumberKey(event)' class="req"/>
                                </div>
                                <div class="form-row">
                                    <input type="text" name="email" id="txtEmail" maxlength="50" placeholder="Email" onchange="validateEmail()" class="req"/>
                                </div>

                                <div class="form-row">
                                    <input type="password" name="password" maxlength="20" id="txtPassword" placeholder="Password" class="req"/>
                                </div>
                                <div class="form-row">
                                    <input type="password" name="ConfirmPassword" maxlength="20" id="txtConfirmPassword" placeholder="Confirm Password" class="req"/>
                                </div>
                                <div class="form-row" style="text-align: center;">
                                    <button type="button" class="button white" onclick="TempUserRegistration()" id="BtnTempAddUser">Submit</button>
                                    <button type="button" class="button white" style="display: none" onclick="AddUser()" id="BtnAddUser">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container" id="VerifyOTP" style="display:none;">
                        <span class="register-title"><strong>Verify OTP</strong></span>
                        <div class="register-div">
                            <div class="register-form">
                                 <div class="form-row">
                                     <p id="pMessage"></p>
                                </div>
                                <div class="form-row">
                                    <input type="text" id="txtSecurityCode" name="SecurityCode" placeholder=" Security Code" />
                                </div>
                                <div class="form-row" style="text-align: center;">
                                    <button type="button"  class="button white"  onclick="VerifyOTP()" id="btnUploadDocument">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </main>
t>  
</asp:Content>

