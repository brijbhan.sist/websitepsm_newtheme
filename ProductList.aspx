﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="ProductList.aspx.cs" Inherits="ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .page {
            text-align: center;
            display: table;
            width: 100%;
            margin: 12px 0 !important;
        }

            .page li a {
                display: block;
                height: 40px;
                width: 40px;
                line-height: 40px;
                text-align: center;
                background: white;
                color: black;
                border-radius: 5%;
            }

            .page li {
                display: inline-block;
                padding: 0 5px;
            }

        .spn {
            display: block;
            height: 40px;
            width: 40px;
            line-height: 40px;
            text-align: center;
            background: #b02525 !important;
            color: #efefef !important;
            border-radius: 5%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="container" class="clear-div">
        <main role="main" class="main">
            <div>
                <div id="categorycontainer">
                    <div class="container">
                        <div class="breadcrumbs">
                            <span><a href="/">Home</a> &gt; </span>
                            <span><a href="#" id="lblCategory"></a>&gt; </span>
                            <span id="lblSubCategory"></span>
                        </div>
                    </div>

                    <div id="category">
                        <div class="container clear-div">
                            <span class="category-title"><strong><span id="spCategoryTitle"></span></strong></span>

                          
                            <div id="totalofResult" class="show-result"></div>

                            <ul class="clear-div itemdiv" id="product">
                            </ul>
                            <div class="page">
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</asp:Content>

