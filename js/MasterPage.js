﻿$('ul.nav li.dropdown').hover(function () {
    $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(150);
}, function () {
    $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(150);
});

function checkRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '0.00') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreq').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '-Select-' || $(this).val().trim() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}
//For input type text and dropdown
function dynamicALL(className) {
    var boolcheck = true;
    try {
        $('.' + className).each(function () {
            if ($(this).val().trim() == '' || $(this).val().trim() == '0.00' || $(this).val().trim() == '0') {
                boolcheck = false;
                $(this).addClass('border-warning');
            } else {
                $(this).removeClass('border-warning');
            }
        });
    } catch (e) {

    }
    return boolcheck;
}
//For input dropdown select2 single
function dynamicInputTypeSelect2(className) {
    var boolcheck = true;
    $('.' + className).each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}
//For input dropdown select2 multiple
function dynamicInputTypeSelect2Multi(className) {
    var boolcheck = true;
    $('.' + className).each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--multiple').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--multiple').removeAttr('style');
        }
    });
    return boolcheck;
}
