﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="ProductDetails.aspx.cs" Inherits="ProductDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="/themes/css/lightslider.min.css" type="text/css" media="all" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="itemcontainer">
        <div class="container" id="breadcrumbsdata">
            <div class="breadcrumbs">
                <span><a href="/">Home</a> &gt; </span>
                <span><a href="#" id="spCategory">E-cigarettes & Vaporizer</a> &gt; </span>
                <span id="spProductName">E-cigarettes & Refills </span>
            </div>
        </div>        
        <div id="itemdetail">
            <div class="container">
                <!-- Product Description Start-->
                <div class="iteminfo clear-div item" id="productDetails">
                </div>
                <!--Product Description End-->
                <div class="item-title">
                    <span><strong>Related Products</strong></span>
                </div>
                <!--Similar Products Start-->
                <div id="item">
                </div>
                <!--Similar Products End-->
            </div>
        </div>
    </div>
</asp:Content>

