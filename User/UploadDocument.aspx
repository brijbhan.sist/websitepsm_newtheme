﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="UploadDocument.aspx.cs" Inherits="User_UploadDocument" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <span><a href="/Manage_Store">Home</a> &gt; </span>
    <span>Document List</span>
    <style>
        .viewdoc {
            color: #F0F0F0;
            position: relative;
            text-transform: uppercase;
            text-decoration: none;
            line-height: 14px;
            border-radius: 5px;
            letter-spacing: 0.5px;
            background: #1e9ff2;
            border-radius: 6px;padding: 2px 9px;
            border: none;
            outline: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Conten2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <span class="cart-title"><strong>Document List</strong></span>
    <div class="order-div clear-div">
        <table class="table document-list" id="DocumentDetails" style="display: none;">
            <thead>
                <tr class="table-heading">
                    <td class="Action">Action</td>
                    <td class="DocumentName">Document Name</td>
                    <td class="IsRequired">Is Required</td>
                    <td class="View">View</td>
                    <td class="UploadDate">Upload Date</td>
                    <td class="Status">Status</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="order-div clear-div">
        <table class="table document-list" id="ApprovedDocumentDetails" style="display: none;">
            <thead>
                <tr class="table-heading">
                    <td class="ADocumentName">Document Name</td>
                    <td class="AView">View</td>
                    <td class="AUploadDate">Upload Date</td>
                    <td class="AStatus">Status</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="modal fade containerheight" id="modalUploadDocument">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="display: block; background: #d60000; color: white;">
                    <h5 class="modal-title">Document List</h5>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <label>Document</label>

                        <input type="text" id="txtDocument" disabled style="margin-top: 0" class="form-control" />

                    </div>
                    <div class="form-row">
                        <label>Upload <span class="required">*</span></label><br />
                        <input type="file" class="req" style="font-size: 20px" id="fileUploadDocument" />
                    </div>
                    <div class="form-row">
                        <br />
                        <span style="color: red; font-size: 14px; font-weight: 400">[ Only PDF, JPG, PNG, DOC File are allowed ]</span>
                    </div>

                    <div class="form-row" style="text-align: right;">
                        <input type="hidden" id="hfDocId" style="display: none" />
                        <input type="button" class="button white" value="Upload" onclick="UploadDocument();" id="btnUploadDocument" />
                        <button type="button" class="button white" title="close" data-dismiss="modal" aria-label="Close">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="/User/JS/UploadDocument.js"></script>
</asp:Content>

