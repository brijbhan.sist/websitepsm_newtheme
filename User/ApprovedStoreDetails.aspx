﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="ApprovedStoreDetails.aspx.cs" Inherits="User_ApprovedStoreDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <span><a href="/Manage_Store">Home</a> &gt; </span>
    <span>>Add Store</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <span class="cart-title"><strong>Add Store</strong></span>

    <div class="add-store clear-div">
        <div class="col clear-div">
            <div class="title"><strong>Store Details</strong></div>
            <div class="col-info">
                <div class="form-row">
                    <label>Business Name :</label>
                    <span id="txtBusinessName" ></span>
                </div>
                <div class="form-row">
                    <label>Store Name :</label>
                    <span id="txtName" ></span>
                </div>
                <div class="form-row">
                    <label>Store Open Time :</label>
                    <span id="fromTime" ></span>
                </div>
            </div>
            <div class="col-info">
                <div class="form-row">
                    <label>OPT License :</label>
                    <span id="txtOPTLicense" ></span>
                </div>
                <div class="form-row">
                    <label>Tax ID :</label>
                    <span id="TaxID" ></span>
                </div>
                <div class="form-row">
                    <label>Store Close Time :</label>
                    <span id="toTime" ></span>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="title"><strong>Billing Address</strong></div>
            <div class="col-info">
                <div class="form-row">
                    <label>Address 1 : </label>
                    <span id="txtBillAddress" ></span>
                </div>
                <div class="form-row">
                    <label>City :</label>
                    <span id="txtBillCity" ></span>
                </div>
            </div>
            <div class="col-info">
                <div class="form-row">
                    <label>Address 2 : </label>
                    <span id="txtBillAddress2" ></span>
                </div>
                <div class="form-row">
                    <label>Zip Code :</label>
                    <span id="txtBillZipcode" ></span>
                </div>
                
            </div>
            <div class="col-info">
                <div class="form-row">
                    <label>State :</label>
                    <span id="txtBillState" ></span>
                </div>
            </div>
            <div class="col-info" style="display: none">
                <div class="form-row">
                    <label>Latitude :</label>
                    <span id="txtBLatitude" ></span>
                </div>
                <div class="form-row">
                    <label>Longitude : </label>
                    <span id="txtBLongitude" ></span>

                </div>
            </div>
        </div>

        <div class="col">
            <div class="title"><strong>Shipping address </strong></div>
            <div class="col-info">
                <div class="form-row">
                    <label>Address 1 :</label>
                    <span id="txtShippingAddress" ></span>
                </div>
                <div class="form-row">
                    <label>City :</label>
                    <span id="txtShipCity" ></span>
                </div>
            </div>
            <div class="col-info">
                 <div class="form-row">
                    <label>Address 2 :</label>
                    <span id="txtShippingAddress2" ></span>
                </div>
                <div class="form-row">
                    <label>Zip Code :</label>
                    <span id="txtShipZipCode" ></span>
                </div>

            </div>
            <div class="col-info">               
                <div class="form-row">
                    <label>State :</label>
                    <span id="txtShipState" ></span>
                </div>
            </div>
            <div class="col-info" style="display: none">
                <div class="form-row">
                    <label>Latitude :</label>
                    <span id="txtSLatitude" ></span>
                </div>
                <div class="form-row">
                    <label>Longitude :</label>
                    <span id="txtSLongitude" ></span>

                </div>
            </div>

        </div>
        <div class="col">
            <div class="title"><strong>Contact Details</strong></div>
            <table class="table store-list" id="tblContactDetails">
                <thead>
                    <tr class="table-heading">
                        <td class="SetAsDefault">Default</td>
                        <td class="Action">Contact Person</td>
                        <td class="Emails">Email</td>
                        <td class="Numbers">Mobile</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

    </div>
</asp:Content>

