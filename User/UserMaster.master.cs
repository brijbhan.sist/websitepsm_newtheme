﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_UserMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/AppJS/HomePageResponsive.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
            if (Session["UserId"] == null)
            {
                Response.Redirect("/");
            }
        }
        if (Session["UserId"] != null)
        {
            var user = "";string status = "";
            if (Session["StoreStatus"]!=null)
            {
                if(Convert.ToInt32(Session["LocationStatus"]) == 0)
                {
                    status = "Pending";
                }
                else
                {
                    status = "Approved";
                }
            }
            if (Session["StoreName"] != null)
            {
                user = Session["StoreName"].ToString();
                if (user.Length > 20)
                {

                    username.InnerHtml = user + " " + "(" + status + ")";
                   
                }
                else
                {
                    username.InnerHtml = Session["StoreName"].ToString() + " " + "(" + status + ")";
                    
                }
            }
            else
            {
                user = Session["CustomerName"].ToString();
                if (user.Length > 20)
                {
                    username.InnerHtml = user;
                   
                }
                else
                {
                    username.InnerHtml = Session["CustomerName"].ToString();
                }
            }
        }
       
    }
}
