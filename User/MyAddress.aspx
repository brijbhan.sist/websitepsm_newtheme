﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="MyAddress.aspx.cs" Inherits="User_MyAddress" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-header">
        <h3>Address Details</h3>
    </div>
    <div class="jumbotron containerheight" style="padding: 20px 0px 20px 20px; background-color: white;">
        <section class="contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="#" onclick="OpenAddressPopup()" class="btn btn-xs btn-info">Add New</a>
                    </div>
                </div>
                <div class="row" id="divAddress"></div>
            </div>
        </section>
        <div class="modal fade" id="AddAddress">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                            <div class="col-md-11">
                                <h5 class="modal-title">Address Details</h5>
                            </div>
                            <div class="col-md-1">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="BillEntry">
                            <div class="col-md-6">
                                <div class="form-control">
                                    <label class="col-sm-12 col-lg-4  ">
                                        Address <span style="color: red">*</span>
                                    </label>
                                    <div class="col-sm-12 col-lg-8">
                                        <input type="text" id="txtBillAddress" name="Address1" placeholder=" Address" class="textprop reqb" />
                                    </div>
                                </div>
                                <div class="form-control">
                                    <label class="col-sm-12 col-lg-4  ">
                                        City 
                                    </label>
                                    <div class="col-sm-12 col-lg-8">
                                        <input type="text" id="txtBillCity" readonly="readonly" name="City" placeholder="City" class="textprop" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-control">
                                    <label class="col-sm-12 col-lg-4  ">
                                        Zip Code <span style="color: red">*</span>
                                    </label>
                                    <div class="col-sm-12 col-lg-8">
                                        <select id="ddlBillZipCode" name="zipcode" onchange="getCityState()" class="ddlreqb" style="width: 100% !important;">
                                            <option value="0">[Choose an option]</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-control">
                                    <label class="col-sm-12 col-lg-4  ">
                                        State 
                                    </label>
                                    <div class="col-sm-12 col-lg-8">
                                        <input type="text" id="txtBillState" readonly="readonly" name="State" placeholder="State" class="textprop" />
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnBilling" onclick="AddBilling()" class="btn btn-xs btn-success">
                            Add
                        </button>
                        <input type="button" value="Close" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close" />
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</asp:Content>

