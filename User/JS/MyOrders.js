﻿$(document).ready(function () {
    getOrderDetails(1);
})
function Pagevalue(e) {
    getOrderDetails(parseInt($(e).attr("page")));
}
function getOrderDetails(PageIndex) {
    var apiUrl = $("#ApiAppKey").html();
    var UserName = $("#hfEmail").val();
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var rc = new Object();
    rc.StoreId = StoreId;
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.Location = $("#hfLocation").val();
    rc.PageIndex = PageIndex;
    rc.PageSize = 5;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/PlaceOrder.asmx/OrderList",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var html = '';
            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    var name = "";
                    var OrderDetail = response.d.responseNewData;
                    console.log(OrderDetail);
                    var list = OrderDetail[0].OrderDetails;
                    $.each(list, function (i, item) {
                        html += '<div class="order-heading clear-div">'
                        html += '<a href="OrderDetails?PageId=' + item.OrderNo + '">'
                        html += '<span>' + item.OrderNo + " (" + item.OrderDate + ")" + '</span>'
                        html += '<span style="float: right;">' + item.Status + '</span>'
                        html += '</a>'
                        html += '</div>'
                        html += '<div class="order-info clear-div">'
                        html += '<div>'
                        html += '<div class="col-label"><strong>Sales Person</strong></div>'
                        html += '<div>' + item.SalesPerson + '</div>'
                        html += '</div>'
                        html += '<div>'
                        html += '<div class="col-label"><strong>Products</strong></div>'
                        html += '<div>' + item.TotalItem + '</div>'
                        html += '</div>'
                        html += '<div>'
                        html += '<div class="col-label"><strong>Shipping Type</strong></div>'
                        html += '<div>' + item.ShippingType + '</div>'
                        html += '</div>'
                        html += '<div>'
                        html += '<div class="col-label"><strong>Total Amount</strong></div>'
                        html += '<div><span>&#36;</span>' + parseFloat(item.PayableAmount).toFixed(2) + '</div>'
                        html += '</div>'
                        html += '<div>'
                        html += '<div class="col-label"><strong>Ship To</strong></div>'
                        html += '<div>' + item.Address + '</div>'
                        html += '</div>'
                        html += '</div >'
                        html += '<br/>'
                    });
                    $("#order_div").html(html);

                    $(".page").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(OrderDetail[0].PageIndex),
                        PageSize: parseInt(OrderDetail[0].PageSize),
                        RecordCount: parseInt(OrderDetail[0].RecordCount)
                    });
                    var j = (OrderDetail[0].PageIndex - 1) * OrderDetail[0].PageSize + 1;
                    var k = j + OrderDetail[0].PageSize - 1;
                    if (k > OrderDetail[0].RecordCount) {
                        k = OrderDetail[0].RecordCount
                    }
                    if (OrderDetail[0].RecordCount > 0) {
                        $("#show_result").html("Showing " + (OrderDetail[0].PageIndex) + " - " + k + " of " + OrderDetail[0].RecordCount + " Results");
                    }
                    else {
                        $("#show_result").hide();
                    }

                }

            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    })
}
function OrderDetails() {

    window.location.href = "/User/OrderDetails.aspx";
}
function ASPSnippetsPager(a, b) {

    var c = '<li><a onclick="Pagevalue(this)" style="cursor:pointer" class="Active" page="{1}">{0}</a></li>';
    var d = '<li><a class="spn">{0}</a></li>';
    var e, f, g; var g = 5; var h = Math.ceil(b.RecordCount / b.PageSize);
    if (b.PageIndex > h) { b.PageIndex = h } var i = "";
    if (h > 1) {
        f = h > g ? g : h; e = b.PageIndex > 1 && b.PageIndex + g - 1 < g ? b.PageIndex : 1;
        if (b.PageIndex > g % 2) {
            if (b.PageIndex == 2) f = 5;
            else f = b.PageIndex + 2
        } else { f = g - b.PageIndex + 1 }
        if (f - (g - 1) > e) { e = f - (g - 1) }
        if (f > h) { f = h; e = f - g + 1 > 0 ? f - g + 1 : 1 }
        var j = (b.PageIndex - 1) * b.PageSize + 1;
        var k = j + b.PageSize - 1;
        if (k > b.RecordCount) {
            k = b.RecordCount
        }
        i = "<span>Records " + (j == 0 ? 1 : j) + " - " + k + " of " + b.RecordCount + "</span>&nbsp;&nbsp;&nbsp;&nbsp; ";
        if (b.PageIndex > 1) {
            i += c.replace("{0}", "First").replace("{1}", "1");
            i += c.replace("{0}", "<<").replace("{1}", b.PageIndex - 1)
        }
        for (var l = e; l <= f; l++) {
            if (l == b.PageIndex) {
                i += d.replace("{0}", l)
            }
            else {
                i += c.replace("{0}", l).replace("{1}", l)
            }
        }
        if (b.PageIndex < h) {
            i += c.replace("{0}", ">>").replace("{1}", b.PageIndex + 1); i += c.replace("{0}", "Last").replace("{1}", h)
        }
    }
    a.html(i);
    try {
        a[0].disabled = false
    }
    catch (m) {
    }
}
(function (a) {
    a.fn.ASPSnippets_Pager = function (b) {
        var c = {};
        var b = a.extend(c, b);
        return this.each(function () { ASPSnippetsPager(a(this), b) })
    }
})(jQuery);