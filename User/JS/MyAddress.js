﻿$(document).ready(function ()
{
    getAddress();
    getZipList();
})
function getAddress() {
    var apiUrl = $("#ApiAppKey").html();
    var UID = parseInt($("#uid").val()) || 0;
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var rc = new Object();
    rc.StoreId = StoreId;
    var data = {
        AccessToken: "a2d8fjhsdkfhsbd" + UID + "gxzn3d8xy7jewbc7x",
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Registration.asmx/GetBilling",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var defaultShip = 0, defaultBill = 0;
            var html = '', html1 = '';
            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    defaultBill = response.d.responseNewData[0].DefaultBillAdd;
                    defaultShip = response.d.responseNewData[0].DefaultShipAdd;
                    var list = response.d.responseNewData[0].BillingShippingList;
                    $.each(list, function (i, item) {
                        html += '<div class="col-md-6 ">'
                        html += '<div class="legend disable" style="min-height:103px">'
                        //html += '<h6>Billing Address</h6>'
                        html += '<div class="col-md-12" style=" padding: 2px; margin: 13px; ">'
                        html += '<p>' + item.Address + ' , </p>'
                        html += '<p><span style="font-weight:600">' + item.City + '</span>,<span style="font-weight:600">' + item.StateName + '</span> , <span style="font-weight:700">' + item.Zipcode + '</span><br/>'
                        // html += ' <input type="button" value="Change Address" onclick="OpenBilling()" class="btn btn-xs btn-info"></p>'
                        html += '</div>'
                        html += '</div>'
                        html += '</div>'
                    });
                    $("#divAddress").html(html);
                }
            }
        }
    })
}
function OpenAddressPopup() {
    $("#AddAddress").modal('show');
}
function getZipList() {
    var apiUrl = $("#ApiAppKey").html();
    var UID = parseInt($("#uid").val()) || 0;
    var data = {
        AccessToken: "a2d8fjhsdkfhsbd" + UID + "gxzn3d8xy7jewbc7x"
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Registration.asmx/ZipCodeList",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                var category = response.d.responseData;
                $("#ddlBillZipCode option:not(:first)").remove();
                $.each(category, function (i, item) {
                    $("#ddlBillZipCode").append("<option value='" + item.Zipcode + "'>" + item.Zipcode + "</option>");
                });
                $("#ddlBillZipCode").select2();
            }
        }
    })
}
function getCityState() {
    var apiUrl = $("#ApiAppKey").html();
    var UID = parseInt($("#uid").val()) || 0;
    var rc = new Object();
    rc.BillZipCode = $("#ddlBillZipCode").val();
    var data = {
        AccessToken: "a2d8fjhsdkfhsbd" + UID + "gxzn3d8xy7jewbc7x",
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Registration.asmx/CityStateCountry",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                $("#txtBillState").val(response.d.responseData[0].State);
                $("#txtBillCity").val(response.d.responseData[0].City);
            }
        }
    })
}
function AddBilling() {
    if (billRequiredField()) {
        var apiUrl = $("#ApiAppKey").html();
        var UID = parseInt($("#uid").val()) || 0;
        var StoreId = parseInt($("#hfStoreId").val()) || 0;
        var rc = new Object();
        rc.BillAddress = $("#txtBillAddress").val(),
        rc.BillZipCode = $("#ddlBillZipCode").val(),
        rc.UserAutoId = UID,
        rc.Type = 0,
        rc.StoreId = StoreId
        var data = {
            AccessToken: "a2d8fjhsdkfhsbd" + UID + "gxzn3d8xy7jewbc7x",
            cr: rc
        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/Registration.asmx/AddShippingBilling",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {

                if (response.d.response == "success") {
                    $("#message").show();
                    $("#message").html("Adress added successfully.");
                    setTimeout(function () {
                        var selectedEffect = 'blind';
                        var options = {};
                        $("#message").hide();
                    }, 3000);
                    getAddress();
                    $("#txtBillAddress").val('');
                    $("#ddlBillZipCode").val(0).change();
                    $("#txtBillCity").val('');
                    $("#txtBillState").val('');
                    $("#AddAddress").modal('hide');
                }
                else {
                    $("#ErrorMessage").show();
                    $("#ErrorMessage").html(response.d.responseMessage);
                    setTimeout(function () {
                        var selectedEffect = 'blind';
                        var options = {};
                        $("#ErrorMessage").hide();
                    }, 3000);
                }
            }
        })
    }
    else {
        $("#ErrorMessage").show();
        $("#ErrorMessage").html('All * fields are mandatory.');
        setTimeout(function () {
            var selectedEffect = 'blind';
            var options = {};
            $("#ErrorMessage").hide();
        }, 3000);
    }
}
function billRequiredField() {
    var boolcheck = true;
    $('.reqb').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning !important');
        } else {
            $(this).removeClass('border-warning !important');
        }
    });

    $('.ddlreqb').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).addClass('border-warning !important');
        } else {
            $(this).removeClass('border-warning !important');
        }
    });

    if ($('#ddlZipCode').val() == '0') {
        $('#ddlZipCode').closest('div').find('.select2-selection__rendered').attr('style', 'border:1px solid #FF9149  !important');
    } else {
        $('#ddlZipCode').closest('div').find('.select2-selection__rendered').removeAttr('style');
    }
    return boolcheck;
}