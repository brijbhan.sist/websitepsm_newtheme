﻿var getid = "";

$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    getid = getQueryString('PageId');
    if (getid != null) {
        OrderDetails(getid);
        OrderItemDetails(getid);
    }
});

function OrderDetails(orderno) {
    var apiUrl = $("#ApiAppKey").html();
    var UserName = $("#hfEmail").val();
    var rc = new Object();
    rc.OrderNo = orderno;
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.Location = $("#hfLocation").val();
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/PlaceOrder.asmx/OrderDetail",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var html = '';

            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    var name = "";
                    var list = response.d.responseNewData; 
                    $.each(list, function (i, item) {
                        html += '<div class="order-heading clear-div">'
                        html += '<span>' + item.OrderNo + " (" + item.OrderDate + ")" + '</span>'
                        html += '<span style="float: right;">' + item.StatusType + '</span>'
                        html += '</div class="cols">'
                        html += '<div class="order-info clear-div">'
                        html += '<div class="cols">'
                        html += '<div class="col-label"><strong>Sales Person</strong></div>'
                        html += '<div col>' + item.SalesPerson + '</div>'
                        html += '</div>'
                        html += '<div class="cols">'
                        html += '<div class="col-label"><strong>Products</strong></div>'
                        html += '<div col>' + item.TotalItem + '</div>'
                        html += '</div>'
                        html += '<div class="cols">'
                        html += '<div class="col-label"><strong>Shipping Type</strong></div>'
                        html += '<div col>' + item.ShippingType + '</div>'
                        html += '</div>'
                        html += '<div class="cols">'
                        html += '<div class="col-label"><strong>Total Amount</strong></div>'
                        html += '<div col><span>&#36;</span>' + parseFloat(item.GrandTotal).toFixed(2) + '</div>'
                        html += '</div>'
                        html += '<div class="cols">'
                        html += '<div class="col-label"><strong>Ship To</strong></div>'
                        html += '<div col>' + item.Address + '</div>'
                        html += '</div>'
                        html += '</div >'
                        html += '<br/>'

                    });
                    $("#order_div").html(html);
                }
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    })
}

function OrderItemDetails(orderno) {
    var apiUrl = $("#ApiAppKey").html();
    var UserName = $("#hfEmail").val();
    var rc = new Object();
    rc.OrderNo = orderno;
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.Location = $("#hfLocation").val();
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/PlaceOrder.asmx/OrderItemDetail",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var html = '';
            if (response.d.response == "success") {


                var html = "";
                var html1 = '';
                $("#tblOrderItem").html('');
                var List1 = response.d.responseNewData;

                if (List1.length > 0) {
                    html += '<table class="order_table" cellspacing="0">'
                    html += '<thead><tr class="head">'
                    html += '<th colspan="2">Product</th>'
                    html += '<th class="product-price">Unit Price</th>'
                    html += '<th class="product-quantity">Quantity</th>'
                    html += ' <th class="product-subtotal">Total</th>'
                    html += '</tr></thead>'
                    html += ' <tbody>'
                    $.each(List1, function (i, item) {
                        html += '<tr>'
                        var productN = simplifyUrl(item.ProductName);
                        html += '<td class="product-thumbnail"><a href="/product/' + productN + '-' + item.ProductId + '">'
                        html += "<img src='" + item.ImageUrl + "' OnError='this.src =\"https://image.mypsm.net/Attachments/default_pic.png\"'  style='height: 96px;' >"
                        html += '</a></td>'
                        html += '<td class="product-name" data-title="Product">'
                        if (item.IsFreeItem == '1') {
                            html += '<a href="/product/' + productN + '-' + item.ProductId + '">' + item.ProductName + ' <span class="badge badge badge-pill badge-success">Free</span></a >'
                        }
                        else if (item.Tax == '1') {
                            html += '<a href="/product/' + productN + '-' + item.ProductId + '">' + item.ProductName + ' <span class="badge badge-pill badge-danger">Taxable</span></a >'
                        }
                        else if (item.IsExchange == '1') {
                            html += '<a href="/product/' + productN + '-' + item.ProductId + '">' + item.ProductName + ' <span class="badge badge-pill badge-primary">Exchange</span></a >'
                        }
                        else {
                            html += '<a href="/product/' + productN + '-' + item.ProductId + '">' + item.ProductName + '</a >'
                        }
                        html += '<div class="product-unit">'
                        html += '</div>'
                        html += '</td >'
                        html += '<td class="product-price" data - title="Price" >'
                        html += '<span>'
                        html += '<bdi><span>&#36;</span>' + parseFloat(item.UnitPrice).toFixed(2) + '</bdi>'
                        html += '</span> '
                        html += '</td>'
                        html += '<td class="product-quantity" data - title="Quantity" >'
                        html += '<span>' + item.Qty + '</span>'
                        html += '</td >'
                        html += '<td class="product-subtotal" data - title="total" >'
                        html += '<span>'
                        html += '<bdi><span>&#36;</span>' + parseFloat(item.NetPrice).toFixed(2) + '</bdi>'
                        html += '</span>'
                        html += '</td >'
                        html += '</tr>'
                    });
                    html += '</tbody>'
                    html += ' </table>'
                    html1 += '<div class="cart_totals">'
                    html1 += '<h2> Cart totals</h2 >'
                    html1 += '<table cellspacing="0" class="order_table">'
                    html1 += '<tr class="cart-subtotal">'
                    html1 += '<th>Item(s) Subtotal</th>'
                    html1 += ' <td data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(List1[0].SubTotal).toFixed(2) + '</bdi></span ></td>'
                    html1 += '</tr >'
                    if (List1[0].OverallDiscAmt > 0) {
                        html1 += '<tr class="cart-subtotal">'
                        html1 += '<th>Discount</th>'
                        html1 += '<td data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(List1[0].OverallDiscAmt).toFixed(2) + '</bdi></span ></td>'
                        html1 += '</tr >'    
                    }

                    if (List1[0].TotalTax > 0) {
                        html1 += '<tr class="cart-subtotal">'
                        html1 += '<th>' + List1[0].PrintLabel +'</th>'
                        html1 += '<td data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(List1[0].TotalTax).toFixed(2);  + '</bdi></span ></td>'
                        html1 += '</tr >'                           
                    }

                    if (List1[0].MLTax > 0) {
                        html1 += '<tr class="cart-subtotal">'
                        html1 += '<th>ML Tax</th>'
                        html1 += '<td data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(List1[0].MLTax).toFixed(2) + '</bdi></span ></td>'
                        html1 += '</tr >'    
                    }

                    if (List1[0].WeigthTax > 0) {
                        html1 += '<tr class="cart-subtotal">'
                        html1 += '<th>Weigth Tax</th>'
                        html1 += '<td data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(List1[0].WeigthTax).toFixed(2) + '</bdi></span ></td>'
                        html1 += '</tr >'                     
                    }

                    html1 += '<tr class="cart-subtotal">'
                    html1 += '<th>Shipping Charge</th>'
                    html1 += '<td data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(List1[0].ShippingCharges).toFixed(2) + '</bdi></span ></td>'
                    html1 += '</tr >'
                    console.log(List1[0].AdjustmentAmt);
                    html1 += '<tr class="cart-subtotal">'
                    html1 += '<th>Adjustment Amount </th>'
                    html1 += '<td data-title="Subtotal"><span><bdi>' + List1[0].AdjustmentAmt + '</bdi></span ></td>'
                    html1 += '</tr >'

                    html1 += '<tr class="order-total" >'
                    html1 += '<th>Grand Total</th>'
                    html1 += '<td data-title="Total"><strong><span><bdi><span>&#36;</span>' + parseFloat(List1[0].GrandTotal).toFixed(2) +'</bdi></span></strong></td>'
                    html1 += '</tr >'                   
                    html1 += '</table>'
                    html1 += '</div>'

                    $("#OrderItemData").append(html);
                    $("#priceSection").append(html1);
                }
                else {
                    abandonSession();
                }
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    });
}