﻿$(document).ready(function () {
    var loc = GetLocation(2);
    if ($("#hfLocation").val() == loc && $("#hfStoreLocation").val() == "0") {
        DocumentDetails();
    }
    else {
        ApprovedDocumentDetails();
    }
})
function DocumentDetails() {
    var apiUrl = $("#ApiAppKey").html();
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var Token = getToken();
    var rc = new Object();
    rc.StoreId = StoreId;
    rc.Location = GetLocation(2);
    rc.UserName = $("#hfEmail").val();
    rc.Token = Token;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsDocument.asmx/DocumentList",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    var list = response.d.responseNewData;
                    $("#DocumentDetails tbody tr").remove();
                    var row = $("#DocumentDetails thead tr").clone(true);
                    $.each(list, function (i, item) {
                        if (item.VerifyStatus == 0)
                        {
                            $(".Action", row).html('<button type="button" class="button white" onclick="OpenDocumentPopup(this)">Upload</button></a>');
                        }
                        else if (item.VerifyStatus == "1") {
                            $(".Action", row).html('<button type="button" class="button white" onclick="OpenDocumentPopup(this)">Edit</button>');
                        }
                        else {
                            $(".Action", row).html('');
                        }
                        if (item.DocumentUrl == "") {
                            $(".View", row).html('<span>---</span>');
                        }
                        else {
                            $(".View", row).html("<a target='_blank' href='/CustomerDocument/" + item.DocumentUrl + "' class='viewdoc'>View</a>");
                        }
                        if (item.VerifyStatus == 0) {
                            $(".Status", row).html('<span class="status-label notapprove">Not uploaded</span>');
                        }
                        else if (item.VerifyStatus == "1") {
                            $(".Status", row).html('<span class="status-label notapprove">not verified</span>');
                        }
                        else if (item.VerifyStatus == "2") {
                            $(".Status", row).html('<span class="status-label approve">verified</span>');
                        }
                        else if (item.VerifyStatus == "3") {
                            $(".Status", row).html('<span class="status-label approve">Re-upload</span>');                          
                        }
                        $(".IsRequired", row).html('<span>' + item.IsRequired + '</span>');
                        $(".DocumentName", row).html('<span class="' + item.DocumentName + '" DocID="' + item.AutoId + '" DocumentName="' + item.DocumentName + '" > ' + item.DocumentName + '</span >');
                        $(".UploadDate", row).text((item.UploadDate == null ? '---' : item.UploadDate));
                        $("#DocumentDetails tbody").append(row);
                        row = $("#DocumentDetails tbody tr:last-child").clone(true);
                    });
                    $('#DocumentDetails tbody tr').removeClass("table-heading")
                    $('#DocumentDetails').show();
                }
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    })
}
function ApprovedDocumentDetails() {
    var apiUrl = $("#ApiAppKey").html();
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var Token = getToken();
    var rc = new Object();
    rc.StoreId = StoreId;
    rc.Location = GetLocation(2);
    rc.StoreLocation = $("#hfLocation").val();
    rc.UserName = $("#hfEmail").val();
    rc.Token = Token;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsDocument.asmx/ApprovedDocumentList",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    var list = response.d.responseNewData;
                    $("#ApprovedDocumentDetails tbody tr").remove();
                    var row = $("#ApprovedDocumentDetails thead tr").clone(true);
                    $.each(list, function (i, item) {

                        $(".AView", row).html("<a target='_default' href='/CustomerDocument/" + (item.DocumentURL == null ? "---" : item.DocumentURL) + "' class='viewdoc'>View</a>");
                        $(".AStatus", row).html('<span class="status-label approve">' + (item.Status == null ? '---' : item.Status) + '</span>');
                        $(".ADocumentName", row).text(item.DocumentName);
                        $(".AUploadDate", row).text((item.UploadedDate == null ? '---' : item.UploadedDate));
                        $("#ApprovedDocumentDetails tbody").append(row);
                        row = $("#ApprovedDocumentDetails tbody tr:last-child").clone(true);
                    });
                    $('#ApprovedDocumentDetails tbody tr').removeClass("table-heading")
                    $('#ApprovedDocumentDetails').show()

                }
                else if (response.d.response == "Token") {
                    $("#modalTokenMessage").modal('show');
                }
            }
        }
    })
}
function OpenDocumentPopup(e) {
    var tr = $(e).closest('tr');
    var docname = tr.find('.DocumentName').find('span').attr('DocumentName')
    $("#txtDocument").val(docname);
    $("#hfDocId").val(tr.find('.DocumentName span').attr("DocID"));
    $("#modalUploadDocument").modal('show');
}
$(function () {
    $('input[type=file]').change(function () {
        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(docx|jpg|jpeg|doc|pdf|png)$");

        if (!(regex.test(val))) {
            $(this).val('');
            swal("", "Only png,pdf,docx format is allowed.", "error");
        }
    });
});
function UploadDocument() {
    if (checkRequiredField()) {
        var timeStamp = Date.parse(new Date());
        var apiUrl = $("#ApiAppKey").html();
        var Token = getToken();
        var fileUpload = $("#fileUploadDocument").get(0);
        var file = timeStamp + "_" + fileUpload.files[0].name;
        if (file != null || file != "") {
            var fileUpload = $("#fileUploadDocument").get(0);
            var files = fileUpload.files;
            var test = new FormData();
            for (var i = 0; i < files.length; i++) {
                test.append(files[i].name, files[i]);
            }
            $.ajax({
                url: "/Handler.ashx?timestamp=" + timeStamp,
                type: "POST",
                contentType: false,
                processData: false,
                data: test,
                beforeSend: function () {
                    $("#roundLoader").show();
                },
                complete: function () {
                    $("#roundLoader").hide();
                },
                success: function (result) {
                    var rc = new Object();
                    rc.StoreId = parseInt($("#hfStoreId").val()) || 0;
                    rc.Document = $("#hfDocId").val();
                    rc.DocumentUrl = file.replace(/\s/g, '');
                    rc.Location = GetLocation(2);
                    rc.UserName = $("#hfEmail").val();
                    rc.Token = Token;
                    var data = {
                        cr: rc
                    };
                    $.ajax({
                        type: "POST",
                        url: apiUrl + "/WebApi/WsDocument.asmx/UploadDocument",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        beforeSend: function () {
                            $("#roundLoader").show();
                        },
                        complete: function () {
                            $("#roundLoader").hide();
                        },
                        success: function (response) {
                            if (response.d.response == "success") {
                                swal("", "Document has been uploaded successfully.", "success").then(function () {
                                    location.reload(true);
                                });

                            }
                            else if (response.d.response == "failed") {
                                swal("", "Oops! Something went wrong.Please try later.", "error")

                            }
                            else if (response.d.response == "Token") {
                                $("#modalTokenMessage").modal('show');
                            }
                            else {
                                location.href = "/";
                            }
                        },
                        error: function (result) {
                            console.log(JSON.parse(result.responseText).d);
                        },
                        failure: function (result) {
                            console.log(JSON.parse(result.responseText).d);
                        }
                    });
                },
                error: function (err) {
                    $("#ErrorMessage").show();
                    $("#ErrorMessage").html('Oops! Something went wrong.Please try later.');
                    setTimeout(function () {
                        $("#ErrorMessage").hide();
                    }, 3000);
                }
            });
        }
        else {
            $("#ErrorMessage").show();
            $("#ErrorMessage").html('All * fields are mandatory.');
            setTimeout(function () {
                $("#ErrorMessage").hide();
            }, 3000);
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}