﻿var storeStatus = 0;
$(document).ready(function () {
    GetStoreList();
})
function GetStoreList() {
    var apiUrl = $("#ApiAppKey").html();
    var UID = parseInt($("#UserAutoId").val()) || 0;
    var location = GetLocation(2);
    var Token = getToken();
    var StoreId = parseInt($("#hfStoreId").val());
    var rc = new Object();
    rc.UserAutoId = UID;
    rc.StoreId = StoreId;
    rc.location = location;
    rc.UserName = $("#hfEmail").val();
    rc.Token = Token;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsStoreMaster.asmx/StoreList",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    var storelist = response.d.responseNewData;
                    $("#store_list tbody tr").remove();
                    var row = $("#store_list thead tr").clone(true);
                    $.each(storelist, function (i, item) {                        
                        if (item.Status == "Approved") {
                            $(".Store", row).html('<div class="btnaction" style="width:25%"><div class= "status" ><span class="label">Status :</span><span class="status-label approve">' + item.Status + '</span></div ><div class="action"><span class="label">Action :</span><button type="button" onclick="Redirectaproved(' + item.StoreId +')" class="button blue small"  style="text-transform:none">View</button></div></div><div class="store-details" style="margin-top: 6px;width:75%"><div class= "storename" title = "Store Name" ><span class="label">Store Name :</span>' + item.StoreName + '</div ><div class="store-email"><span class="label">Email :</span><a href="#">' + item.Email + '</a></div></div >');
                        }
                        else {
                            $(".Store", row).html('<div class="btnaction" style="width:25%"><div class= "status" ><span class="label">Status :</span><span class="status-label notapprove" style="font-size:12px">' + item.Status + '</span></div ><div class="action"><span class="label">Action :</span><button type="button" onclick="Redirectdrfaft(' + item.AutoId +')" class="button blue small" style="text-transform:none">View</button></div></div><div class="store-details" style="margin-top: 6px;width:75%"><div class= "storename" title = "Store Name" ><span class="label">Store Name :</span>' + item.StoreName + '</div ><div class="store-email"><span class="label">Email :</span><a href="#">' + item.Email + '</a></div></div >');
                        }
                        $(".Phones", row).html('<div class="store-phone"><span class="label">Mobile No :</span>' + item.Mobile + '</div><div class= "store-phone" > <span class="label">Phone :</span>' + item.Landlineno1 + '</div >');
                        $(".Address", row).html('<div><span class="label">Shipping Address :</span> ' + item.ShippingAddress + '</div><div><span class="label">Billing Address :</span>' + item.BillingAddress + '</div >');
                        $("#store_list tbody").append(row);
                        row = $("#store_list tbody tr:last").clone(true);
                    });
                    $('#store_list tbody tr').removeClass("table-heading")
                }
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }           
        }
    })
}
function Redirectaproved(Autoid) {
    location.href = "/Approved_Store_Details?PageId=" + Autoid
}
function Redirectdrfaft(Autoid) {
    location.href = "/Store_Registration?PageId=" + Autoid
}
function GetApprovedStoreList() {
    var location = GetLocation(2);
    var apiUrl = $("#ApiAppKey").html();
    var UID = parseInt($("#UserAutoId").val()) || 0;
    var Token = getToken();
    var StoreId = parseInt($("#hfStoreId").val());
    var rc = new Object();
    rc.UserAutoId = UID;
    rc.Location = location;
    rc.Token = Token;
    rc.UserName = $("#hfEmail").val();
    rc.StoreId = StoreId;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsStoreMaster.asmx/UserApprovedStoreList",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    var storelist = response.d.responseNewData;
                    $("#tblAStoreList tbody tr").remove();
                    var row = $("#tblAStoreList thead tr").clone(true);
                    $("#StoreAEmptyTable").hide();
                    $.each(storelist, function (i, item) {
                        $(".ACustomerName", row).text(item.StoreName);                       
                        if (item.success == '1') {
                            $(".AStatus", row).html("<span class='pills'>" + item.Status + "</span>");
                        }
                        else {
                            $(".AStatus", row).html("<span class='pills'>" + item.Status + "</span>");
                        }
                        $(".AStatus span", row).css('background-color', item.ColorCode);
                        
                        $(".ABillingAddress", row).text(item.BillingAddress);
                        $(".AShippingAddress", row).text(item.ShippingAddress);
                        $("#tblAStoreList tbody").append(row);
                        $(".Action", row).html("<a title='Edit' href='/Approved_Store_Details?PageId=" + item.StoreId + "'><span class='fa fa-eye' ></span></a>"); StoreId
                        row = $("#tblAStoreList tbody tr:last").clone(true);
                    });
                }
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
            else {
                $("#StoreAEmptyTable").show();
                $("#tblAStoreList").hide();
            }
        }
    })
}

