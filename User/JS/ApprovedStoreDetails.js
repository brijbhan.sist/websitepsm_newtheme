﻿var TimeFrom = "", TimeTo = "";
$(document).ready(function () {

    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var PageId = getQueryString('PageId');
    if (PageId != null) {
        UserApprovedStoreDetails(PageId);
    }
});

function UserApprovedStoreDetails(PageId) {
    var apiUrl = $("#ApiAppKey").html();
    var location = GetLocation(2);
    var Token = getToken();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var rc = new Object();
    rc.UserAutoId = UserAutoId;
    rc.StoreId = PageId;
    rc.Location = location;
    rc.UserName = $("#hfEmail").val();
    rc.Token = Token;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsStoreMaster.asmx/UserApprovedStoreDetails",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                var store = response.d.responseNewData[0].StoreDeatils;
                var CPerson = response.d.responseNewData[0].CPDeatils;
                $("#lblAutoId").text(store.AutoId);
                $("#lblBillingAutoId").text(store[0].BillAutoId);
                $("#lblShippingAutoId").text(store[0].ShipAutoId);

                $("#txtBusinessName").text(store[0].BusinessName);
                $("#txtName").text(store[0].StoreName);
                $("#txtOPTLicense").text(store[0].OPTLicense);
                $("#txtTaxID").text(store[0].TaxId);
                $("#txtContactPerson").text(store.ContactPerson);

                $("#txtBillAddress").text(store[0].BAddress);
                $("#txtBillAddress2").text(store[0].BAddress2);
                $("#txtBillCity").text(store[0].BCity);
                $("#txtBillZipcode").text(store[0].BZipcode);
                $("#txtBillState").text(store[0].BState);

                $("#txtShippingAddress").text(store[0].SAddress);
                $("#txtShippingAddress2").text(store[0].SAddress2);
                $("#txtShipCity").text(store[0].SCity);
                $("#txtShipZipCode").text(store[0].SZipcode);
                $("#txtShipState").text(store[0].SState);

                $("#fromTime").text(store[0].OptimotwFrom);
                $("#toTime").text(store[0].OptimotwTo);
                $("#txtBLatitude").text(store[0].BillingLatitude);
                $("#txtBLongitude").text(store[0].BillingLongitude);
                $("#txtSLatitude").text(store[0].ShippingLatitude);
                $("#txtSLongitude").text(store[0].ShippingLongitude);

                $("#tblContactDetails tbody tr").remove();
                var row = $("#tblContactDetails thead tr").clone(true);
                $.each(CPerson, function (index, item) {
                    $(".Action", row).html('<div><div><span class="label">Person Name :</span>' + item.ContactPerson + '</div><div><span class="label">Type :</span> ' + item.TypeName + '</div></div>');
                    if (item.ISDefault == 1) {
                        $(".SetAsDefault", row).html('<div><span class="label">Default :</span><input type="radio"  disabled="disabled"  name="Default" checked="true"/></div>');
                    }
                    else {
                        $(".SetAsDefault", row).html('<div><span class="label">Default :</span><input type="radio"  disabled="disabled" name="Default" /></div>');
                    }
                    $(".Emails", row).html('<div><span class="label">Email :</span><a href="#">' + item.Email + '</a></div><div> <span class="label">Alternate Email :</span><a href="#">' + item.AlternateEmail + '</a></div>');

                    $(".Numbers", row).html('<div class="mobile"><div><span class="label">Mobile :</span>' + item.MobileNo + '</div ><div><span class="label">Fax No :</span>' + item.Fax + '</div></div><div class="mobile"><div><span class="label">Landline No 1 :</span>' + item.Landline + '</div><div><span class="label">Landline No 2 :</span>' + item.Landline2 + '</div></div>');
                    $('#tblContactDetails tbody').append(row);
                    row = $("#tblContactDetails tbody tr:last").clone(true);

                });
                $('#tblContactDetails tbody tr').removeClass("table-heading")               
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    })
}