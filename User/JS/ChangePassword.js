﻿$(document).ready(function () {

})
function ChangePassword() {
    if (checkRequiredField()) {
        var apiUrl = $("#ApiAppKey").html();
        var rc = new Object();
        rc.OldPassword = $("#OldPassword").val();
        rc.NewPassword = $("#NewPassword").val();
        if ($("#NewPassword").val() != $("#ConfirmPassword").val()) {//condition added on 11/30/2019 By Rizwan Ahmad
            toastr.error('Confirmed password must be same as New password.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return false
        }
        var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
        rc.UserId = UserAutoId;
        rc.Location = GetLocation(1);
        var data = {
            cr: rc

        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/ResetPasswordMaster.asmx/ResetPassword",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
   
                if (response.d.response == "success") {
                    if (response.d.responseMessage == "Password updated successfully.") {
                        $("#OldPassword").val('');
                        $("#NewPassword").val('');
                        $("#ConfirmPassword").val('');
                        swal("", "Password changed successfully.", "success");

                    }
                    else {
                        swal("", "" + response.d.responseMessage, "error");
                    }
                }
                else {
                    swal("", "" + response.d.responseMessage, "error");
                }
            }
        })
    }
    else {
        toastr.error('All fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

//------------------------------------------------- Forgot Password JS Fuction-------------------------------
function verifyForgotEmail() {

    var elementValue = $("#txtForgotEmail").val(); 
    if (checkRequiredField1()) { 
        if (elementValue != "" || elementValue == "") {
            var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var check = EmailCodePattern.test(elementValue);
            if (!check) {
                toastr.error('Invalid email address.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return false
            }
        }
        var apiUrl = $("#ApiAppKey").html();
        var rc = new Object();
        rc.ForgotEmailId = $("#txtForgotEmail").val();
        rc.Location = GetLocation(1);
        var data = {
            cr: rc
        };

        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/ResetPasswordMaster.asmx/VerifyForgotEmail",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                if (response.d.response == "success") {
                    if (response.d.responseMessage == "OTP send to your entered Email ID successfully.") {

                        $("#pMessage").html('Please enter 6 digit otp which is sent on your register <span style="font-weight: 600;margin-right: 5px;">' + '</span>Email ID ' + '( ' + $("#txtForgotEmail").val() + ' )');
                        $("#VerifyForgotOTP").show();
                        $("#forgotpassword").hide();
                        $("#forgotOTPEnter").hide();
                    }
                    else {
                        swal("", "" + response.d.responseMessage, "error");
                    }
                }
                else {
                    swal("", "" + response.d.responseMessage, "error");
                }
            }

        })
    }
    else {
        toastr.error('Email feild is mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function ResendOTP() {
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.ForgotEmailId = $("#txtForgotEmail").val();
    rc.Location = GetLocation(1);
    var data = {
        cr: rc
    };

    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/ResetPasswordMaster.asmx/VerifyForgotEmail",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (response.d.responseMessage == "OTP send to your entered Email ID successfully.") {
                    $("#forgotpassword").hide();
                    $("#forgotOTPEnter").hide();
                    $("#VerifyForgotOTP").show();
                    $("#pMessage").html('Please enter 6 digit otp which is sent on your register <span style="font-weight: 600;margin-right: 5px;">' + '</span>Email ID ' + '( ' + $("#txtForgotEmail").val() + ' )');
                    swal("", "Resend OTP successfully sent.", "success");

                }
                else {
                    swal("", "" + response.d.responseMessage, "error");
                }
            }
            else {
                swal("", "" + response.d.responseMessage, "error");
            }
        }

    })
}

function VerifyOTP() {

    if ($("#txtForgotSecurityCode").val() == "") {
        swal("", "Please enter OTP.", "error");
    }
    else {
        var apiUrl = $("#ApiAppKey").html();
        var rc = new Object();
        rc.ForgotEmailId = $("#txtForgotEmail").val();
        rc.OTP = $("#txtForgotSecurityCode").val()
        rc.Location = GetLocation(1);
        var data = {
            cr: rc
        };

        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/ResetPasswordMaster.asmx/VerifyOTP",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                if (response.d.response == "success") {
                    if (response.d.responseMessage == "Valid") {
                        $("#VerifyForgotOTP").hide();
                        $("#forgotpassword").show();
                        $("#forgotpassword").hide();
                        $("#forgotOTPEnter").show();
                    }
                }
                else {
                    swal("", response.d.responseMessage, "error");
                    $("#txtForgotSecurityCode").val() == "";
                }
            }
        })
    }
}

function updateNewForgottenPassword() {
    if (checkRequiredField()) {
        var apiUrl = $("#ApiAppKey").html();
        var rc = new Object();
        rc.NewPassword = $("#txtNewForgottenPassword").val();
        if ($("#txtNewForgottenPassword").val() != $("#txtNewForgottenConfirmPassword").val()) {//condition added on 11/30/2019 By Rizwan Ahmad
            toastr.error('Confirmed password must be same as New password.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return false
        }
        rc.ForgotEmailId = $("#txtForgotEmail").val();
        rc.Location = GetLocation(1);
        var data = {
            cr: rc

        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/ResetPasswordMaster.asmx/updateNewForgottenPassword",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                if (response.d.response == "success") {
                    if (response.d.responseMessage == "Password reset successfully.") {
                        $("#txtNewForgottenPassword").val('');
                        $("#txtNewForgottenConfirmPassword").val('');
                       
                        swal({
                            title: "",
                            text: "Password reset successfully.",
                            icon: "success",
                            showCancelButton: false,
                            buttons: {
                                confirm: {
                                    text: "Ok",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: true
                                }
                            }
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                location.href = "/Login";
                            }
                        });
                            
                }
                    else {
                        swal("", " " + response.d.responseMessage, "error");
                    }
                }
                else {
                    swal("", " " + response.d.responseMessage, "error");
                }
            }
        })
    }
    else {
        toastr.error('All fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


function checkRequiredField1() {
    var boolcheck = false;
    $('.req').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '0.00') {
            boolcheck = true;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }

    });
    return boolcheck;
}



