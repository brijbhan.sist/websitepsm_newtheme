﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="StoreList.aspx.cs" Inherits="User_StoreList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <span><a href="/Manage_Store">Home</a> &gt; </span>
    <span>Store List</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <span class="cart-title"><strong>Store List</strong></span>
    <div class="order-div clear-div">
        <table class="table store-list" id="store_list"> 
            <thead>
                <tr class="table-heading">
                    <td class="Store" style="padding:18px 0 10px 10px;width: 494px">Store Name</td>
                    <td class="Phones" style="padding: 7px 3px 3px 10px;width:100px">Main Phone</td>
                    <td class="Address" style="padding-bottom:0">Address</td>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
    <%--  <script src="/User/JS/StoreList.js"></script>--%>
</asp:Content>

