﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="User_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <span><a href="/Manage_Store">Home</a> &gt; </span>
    <span>Store List</span>
    <link rel="stylesheet" href="/themes/css/login.css" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="logincontainer">
        <div id="login-page">
            <div class="container">
                <span class="login-title"><strong>Change Password</strong></span>
                <div class="login-div">
                    <div class="login-form">
                        <div class="form-row">
                            <input type="password" id="OldPassword" placeholder="Old Password" class=" req" />
                        </div>

                        <div class="form-row">
                            <input type="password" id="NewPassword" placeholder="New Password" class=" req" />
                        </div>
                        <div class="form-row">
                            <input type="password" id="ConfirmPassword" placeholder="Confirm Password" class="req" />
                        </div>
                        <div style="text-align: center;">
                            <button type="button" class="button white" onclick="ChangePassword()" id="BtnChangePassword">Change Password</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


