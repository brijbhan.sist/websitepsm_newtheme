﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="OrderDetails.aspx.cs" Inherits="User_OrderDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <span><a href="/Manage_Store">Home</a> &gt; </span>
    <span>>Order Details</span>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <span class="cart-title"><strong>Order Details</strong></span>
    <div class="order-div" id="order_div">
    </div>
    <div class="cart-products-loaded" id="OrderItemData" style="background: white;">
    </div>
    <div class="cart-collaterals" id="priceSection">

    </div>
</asp:Content>

