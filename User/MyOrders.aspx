﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="MyOrders.aspx.cs" Inherits="User_MyOrders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <span><a href="/Manage_Store">Home</a> &gt; </span>
    <span>My Orders</span>
    <style>
        .page {
            text-align: center;
            display: table;
            width: 100%;
            margin: 12px 0 !important;
        }

            .page li a {
                display: block;
                height: 40px;
                width: 40px;
                line-height: 40px;
                text-align: center;
                background: white;
                color: black;
                border-radius: 5%;
            }

            .page li {
                display: inline-block;
                padding: 0 5px;
            }

        .spn {
            display: block;
            height: 40px;
            width: 40px;
            line-height: 40px;
            text-align: center;
            background: #b02525 !important;
            color: #efefef !important;
            border-radius: 5%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <span class="cart-title"><strong>My Orders</strong></span>
    <div class="order-div" id="order_div">
    </div>
    <div class="show-result" id="show_result">Showing 1 - 1 of 1 Results</div>
     <div class="page"></div>
</asp:Content>

