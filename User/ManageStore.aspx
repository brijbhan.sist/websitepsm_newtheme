﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="ManageStore.aspx.cs" Inherits="User_ManageStore" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .page-header {
            padding-bottom: 9px;
            margin: 20px 0 0 !important;
            border-bottom: none;
        }
    </style>
    <script type="text/javascript">      
        $("#Menu").attr("display", "none");
    </script>
    <div class="page-header hideRespo">
        <h3>Dashboard</h3>
    </div>
    <div id="manageStore">
    </div>
</asp:Content>

