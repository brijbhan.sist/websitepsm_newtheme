﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Checkout.aspx.cs" Inherits="Checkout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="container">
        <main role="main" class="main">
            <div>
                <div id="cartcontainer">
                    <div class="container">
                        <div class="breadcrumbs">
                            <span><a href="/">Home</a> &gt; </span>
                            <span>Checkout</span>
                        </div>
                    </div>
                      <div id="checkout-page">
                                <div class="container">
                                    <span class="checkout-title"><strong>Checkout</strong></span>
                                    <div>
                                        <div class="checkout-div clear-div">
                                            <form name="frmcheckout" id="frmcheckout">
                                                <div class="checkout-left">
                                                <div id="divBillingAddress"></div>
                                                <div id="divShippingAddress"></div>
                                                 <div class="col method">
                                                        <div class="title"><strong>Shipping Method</strong></div>
                                                        <div id="shipDIV"></div>
                                                    </div>
                                                <div class="col method">
                                                        <div class="title">
                                                            <strong>Payment Method</strong>
                                                        </div>
                                                        <div id="PayDIV"></div>
                                                    </div>
                                                </div>
                                                <div class="checkout-right">
                                                    <div class="col shopping">
                                                        <div class="title"><strong>Shopping Cart</strong></div>
                                                        <div><span style="color:red" id="spTotalItem"></span> <span style="color:red">Items</span></div>
                                                        <div>
                                                            <span>
                                                                Subtotal
                                                                <strong>
                                                                    <span>$<span id="spSubTotal">0.00</span></span>
                                                                </strong>
                                                            </span>
                                                        </div>
                                                        <div>
                                                            <span>
                                                                Shipping
                                                                <strong>
                                                                    <span>$<span id="spShipping">0.00</span></span>
                                                                </strong>
                                                            </span>
                                                        </div>
                                                         <div id="trWeightTax" style="display:none">
                                                            <span>
                                                                Weight Tax
                                                                <strong>
                                                                    <span>$<span id="spWeightTax">0.00</span></span>
                                                                </strong>
                                                            </span>
                                                        </div>
                                                         <div id="trMLTax" style="display:none">
                                                            <span>
                                                                ML Tax
                                                                <strong>
                                                                    <span>$<span id="spMLTax">0.00</span></span>
                                                                </strong>
                                                            </span>
                                                        </div>
                                                        <div class="total">
                                                            <span>
                                                                Total
                                                                <strong>
                                                                    <span>$<span id="spTotal"></span></span>
                                                                </strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="cart-button" style="">
                                                        <button id="btnPlaceOrder" type="button" onclick="PlaceOrder()" class="button added_to_cart">Place Order</button>
                                                    </div>

                                                </div>
                                            </form>
                                            <div class="clear-div"></div>
                                            <div class="policy-content">
                                                <div>
                                                    <img src="Themes/img/common/security-icon.png">
                                                    <div>
                                                        <a href="#" target="_blank">
                                                            Security Policy (edit with Customer reassurance module)
                                                        </a>
                                                    </div>
                                                </div>

                                                <div>
                                                    <img src="Themes/img/common/ship-icon.png">
                                                    <div>
                                                        <a href="#" target="_blank">
                                                            Delivery Policy (edit with Customer reassurance module)
                                                        </a>
                                                    </div>
                                                </div>

                                                <div>
                                                    <img src="Themes/img/common/return-icon.png" />
                                                    <div>
                                                        <a href="#" target="_blank">
                                                            Return Policy (edit with Customer reassurance module)
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
    <div class="modal fade" id="Shipping">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-11">
                            <h5 class="modal-title">Address</h5>
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                            <div id="divPopUpShippingAddress"></div>
                    </div>
                    <div class="row" id="ShipEntry" style="display:none">
                        <div class="col-md-6">
                            <div class="form-control">
                                <label class="col-sm-12 col-lg-4">
                                    Address <span style="color: red">*</span>
                                </label>
                                <div class="col-sm-12 col-lg-8">
                                    <input type="text" id="txtShippingAddress" name="Address1" placeholder="Shipping Address" class="textprop reqs" />
                                </div>
                            </div>

                            <div class="form-control">
                                <label class="col-sm-12 col-lg-4  ">
                                    City 
                                </label>
                                <div class="col-sm-12 col-lg-8">
                                    <input type="text" id="txtShipCity" readonly="readonly" name="City" placeholder="City" class="textprop" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-control">
                                <label class="col-sm-12 col-lg-4  ">
                                    Zip Code <span style="color: red">*</span>
                                </label>
                                <div class="col-sm-12 col-lg-8">
                                    <select id="ddlShippingZipCode" name="zipcode" class="ddlreqs" style="width: 100% !important;">
                                        <option value="0">[Choose an option]</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-control">
                                <label class="col-sm-12 col-lg-4  ">
                                    State 
                                </label>
                                <div class="col-sm-12 col-lg-8">
                                    <input type="text" id="txtShipState" readonly="readonly" name="State" placeholder="State" class="textprop" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                     <button type="button" id="btnShowShipping" onclick="showShipping()" class="btn btn-xs btn-success" >
                                Add new Address
                            </button>
                     <button type="button" id="btnAddShipping" style="display:none" onclick="AddShipping()" class="btn btn-xs btn-success" >
                                Add
                            </button>
                    <input type="button" value="Close" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"/>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Billing">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-11">
                            <h5 class="modal-title">Address</h5>
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                            <div id="divPopUpBillingAddress"></div>
                    </div>
                    <br />
                    <div class="row"  id="BillEntry" style="display:none">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                    <label class="col-sm-12 col-lg-4  ">Billing Address <span style="color: red">*</span></label>
                                    <div class="col-sm-12 col-lg-8">
                                        <input type="text" id="txtBillAddress" name="Address1" placeholder=" Billing Address" class="textprop reqb" />
                                    </div>                                    
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  
                                        <div class="form-group row">
                                        <label class="col-sm-12 col-lg-4  ">City  </label>
                                        <div class="col-sm-12 col-lg-8">
                                            <input type="text" id="txtBillCity" readonly="readonly" name="City" placeholder="City" class="textprop" />
                                        </div>
                                    </div>                                       
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-12 col-lg-4  ">Zip Code <span style="color: red">*</span></label>
                                        <div class="col-sm-12 col-lg-8">
                                            <select id="ddlBillZipCode" name="zipcode" class="ddlreqb" style="width: 100% !important;">
                                                <option value="0">[Choose an option]</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-12 col-lg-4  ">State </label>
                                        <div class="col-sm-12 col-lg-8">
                                            <input type="text" id="txtBillState" readonly="readonly" name="State" placeholder="State" class="textprop" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                     <button type="button" style="display:none" id="btnShowBilling" onclick="showBilling()" class="btn btn-xs btn-success" > Add new Address</button>
                     <button type="button" style="display:none" id ="btnBilling" onclick="AddBilling()" class="btn btn-xs btn-success" >Add</button>
                     <input type="button" value="Close" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"/>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

