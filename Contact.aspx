﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/Themes/css/contact.css" rel="stylesheet" type="text/css" media="all"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--======== Services Inner ============-->

    <main role="main" class="main">
        <div>
            <div id="contactcontainer">
                <div class="container">
                    <div class="breadcrumbs">
                        <span><a href="/">Home</a> &gt; </span>
                        <span>Contact us</span>
                    </div>
                </div>
                <div id="contact-page">
                    <div class="container">
                        <span class="contact-title"><strong>Contact us</strong></span>
                        <div class="contact-div clear-div">
                            <div class="contact-form clear-div">
                                <div class="form-row">
                                    <input type="text" id="txtName" name="name" value="" placeholder="Name" class="req" />
                                </div>
                                <div class="form-row">
                                    <input type="email" id="txtEmail" name="email" value="" placeholder="Email" class="req" onchange="validateEmail(this)"/>
                                </div>
                                <div class="form-row">
                                    <select name="type" id="ddltype"  class="ddlreq">
                                        <option  value="0">Select Type</option>
                                        <option value="order">About My Order</option>
                                        <option value="device">About Devices</option>
                                        <option value="wholesale">About Retail/Wholesale</option>
                                        <option value="other">Other Reason</option>
                                    </select>
                                </div>
                                <div class="form-row">
                                    <input type="text" name="number" id="txtPhoneNo" value="" maxlength="10" onkeypress='return isNumberKey(event)' placeholder="Phone Number"  class="req"/>
                                </div>
                                <div class="form-row">                                 
                                    <input class="input-text" name="birth_date" id="birth_date" placeholder="dd/mm/yyyy"   type='date' min='1899-01-01' max='2000-13-13'>
                                </div>
                                <div class="form-row">
                                    <textarea rows="3" id="txtQuery" name="message" class="req"  placeholder="Message"></textarea>
                                </div>
                                <div class="form-row">
                                    <button name="send" type="button" class="button white" onclick="SendMail()">Send Message</button>
                                </div>
                            </div>
                            <div class="contact-detail">
                                <ul>
                                    <li class="email">
                                        <strong>E-mail</strong>
                                        <span>sales@mypricesmart.com</span>
                                    </li>
                                    <li class="faq">
                                        <a href="/asked-question">
                                            <strong>F.A.Q</strong>
                                            <span>Frequently asked questions</span>
                                        </a>
                                    </li>
                                    <li class="support">
                                        <strong>Customer Support</strong>
                                        <span>sales@mypricesmart.com</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="contact-address">
                                <strong>Office:</strong><br />
                                2500 Hamilton Blvd, Unit # B,<br />
                                South Plainfield, NJ 07080<br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="/AppJS/ContactUs.js"></script>


    <!--========== /Services Inner ===============-->
</asp:Content>

