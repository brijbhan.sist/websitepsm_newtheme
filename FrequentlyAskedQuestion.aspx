﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="FrequentlyAskedQuestion.aspx.cs" Inherits="FrequentlyAskedQuestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="/Themes/css/faq.css" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <main role="main" class="main">
        <div>
            <div id="faqcontainer">
                <div class="container">
                    <div class="breadcrumbs">
                        <span><a href="/">Home</a> &gt; </span>
                        <span>FAQs </span>
                    </div>
                </div>
                <div id="faqdiv">
                    <div class="container clear-div">
                        <span class="faq-title"><strong>FAQs</strong></span>

                        <ul class="faq-list">
                            <li>
                                <h5>Do you have a minimum order?</h5>
                                <div>
                                    <p>Yes, we have a minimum order of $350.00. There is a $15 handling charge for orders less than $350.00 from current existing customers.</p>
                                </div>
                            </li>
                            <li>
                                <h5>Why do I need an account?</h5>
                                <div>
                                    <p>We sell wholesale to business accounts only. You will need to register an account with us before you can order online or view any pricing information. Once you register online, we will ask you to submit your business-related documents. Once you submit all required information, we will review all information and approve your account. Once your account registration is approved then you will receive an email from us confirming your account registration.</p>
                                </div>
                            </li>
                            <li>
                                <h5>How can I track my order or shipment?</h5>
                                <div>
                                    <p>
                                        Generally, System will send the auto-generated shipping email following to order confirmation once the order shipped from our shipping facility. You can track the shipment on the shipping
                                                    carrier website by entering the tracking no. or shipping info sent to you.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <h5>How do I create and manage an account?</h5>
                                <div>
                                    <p>
                                        To create a new registration from the home page, click the "New Customer" link. Fill out the form and click 'Submit'. You must verify your email address by clicking on the email verification link that will be automatically emailed to you. If you don’t see it in your inbox, please check your spam/junk folder and whitelist pricemaster.com in your email program to
                                                    ensure future emails can get through to your inbox.
                                    </p>
                                    <p>
                                        It may take up to 2 business days for your account to be qualified and approved. Once the web account is created, you can manage your ship to locations, view past online order history, and change your password.
                                    </p>
                                    <p>
                                        If you already have an account with us and would like to access to the website then all you have to do is to call or email our customer service dept or your account manager at (888) 516-9776 or on the contact us page and request web access. You can also make this request online by clicking this link and selecting "yes" to the question as to whether you are a current customer.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <h5>Do you have a Catalog?</h5>
                                <div>
                                    <p>
                                        Yes, if you'd like to request the latest one to be mailed to you please contact your account manager.
                                    </p>
                                    <p>
                                        Our website is the best way to review current products/prices and check on item availability and place orders.
                                    </p>

                                </div>
                            </li>
                            <li>
                                <h5>Will I receive an order confirmation?</h5>
                                <div>
                                    <p>
                                        Yes, once we receive your online order, we will send you an e-mail confirmation so you know that your order has been received.
                                    </p>

                                </div>
                            </li>
                            <li>
                                <h5>What payment options do you accept?</h5>
                                <div>
                                    <p>
                                        We accept Visa and Master Card, ACH Debit, Checks, Cash, Bank Wire, and Certified Checks and Money Orders. Checks, whether Certified or not, as well as Money Orders and ACH Debits, must clear before your account is credited and product can be shipped or picked up.
                                    </p>

                                </div>
                            </li>
                            <li>
                                <h5>How do I return the product for Credit or Refund?</h5>
                                <div>
                                    <p>
                                        In the event that you need to return any product, you must contact your account manager, and have a Return Authorization issued. The Return Authorization will state the reason why the product is being returned, as well as the list of products and quantity of returns, as well as appropriate credit value.
                                        as well as Money Orders and ACH Debits, must clear before your account is credited and product can be shipped or picked up.
                                    </p>
                                    <p>
                                        Displays and Products must be returned complete, and in a resalable condition. Any labels/stickers and marking must be removed. We cannot accept damaged or expired products or partial returns
                                    </p>
                                    <p>
                                        All returns MUST be preapproved, returns for a reason other than seller’s error will be subject to 10% handling and restocking charge.
                                    </p>
                                    <p>
                                        Returns MUST be claimed within 30 days of product receipt.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <h5>Why do you require a Resale Tax ID?</h5>
                                <div>
                                    <p>
                                        All accounts are required to provide a copy of their Resale Tax ID. We do not sell directly to consumers, and we do not collect any taxes on those products you purchase. It is company policy to have a Resale Tax ID form/copy on file for every customer.
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                jQuery(function ($) {
                    $("ul.faq-list li > h5").click(function () {
                        var $this = $(this);
                        $this.siblings("div").slideToggle();
                        $("div", $this.parent().siblings("li")).slideUp();
                    });
                });
            </script>
        </div>
    </main>
</asp:Content>

