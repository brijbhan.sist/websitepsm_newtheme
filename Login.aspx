﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="/themes/css/login.css" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <main role="main" class="main">
        <div>
            <div id="logincontainer">
                <div class="container">
                    <div class="breadcrumbs">
                        <span><a href="/">Home</a> &gt; </span>
                        <span>Login</span>
                    </div>
                </div>
                <div id="login-page">
                    <div class="container">
                        <span class="login-title"><strong>Login</strong></span>
                        <div class="login-div">
                            <div class="login-form">
                                <div class="form-row">
                                    <input type="email" name="email" id="txtUserId" placeholder="Email ID" />
                                </div>

                                <div class="form-row">
                                    <input type="password" name="password" id="txtPassword" placeholder="Password" />
                                </div>
                                <div class="form-row">
                                    <a href="/registration" style="text-align: left; color: #0026ff;" class="text-left">Create an account</a>
                                    <a href="/forgottenpassword" style="color: red; float: right" class="text-right">Forgot Password ?</a>


                                </div>
                                <div class="form-row" style="text-align: center;">
                                    <button name="login" class="button white" type="button" onclick="NewLogin()">Login</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="modal fade" id="modalStoreList" tabindex="1" data-backdrop="static">
        <div class="modal-dialog modal-xs">
            <div class="modal-content" style="width: 60%; margin: 0 auto;">
                <div class="modal-header" style="display: block; background: #d60000; color: white; height: 20px">
                    <h4 class="modal-title">Please select store</h4>
                </div>
                <div class="modal-body" style="position: relative; padding: 0px;">
                    <div id="divPopUpStoreList"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/AppJS/Login.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

