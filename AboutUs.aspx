﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" href="/Themes/css/about.css" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <main role="main" class="main">
                    <div>
                        <div id="aboutcontainer">
                            <div class="container">
                                <div class="aboutinfo">
                                    <div class="about-left">
                                        <img src="/Themes/img/about/about-us.jpg" />
                                    </div>
                                    <div class="about-right">
                                        <div class="whoweare">
                                            <div>
                                                <img src="/Themes/img/about/aboutus-icon.png" />
                                            </div>
                                            <div class="content-title">
                                                <div>Who we are</div>
                                            </div>
                                            <p>
                                                We are here for you to satisfy with the range of product mix of General Merchandised Products like OTC, Cosmetics, Toiletries, Novelties, house hold accessories, etc. Our aim is to give you the
                                                full range of products with smart price tag with the best after sales services.
                                            </p>

                                            <p>We have thousand of products from your favourite brands like Durcell, Advil, Kodak, Shell, Johnson & Johnson, Trojan, Bic, Zippo, Tylenol etc.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="blocks-title">Why MyPSM?</div>

                                <div class="feature-div clear-div">
                                    <div class="feature-blocks">
                                        <a href="#">
                                            <img src="/Themes/img/about/quality-icon.png" />
                                            <div>High Quality Products</div>
                                        </a>
                                    </div>
                                    <div class="feature-blocks">
                                        <a href="#">
                                            <img src="/Themes/img/about/smartprice-icon.png" />
                                            <div>Smart Price Tag</div>
                                        </a>
                                    </div>
                                    <div class="feature-blocks">
                                        <a href="#">
                                            <img src="/Themes/img/about/product-variety-icon.png" />
                                            <div>Large Variety of Products</div>
                                        </a>
                                    </div>
                                    <div class="feature-blocks">
                                        <a href="#">
                                            <img src="/Themes/img/about/shipping-icon.png" />
                                            <div>Faster Shipping</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="aboutus-content">
                                    <div>
                                        <p>
                                            We are here for you to satisfy with the range of product mix of General Merchandised Products like OTC, Cosmetics,Toiletries, Novelties, house hold accessories etc. Our aim is to give you the
                                            full range of products with smart price tag with the best after sales services.
                                        </p>

                                        <p>
                                            1. Placing Order Minimum Order for Shipping We are available for order booking from Monday to Friday (10.00 am to 5.00 pm) on Phone 732-733-2929 Fax: 732-595-7579. Please mention Product code
                                            while order booking. Order book before 2.30 pm will be considered for same day dispatch and prior to the next day delivery. Please read our Product Order Manual carefully or Please feels free to
                                            ask our sales staff for the quantity standard and minimum volume for the order placement.
                                        </p>

                                        <p>
                                            Example : Each/Single Count, <br />
                                            UT : Multiple Count, <br />
                                            PU : Partial Unit (eg. Â½ or Â¼ unit) (Feel free to ask our sales team for the assistance)
                                        </p>

                                        <p>
                                            2. Pricing The prices shown in this catalogue for your easy assistance to place the order, as you can ask sales team for the final prices. Price Smart is subject to change prices without prior
                                            notice. All rights reserve to M/s Price Smart LLC.
                                        </p>

                                        <p>
                                            3. Terms/Mode of Payment and Return Checks Policy 100% advance payment for each order placed. We accept Cashier Checks, money order, wire transfer, fax checks or COD Company Checks. Service charge
                                            for return checks $25, order will not be dispatched until the return check has been in cashed by other modes.
                                        </p>

                                        <p>
                                            4. Credit/Return Procedure Replacement is as respective company's replacement policy. You will be entitled for replacement or good return once it has been approved the Sales Manager. Credit memo
                                            will be issue in case of goods unavailability in case of good replacement or goods in return. Pl. ask for the Replacement/Goods Return Request form for necessary formalities. Goods/credit note can
                                            be only issued once it is sanctioned by the Sales Manager. All rights reserved to the Price Smart Lcc for its return policy. Goods are not entailed for the replacement or goods return which do not
                                            meets its our replacement/goods return policy or in some can 20% fees can be applied for the replacement or goods return if it is sanctioned by the sales manager. Festivals and Seasonable cannot
                                            be replaced or consider for goods return. Pl. read our Product Guide Manual for the list of products which are not entitled for the replacement or Good Return.
                                        </p>

                                        <p>
                                            5. Return Polices from Dead/Access Stock We do not dump products for achieving targeted sales or do not motivate to make a order for Free Shipping. You can request Dead or Access Stock through RMA
                                            â€“ Return Authorization Form. On approval RAM receipt will issued to pick up said Dead/Access Stock. Goods are not entailed for the replacement or goods return which do not meets it's our
                                            replacement/goods return policy or in some can 20% fees can be applied for the replacement or goods return if it is sanctioned by the sales manager. All the goods returned must be in its standard
                                            position; terms apply as per each product standard.
                                        </p>

                                        <p>Note : You are required to mention Resale Tax Id for all dealings. / Important notice: All rights reserved to the PRICE SMART LCC 2500 Hamilton BLVD, STE B, South Plainfield, NJ 07080</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
</asp:Content>

