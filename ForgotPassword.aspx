﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="/themes/css/login.css" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <main role="main" class="main">
        <div>
               <div id="logincontainer">
                <div class="container">
                    <div class="breadcrumbs">
                        <span><a href="/">Home</a> &gt; </span>
                        <span>Forgot Password</span>
                    </div>
                </div>
                <div id="login-page">

                    <div class="container" id="forgotpassword">
                        <span class="login-title"><strong>Forgot Password</strong></span>
                        <div class="login-div">
                             <div class="login-form" >
                                  <input type="text" id="txtForgotEmail" placeholder="Forgotten Email" class="req" />
                                <div class="form-row" style="text-align: center;">
                                    <button type="button" class="button white" onclick="verifyForgotEmail()" id="BtnSubmitForgotEmail">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container" id="VerifyForgotOTP" style="display: none;">
                        <span class="login-title"><strong>Verify OTP</strong></span>
                         <div class="login-div">
                            <div class="login-form">
                                <div class="form-row">
                                    <p id="pMessage"></p>
                                </div>

                                <div class="form-row">
                                    <input type="text" id="txtForgotSecurityCode" name="SecurityCode"  onkeypress='return isNumberKey(event)' placeholder=" Security Code" />
                                </div>

                                 <div class="form-row" style="text-align:right">
                                    <a href="#" style="color:red" class="text-right" onclick="ResendOTP()">Resent OTP ?</a>
                                </div>

                                <div class="form-row" style="text-align: center;">
                                    <button type="button" class="button white" onclick="VerifyOTP()" <%--id="btnUploadDocument"--%>>VERIFY</button>
                                </div>

                            </div>
                        </div>
                    </div>

                      <div class="container" id="forgotOTPEnter" style="display: none;">
                        <span class="login-title"><strong>New Password</strong></span>
                        <div class="login-div">

                            <div class="login-form">
                                <div class="form-row">
                                    <input type="password" name="password" id="txtNewForgottenPassword" placeholder="New Password" class="req" />
                                </div>
                                <div class="form-row">
                                    <input type="password" name="ConfirmPassword" id="txtNewForgottenConfirmPassword" placeholder="Confirm New Password" class="req" />
                                </div>
                                <div class="form-row" style="text-align: center;">
                                    <button type="button" class="button white" onclick="updateNewForgottenPassword()" id="BtnTempAddUser">Submit</button>
                                   <%-- <button type="button" class="button white" style="display: none" onclick="AddUser()" id="BtnAddUser">Submit</button>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</asp:Content>

