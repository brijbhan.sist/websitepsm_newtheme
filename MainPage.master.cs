﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MainPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        
        if (Session["UserId"] != null)
        {
            var user = "";
            UserAutoId.Value = Session["UserId"].ToString();
            hfStoreId.Value = Session["StoreID"].ToString();
            hfLocation.Value = Session["Location"].ToString();
            hfStoreLocation.Value = Session["LocationStatus"].ToString();
            hfStoreStatus.Value = Session["StoreStatus"].ToString();
            hfEmail.Value = Session["EmailId"].ToString();
            if(hfStoreStatus.Value==null || hfStoreStatus.Value=="0")
            {
                ViewCart.Visible = false;
            }
            //if (Session["StoreName"] != null)
            //{
            //    user = Session["StoreName"].ToString();
            //    if (user.Length > 20)
            //    {
            //        spUserID.InnerHtml = user.Substring(0, 20) + "...";
            //    }
            //    else
            //    {
            //        spUserID.InnerHtml = Session["StoreName"].ToString();
            //    }
            //}
            //else
            //{
            //    user = Session["CustomerName"].ToString();
            //    if (user.Length > 20)
            //    {
            //        spUserID.InnerHtml = user.Substring(0, 20) + "...";
            //    }
            //    else
            //    {
            //        spUserID.InnerHtml = Session["CustomerName"].ToString();
            //    }
            //}
        }
        else
        {
            UserAutoId.Value = "";

        }

    }
}
