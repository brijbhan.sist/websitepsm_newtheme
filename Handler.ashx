﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.IO;
using System.Data;
using Newtonsoft.Json;
public class Handler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            string timeStamp = context.Request.QueryString["timestamp"].ToString();
            string fname = "";
            DataTable dt = new DataTable();
            DataRow dtRow;
            dt.Columns.Add("URL");
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = file.FileName;
                    }
                    string trim = fname.Replace( " ", "" );
                    fname = trim;
                    fname = timeStamp + "_" + fname;
                    dtRow = dt.NewRow();
                    dtRow["URL"] = fname;
                    dt.Rows.Add(dtRow);
                    string fullname = Path.Combine(context.Server.MapPath("~/CustomerDocument"), fname);
                    file.SaveAs(fullname);
                    string[] GtUrl = HttpContext.Current.Request.Url.Host.ToString().Split('.');
                    try
                    {
                        if (GtUrl[0].ToString().ToLower() == "admin")
                        {
                            if (!Directory.Exists(@"D:\PSMSERVER\MYPSM.NET\wwwroot\CustomerDocument\"))
                            {
                                Directory.CreateDirectory(@"D:\PSMSERVER\MYPSM.NET\wwwroot\CustomerDocument\");
                            }
                            System.IO.File.Copy(fullname, @"D:\PSMSERVER\MYPSM.NET\wwwroot\CustomerDocument\" + fname, true);
                        }
                        else
                        {
                            if (!Directory.Exists(@"D:\PSMSERVER\admin.mypsm.net\wwwroot\CustomerDocument\"))
                            {
                                Directory.CreateDirectory(@"D:\PSMSERVER\admin.mypsm.net\wwwroot\CustomerDocument\");
                            }
                            System.IO.File.Copy(fullname, @"D:\PSMSERVER\admin.mypsm.net\wwwroot\CustomerDocument\" + fname, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(ex.Message);
                        HttpContext.Current.Response.StatusCode = 200;
                    }
                }
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write(JsonConvert.SerializeObject(dt));
            HttpContext.Current.Response.StatusCode = 200;
        }
        catch (Exception ex)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write(ex.Message);
            HttpContext.Current.Response.StatusCode = 400;
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}