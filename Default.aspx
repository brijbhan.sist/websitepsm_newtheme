﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

   <div class="clear-div" style="margin-top: 8px;"></div>
      <main role="main" class="main">
               <div>
                  <div id="homepage">
                      <div id="home_slider" class="container">
                        <ul id="home_slider_inner">                         
                        </ul>
                     </div>
                     <script>
                         /* Js for Home Slider */
                         jQuery(function () {
                             window['home_slider'] = jQuery('#home_slider_inner').addClass('slider_set').lightSlider({
                                 gallery: false, item: 1, slideMargin: 0, speed: 700, cssEasing: 'ease-in', loop: true, vertical: false, controls: true, auto: true, pause: 4000, pager: false,
                                 prevHtml: '<div class="slider-left">&nbsp;</div>', nextHtml: '<div class="slider-right">&nbsp;</div>',
                                 onBeforeSlide: function (el) { jQuery(el).removeClass('slider_set'); }, onAfterSlide: function (el) { jQuery(el).addClass('slider_set'); },
                             });
                         });
                     </script>
                     <script type="text/javascript">
                         function makeResize() {
                             var imageSrc = $("#home_slider ul li a img");
                             if ($(window).width() <= 850) {
                                 $(imageSrc).each(function (key, value) {
                                     if ($(value).data('src') == 'undefined' || $(value).data('src') == '' || $(value).data('src') == null) {
                                     } else {
                                         $(value).attr('src', $(value).data('src'));
                                     }

                                 });
                             }
                         }

                         $(document).ready(function () {
                             $(window).resize(function () {
                                 makeResize();
                             });
                             makeResize();
                         });
                     </script>
                      <div class="container" >
                        <!-- Product Image Section -->
                        <div class="clear-div product-image-block"  id="bannerbox">
                          
                        </div>
                     </div>
                      <div class="slider-bgimage">
                        <!-- Latest Products -->
                        <div class="latest-products clear-div slider2 container" id="Sec_latest_products" style="display:none">
                           <div class="slider-title">Latest Products</div>
                           <ul class="cs-hidden clear-div product-slider itemdiv" id="latest_products">
                             
                           </ul>
                        </div>
                        <!-- Div for latest product -->
                        <!-- Start Div for Special product -->
                        <div class="special-products clear-div slider2 container" id="Sec_special_products" style="display:none">
                           <div class="slider-title">Special Products</div>
                           <ul class="cs-hidden clear-div product-slider itemdiv" id="special_products">
                              
                           </ul>
                        </div>
                        <!-- End Div for Special product -->
                        <!-- Start Div for Best Selling product -->
                        <div class="selling-products clear-div slider2 container" id="Sec_selling_products" style="display:none">
                           <div class="slider-title">Best Selling Products</div>
                           <ul class="cs-hidden clear-div product-slider itemdiv" id="selling_products">
                             
                           </ul>
                        </div>
                     </div>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $('.product-slider').lightSlider({  // Js for Latest Products, Special Products, Best Selling Products
                                 item: 5,
                                 loop: true,
                                 slideMove: 2,
                                 easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                                 speed: 600,
                                 pager: false,
                                 slideMargin: 20,
                                 keyPress: true,
                                 enableDrag: false,
                                 freeMove: false,
                                 controls: true, auto: false, pause: 4000,
                                 prevHtml: '<div class="slider-left">&nbsp;</div>', nextHtml: '<div class="slider-right">&nbsp;</div>',
                                 responsive: [
                                     {
                                         breakpoint: 1200,
                                         settings: {
                                             item: 4,
                                             slideMove: 1,
                                             slideMargin: 20,
                                         }
                                     },
                                     {
                                         breakpoint: 950,
                                         settings: {
                                             item: 3,
                                             slideMove: 1,
                                             slideMargin: 20,
                                         }
                                     },
                                     {
                                         breakpoint: 725,
                                         settings: {
                                             item: 2,
                                             slideMargin: 20,
                                             slideMove: 1
                                         }
                                     },
                                     {
                                         breakpoint: 490,
                                         settings: {
                                             item: 1,
                                             slideMove: 1
                                         }
                                     }
                                 ]
                             });
                         });
                     </script>
                        <!-- Our Client -->
                     <div class="our-clients clear-div slider3 container">
                        <div class="slider-title">Our Clients</div>
                        <ul id="client" class="cs-hidden clear-div small-slider">
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/cleint-logo-sunoco.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/client-logo-7-11.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/client-logo-circlek.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/client-logo-mobil.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/client-logo-shell.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/client-logo-valero.jpg"/></a>
                              </div>
                           </li>
                        </ul>
                     </div>
                     <!-- Div for Our Clients -->
                     <!-- Top Brands -->
                     <div class="top-brands clear-div slider3 container">
                        <div class="slider-title">Top Brands</div>
                        <ul id="brand" class="cs-hidden clear-div small-slider">
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/brand-logo-bic.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/brand-logo-5hr-energy.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/brand-logo-duracell.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/brand-logo-johnson.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/brand-logo-juul.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/brand-logo-png.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/brand-logo-trojan.jpg"/></a>
                              </div>
                           </li>
                           <li>
                              <div>
                                 <a href="#"><img src="/Themes/img/home/brand-logo-zippo.jpg"/></a>
                              </div>
                           </li>
                        </ul>
                     </div>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $('.small-slider').lightSlider({    // Js for Our Client, Our Brand
                                 item: 6,
                                 loop: true,
                                 slideMove: 2,
                                 easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                                 speed: 600,
                                 pager: false,
                                 auto: false, pause: 4000,
                                 prevHtml: '<div class="slider-left">&nbsp;</div>', nextHtml: '<div class="slider-right">&nbsp;</div>',
                                 responsive: [
                                     {
                                         breakpoint: 1301,
                                         settings: {
                                             item: 5,
                                             slideMove: 1,
                                             slideMargin: 20,
                                             controls: true,
                                         }
                                     },
                                     {
                                         breakpoint: 1201,
                                         settings: {
                                             item: 4,
                                             slideMove: 1,
                                             slideMargin: 20,
                                             controls: true,
                                         }
                                     },
                                     {
                                         breakpoint: 950,
                                         settings: {
                                             item: 3,
                                             slideMove: 1,
                                             slideMargin: 20,
                                             controls: true,
                                         }
                                     },
                                     {
                                         breakpoint: 725,
                                         settings: {
                                             item: 2,
                                             slideMargin: 20,
                                             slideMove: 1,
                                             controls: false,
                                             autoWidth: true,
                                         }
                                     },
                                     {
                                         breakpoint: 490,
                                         settings: {
                                             item: 1,
                                             slideMove: 1,
                                             controls: false,
                                             autoWidth: true,
                                         }
                                     }
                                 ]
                             });
                         });
                     </script>
                     <!-- Div for Top Brands -->
                     <div class="slider-bgimage" style="display:none">
                        <!-- Start Div for Our Locations -->
                        <div class="our-location clear-div container">
                           <div class="slider-title">Our Locations</div>
                           <div class="location location-img"></div>
                           <div class="location">
                              <div class="sub-location">            
                                 <a href="#"><img src="/Themes/img/home/map-pin.jpg"></a>
                              </div>
                              <span>
                              New Jersey        
                              </span>
                              <p>
                                 2500 Hamilton Blvd, <br> Unit # B,
                                 South Plainfield, <br>NJ 07080        
                              </p>
                           </div>
                           <div class="location">
                              <div class="sub-location">            
                                 <a href="#"><img src="/Themes/img/home/map-pin.jpg"></a>
                              </div>                               
                              <span>
                              Pennsylvania        
                              </span>
                             <p>774 American Drive <br>  STE # B, Bensalem,
                                 <br>PA 19020
                              </p>
                           </div>
                           <div class="location">
                              <div class="sub-location">            
                                 <a href="#"><img src="/Themes/img/home/map-pin.jpg"></a>
                              </div>
                              <span>
                              North Pennsylvania        
                              </span>
                              <p>161 Centerpoint Blvd, <br> Jenkins Township,
                                    <br>PA 18640   
                              </p>
                           </div>
                           <div class="location">
                              <div class="sub-location">            
                                 <a href="#"><img src="/Themes/img/home/map-pin.jpg"></a>
                              </div>
                              <span>
                              West Pennsylvania        
                              </span>
                              <p>3251 Old Frankstown Rd, <br> STE G,
Pittsburgh,<br> PA 15239   
                              </p>
                           </div>
                           <div class="location">
                              <div class="sub-location">            
                                 <a href="#"><img src="/Themes/img/home/map-pin.jpg"></a>
                              </div>
                              <span>
                              Connecticut        
                              </span>
                              <p>40 Front Ave, Unit A, <br> West Haven, <br>CT 06516   
                              </p>
                           </div>
                        </div>
                        <!-- End Div for Our Locations -->
                     </div>
                  </div>
               </div>
            </main>
</asp:Content>

