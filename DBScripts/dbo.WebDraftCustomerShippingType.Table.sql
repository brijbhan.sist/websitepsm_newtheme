USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebDraftCustomerShippingType]    Script Date: 01/13/2020 21:27:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebDraftCustomerShippingType](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAutoId] [int] NULL,
	[ShippingAutoId] [int] NULL,
	[ShippingCharge] [decimal](18, 2) NULL,
	[IsDefault] [int] NULL
) ON [PRIMARY]
GO
