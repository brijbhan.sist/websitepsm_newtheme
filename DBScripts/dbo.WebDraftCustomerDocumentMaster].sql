
drop table [dbo].[WebDraftCustomerDocumentMaster]

CREATE TABLE [dbo].[WebDraftCustomerDocumentMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NULL,
	[WebAdminDocumentId] [int] NULL,
	[DocumentUrl] [varchar](max) NULL,
	[VerifyStatus] [int] NULL,
	[UploadBy] [int] NULL,
	[ApprovedBy] [int] NULL,
	[UploadDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[WebDraftCustomerDocumentMaster]  WITH CHECK ADD  CONSTRAINT [FK_WebDraftCustomerDocumentMaster_WebAdminDocumentMaster] FOREIGN KEY([WebAdminDocumentId])
REFERENCES [dbo].[WebAdminDocumentMaster] ([AutoId])
GO

ALTER TABLE [dbo].[WebDraftCustomerDocumentMaster] CHECK CONSTRAINT [FK_WebDraftCustomerDocumentMaster_WebAdminDocumentMaster]
GO


