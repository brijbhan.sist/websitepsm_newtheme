--drop table WebCartMaster

CREATE TABLE [dbo].[WebCartMaster](
[AutoId] [int] IDENTITY(1,1) NOT NULL,
[UserId] [int] NULL,
[StorId] [int] NULL,
[ShippAddrId] [int] NULL,
[BillAddrId] [int] NULL,
[TotalAmount] decimal(18,2) NULL,
[MltaxRate]  decimal(18,2) NULL,
[TotalMLqty]  decimal(18,2) NULL,
[Mltax]   decimal(18,2) NULL,
[WeightTaxRate] decimal(18,2) NULL,
[TotalWeightqty]  decimal(18,2) NULL,
[WeightTax] decimal(18,2) NULL,
[PaybleAmount] decimal(18,2) NULL,
[CartDate] [datetime] NULL  DEFAULT (getdate())
) ON [PRIMARY]
GO

alter table   [dbo].[WebCartMaster]  drop column  [TotalAmount]
alter table   [dbo].[WebCartMaster]  drop column  [MltaxRate]
alter table   [dbo].[WebCartMaster]  drop column  [TotalMLqty]
alter table   [dbo].[WebCartMaster]  drop column  [Mltax]
alter table   [dbo].[WebCartMaster]  drop column  [TotalWeightqty]
alter table   [dbo].[WebCartMaster]  drop column  [WeightTax]
alter table   [dbo].[WebCartMaster]  drop column  [PaybleAmount]
go

alter table [dbo].[WebCartMaster] add  [TotalAmount]  AS ([dbo].[FN_Web_Cart_TotalAmount]([AutoId]))
alter table [dbo].[WebCartMaster] add [MltaxRate]  AS ([dbo].[FN_Web_Cart_MLtaxApply]([AutoId]))
alter table [dbo].[WebCartMaster] add [TotalMLqty]  AS ([dbo].[FN_Web_Cart_TotalMLqty]([AutoId]))
alter table [dbo].[WebCartMaster] add [Mltax]  AS ([dbo].[FN_Web_Cart_Mltax]([AutoId]))
alter table [dbo].[WebCartMaster] add [TotalWeightqty]  AS ([dbo].[FN_Web_Cart_TotalWeightqty]([AutoId]))
alter table [dbo].[WebCartMaster] add [WeightTax]  AS ([dbo].[FN_Web_Cart_WeightTax]([AutoId]))
alter table [dbo].[WebCartMaster] add [PaybleAmount]  AS ([dbo].[FN_Web_Cart_PaybleAmount]([AutoId]))