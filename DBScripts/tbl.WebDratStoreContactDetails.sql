USE [PriceSmart]
GO

/****** Object:  Table [dbo].[WebDratStoreContactDetails]    Script Date: 06/26/2020 21:14:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WebDratStoreContactDetails](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[StoreAutoId] [int] NULL,
	[ContactPerson] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Mobile] [varchar](12) NULL,
	[AlternateEmail] [varchar](50) NULL,
	[Landlineno1] [varchar](12) NULL,
	[FaxNo] [varchar](25) NULL,
	[Landlineno2] [varchar](12) NULL,
	[ContactType] [int] NULL
) ON [PRIMARY]
GO


