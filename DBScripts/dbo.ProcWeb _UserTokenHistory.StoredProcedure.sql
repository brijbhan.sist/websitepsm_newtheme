USE [PriceSmart]
GO
/****** Object:  StoredProcedure [dbo].[ProcWeb _UserTokenHistory]    Script Date: 01/13/2020 21:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[ProcWeb_UserTokenHistory]
@Opcode INT=NULL,     
@Token varchar(max),
@UserID varchar(50),                                         
@IsException bit out,                                                          
@ExceptionMessage varchar(max) out     

AS
BEGIN
    SET @IsException=0
	SET @ExceptionMessage=''
     IF @Opcode=11
	 BEGIN
          INSERT INTO TokenHistory (Token,UserID,Status,GenreateDate,LastUsed)  VALUES ( @Token , @UserID, 'A', GETDATE(),GETDATE() )
	 END
	 IF @Opcode=21
	 BEGIN
	      UPDATE TokenHistory SET Status='I',LastUsed=GetDate() WHERE USERID=@USERID and Token=@Token  
		  Select 'InActive' as Status
	 END
	 IF @Opcode=31
	 BEGIN
		IF EXISTS(SELECT * FROM  TokenHistory WHERE USERID=@USERID and Token=@Token and Status='A')  
		BEGIN  
			--declare @lastdate datetime=null,@currentdate datetime=null  
			--SELECT @lastdate=CONVERT(VARCHAR(10), LastUsed, 111),@currentdate=CONVERT(VARCHAR(10), GETDATE(), 111) from TokenHistory   
			--WHERE  USERID=@USERID and Token=@Token
			--	IF(@lastdate=@currentdate) -- Comparing date equality  
			--	BEGIN  
					Declare @MinuteDiff INT,@SetTime INT=60 --calculating minute difference  
					SET @MinuteDiff =(SELECT top 1 DATEDIFF(MINUTE,LastUsed , GETDATE()) 
					FROM TokenHistory WHERE  USERID=@USERID and Token=@Token order by TokenId desc)
						if(@MinuteDiff>=@SetTime)  
						BEGIN  
							Update TokenHistory Set Status='I' WHERE  USERID=@USERID and Token=@Token  
							Select 'InActive' as Status  
						END  
						ELSE  
						BEGIN  
							Update TokenHistory Set LastUsed=GetDate() WHERE  USERID=@USERID and Token=@Token  
							Select case when Status='I' then 'InActive' when Status='A' then 'Active' end as Status from TokenHistory WHERE  USERID=@USERID and Token=@Token  
						END  
				--END  
		END
		ELSE
		BEGIN
		     Select 'InActive' as Status
		END  
	END
END
--SELECT top 2 * FROM  TokenHistory order by TokenId desc

GO
