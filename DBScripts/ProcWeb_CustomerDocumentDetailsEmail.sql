 alter procedure ProcWeb_CustomerDocumentDetailsEmail
 @StoreId int,
 @AutoId int,
 @Subject varchar(250)
 AS                 
 BEGIN

 --declare  @DriveLocation varchar(250)
	--SET @DriveLocation='D:\DraftProduct\'+UPPER((select CompanyId from [psmnj.a1whm.com].[dbo].CompanyDetails))+'_Created'+
	--convert(varchar(50),Format(GetDate(),'MM_dd_yyyy_hh_mm_tt'))+'.html'

	Declare @html varchar(Max)='',@Location varchar(50),@tr varchar(max),@td varchar(max)='',@num int,@query nvarchar(MAX),@count int,
	@StoreName varchar(50),@DocumentName varchar(50),@Description varchar(max),@IsRequired varchar(25),@UploadDate varchar(20)
	

	select @DocumentName=WAD.DocumentName,@Description=WAD.Description,@IsRequired=(Case when WAD.IsRequired=1 
	then 'Required' when WAD.IsRequired=0 then 'Not Required' end),
	@StoreName=(Select StoreName from [PriceSmart].[dbo].WebStoreDraft where AutoId=@StoreId),
	@UploadDate=ISNULL(Format(WCD.UploadDate,'MM/dd/yyyy'),'')            
	FROM [PriceSmart].[dbo].WebAdminDocumentMaster AS WAD                
	INNER JOIN [PriceSmart].[dbo].WebDraftCustomerDocumentMaster AS WCD ON               
	WAD.AutoId=WCD.WebAdminDocumentId  and WCD.StoreId=@StoreId and WAD.Status=1  AND WCD.AutoId=@AutoId 

	set @html='
	    <table class="table tableCSS" border="1" style="width:40%;border: 1px solid black;border-collapse:collapse;">
		<thead>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:10px;">Customer''s Document</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">Pricesmart Website</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+Convert(varchar(50),FORMAT(GetDate(),'MM/dd/yyyy hh:ss tt'))+'</h4>
				</td>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		<br>
		<table class="table tableCSS" border="1" style="width:40%;border: 1px solid black;border-collapse:collapse;"> 
		<thead>
		<tr>
		    <td style=''text-align:center;''><b>Store Name</b></td>
			<td style=''text-align:left;''>'+@StoreName+'</td>
       </tr>
	   <tr>
		    <td style=''text-align:center;''><b>Document Name</b></td>
			<td style=''text-align:left;''>'+@DocumentName+'</td>
       </tr>
	   <tr>
			<td style=''text-align:center;''><b>Is Required</b></td>
			<td style=''text-align:left;''>'+@IsRequired+'</td>
       </tr>
	   <tr>
			<td style=''text-align:center;''><b>Upload Date</b></td>
		    <td style=''text-align:left;''>'+@UploadDate+'</td>
       </tr>
	   <tr>
			<td style=''text-align:center;''><b>Description</b></td>
			<td style=''text-align:left;''>'+@Description+'</td>
       </tr>
	   </thead>
		<tbody></tbody></table>'
	--EXEC [psmnj.a1whm.com].dbo.spWriteToFile @DriveLocation, @html 

	-----------------------------------------Code For Email---------------------------------------------------------------
	Declare  @FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max)
	SELECT @FromEmailId=EmailId,@Port=port,@SMTPServer=server,@Password=convert(varchar(50), DecryptByPassPhrase('ESS',Pass)),
	@SSL=ssl from [psmnj.a1whm.com].[dbo].EmailServerMaster 
	where  SendTo='Developer' 
	set @BCCEmailId=(select BCCEmail from [PriceSmart].[dbo].EmailCategory where Type='Document Upload')
	set @ToEmailId=(select TOEmail from [PriceSmart].[dbo].EmailCategory where Type='Document Upload')
	SET @CCEmailId=(select CCCEmail from [PriceSmart].[dbo].EmailCategory where Type='Document Upload')


	if(@DocumentName!='')
	BEGIN
			EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@FromEmailId,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId =@ToEmailId,
			@CCEmailId =@CCEmailId,
			@BCCEmailId =@BCCEmailId,  
			@Subject =@Subject,
			@EmailBody = @html,
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='Customer Document''s Details', 
			@isException=0,
			@exceptionMessage=''  
	END
  END