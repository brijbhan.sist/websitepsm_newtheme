USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebDraftCustomerDocumentMaster]    Script Date: 01/13/2020 21:27:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebDraftCustomerDocumentMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NULL,
	[WebAdminDocumentId] [int] NULL,
	[DocumentUrl] [varchar](max) NULL,
	[VerifyStatus] [int] NULL,
	[UploadBy] [int] NULL,
	[ApprovedBy] [int] NULL,
	[UploadDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[WebDraftCustomerDocumentMaster] ON 

INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (1, 4, 2, N'1578507915000_inventory.png', 2, 0, 1, CAST(N'2020-01-08T23:55:49.070' AS DateTime), CAST(N'2020-01-08T23:55:49.070' AS DateTime))
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (2, 4, 1, N'1578513601000_inventory.png', 2, 0, 1, CAST(N'2020-01-09T01:30:18.057' AS DateTime), CAST(N'2020-01-09T01:30:18.057' AS DateTime))
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (3, 7, NULL, NULL, 1, 7, NULL, CAST(N'2020-01-11T03:52:57.033' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (4, 7, NULL, NULL, 1, 7, NULL, CAST(N'2020-01-11T04:09:16.957' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (5, 7, NULL, NULL, 1, 7, NULL, CAST(N'2020-01-11T04:11:04.517' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (6, 7, NULL, NULL, 1, 7, NULL, CAST(N'2020-01-11T04:15:06.300' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (8, 0, NULL, NULL, 1, 0, NULL, CAST(N'2020-01-11T04:31:10.910' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (9, 0, NULL, NULL, 1, 0, NULL, CAST(N'2020-01-11T04:32:35.643' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (11, 7, NULL, NULL, 1, 7, NULL, CAST(N'2020-01-11T04:36:20.877' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (12, 7, NULL, NULL, 1, 7, NULL, CAST(N'2020-01-11T04:38:47.360' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (13, 7, 0, NULL, 1, 7, NULL, CAST(N'2020-01-11T04:46:25.830' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (14, 7, 4, N'1578698244000_inventory.png', 1, 7, NULL, CAST(N'2020-01-11T04:47:51.017' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (15, 7, 1, N'1578698855000_inventory.png', 1, 7, NULL, CAST(N'2020-01-11T04:57:40.410' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (7, 0, NULL, NULL, 1, 0, NULL, CAST(N'2020-01-11T04:29:26.360' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (10, 7, NULL, NULL, 1, 7, NULL, CAST(N'2020-01-11T04:34:38.440' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (16, 7, 2, N'1578786608000_inventory.png', 1, 7, NULL, CAST(N'2020-01-12T05:22:52.410' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (17, 7, 3, N'1578787523000_inventory.png', 1, 7, NULL, CAST(N'2020-01-12T05:35:55.550' AS DateTime), NULL)
INSERT [dbo].[WebDraftCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (18, 7, 5, N'1578790809000_inventory.png', 1, 7, NULL, CAST(N'2020-01-12T06:30:15.970' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[WebDraftCustomerDocumentMaster] OFF
