USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[CustomerMaster]    Script Date: 01/03/2020 23:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](15) NULL,
	[CustomerName] [varchar](100) NULL,
	[CustomerType] [int] NULL,
	[Email] [varchar](50) NULL,
	[AltEmail] [varchar](50) NULL,
	[Contact1] [varchar](30) NULL,
	[Contact2] [varchar](50) NULL,
	[DefaultBillAdd] [int] NULL,
	[DefaultShipAdd] [int] NULL,
	[SalesPersonAutoId] [int] NULL,
	[Status] [int] NULL,
	[Terms] [int] NULL,
	[MobileNo] [varchar](20) NULL,
	[FaxNo] [varchar](20) NULL,
	[TaxId] [varchar](50) NULL,
	[ContactPersonName] [varchar](100) NULL,
	[LastOrderDate] [date] NULL,
	[BusinessName] [varchar](250) NULL,
	[OPTLicence] [varchar](250) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[updatedBy] [int] NULL,
	[UpdateOn] [datetime] NULL,
	[WebStatus] [int] NULL,
	[LocationAutoId] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varbinary](max) NULL,
	[IsAppLogin] [int] NULL,
 CONSTRAINT [PK__Customer__6B232905337BEFCC] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomerMaster] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CustomerMaster] ADD  DEFAULT (getdate()) FOR [UpdateOn]
GO
ALTER TABLE [dbo].[CustomerMaster] ADD  DEFAULT ((0)) FOR [WebStatus]
GO
ALTER TABLE [dbo].[CustomerMaster]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerMaster_EmployeeMaster] FOREIGN KEY([SalesPersonAutoId])
REFERENCES [dbo].[EmployeeMaster] ([AutoId])
GO
ALTER TABLE [dbo].[CustomerMaster] CHECK CONSTRAINT [FK_CustomerMaster_EmployeeMaster]
GO
