USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebCartItemMaster]    Script Date: 01/13/2020 21:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebCartItemMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CartMasterId] [int] NOT NULL,
	[ProductId] [int] NULL,
	[ProductUnit] [varchar](50) NULL,
	[PerUnitQty] [int] NULL,
	[OrderQty] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[NetPrice] [decimal](18, 2) NULL,
	[PerMLQty] [decimal](18, 2) NULL,
	[TotalMLQty] [decimal](18, 2) NULL,
	[PerWeightQty] [decimal](18, 2) NULL,
	[TotalWeightQty] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
