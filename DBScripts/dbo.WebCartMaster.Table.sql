USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebCartMaster]    Script Date: 01/13/2020 21:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebCartMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[StorId] [int] NULL,
	[AddDate] [datetime] NULL,
	[SubTotal] [decimal](18, 2) NULL,
	[Discount] [decimal](18, 2) NULL,
	[DiscAmount] [decimal](18, 2) NULL,
	[TaxId] [int] NULL,
	[TaxValue] [decimal](18, 2) NULL,
	[TaxAmount] [decimal](18, 2) NULL,
	[TotalMLQty] [int] NULL,
	[TotalMLTax] [decimal](18, 2) NULL,
	[TotalWeightQty] [int] NULL,
	[TotalWeightTaxt] [decimal](18, 2) NULL,
	[NetPayableAmount] [decimal](18, 2) NULL,
	[ShippingId] [int] NOT NULL,
	[PaymentId] [int] NOT NULL,
	[ShippAddrId] [int] NULL,
	[BillAddrId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebCartMaster] ADD  CONSTRAINT [DF_WebCartMaster_AddDate]  DEFAULT (getdate()) FOR [AddDate]
GO
ALTER TABLE [dbo].[WebCartMaster] ADD  DEFAULT ((0)) FOR [ShippingId]
GO
ALTER TABLE [dbo].[WebCartMaster] ADD  DEFAULT ((0)) FOR [PaymentId]
GO
ALTER TABLE [dbo].[WebCartMaster] ADD  DEFAULT ((0)) FOR [ShippAddrId]
GO
ALTER TABLE [dbo].[WebCartMaster] ADD  DEFAULT ((0)) FOR [BillAddrId]
GO
