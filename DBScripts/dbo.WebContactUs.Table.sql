USE [PriceSmart]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebContactUs](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Query] [varchar](max) NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

alter table [dbo].[WebContactUs] add QueryType varchar(50),Date datetime,PhoneNo varchar(12),CreateDate datetime