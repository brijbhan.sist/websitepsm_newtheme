USE [PriceSmart]
GO
/****** Object:  Table [dbo].[APIUrlDetails]    Script Date: 01/13/2020 21:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[APIUrlDetails](
	[Autoid] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Url] [varchar](max) NOT NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[APIUrlDetails] ON 

INSERT [dbo].[APIUrlDetails] ([Autoid], [Name], [Url], [Status]) VALUES (1, N'psmnj', N'http://psmnj.a1whm.com/
', 0)
SET IDENTITY_INSERT [dbo].[APIUrlDetails] OFF
