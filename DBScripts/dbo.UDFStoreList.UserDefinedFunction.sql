USE [PriceSmart]
GO
/****** Object:  UserDefinedFunction [dbo].[UDFStoreList]    Script Date: 3/7/2020 10:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter function [dbo].[UDFStoreList](  
    @StoreLocation varchar(10)=NULL,  
    @StoreId INT=NULL  
)  
RETURNS Varchar(50)  
AS   
BEGIN  
 Declare @StoreName varchar(50)  
 IF @StoreLocation='psmnj'  
 BEGIN  
    SET @StoreName=(SELECT CustomerName as StoreName from [psmnj.a1whm.com].[dbo].CustomerMaster where AutoId=@StoreId)  
 END 
 ELSE IF @StoreLocation='psmct'  
 BEGIN  
    SET @StoreName=(SELECT CustomerName as StoreName from [psmct.a1whm.com].[dbo].CustomerMaster where AutoId=@StoreId)  
 END ELSE IF @StoreLocation='psmnpa'  
 BEGIN  
    SET @StoreName=(SELECT CustomerName as StoreName from [psmnpa.a1whm.com].[dbo].CustomerMaster where AutoId=@StoreId)  
 END 
 ELSE IF @StoreLocation='psmpa'  
 BEGIN  
    SET @StoreName=(SELECT CustomerName as StoreName from [psmpa.a1whm.com].[dbo].CustomerMaster where AutoId=@StoreId)  
 END 
 ELSE IF @StoreLocation='psmwpa'  
 BEGIN  
    SET @StoreName=(SELECT CustomerName as StoreName from [psmwpa.a1whm.com].[dbo].CustomerMaster where AutoId=@StoreId)  
 END 
 ELSE IF @StoreLocation='psm'  
 BEGIN  
    SET @StoreName=(SELECT CustomerName as StoreName from [psm.a1whm.com].[dbo].CustomerMaster where AutoId=@StoreId)  
 END 
 ELSE IF @StoreLocation is null  
 BEGIN  
     SET @StoreName=(SELECT StoreName FROM WebStoreDraft where AutoId=@StoreId)  
 END  
 RETURN @StoreName  
END
GO
