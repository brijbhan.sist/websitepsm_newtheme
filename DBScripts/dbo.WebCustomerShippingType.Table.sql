USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebCustomerShippingType]    Script Date: 01/13/2020 21:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebCustomerShippingType](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAutoId] [int] NULL,
	[ShippingAutoId] [int] NULL,
	[ShippingCharge] [decimal](18, 2) NULL,
	[IsDefault] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebCustomerShippingType] ADD  DEFAULT ((0.00)) FOR [ShippingCharge]
GO
ALTER TABLE [dbo].[WebCustomerShippingType] ADD  DEFAULT ((0)) FOR [IsDefault]
GO
