USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebAdminDocumentMaster]    Script Date: 01/13/2020 21:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebAdminDocumentMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentName] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
	[IsRequired] [int] NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[WebAdminDocumentMaster] ON 

INSERT [dbo].[WebAdminDocumentMaster] ([AutoId], [DocumentName], [Description], [IsRequired], [Status]) VALUES (1, N'Driving License', N'Driving License', 1, 1)
INSERT [dbo].[WebAdminDocumentMaster] ([AutoId], [DocumentName], [Description], [IsRequired], [Status]) VALUES (2, N'Voter Id Card', N'Voter Id Card', 1, 1)
INSERT [dbo].[WebAdminDocumentMaster] ([AutoId], [DocumentName], [Description], [IsRequired], [Status]) VALUES (3, N'Aadhar Card', N'Aadhar Card', 1, 1)
INSERT [dbo].[WebAdminDocumentMaster] ([AutoId], [DocumentName], [Description], [IsRequired], [Status]) VALUES (4, N'Pan Card', N'Pan Card', 1, 1)
INSERT [dbo].[WebAdminDocumentMaster] ([AutoId], [DocumentName], [Description], [IsRequired], [Status]) VALUES (5, N'Store', N'Store Registration Document', 1, 1)
INSERT [dbo].[WebAdminDocumentMaster] ([AutoId], [DocumentName], [Description], [IsRequired], [Status]) VALUES (6, N'Business', N'Business Registration Document', 1, 1)
SET IDENTITY_INSERT [dbo].[WebAdminDocumentMaster] OFF
ALTER TABLE [dbo].[WebAdminDocumentMaster] ADD  DEFAULT ((0)) FOR [IsRequired]
GO
ALTER TABLE [dbo].[WebAdminDocumentMaster] ADD  DEFAULT ((0)) FOR [Status]
GO
