alter procedure [dbo].[ProcWeb_Document]
@Opcode int=NULL,   
@AutoId int=null,
@StoreId int=null,              
@Location varchar(50)=null, 
@Document int=null,            
@DocumentUrl varchar(120)=null,    
@StoreLocation varchar(10)=null, 
@UserId int=null, 
@UserName varchar(50)=null,
@isException bit out,                        
@exceptionMessage varchar(max) out 
AS                                
BEGIN                                
  SET @IsException=0                                
  SET @ExceptionMessage='' 
	IF @Opcode=51 --Draft document list    
	BEGIN    
	SELECT WAD.AutoId,WAD.DocumentName,WAD.Description,Case when WAD.IsRequired=1 then 'Required' when WAD.IsRequired=0 then 'Not Required' end as IsRequired,                
	isnull(VerifyStatus,0) as VerifyStatus,WCD.AutoId as WebAutoId, Format(WCD.ApprovedDate,'MM/dd/yyyy') as ApprovedDate,Format(WCD.UploadDate,'MM/dd/yyyy') as UploadDate,isnull(DocumentUrl,'') as DocumentUrl            
	FROM [PriceSmart].[dbo].WebAdminDocumentMaster AS WAD                
	LEFT JOIN [PriceSmart].[dbo].WebDraftCustomerDocumentMaster AS WCD ON                 
	WAD.AutoId=WCD.WebAdminDocumentId  and WCD.StoreId=@StoreId and WAD.Status=1               
	for json path,include_null_values   
	
	Select AutoId,DocumentName+' - '+case when isRequired=1 then 'Required' end as DocumentName from [PriceSmart].[dbo].WebAdminDocumentMaster 
	for json path,include_null_values                    
	END  
	IF @Opcode=52 --Upload Document Admin               
 BEGIN                
		IF EXISTS(SELECT * FROM [PriceSmart].[dbo].WebDraftCustomerDocumentMaster 
		WHERE StoreId=@StoreId AND WebAdminDocumentId=@Document)              
		BEGIN   
			SET  @UserId=(Select top 1 UserId from [PriceSmart].[dbo].WebUserStoreRelMaster where StoreId=@StoreId)           
			UPDATE [PriceSmart].[dbo].WebDraftCustomerDocumentMaster SET DocumentUrl=@DocumentUrl,VerifyStatus=1,UploadBy=@UserId             
			WHERE StoreId=@StoreId AND WebAdminDocumentId=@Document  
			SET @AutoId=(Select AutoId FROM [PriceSmart].[dbo].WebDraftCustomerDocumentMaster where StoreId=@StoreId AND WebAdminDocumentId=@Document)
			EXEC [PriceSmart].[dbo].ProcWeb_CustomerDocumentDetailsEmail @Subject='New document uploaded.',@StoreId=@StoreId,@AutoId=@AutoId
		END              
		ELSE              
		BEGIN              
			Insert into [PriceSmart].[dbo].WebDraftCustomerDocumentMaster (StoreId,WebAdminDocumentId,DocumentUrl,VerifyStatus,UploadBy,UploadDate) VALUES               
			(@StoreId,@Document,@DocumentUrl,1,(Select top 1 UserId from [PriceSmart].[dbo].WebUserStoreRelMaster where StoreId=@StoreId) ,GETDATE())                
		    SET @AutoId=SCOPE_IDENTITY()
			EXEC [PriceSmart].[dbo].ProcWeb_CustomerDocumentDetailsEmail @Subject='Document updated.',@StoreId=@StoreId,@AutoId=@AutoId
		END              
  END 
  IF @Opcode=53 --User Document List               
 BEGIN          
		SELECT WAD.AutoId,WAD.DocumentName,WAD.Description,Case when WAD.IsRequired=1 then 'Required' when WAD.IsRequired=0 then 'Not Required' end as IsRequired,                
		VerifyStatus,WCD.AutoId as WebAutoId, Format(WCD.ApprovedDate,'MM/dd/yyyy') as ApprovedDate,Format(WCD.UploadDate,'MM/dd/yyyy') as UploadDate             
		FROM [PriceSmart].[dbo].WebAdminDocumentMaster AS WAD                
		LEFT JOIN [PriceSmart].[dbo].WebDraftCustomerDocumentMaster AS WCD ON               
		WAD.AutoId=WCD.WebAdminDocumentId  and WCD.StoreId=@StoreId and WAD.Status=1               
		for json path,include_null_values                        
 END    
 IF @opcode=54 --Approved Document List  
 BEGIN  
		Declare @DocQuery varchar(max)=''  
		SET @DocQuery='Select DocumentName,DocumentURL,format(UploadedDate,''MM/dd/yyyy'') as UploadedDate,''Approved'' as Status from  
		['+@StoreLocation+'.a1whm.com].[dbo].CustomerDocumentMaster where CustomerAutoId='+Convert(varchar,@StoreId)+'          
		for json path,include_null_values '  
		Execute( @DocQuery)      
 END    
END 