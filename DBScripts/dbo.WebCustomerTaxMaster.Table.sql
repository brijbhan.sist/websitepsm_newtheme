USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebCustomerTaxMaster]    Script Date: 01/13/2020 21:27:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebCustomerTaxMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[TaxType] [varchar](50) NULL,
	[TaxValue] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
