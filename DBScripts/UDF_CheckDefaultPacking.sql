--Alter table ProductMaster add Product_ShowOnWebsite as (DBO.UDF_CheckDefaultPacking(ProductId))
Create Function DBO.UDF_CheckDefaultPacking
(
@ProductID int
)
Returns int
AS
BEGIN
     Declare @Output int
	 IF EXISTS(
     Select PM.AutoId from ProductMaster as PM 
	 INNER JOIN PackingDetails as PD on
	 PM.AutoId=PD.ProductAutoId AND PM.PackingAutoId=PD.UnitType Where PM.ProductId=@ProductID)
	 BEGIN
	      SET @Output=1
	 END
	 ELSE
	 BEGIN
	      SET @Output=0
	 END
	 Return @Output
END