USE [PriceSmart]
GO
/****** Object:  Table [dbo].[BannerCategoryMaster]    Script Date: 3/7/2020 10:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BannerCategoryMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[Category] [varchar](100) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_BannerCategory] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
