USE [PriceSmart]
GO 



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateMaster](
	[AutoId] [int] NOT NULL,
	[TemplateName] [varchar](50) NULL,
	[Subject] [varchar](500) NULL,
	[EmailBody] [nvarchar](max) NULL,
 CONSTRAINT [PK_EmailTemplateMaster] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
