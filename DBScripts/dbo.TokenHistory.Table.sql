USE [PriceSmart]
GO
/****** Object:  Table [dbo].[TokenHistory]    Script Date: 01/13/2020 21:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TokenHistory](
	[TokenId] [int] IDENTITY(1,1) NOT NULL,
	[Token] [varchar](max) NULL,
	[UserID] [varchar](50) NULL,
	[Status] [char](1) NULL,
	[GenreateDate] [datetime] NULL,
	[LastUsed] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TokenHistory] ON 

INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (21, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3ODkyNTcsImV4cCI6MTU3ODc5MTA1NywiaWF0IjoxNTc4Nzg5MjU3fQ.sDi6HDtpho5MTQub1wRKgA-55ctiALcV6xIr-rn_Gzk', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:04:18.423' AS DateTime), CAST(N'2020-01-12T06:04:18.423' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (26, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3ODk3NjksImV4cCI6MTU3ODc5MTU2OSwiaWF0IjoxNTc4Nzg5NzY5fQ.t8g1oOh2K7rHQBPNRpJZWiDkdNVXleUj5vqogF8BTa4', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:12:50.720' AS DateTime), CAST(N'2020-01-12T06:12:50.720' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (20, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3ODkxMjMsImV4cCI6MTU3ODc5MDkyMywiaWF0IjoxNTc4Nzg5MTIzfQ.WFPbnTPWmFvBWdRJk-qz8MCzCl94S6F26uz4bBMtSeY', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:02:04.487' AS DateTime), CAST(N'2020-01-12T06:02:04.487' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (22, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3ODk0MjcsImV4cCI6MTU3ODc5MTIyNywiaWF0IjoxNTc4Nzg5NDI3fQ.Pi7_s-uc2qS0YlY3V98Ut9P9voWBByipwRemoxEIsfc', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:07:08.377' AS DateTime), CAST(N'2020-01-12T06:07:08.377' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (25, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3ODk3NTYsImV4cCI6MTU3ODc5MTU1NiwiaWF0IjoxNTc4Nzg5NzU2fQ.GKGlKgFRKYyo96Hc7CrfLhdbxTeXPqFqLda68N8BLyk', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:12:37.957' AS DateTime), CAST(N'2020-01-12T06:12:37.957' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (27, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3ODk4NzAsImV4cCI6MTU3ODc5MTY3MCwiaWF0IjoxNTc4Nzg5ODcwfQ.fM0R62fYXIjhNphORGUG2VSB1MGig0oDmEqVZj9t4KE', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:14:31.877' AS DateTime), CAST(N'2020-01-12T06:14:31.877' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (28, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3OTAwNzAsImV4cCI6MTU3ODc5MTg3MCwiaWF0IjoxNTc4NzkwMDcwfQ.fEtV72E1L5bXvUDDgqtNj9HNe_AuMAe_yeAdlRSDVi8', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:17:51.313' AS DateTime), CAST(N'2020-01-12T06:17:51.313' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (30, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3OTA2ODksImV4cCI6MTU3ODc5MjQ4OSwiaWF0IjoxNTc4NzkwNjg5fQ.rS5Bd7g52dgBNfi5NVl48gUufy-YYjCzEgGf-_j59aU', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:28:11.017' AS DateTime), CAST(N'2020-01-12T06:30:17.893' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (19, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3ODczNzYsImV4cCI6MTU3ODc4OTE3NiwiaWF0IjoxNTc4Nzg3Mzc2fQ.xGtumVLKw5xbalsYTGPUOh58QYIPZhO72dfXqkLUrAY', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T05:32:57.690' AS DateTime), CAST(N'2020-01-12T05:53:12.173' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (23, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3ODk1MzYsImV4cCI6MTU3ODc5MTMzNiwiaWF0IjoxNTc4Nzg5NTM2fQ.GmH4hG5f4l7W72I-FuZYZ9z4cYUqtfZGHyf4BLhI2go', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:08:58.017' AS DateTime), CAST(N'2020-01-12T06:08:58.017' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (24, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3ODk3MjAsImV4cCI6MTU3ODc5MTUyMCwiaWF0IjoxNTc4Nzg5NzIwfQ.bRFQshGe1Huhco6K2cqI5s1LKBYQ7xu1O4PT23JynYk', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:12:01.563' AS DateTime), CAST(N'2020-01-12T06:12:01.563' AS DateTime))
INSERT [dbo].[TokenHistory] ([TokenId], [Token], [UserID], [Status], [GenreateDate], [LastUsed]) VALUES (29, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImZpdHRodXNlckBnbWFpbC5jb20iLCJuYmYiOjE1Nzg3OTAzMzIsImV4cCI6MTU3ODc5MjEzMiwiaWF0IjoxNTc4NzkwMzMyfQ.t-a3fkKgMmKPJ9axO6JnKqPY9gLsBB8pVVXIss3WHpM', N'fitthuser@gmail.com', N'A', CAST(N'2020-01-12T06:22:13.347' AS DateTime), CAST(N'2020-01-12T06:22:13.347' AS DateTime))
SET IDENTITY_INSERT [dbo].[TokenHistory] OFF
