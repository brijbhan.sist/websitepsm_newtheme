USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebStoreDraft]    Script Date: 01/13/2020 21:27:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebStoreDraft](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessName] [varchar](150) NULL,
	[StoreName] [varchar](150) NULL,
	[OPTLicense] [varchar](50) NULL,
	[TaxID] [varchar](20) NULL,
	[BillAutoId] [int] NULL,
	[ShipAutoId] [int] NULL,
	[ContactPerson] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Mobile] [varchar](12) NULL,
	[AlternateEmail] [varchar](50) NULL,
	[MainPhone] [varchar](12) NULL,
	[FaxNo] [varchar](25) NULL,
	[StorePhone] [varchar](12) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[WebStoreDraft] ON 

INSERT [dbo].[WebStoreDraft] ([AutoId], [BusinessName], [StoreName], [OPTLicense], [TaxID], [BillAutoId], [ShipAutoId], [ContactPerson], [Email], [Mobile], [AlternateEmail], [MainPhone], [FaxNo], [StorePhone], [Status]) VALUES (5, N'Third Business', N'Third Store', N'OPTLI123', N'TX123', 10, 9, N'', N'', N'7894561235', N'', N'6548489498', N'', N'3216548948', 0)
INSERT [dbo].[WebStoreDraft] ([AutoId], [BusinessName], [StoreName], [OPTLicense], [TaxID], [BillAutoId], [ShipAutoId], [ContactPerson], [Email], [Mobile], [AlternateEmail], [MainPhone], [FaxNo], [StorePhone], [Status]) VALUES (7, N'XASDD', N'Sixth Second', N'ZXCV', N'', 13, 12, N'', N'', N'8976451238', N'', N'8976453128', N'', N'7896453128', 0)
INSERT [dbo].[WebStoreDraft] ([AutoId], [BusinessName], [StoreName], [OPTLicense], [TaxID], [BillAutoId], [ShipAutoId], [ContactPerson], [Email], [Mobile], [AlternateEmail], [MainPhone], [FaxNo], [StorePhone], [Status]) VALUES (4, N'Seven Eleven', N'7 Store', N'OPT123', N'123', 8, 7, N'Eleven', N'eleven@gmail.com', N'7894561235', N'seven11@gmail.com', N'9784561325', N'FX101', N'7894561235', 0)
INSERT [dbo].[WebStoreDraft] ([AutoId], [BusinessName], [StoreName], [OPTLicense], [TaxID], [BillAutoId], [ShipAutoId], [ContactPerson], [Email], [Mobile], [AlternateEmail], [MainPhone], [FaxNo], [StorePhone], [Status]) VALUES (9, N'', N'Ninth Store', N'', N'', 15, 15, N'', N'', N'7896453128', N'', N'8976451238', N'', N'8976451238', 0)
INSERT [dbo].[WebStoreDraft] ([AutoId], [BusinessName], [StoreName], [OPTLicense], [TaxID], [BillAutoId], [ShipAutoId], [ContactPerson], [Email], [Mobile], [AlternateEmail], [MainPhone], [FaxNo], [StorePhone], [Status]) VALUES (11, N'', N'Eleventh Store', N'', N'', 18, 17, N'', N'', N'8976451238', N'', N'8976453128', N'', N'8976453128', 0)
INSERT [dbo].[WebStoreDraft] ([AutoId], [BusinessName], [StoreName], [OPTLicense], [TaxID], [BillAutoId], [ShipAutoId], [ContactPerson], [Email], [Mobile], [AlternateEmail], [MainPhone], [FaxNo], [StorePhone], [Status]) VALUES (8, N'A', N'ZXCV', N'ZXCV', N'sdgds', 14, 14, N'', N'', N'7896453128', N'', N'7894562318', N'', N'8976453128', 0)
INSERT [dbo].[WebStoreDraft] ([AutoId], [BusinessName], [StoreName], [OPTLicense], [TaxID], [BillAutoId], [ShipAutoId], [ContactPerson], [Email], [Mobile], [AlternateEmail], [MainPhone], [FaxNo], [StorePhone], [Status]) VALUES (10, N'', N'Tenth Store', N'', N'', 16, 16, N'', N'', N'8976453128', N'', N'7894562318', N'', N'7896453128', 0)
SET IDENTITY_INSERT [dbo].[WebStoreDraft] OFF
ALTER TABLE [dbo].[WebStoreDraft] ADD  DEFAULT ((0)) FOR [Status]
GO
