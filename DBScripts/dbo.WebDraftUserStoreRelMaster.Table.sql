USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebDraftUserStoreRelMaster]    Script Date: 01/13/2020 21:27:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebDraftUserStoreRelMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[UserId] [int] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[WebDraftUserStoreRelMaster] ON 

INSERT [dbo].[WebDraftUserStoreRelMaster] ([AutoId], [StoreId], [UserId]) VALUES (1, 4, 14)
INSERT [dbo].[WebDraftUserStoreRelMaster] ([AutoId], [StoreId], [UserId]) VALUES (2, 5, 21)
INSERT [dbo].[WebDraftUserStoreRelMaster] ([AutoId], [StoreId], [UserId]) VALUES (3, 6, 23)
INSERT [dbo].[WebDraftUserStoreRelMaster] ([AutoId], [StoreId], [UserId]) VALUES (4, 7, 23)
INSERT [dbo].[WebDraftUserStoreRelMaster] ([AutoId], [StoreId], [UserId]) VALUES (6, 9, 26)
INSERT [dbo].[WebDraftUserStoreRelMaster] ([AutoId], [StoreId], [UserId]) VALUES (5, 8, 25)
INSERT [dbo].[WebDraftUserStoreRelMaster] ([AutoId], [StoreId], [UserId]) VALUES (7, 10, 27)
INSERT [dbo].[WebDraftUserStoreRelMaster] ([AutoId], [StoreId], [UserId]) VALUES (8, 11, 28)
SET IDENTITY_INSERT [dbo].[WebDraftUserStoreRelMaster] OFF
