USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebCustomerDocumentMaster]    Script Date: 01/13/2020 21:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebCustomerDocumentMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NULL,
	[WebAdminDocumentId] [int] NULL,
	[DocumentUrl] [varchar](max) NULL,
	[VerifyStatus] [int] NULL,
	[UploadBy] [int] NULL,
	[ApprovedBy] [int] NULL,
	[UploadDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[WebCustomerDocumentMaster] ON 

INSERT [dbo].[WebCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (1, 4, 1, N'1578504702000_inventory.png', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[WebCustomerDocumentMaster] ([AutoId], [StoreId], [WebAdminDocumentId], [DocumentUrl], [VerifyStatus], [UploadBy], [ApprovedBy], [UploadDate], [ApprovedDate]) VALUES (2, 4, 1, N'1578504711000_inventory.png', 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[WebCustomerDocumentMaster] OFF
