alter PROCEDURE [dbo].[ProcWebPlaceOrderEmail]           
@StoreId int=null,
@OrderAutoId int=NULL,
@i int=1
AS          
BEGIN
if @i=1
BEGIN
	DECLARE @OrderNo varchar(50), @UserAutoId int,@FromEmailId varchar(100)=null,@FromName varchar(100)='Pricesmart',@smtp_userName varchar(100)=null,@PSW varchar(20)=null,@SMTPServer varchar(20)=null,
	@Port INT=null,@SSL varchar(20)=null,@ToEmailId varchar(100)=null,@Password varchar(500),@CCEmailId varchar(100)=null,@BCCEmailId varchar(100)=null,@Subject varchar(100)='Order has been placed'
	,@EmailBody varchar(500)=null,@Name varchar(100),@Email varchar(200),@SourceApp varchar(100)=null,@SubUrl varchar(100)='mypsm.net'
	
	select @UserAutoId=UserId from PriceSmart.dbo.WebUserStoreRelMaster where StoreId=@StoreId
	select @OrderNo=OrderNo from [dbo].OrderMaster where AutoId=@OrderAutoId
	select @Email=Email ,@Name=(FirstName+' '+LastName) from PriceSmart.dbo.WebUserRegistration where AutoId=@UserAutoId
	
	SET @EmailBody='Hi '+ @Name+' ,'+'
	<br/><br/>'+convert(varchar(10), @OrderNo)+' '+'has been placed	
	<br/></br>This is a system generated email, do not reply to this email id.
	<br>Thanks and Regards,
	<br>PriceSmart'
	select @BCCEmailId=BCCEmail,@CCEmailId=CCCEmail from PriceSmart.dbo.EmailCategory where Type='PlaceOrder'
	SELECT @FromEmailId=EmailId,@Port=port,@SMTPServer=server,@Password=convert(varchar(100),DecryptBypassphrase('WHM',Pass)),@SSL=ssl FROM [PriceSmart].[dbo].EmailServerMaster
		  
	EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] @Opcode=11,@FromEmailId=@FromEmailId,@FromName=@FromName,@smtp_userName=@FromEmailId,@Password=@Password,@SMTPServer=@SMTPServer,
	@Port=@Port,@SSL=@SSL,@ToEmailId=@Email,@CCEmailId=@CCEmailId,@BCCEmailId=@BCCEmailId,@Subject=@Subject,@EmailBody=@EmailBody,@SourceApp=@SourceApp,@SubUrl=@SubUrl,@IsException=0,@ExceptionMessage=''
END 
END