USE [PriceSmart]
GO
/****** Object:  StoredProcedure [dbo].[ProcWeb_ManageBanner]    Script Date: 3/7/2020 10:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter Procedure [dbo].[ProcWeb_ManageBanner]                                  
  @Opcode INT=NULL,        
    @IsException bit out,                                                                        
  @ExceptionMessage varchar(max) out                                        
AS 
BEGIN                                  
  SET @IsException=0                                  
  SET @ExceptionMessage=''              
  IF @Opcode=41                      
	BEGIN                              
		BEGIN TRY 
			SELECT bcm.Category,mb.BannerName,mb.Title,mb.Subtitle,((select BannerUrl from APIUrlDetails)+mb.ImageUrl) as ImageUrl,mb.Link,mb.BannerCategory FROM ManageBanner mb
			INNER JOIN BannerCategoryMaster bcm ON bcm.AutoId=mb.BannerCategory where mb.status=1 for json path
		END TRY 
		BEGIN CATCH
		SET @IsException=1                                  
		SET @ExceptionMessage=ERROR_MESSAGE()  
		END CATCH
	END
  IF @Opcode=42                      
	BEGIN                              
		BEGIN TRY 		
			select AutoId,((select SliderUrl from APIUrlDetails)+ ImageUrl) as ImageUrl,Link from ManageSlider 
			for json path 		
		END TRY 
		BEGIN CATCH
		SET @IsException=1                                  
		SET @ExceptionMessage=ERROR_MESSAGE()  
		END CATCH
	END
END
GO
