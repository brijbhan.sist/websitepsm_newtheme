USE [PriceSmart]
GO

/****** Object:  Table [dbo].[EmailCategory]    Script Date: 10/09/2020 23:09:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmailCategory](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
	[BCCEmail] [varchar](200) NULL,
	[CCCEmail] [varchar](200) NULL,
	[TOEmail] [varchar](200) NULL,
) ON [PRIMARY]
GO

insert into [dbo].[EmailCategory]([Type],[BCCEmail],[CCCEmail])
values('Singup','naim.uddeen.786@gmail.com','uneel913@gmail.com')
insert into [dbo].[EmailCategory]([Type],[BCCEmail],[CCCEmail])
values('ApprovedStore','brijbhan.sist@gmail.com','uneel913@gmail.com')


insert into [dbo].[EmailCategory]([Type],[BCCEmail],[CCCEmail])
values('ComposeEmail','brijbhan.sist@gmail.com','uneel913@gmail.com')

