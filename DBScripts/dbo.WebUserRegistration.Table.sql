USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebUserRegistration]    Script Date: 01/13/2020 21:27:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebUserRegistration](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Mobile] [varchar](10) NULL,
	[Email] [varchar](50) NULL,
	[Password] [varbinary](max) NULL,
	[Status] [bit] NULL,
	[RegistrationDate] [datetime] NULL,
 CONSTRAINT [PK__WebUserR__6B232905A715AF99] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[WebUserRegistration] ON 

INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (14, N'7 Eleven', N'Store', N'7985641238', N'seven11@gmail.com', 0x0200000070D8B1C20D6A0A4B4EEC439E8B67642ED651AF3412D5864791CA74FBDC886937, 1, CAST(N'2020-01-07T22:12:02.833' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (15, N'Exon Gas', N'Station', N'7894561288', N'exon@gmail.com', 0x020000006686F2CFC49888BE45D9662D2E6B6C7C49B3CDD4341A675BE80ACC7231515934, 1, CAST(N'2020-01-09T04:18:02.273' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (16, N'Rizwan', N' Ahmad', N'8726462217', N'rizwan@mageinfotech.com', 0x0200000066DF647E15DA318EC2ECF3F3B162B7574909D219B8DFF09B82B997D60E7ECAB2, 1, CAST(N'2020-01-09T06:46:50.633' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (17, N'xyz', N'sadsd', N'7896453128', N'xyz@gmail.com', 0x02000000BB3894EC26130E1C3CE1F18C58D717F5A656ABB907DF0DD3CADC8DA7DC4DF90C, 1, CAST(N'2020-01-09T06:51:30.900' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (18, N'Test', N'User3', N'7894561265', N'testuser2@gmail.com', 0x0200000053328B53C24F2F62C7CECD21A8DD5B5F2F0123BB9429FFF5B443CB9762D19946, 1, CAST(N'2020-01-09T21:39:36.163' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (19, N'First ', N'User', N'8975412489', N'firstuser@gmail.com', 0x020000000CA8436DB5239700557B86FDC197A2BDF7B63FADA2BD217797F9E6821CA19B5A, 1, CAST(N'2020-01-09T21:42:26.853' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (20, N'Second', N'User', N'7845612897', N'seconduser@gmail.com', 0x02000000D88D309BD80B6AB764AA592BB3A26BD556FE87E26B2D57921CD1DC29C53EED19, 1, CAST(N'2020-01-09T21:55:26.307' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (21, N'Third', N'User', N'8794561234', N'thirduser@gmail.com', 0x02000000AAAEBA06B8C8B246F1F602CF490AD09A24AE51C0F178C41A436FFF1AB4CB01F8, 1, CAST(N'2020-01-09T21:59:49.117' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (22, N'Fourth ', N'User', N'6987565456', N'fourthstore@gmail.com', 0x02000000D5EFB09AC0756C55BC6841F3685830DA9A2E810C3A674380EF9616083085BFDA, 1, CAST(N'2020-01-09T22:32:01.103' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (23, N'Fifth', N'User', N'9548798545', N'fitthuser@gmail.com', 0x02000000C3FDF6FE2665538E03544CE9AC0C8E7585249F7F26894E3764A3BF830696B7D5, 1, CAST(N'2020-01-09T22:34:21.227' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (24, N'Seventh', N'User', N'8795648978', N'seventhuser@gmail.com', 0x020000003182A115FA23FB6A7A9C89A27A8C9457ADF74FD6AEE10141602DFE823B27D61D, 1, CAST(N'2020-01-09T23:54:58.483' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (25, N'Eitth', N'User', N'7894561235', N'eitthuser@gmail.com', 0x020000004CBEDA8DDA871D5D17B7CBBE691FF6CFB732EA9B8143A1AC86C3956CF76087D6, 1, CAST(N'2020-01-09T23:57:19.063' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (26, N'Nineth', N'User', N'8794512365', N'ninthuser@gmail.com', 0x0200000099E81B3642C27165F70B16119980AAD01EBC4EF60E61194CCC2FFD39E8C3ADF7, 1, CAST(N'2020-01-10T00:00:18.030' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (27, N'Tenth', N'User', N'7894562318', N'tenthuser@gmail.com', 0x02000000D897A10AF35758B9D9C57665F193337FD1BC123034CA64A4103FF895B1975194, 1, CAST(N'2020-01-10T00:04:19.453' AS DateTime))
INSERT [dbo].[WebUserRegistration] ([AutoId], [FirstName], [LastName], [Mobile], [Email], [Password], [Status], [RegistrationDate]) VALUES (28, N'Eleventh', N'User', N'8794561235', N'eleventhuser@gmail.com', 0x020000004661245A7826A8039D1247A44867408C4B87064B3FD575D4D8B111ECAF64D7C8, 1, CAST(N'2020-01-10T00:57:53.047' AS DateTime))
SET IDENTITY_INSERT [dbo].[WebUserRegistration] OFF
ALTER TABLE [dbo].[WebUserRegistration] ADD  CONSTRAINT [DF__WebUserRe__Statu__5B196B42]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[WebUserRegistration] ADD  CONSTRAINT [DF_WebUserRegistration_RegistrationDate]  DEFAULT (getdate()) FOR [RegistrationDate]
GO
