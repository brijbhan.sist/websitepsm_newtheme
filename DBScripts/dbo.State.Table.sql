USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[State]    Script Date: 01/03/2020 23:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[StateCode] [varchar](2) NULL,
	[StateName] [varchar](200) NULL,
	[Status] [int] NULL,
	[CountryId] [int] NULL,
 CONSTRAINT [PK__State__6B2329053EF15830] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[State] ADD  DEFAULT ((0)) FOR [CountryId]
GO
