ALTER Procedure [dbo].[ProcWeb_Registration]                                
  @Opcode INT=NULL,                                
  @Autoid int =null,                               
  @FirstName varchar(50)=null,                              
  @LastName varchar(50)=null,                               
  @CompanyName varchar(50)=null,                                
  @CustomerName varchar(50)=null,                          
  @OPTLicense varchar(50)=null,                              
  @TaxId varchar(50)=null,                                
  @BillAddress varchar(MAX)=null,                          
  @BillCity varchar(50)=null,                            
  @BillZipCode varchar(50)=null,                           
  @BillState varchar(50)=null,                           
  @ShippingAddress  varchar(50)=null,                          
  @ShipCity  varchar(50)=null,                          
  @ShippingZipCode  varchar(50)=null,                     
  @IsDefault int=null,                    
  @Type int=null,                         
  @ShipState  varchar(50)=null,                          
  @ContactPerson varchar(50)=null,                                
  @Email varchar(80)=null,                             
  @AlternateEmail varchar(50)=null,                          
  @FaxNo varchar(50)=null,                          
  @MainPhone varchar(12)=null,                          
  @WorkPhone varchar(12)=null,                          
  @Phone varchar(12)=null,                               
  @UserId varchar(50)=null,                           
  @Password varchar(MAX)=null,                     
  @StoreId int=null,              
  @Location varchar(50)=null,  
  @StoreOpenTime time(7)=null,  
  @StoreCloseTime time(7)=null,  
  @Document int=null,            
  @DocumentUrl varchar(120)=null,    
  @StoreLocation varchar(10)=null,   
  @BillingLatitude varchar(25)=null,
  @BillingLongitude varchar(25)=null,
  @ShippingLatitude varchar(25)=null,
  @ShippingLongitude varchar(25)=null,
  @OTP INT=null,
  @IsException bit out,                                                                      
  @ExceptionMessage varchar(max) out                                      
AS                                
BEGIN                                
  SET @IsException=0                                
  SET @ExceptionMessage=''            
             Declare @BillAutoId int,@ShipAutoId int                
  IF @Opcode=12                    
   BEGIN                              
  BEGIN TRY                              
   BEGIN TRAN                              
   IF EXISTS(SELECT * FROM [PriceSmart].[dbo].WebUserRegistration WHERE Email=@Email)                              
   BEGIN                              
    SET @IsException=1                                
    SET @ExceptionMessage='EmailId already exists '+'('+@Email+').'+' Try another.'                                
    END                              
    ELSE                              
    BEGIN                                     
         INSERT INTO [PriceSmart].[dbo].WebUserRegistration (FirstName,LastName,Mobile,Email,Password) VALUES (@FirstName,@LastName,@Phone,@Email,            
         EncryptByPassPhrase('WHM',@Password)) 
		 DELETE FROM [PriceSmart].[dbo].WebTempUserRegistration Where Email=@Email
    END                              
   COMMIT TRAN                              
  END TRY                              
  BEGIN CATCH                              
  ROLLBACK TRANSACTION                               
   SET @IsException=1                                
   SET @ExceptionMessage=ERROR_MESSAGE()                                
  END CATCH                              
  END                       
  IF @Opcode=13                    
  BEGIN           
   BEGIN TRY                              
   BEGIN TRAN                                                        
    IF @Type=0                              
    BEGIN                     
     INSERT INTO   [dbo].BillingAddress (CustomerAutoId,Address,State,Zipcode,IsDefault,BCityAutoId)                          
     VALUES (@StoreId,@BillAddress,@BillState,@BillZipCode,@IsDefault,@BillCity)                          
     SET @BillAutoId=SCOPE_IDENTITY()                      
    END                              
ELSE                              
    BEGIN                                     
     INSERT INTO   [dbo].BillingAddress (CustomerAutoId,Address,State,Zipcode,IsDefault,BCityAutoId)                          
     values (@StoreId,@ShippingAddress,@ShipState,@ShippingZipCode,@IsDefault,@ShipCity)                          
     SET @ShipAutoId=SCOPE_IDENTITY()                      
    END                              
   COMMIT TRAN                              
   END TRY                              
  BEGIN CATCH                              
   ROLLBACK TRANSACTION                               
    SET @IsException=1                                
    SET @ExceptionMessage=ERROR_MESSAGE()                                
  END CATCH                              
  END                      
  IF @Opcode=14 
  BEGIN                             
       IF @Type=0 --Updated on 10/24/2019 05:51 AM                    
    BEGIN                    
        UPDATE WebCartMaster SET BillAddrId=@Autoid Where StorId=@StoreId                     
    END                    
    ELSE                    
    BEGIN                    
        UPDATE WebCartMaster SET ShippAddrId=@Autoid Where StorId=@StoreId                    
    END                    
  END                    
                           
  IF @Opcode=40                    
 BEGIN                                  
	IF EXISTS(SELECT * from [PriceSmart].[dbo].WebUserRegistration where Email=@UserId and CONVERT(varchar(100),DecryptByPassphrase('WHM',Password))=@Password and Status=1)                                                        
	BEGIN         
			Declare @UserAutoId int        
			SET @UserAutoId=(SELECT AutoId from [PriceSmart].[dbo].WebUserRegistration where Email=@UserId and CONVERT(varchar(100),DecryptByPassphrase('WHM',Password))=@Password)        
        
			IF EXISTS(SELECT * FROM [PriceSmart].[dbo].WebUserStoreRelMaster Where  UserId= @UserAutoId)        
			BEGIN                  
				Select StoreId,DBO.UDFStoreList(StoreLocation,StoreId) AS StoreName,StoreLocation,StoreStatus,LocationStatus,WUR.Email,WUR.AutoId AS UserId,            
				ISNULL(FirstName,'')+''+ISNULL(LastName,'') AS UserName            
				from [PriceSmart].[dbo].WebUserStoreRelMaster AS WUSRM INNER JOIN [PriceSmart].[dbo].WebUserRegistration as WUR ON WUSRM.UserId=WUR.AutoId where WUR.Email=@UserId and             
				CONVERT(varchar(100),DecryptByPassphrase('WHM',WUR.Password))=@Password for json path,include_null_values         
			END        
			ELSE        
			BEGIN        
				Select 0 as StoreId,null as StoreName,null as StoreLocation,0 as StoreStatus,0 as LocationStatus,Email,AutoId AS UserId,        
				ISNULL(FirstName,'')+' '+ISNULL(LastName,'') AS UserName  from [PriceSmart].[dbo].WebUserRegistration as WUR where WUR.Email=@UserId and             
				CONVERT(varchar(100),DecryptByPassphrase('WHM',WUR.Password))=@Password for json path,include_null_values            
			END              
	END              
	ELSE                                
	BEGIN                                
		SET @IsException=1                                                       
		SET @ExceptionMessage='Username Or Password Incorrect.'                                 
	END                        
 END             
  IF @Opcode=41                    
   BEGIN                
        Select Zipcode from   [dbo].ZipMaster Where Status=1                                
   END                                
  IF @Opcode=42                    
   BEGIN                                
		SELECT c.Country,s.StateName,cm.CityName from   [dbo].Country c                                
		Inner join   [dbo].State as s on                                
		c.AutoId=s.CountryId                                
		Inner join   [dbo].CityMaster cm on                 
		s.AutoId=cm.StateId                                
		Inner join   [dbo].ZipMaster on                                
		ZipMaster.CityId=cm.AutoId                                
		Where ZipMaster.Zipcode=@BillZipCode                                 
   END                              
                             
  IF @Opcode=44  --For Billing                            
 BEGIN                           
		Select cmm.DefaultBillAdd,cmm.DefaultShipAdd ,(                    
		SELECT BA.AutoId,Address,S.StateName,Zipcode,IsDefault,CM.CityName as City FROM  [dbo].BillingAddress as BA                            
		INNER JOIN  [dbo].CityMaster AS CM ON                   
		CM.AutoId=BA.BCityAutoId                            
		INNER JOIN  [dbo].State AS S ON                            
		S.AutoId=CM.StateId                          
		WHERE CustomerAutoId=@StoreId                     
		for json path,include_null_values   ) as BillingList,(                    
		SELECT SA.AutoId,Address,S.StateName,Zipcode,IsDefault,CM.CityName as City FROM  [dbo].ShippingAddress as SA                            
		INNER JOIN  [dbo].CityMaster AS CM ON                   
		CM.AutoId=SA.SCityAutoId                            
		INNER JOIN  [dbo].State AS S ON                            
		S.AutoId=CM.StateId                          
		WHERE CustomerAutoId=@StoreId                     
		for json path,include_null_values   ) as ShippingList                    
		from  [dbo].CustomerMaster as cmm Where cmm.AutoId=@StoreId  for json path,include_null_values                            
   END                             
  IF @Opcode=45 --For Shipping   Currently Not Using                          
  BEGIN                       
		SELECT SA.AutoId,Address,State,Zipcode,IsDefault,S.StateName,CM.CityName as City FROM   [dbo].ShippingAddress as SA                            
		INNER JOIN   [dbo].CityMaster AS CM ON                            
		CM.AutoId=SA.SCityAutoId                            
		INNER JOIN   [dbo].State AS S ON                            
		S.AutoId=CM.StateId                            
		WHERE CustomerAutoId= @StoreId                            
   END   
  IF @Opcode=48 --Address List                    
   BEGIN                    
		SELECT BA.AutoId,CustomerAutoId,Address,S.StateName,Zipcode,IsDefault,BCityAutoId,City FROM   [dbo].BillingAddress AS BA                    
		INNER JOIN   [dbo].State S on               
		BA.State=S.AutoId                    
		Where CustomerAutoId=@StoreId for Json path                    
   END                              
  IF @Opcode=50                              
   BEGIN                     
		Select @UserId=Email,@Password=CONVERT(varchar(100),DecryptByPassphrase('WHM',Password)) from [PriceSmart].[dbo].WebUserRegistration  where AutoId=@Autoid   
  
		Select StoreId,DBO.UDFStoreList(StoreLocation,StoreId) AS StoreName,StoreLocation,StoreStatus,LocationStatus            
		from [PriceSmart].[dbo].WebUserStoreRelMaster AS WUSRM INNER JOIN [PriceSmart].[dbo].WebUserRegistration as WUR ON WUSRM.UserId=WUR.AutoId where WUR.Email=@UserId and             
		CONVERT(varchar(100),DecryptByPassphrase('WHM',WUR.Password))=@Password for json path,include_null_values             
		END            
		
 IF @opcode=55 --Time Window 
 BEGIN          
		select convert(varchar(10),left(TimeFrom,5)) as TimeFrom,Convert(varchar(10),left(TimeTo,5)) as TimeTo,  
		CONVERT(varchar(15),CAST(TimeFrom AS TIME),100) as FromTime,CONVERT(varchar(15),CAST(TimeTo AS TIME),100)   
		as ToTime from [PriceSmart].[dbo].TimeWindow  for json path,include_null_values    
 END 
 IF @Opcode=56
 BEGIN
        SELECT FirstName+' '+isnull(LastName,'') AS Name,Mobile,Email FROM [PriceSmart].[dbo].WebUserRegistration Where AutoId=@Autoid
		for json path,include_null_values  
 END
 IF @Opcode=57
 BEGIN
     BEGIN TRY
	     IF EXISTS(SELECT * FROM [PriceSmart].[dbo].WebUserRegistration WHERE Email=@Email)
		 BEGIN
		    SET @IsException=0
		    SET @ExceptionMessage='EmailIdExist' 
		 END
		 ELSE
		 BEGIN
           SET @OTP=(SELECT LEFT(CAST(RAND()*1000000000+999999 AS INT),6))
		   INSERT INTO [PriceSmart].[dbo].WebTempUserRegistration (FirstName,LastName,Mobile,Email,Password,Status,RegistrationDate,OTP)
		   VALUES (@FirstName,@LastName,@Phone,@Email,EncryptByPassPhrase('WHM',@Password),0,getdate(),@OTP) 

		   DECLARE @FromEmailId varchar(100)=null,@FromName varchar(100)='Pricesmart',@smtp_userName varchar(100)=null,@PSW varchar(20)=null,@SMTPServer varchar(20)=null,
		   @Port INT=null,@SSL varchar(20)=null,@ToEmailId varchar(100)=null,@CCEmailId varchar(100)=null,@BCCEmailId varchar(100)=null,@Subject varchar(100)='Signup Verification-Pricesmart'
		   ,@EmailBody varchar(500)=null,@SourceApp varchar(100)=null,@SubUrl varchar(100)='mypsm.net'

			SET @EmailBody='Hi '+ @FirstName+' '+@LastName+','+'
			<br/><br/>Your OTP is:'+' '+convert(varchar(10), @OTP)+'	
			<br/></br>This is system generated email.Please do not reply
			<br>Thanks and Regards,
			<br>PriceSmart'
		   select @BCCEmailId=BCCEmail,@CCEmailId=CCCEmail from PriceSmart.dbo.EmailCategory where Type='Singup'
		   SELECT @FromEmailId=EmailId,@Port=port,@SMTPServer=server,@Password=convert(varchar(100),DecryptBypassphrase('WHM',Pass)),@SSL=ssl,@Autoid=CreatedBy FROM [PriceSmart].[dbo].EmailServerMaster
		  
		   EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] @Opcode=11,@FromEmailId=@FromEmailId,@FromName=@FromName,@smtp_userName=@FromEmailId,@Password=@Password,@SMTPServer=@SMTPServer,
		   @Port=@Port,@SSL=@SSL,@ToEmailId=@Email,@CCEmailId=@CCEmailId,@BCCEmailId=@BCCEmailId,@Subject=@Subject,@EmailBody=@EmailBody,@SourceApp=@SourceApp,@SubUrl=@SubUrl,@IsException=0,@ExceptionMessage=''
		 END
	 END TRY
	 BEGIN CATCH
	      SET @IsException=1
		  SET @ExceptionMessage=ERROR_MESSAGE()
	 END CATCH
 END
 IF @Opcode=58
 BEGIN
     BEGIN TRY
	       IF EXISTS(SELECT * FROM [PriceSmart].[dbo].WebTempUserRegistration WHERE OTP=@OTP AND Email=@Email)
		   BEGIN
		        SET @IsException=0
		        SET @ExceptionMessage='Valid'
		   END
		   ELSE
		   BEGIN
		        SET @IsException=1
		        SET @ExceptionMessage='Invalid security code'
		   END
	 END TRY
	 BEGIN CATCH
	      SET @IsException=1
		  SET @ExceptionMessage=ERROR_MESSAGE()
	 END CATCH
 END
END   