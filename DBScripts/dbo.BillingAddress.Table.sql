USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[BillingAddress]    Script Date: 01/03/2020 23:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillingAddress](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAutoId] [int] NULL,
	[Address] [varchar](200) NULL,
	[State] [int] NULL,
	[Zipcode] [varchar](20) NULL,
	[IsDefault] [int] NULL,
	[BCityAutoId] [int] NULL,
	[City]  AS ([DBO].[FN_CityName]([BCityAutoId])),
	[ZipcodeAutoid] [int] NULL,
 CONSTRAINT [PK__BillingA__6B2329056FC0B109] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
