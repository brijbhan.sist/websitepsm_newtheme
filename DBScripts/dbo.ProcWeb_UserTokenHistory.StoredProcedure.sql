USE [PriceSmart]
GO
/****** Object:  StoredProcedure [dbo].[ProcWeb_UserTokenHistory]    Script Date: 3/7/2020 10:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure [dbo].[ProcWeb_UserTokenHistory]
@Opcode INT=NULL,     
@Token varchar(max),
@UserID varchar(50),                                         
@IsException bit out,                                                          
@ExceptionMessage varchar(max) out     

AS
BEGIN
    SET @IsException=0
	SET @ExceptionMessage=''
     IF @Opcode=11
	 BEGIN
          INSERT INTO [PriceSmart].[dbo].TokenHistory (Token,UserID,Status,GenreateDate,LastUsed)  VALUES ( @Token , @UserID, 'A', GETDATE(),GETDATE() )
	 END
	 IF @Opcode=21
	 BEGIN
	      UPDATE [PriceSmart].[dbo].TokenHistory SET Status='I',LastUsed=GetDate() WHERE USERID=@USERID and Token=@Token  
	 END
	 IF @Opcode=31
	 BEGIN
		IF EXISTS(SELECT * FROM   [PriceSmart].[dbo].TokenHistory WHERE USERID=@USERID and Token=@Token and Status='A')  
		BEGIN  
			declare @lastdate datetime=null,@currentdate datetime=null  
			SELECT @lastdate=CONVERT(VARCHAR(10), LastUsed, 111),@currentdate=CONVERT(VARCHAR(10), GETDATE(), 111) from [PriceSmart].[dbo].TokenHistory   
			WHERE  USERID=@USERID and Token=@Token
				IF(@lastdate=@currentdate) -- Comparing date equality  
				BEGIN  
					Declare @MinuteDiff varchar(2),@SetTime varchar(2)='600' --calculating minute difference  
					SELECT @MinuteDiff=DATEDIFF(MINUTE,LastUsed , GETDATE() )  
					FROM [PriceSmart].[dbo].TokenHistory WHERE  USERID=@USERID and Token=@Token 
						if(@MinuteDiff>=@SetTime)  
						BEGIN  
							Update [PriceSmart].[dbo].TokenHistory Set Status='I' WHERE  USERID=@USERID and Token=@Token  
							Select 'InActive' as Status  
						END  
						ELSE  
						BEGIN  
							Update [PriceSmart].[dbo].TokenHistory Set LastUsed=GetDate() WHERE  USERID=@USERID and Token=@Token  
							Select case when Status='I' then 'InActive' when Status='A' then 'Active' end as Status from [PriceSmart].[dbo].TokenHistory WHERE  USERID=@USERID and Token=@Token  
						END  
				END 
				ELSE
				BEGIN 
				    Update [PriceSmart].[dbo].TokenHistory Set Status='I' WHERE  USERID=@USERID and Token=@Token  
				     Select 'InActive' as Status  
				END 
		END
		ELSE
		BEGIN
		     Select 'InActive' as Status
		END  
	END
END


GO
