USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcWeb_ProductList]    Script Date: 12/31/2020 3:15:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ProcWeb_ProductList]      
@AutoId INT=NULL,                            
 @Opcode INT=NULL,                               
 @timeStamp datetime =null,                              
 @CategoryID int=null,                         
 @SubCategoryID int=null,                           
 @ProductId  int=null,              
 @Quantity int=null,
 @OrderQty int =null,
 @Operation int=null,              
 @CityId int=null,        
 @UserID int=null,
 @UserName varchar(50)=null,
 @StoreId int=null,       
 @Type int=null,                                   
 @IsException bit out,                                    
 @PageIndex INT =1,                                  
 @PageSize INT=28,                                  
 @RecordCount INT=null,  
 @Location varchar(10)=null,
 @ExceptionMessage varchar(max) out                                           
AS                              
BEGIN                              
   SET @IsException=0                                                  
   SET @ExceptionMessage='' 
  
   BEGIN TRY                              
   IF @Opcode=41
   BEGIN                          
    Select CM.CategoryName,sm.SubcategoryName from [dbo].CategoryMaster as cm                            
    inner join [dbo].SubCategoryMaster as sm on                            
    sm.CategoryAutoId=cm.AutoId  Where sm.AutoId=@SubCategoryID AND cm.IsShow=1 AND sm.IsShow=1                           
   
    declare @PriceLevelAutoid int 
	SET @PriceLevelAutoid=(select PriceLevelAutoid from customerpricelevel where CustomerAutoId=@StoreId)
                           
    SELECT ROW_NUMBER() OVER(ORDER BY ProductName) AS RowNumber, *, 
	(case when temp is null then price  when temp>=MinPrice then temp else Price end)  as MainPrice into #Results FROM                              
    (                             
    Select PM.AutoId,PM.ProductId, PM.ProductName ,((Select Url from APIUrlDetails)+ (case when PM.ThirdImage='' 
	then PM.ImageUrl else PM.ThirdImage end) )                             
    as ImageUrl,PD.Qty as Quantity,UM.UnitType,PD.Price as Price,PD.minprice as MinPrice,--
	(
	select top 1 CustomPrice from ProductPricingInPriceLevel as pl where pl.pricelevelAutoid=@PriceLevelAutoid
	and pl.ProductAutoid=pm.Autoid  and pl.unitAutoid=pd.unitType order by autoid desc
	) as temp,Description  as Description,isnull(PM.IsOutOfStock,0) as AvailableInStock,                            
    PM.CategoryAutoId, PM.SubcategoryAutoId from [dbo].PackingDetails as PD                              
    inner join [dbo].UnitMaster as UM on                               
    PD.UnitType=UM.AutoId                               
    inner join [dbo].ProductMaster as PM on                              
    PD.ProductAutoId=PM.AutoId and PD.UnitType=PM.PackingAutoId                               
    Where PM.SubcategoryAutoId=@SubCategoryID and PM.ProductStatus=1 and pm.ShowOnWebsite=1
	and pm.ImageUrl is not null
    ) AS t  ORDER BY ProductName                            
                            
    SELECT COUNT(AutoId) AS RecordCount, case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results                            
    SELECT *  FROM #Results                              
    WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                             
   END   
   ELSE IF @Opcode=42                          
   BEGIN    
        Declare @Url varchar(50)
		SET @Url=(Select Url from APIUrlDetails)
		set @PriceLevelAutoid =(select PriceLevelAutoid from customerpricelevel where CustomerAutoId=@StoreId)
		select * ,(case when temp is null then Price  when temp>=MinPrice then temp else Price end) as MainPrice from
		(
		Select PM.AutoId, PM.ProductId, PM.ProductName,
		(@Url+ (case when PM.ThirdImage is null then PM.ImageUrl else PM.ThirdImage end) ) as Thumbnaill_400,@Url+ ImageUrl as ImageUrl,@Url+ ThumbnailImageUrl as Thumbnail_100,
		PD.Qty as Quantity,UM.UnitType,PD.Price as Price,PD.minprice as MinPrice,
		(
		select top 1 CustomPrice from ProductPricingInPriceLevel as pl where pl.pricelevelAutoid=@PriceLevelAutoid
		and pl.ProductAutoid=pm.Autoid  and pl.unitAutoid=pd.unitType order by autoid desc
		) as temp,
		PM.Description  as Description,                              
		PM.CategoryAutoId, PM.SubcategoryAutoId,(select BrandName from [dbo].BrandMaster where AutoId=PM.BrandAutoId) as BrandName,                          
		isnull(PM.IsOutOfStock,0) as AvailableInStock,               
		(Select CategoryName from [dbo].CategoryMaster where AutoId=PM.CategoryAutoId) as CategoryName,                
		(Select SubcategoryName from [dbo].SubCategoryMaster where AutoId=PM.SubcategoryAutoId) as SubcategoryName,
		(
		 Select  @Url+'/Attachments/'+ImageUrl as ImageUrl from Tbl_ProductImage as p where p.ProductAutoId=pm.AutoId for json path,include_null_values
		 --Select top 3 @Url+'Attachments/'+ImageUrl as ImageUrl,@Url+'productThumbnailImage/'+Thumbnail_100 as Thumbnail_100,@Url+'productThumbnailImage/'+Thumbnaill_400 as Thumbnaill_400 from Tbl_ProductImage as p where p.ProductAutoId=pm.AutoId for json path,include_null_values
		) as ImageList
		from [dbo].ProductMaster as PM                           
		inner join [dbo].PackingDetails as PD on                                  
		PD.ProductAutoId=PM.AutoId and PD.UnitType=PM.PackingAutoId                           
		inner join [dbo].UnitMaster as UM on                    
		PD.UnitType=UM.AutoId  
		Where PM.ProductId=@ProductId and PM.ProductStatus=1 
		) as t for json path,include_null_values

   END    
   ELSE IF @Opcode=43  --Category List                      
   BEGIN                        
		Select distinct  cm.SeqNo,cm.AutoId,cm.CategoryName,
		(
		select scm.AutoId as subCatAutoId,scm.SubcategoryName from SubCategoryMaster as scm where cm.autoid=scm.categoryAutoId 
		and SubCategory_ShowonWebsite=1 order by scm.SeqNo,scm.SubcategoryName
		for json path
		) as SubcategoryName
		 from   CategoryMaster as cm  
		Inner join SubCategoryMaster as scm on cm.autoid=scm.categoryAutoId
		Where
		cm.Status=1 and scm.Status=1 and cm.isShow=1 and scm.isShow=1 AND (cm.AutoId NOT IN (103)) 	
		and Category_ShowonWebsite=1
	
		order by cm.SeqNo,cm.CategoryName asc             
		for json path
   END               
   IF @Opcode=44  --Calculate Price                
   BEGIN     
   
		set @PriceLevelAutoid =(select PriceLevelAutoid from customerpricelevel where CustomerAutoId=@StoreId)
		Declare @DefaultQty int,@DefaultPrice decimal(18,2),@FindActualQty int,@FindActualPrice decimal(18,2),@UnitType int,@OutQty int                            
		Select @UnitType=PackingAutoId from [dbo].ProductMaster Where AutoId=@ProductId              
		Select @DefaultQty=Qty from [dbo].PackingDetails Where ProductAutoId=@ProductId and UnitType=@UnitType  
		
		Select @DefaultPrice=(select (case when temp is null then Price  when temp>=MinPrice then temp else Price end) as MainPrice from
		(
		Select PD.Price as Price,PD.minprice as MinPrice,
		(
		select top 1 CustomPrice from ProductPricingInPriceLevel as pl where pl.pricelevelAutoid=@PriceLevelAutoid
		and pl.ProductAutoid=pm.Autoid  and pl.unitAutoid=pd.unitType order by autoid desc
		) as temp
		from [dbo].ProductMaster as PM                           
		inner join [dbo].PackingDetails as PD on                                  
		PD.ProductAutoId=PM.AutoId and PD.UnitType=PM.PackingAutoId                           
		inner join [dbo].UnitMaster as UM on                    
		PD.UnitType=UM.AutoId  
		Where Pd.ProductAutoId=@ProductId and PM.ProductStatus=1 

		) as t)   

    IF @Operation=0  --Plus              
	BEGIN              
		BEGIN TRY              
			SET @OutQty=(@Quantity+@DefaultQty)              
			SET @FindActualQty=(@OutQty/@DefaultQty)              
			SET @FindActualPrice=(@FindActualQty*@DefaultPrice)              
			SELECT @FindActualPrice as Price,@OutQty as Quantity              
		END TRY              
		BEGIN CATCH              
			SET @IsException=1                              
			SET @ExceptionMessage=ERROR_MESSAGE()                        
		END CATCH              
	END              
    ELSE IF @Operation=1 --Minus              
    BEGIN              
	BEGIN TRY              
		SET @OutQty=(@Quantity-@DefaultQty)              
		IF(@OutQty<=@DefaultQty)              
		BEGIN              
			SET @FindActualPrice=@DefaultPrice              
			SET @OutQty=@DefaultQty              
		END              
		ELSE BEGIN              
			SET @FindActualQty=(@OutQty%@DefaultQty)              
			IF(@FindActualQty!=0)              
			BEGIN              
				SET @OutQty=@DefaultQty              
				SET @FindActualPrice=@DefaultPrice              
			END              
			ELSE              
			BEGIN              
				SET @FindActualPrice=((@OutQty*@DefaultPrice)/@DefaultQty)              
			END              
		END              
		SELECT Cast(@FindActualPrice as decimal(10,2)) as Price,@OutQty as Quantity              
		END TRY              
		BEGIN CATCH              
			SET @IsException=1                              
			SET @ExceptionMessage=ERROR_MESSAGE()                        
	END CATCH              
	END              
	ELSE IF @Operation=2 -- On Quantity text change              
	BEGIN              
		BEGIN TRY              
			SET @FindActualQty=@Quantity%@DefaultQty   
			IF @Quantity=0   
			BEGIN  
				SET @OutQty=@DefaultQty              
				SET @FindActualPrice=@DefaultPrice    
			END  
			ELSE IF(@FindActualQty!=0)              
			BEGIN              
				SET @OutQty=@DefaultQty              
				SET @FindActualPrice=@DefaultPrice              
			END              
			ELSE BEGIN      
				SET @OutQty=@Quantity                 
				SET @FindActualPrice=ISNULL((@Quantity/@DefaultQty),0)*@DefaultPrice              
			END              
			SELECT @FindActualPrice as Price,@OutQty as Quantity       --Updated on 10/17/2019 22:31          
		END TRY              
		BEGIN CATCH              
			SET @IsException=1                              
			SET @ExceptionMessage=ERROR_MESSAGE()                        
		END CATCH              
  END       
 ELSE IF @Operation=3 -- On Quantity text change on cart master page             
 BEGIN              
	BEGIN TRY            
		SET @FindActualQty=@Quantity%@DefaultQty   
		IF @Quantity=0   
		BEGIN  
			SET @OutQty=@DefaultQty              
			SET @FindActualPrice=@DefaultPrice    
		END  
		ELSE IF(@FindActualQty!=0)              
		BEGIN              
			SET @OutQty=@DefaultQty              
			SET @FindActualPrice=@DefaultPrice              
		END              
		ELSE BEGIN      
			SET @OutQty=@Quantity                 
			SET @FindActualPrice=ISNULL((@Quantity/@DefaultQty),0)*@DefaultPrice 
			Declare @Atid int    
			SELECT @Atid=AutoId FROM WEBCartMaster Where UserId=@UserId and StorId=@StoreId   
			--UPDATE WebCartItemMaster SET TotalPieces=@OutQty,OrderQty=ISNULL((@Quantity/PerUnitQty),0),NetPrice=@FindActualPrice   
			--Where CartAutoId=@Atid and ProductId=@ProductId         
		END            
		SELECT @FindActualPrice as Price,@OutQty as Quantity            
	END TRY            
	BEGIN CATCH            
		SET @IsException=1                            
		SET @ExceptionMessage=ERROR_MESSAGE()                      
	END CATCH              
 END              
 END                
   IF @Opcode=45            
   BEGIN            
        Select AutoId,CityId,Zipcode from [dbo].ZipMaster Where Status=1            
   END            
   IF @Opcode=46            
   BEGIN            
  SELECT cm.CityName,StateName,c.Country from [dbo].State s            
  inner join [dbo].CityMaster as cm on             
  s.AutoId=cm.StateId            
  inner join [dbo].Country as c on            
  c.AutoId=s.CountryId            
  WHERE cm.AutoId=@CityId            
   END        
   IF @Opcode=47      --Cart list         
   BEGIN 
		Declare @TotalPrice decimal(18,2),@Aid int          
          
		SELECT @Aid=AutoId FROM [dbo].WEBCartMaster Where UserId=@AutoId and StorId=@StoreId          
          
		Select @TotalPrice=SUM(NetPrice) from [dbo].WebCartItemMaster Where CartAutoId=@Aid           
  
		Select CIM.AutoId,pm.autoid as ProductAutoId,PM.ProductId,PM.ProductName,CIM.OrderQty as Qty,UnitPrice,NetPrice,@TotalPrice as  TotalPrice,
		(Select Url from APIUrlDetails)+PM.ThumbnailImageUrl as ImageUrl, PM.AutoId as ProductId,          
		(Select Url from APIUrlDetails)+PM.ThumbnailImageUrl as ImageUrl, PM.AutoId as ProductId,UM.UnitType,          
		case when UnitAutoId=3 then Convert(varchar,(OrderQty/QtyPerUnit))+' '+(SELECT UnitType FROM UnitMaster Where AutoId=UnitAutoId)
	else
	Convert(varchar,OrderQty)+' pieces' + '('+Convert(varchar,(OrderQty/QtyPerUnit))+' '+(SELECT UnitType FROM UnitMaster Where AutoId=UnitAutoId)+')' end as DefaultQty
		from 
		[dbo].WebCartItemMaster AS CIM            
		INNER JOIN  ProductMaster AS PM on CIM.ProductId=PM.AutoId          
		INNER JOIN  PackingDetails AS PD on PM.AutoId=PD.ProductAutoId and PM.PackingAutoId=PD.UnitType 
		INNER JOIN  [dbo].UNitMaster as UM on  UM.AutoId=CIM.UnitAutoId  
		Where CIM.CartAutoID=@Aid 
 END 
 
   IF @Opcode=48            
   BEGIN  
       Declare @CartId INT
	   SELECT @CartId=CartAutoId from WebCartItemMaster Where AutoId=@AutoId
	   IF((SELECT COUNT(AUTOID) FROM WebCartItemMaster WHERE CartAutoId=@CartId)=1)
	   BEGIN 
	       DELETE FROM WebCartMaster Where AutoId=@CartId
	       DELETE from WebCartItemMaster Where AutoId=@AutoId
	   END
	   ELSE
	   BEGIN
           DELETE from WebCartItemMaster Where AutoId=@AutoId       
	   END
   END
   IF @Opcode=49            
   BEGIN            
	Declare @Carid int,@record int,@ShippingCharge decimal(18,2),@shipId int
	select @Carid=AutoId from [dbo].[WebCartMaster] where  UserId=@UserId and StorId=@StoreId
	select @record=count(*) from  [dbo].[WebCartItemMaster] where CartAutoId=@Carid

	Select @ShippingCharge=case when cm.PaybleAmount>st.MinimumOrderAmount then ISNULL(st.ShippingCharge,0) else ISNULL(st.ShippingCharge+st.ExtraShippingCharge,0) end,
	@shipId=ShippingAutoId from [dbo].WebCartMaster as cm 
   inner join [PriceSmart].[dbo].WebCustomerShippingType as st on cm.ShippingId=st.ShippingAutoId 
   and cm.StorId=st.CustomerAutoId Where StorId=@StoreId--Updated on 10/15/2019 01:26 AM          
      
	select  @record as TotalItem,Totalamount as SubTotal,isnull(@ShippingCharge,0) as Shipping,Mltax ,WeightTax,
	(ISNULL(PaybleAmount,0)+ISNULL(@ShippingCharge,0)) as PaybleAmount,ShippingId,PaymentId,
	(        
    SELECT WPM.AutoId,PM.PaymentMode,WPM.PaymentAutoId,IsDefault from [PriceSmart].[dbo].WebCustomerPaymentMode as WPM        
   INNER JOIN  PAYMENTModeMaster as PM on WPM.PaymentAutoId=PM.AutoID        
    Where CustomerAutoId=@StoreId           
    for json path,include_null_values        
   ) as PaymentList,
   (        
     SELECT WS.AutoId,WS.WebsiteDisplayName as ShippingType,WS.ShippingAutoId,
		case when WCM.PaybleAmount>WS.MinimumOrderAmount then ISNULL(WS.ShippingCharge,0.00) else ISNULL(WS.ShippingCharge+WS.ExtraShippingCharge,0.00) end as ShippingCharge
		,IsDefault FROM [PriceSmart].[dbo].WebCustomerShippingType as WS        
		INNER JOIN  ShippingType AS ST on WS.ShippingAutoId=ST.AutoId 
		INNER JOIN WebCartMaster AS WCM on WS.CustomerAutoId=WCM.StorId
		Where CustomerAutoId=@StoreId         
    for json path,include_null_values        
   ) as ShippingList 
   from [dbo].[WebCartMaster] 
	Where UserId=@UserId and StorId=@StoreId 
	for json path,include_null_values        
   END

   IF @Opcode=50        
   BEGIN        
        IF(@Type=1) --Update on 10/15/2019 01:09 AM        
  BEGIN        
       UPDATE [dbo].WebCartMaster SET ShippingId=@AutoId WHere StorId=@StoreId        
  END        
  ELSE        
  BEGIN        
       UPDATE [dbo].WebCartMaster SET PaymentId=@AutoId WHere StorId=@StoreId        
  END   
END
IF @Opcode=51
BEGIN		
        SET @PriceLevelAutoid=(select PriceLevelAutoid from customerpricelevel where CustomerAutoId=@StoreId)
		select top 10 * ,(case when temp is null then Price  when temp>=MinPrice then temp else Price end) as MainPrice from
		(
		Select PM.AutoId, PM.ProductId, PM.ProductName,
		((Select Url from APIUrlDetails)+ (case when PM.ThirdImage=''  then PM.ImageUrl else PM.ThirdImage end) ) as ImageUrl,
		PD.Qty as Quantity,UM.UnitType,PD.Price as Price,PD.minprice as MinPrice,
		(
		select top 1 CustomPrice from ProductPricingInPriceLevel as pl where pl.pricelevelAutoid=@PriceLevelAutoid
		and pl.ProductAutoid=pm.Autoid  and pl.unitAutoid=pd.unitType order by autoid desc
		) as temp,
		Description  as Description,PM.CreateDate,                               
		PM.CategoryAutoId, PM.SubcategoryAutoId,(select BrandName from [dbo].BrandMaster where AutoId=PM.BrandAutoId) as BrandName,                          
		isnull(PM.IsOutOfStock,0) as AvailableInStock,                
		(Select CategoryName from [dbo].CategoryMaster where AutoId=PM.CategoryAutoId) as CategoryName,                
		(Select SubcategoryName from [dbo].SubCategoryMaster where AutoId=PM.SubcategoryAutoId) as SubcategoryName                         
		from [dbo].ProductMaster as PM                           
		inner join [dbo].PackingDetails as PD on                                  
		PD.ProductAutoId=PM.AutoId and PD.UnitType=PM.PackingAutoId                           
		inner join [dbo].UnitMaster as UM on                    
		PD.UnitType=UM.AutoId  
		 where PM.ProductStatus=1 and ShowOnWebsite=1
		) as  t order by t.CreateDate desc 
END

IF @Opcode=52 --for special product
BEGIN
		SET @PriceLevelAutoid=(select PriceLevelAutoid from customerpricelevel where CustomerAutoId=ISNULL(@StoreId,0))
		SET @PriceLevelAutoid=(select PriceLevelAutoid from customerpricelevel where CustomerAutoId=ISNULL(@StoreId,0))

		select ROW_NUMBER() OVER(ORDER BY ProductName) AS RowNumber, *,(case when temp is null then Price  when 
		temp>=MinPrice then temp else Price end) as MainPrice into #Results1 from
		(
		Select PM.AutoId, PM.ProductId, PM.ProductName,
		((Select Url from APIUrlDetails)+ (case when PM.ThirdImage=''  then PM.ImageUrl else PM.ThirdImage end) ) as ImageUrl,
		PD.Qty as Quantity,UM.UnitType,PD.Price as Price,PD.minprice as MinPrice,
		(
		select top 1 CustomPrice from ProductPricingInPriceLevel as pl where pl.pricelevelAutoid=ISNULL(@PriceLevelAutoid,0)
		and pl.ProductAutoid=pm.Autoid  and pl.unitAutoid=pd.unitType order by autoid desc
		) as temp,
		Description  as Description,PM.CreateDate,                               
		PM.CategoryAutoId, PM.SubcategoryAutoId,(select BrandName from [dbo].BrandMaster where AutoId=PM.BrandAutoId) as BrandName,                          
		isnull(PM.IsOutOfStock,0) as AvailableInStock,                
		(Select CategoryName from [dbo].CategoryMaster where AutoId=PM.CategoryAutoId) as CategoryName,                
		(Select SubcategoryName from [dbo].SubCategoryMaster where AutoId=PM.SubcategoryAutoId) as SubcategoryName                         
		from [dbo].ProductMaster as PM                           
		inner join [dbo].PackingDetails as PD on                                  
		PD.ProductAutoId=PM.AutoId and PD.UnitType=PM.PackingAutoId                           
		inner join [dbo].UnitMaster as UM on                    
		PD.UnitType=UM.AutoId  
		where PM.ProductStatus=1 and ShowOnWebsite=1 and pm.AutoId in
		(
		select ProductId from  WebsiteProductCategory as WPC
		INNER JOIN WebsiteProductCategoryType as WPCT ON 
		WPC.Type=WPCT.AutoId
		where WPC.Type=1
		) 
		) as t
		SELECT COUNT(ProductId) AS RecordCount, case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results1                            
		 SELECT *  FROM #Results1                              
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                             
		
END
IF @Opcode=53 --for best seller 
BEGIN
		SET @PriceLevelAutoid=(select PriceLevelAutoid from customerpricelevel where CustomerAutoId=ISNULL(@StoreId,0))
		SET @PriceLevelAutoid=(select PriceLevelAutoid from customerpricelevel where CustomerAutoId=ISNULL(@StoreId,0))

		select ROW_NUMBER() OVER(ORDER BY ProductName) AS RowNumber, *,(case when temp is null then Price  when 
		temp>=MinPrice then temp else Price end) as MainPrice into #Results2 from
		(
		Select PM.AutoId, PM.ProductId, PM.ProductName,
		((Select Url from APIUrlDetails)+ (case when PM.ThirdImage=''  then PM.ImageUrl else PM.ThirdImage end) ) as ImageUrl,
		PD.Qty as Quantity,UM.UnitType,PD.Price as Price,PD.minprice as MinPrice,
		(
		select top 1 CustomPrice from ProductPricingInPriceLevel as pl where pl.pricelevelAutoid=ISNULL(@PriceLevelAutoid,0)
		and pl.ProductAutoid=pm.Autoid  and pl.unitAutoid=pd.unitType order by autoid desc
		) as temp,
		Description  as Description,PM.CreateDate,                               
		PM.CategoryAutoId, PM.SubcategoryAutoId,(select BrandName from [dbo].BrandMaster where AutoId=PM.BrandAutoId) as BrandName,                          
		isnull(PM.IsOutOfStock,0) as AvailableInStock,                
		(Select CategoryName from [dbo].CategoryMaster where AutoId=PM.CategoryAutoId) as CategoryName,                
		(Select SubcategoryName from [dbo].SubCategoryMaster where AutoId=PM.SubcategoryAutoId) as SubcategoryName                         
		from [dbo].ProductMaster as PM                           
		inner join [dbo].PackingDetails as PD on                                  
		PD.ProductAutoId=PM.AutoId and PD.UnitType=PM.PackingAutoId                           
		inner join [dbo].UnitMaster as UM on                    
		PD.UnitType=UM.AutoId  
		where PM.ProductStatus=1 and ShowOnWebsite=1 and pm.AutoId in
		(
		select ProductId from  WebsiteProductCategory as WPC
		INNER JOIN WebsiteProductCategoryType as WPCT ON 
		WPC.Type=WPCT.AutoId 
		where WPC.Type=2
		) 
		) as t
		SELECT COUNT(ProductId) AS RecordCount, case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results2                            
		 SELECT *  FROM #Results2                              
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                             
		
END
   IF @Opcode=11  --Add to Cart        
   BEGIN        
  
			IF @Quantity=0
			BEGIN
					SET @IsException=1                                                  
					SET @ExceptionMessage='Order qty should be greater than zero.'
			END
			ELSE
			BEGIN
			 BEGIN TRY
				Declare @CartAutoId int,@UnitAutoId int,@BillAddrId int, @ShippAddrId int,@UnitPrice decimal(8,2),@Qtyperunit int,
				@WeightPerQty decimal(18,2),@MLPerQty decimal(18,2),@WeighttaxRate decimal(18,2),@DefPaymentId int,@DefShippingId int

				set @PriceLevelAutoid =(select top 1 PriceLevelAutoid from customerpricelevel where CustomerAutoId=@StoreId)

				Select  @UnitAutoId=packingautoid,@WeightPerQty=case when IsApply_Oz=1 then isnull(WeightOz,0) else 0 end,
				@MLPerQty=case when IsApply_ML=1 then isnull(MLQty,0) else 0 end
				from [dbo].productmaster Where AutoId=@ProductId      
	
				set @UnitPrice=
				(	select (case when temp is null then Price  when temp>=MinPrice then temp else Price end) as MainPrice from
					(
						Select PD.Price as Price,PD.minprice as MinPrice,
						(
						select top 1 CustomPrice from ProductPricingInPriceLevel as pl where pl.pricelevelAutoid=@PriceLevelAutoid
						and pl.ProductAutoid=pm.Autoid  and pl.unitAutoid=pd.unitType order by autoid desc
						) as temp
						from [dbo].ProductMaster as PM                          
						inner join [dbo].PackingDetails as PD on                                  
						PD.ProductAutoId=PM.AutoId and PD.UnitType=PM.PackingAutoId                          
						inner join [dbo].UnitMaster as UM on                    
						PD.UnitType=UM.AutoId  
						Where Pd.ProductAutoId=@ProductId and PM.ProductStatus=1                            
					) as t
				)  

				Select @Qtyperunit=Qty from  PackingDetails Where ProductAutoId=@ProductId and UnitType=@UnitAutoId
				Select @ShippAddrId=AutoId from ShippingAddress Where CustomerAutoId=@StoreId and IsDefault=1
				select  @BillAddrId=AutoId from BillingAddress where CustomerAutoId=@StoreId and IsDefault=1
				SELECT @WeighttaxRate=isnull(Value,0.00) FROM [dbo].[Tax_Weigth_OZ]
				Select @DefPaymentId=PaymentAutoId from [PriceSmart].[dbo].WebCustomerPaymentMode Where CustomerAutoId=@StoreId AND IsDefault=1
				Select @DefShippingId=ShippingAutoId from [PriceSmart].[dbo].WebCustomerShippingType Where CustomerAutoId=@StoreId AND IsDefault=1

				IF NOT EXISTS(SELECT * FROM  [dbo].[WebCartMaster] WHERE UserId=@AutoId and StorId=@StoreId)
				BEGIN
					insert into  dbo.WebCartMaster(UserId,StorId,ShippAddrId,BillAddrId,WeighttaxRate,ShippingId,PaymentId)
					values(@AutoId,@StoreId,@ShippAddrId,@BillAddrId,@WeighttaxRate,@DefShippingId,@DefPaymentId)
					set @CartAutoId =(SELECT SCOPE_IDENTITY())
				END            
				ELSE            
				BEGIN   
					SET @CartAutoId=(SELECT AutoId FROM [dbo].[WebCartMaster]  WHERE UserId=@AutoId and StorId=@StoreId)            
				END
				if(@Quantity%@Qtyperunit!=0)
				BEGIN
					SET @Quantity=@Qtyperunit
				END

				IF NOT EXISTS(SELECT * FROM [dbo].[WebCartItemMaster] WHERE CartAutoId=@CartAutoId AND ProductId=@ProductId)            
				BEGIN
					insert into [dbo].[WebCartItemMaster](CartAutoId,ProductId,UnitAutoId,OrderQty,Qtyperunit,UnitPrice,MLPerQty,WeightPerQty)
					values(@CartAutoId,@ProductId,@UnitAutoId,@Quantity,@Qtyperunit,@UnitPrice,@MLPerQty,@WeightPerQty)  
				END
				ELSE            
				BEGIN            
					UPDATE [dbo].[WebCartItemMaster] SET OrderQty=OrderQty+@Quantity
					WHERE CartAutoId=@CartAutoId AND ProductId=@ProductId            
				END 
				END TRY
				BEGIN CATCH
					SET @IsException=1                                                  
					SET @ExceptionMessage='Oops, something went wrong.Please try later'
				END CATCH
			END
END  

   IF @Opcode=21            
   BEGIN            
	declare @Qty int 
	Select @UnitType=PackingAutoId from  [dbo].[ProductMaster] Where AutoId=@ProductId 
	Select @Qty=Qty from  [dbo].[PackingDetails] Where ProductAutoId=@ProductId and UnitType=@UnitType 
	Select @CartAutoId=AutoId from [dbo].[WebCartMaster] where UserId=@UserId and storid=@storeId
	IF @Operation=0  --Plus  
	BEGIN
	update [dbo].[WebCartItemMaster] set OrderQty=(OrderQty+@Qty) where CartAutoId=@CartAutoId and ProductId=@ProductId
	END
	IF @Operation=1  --minus  
	BEGIN
	set @OrderQty=(select OrderQty from [dbo].[WebCartItemMaster] where CartAutoId=@CartAutoId and ProductId=@ProductId )
	if(@OrderQty>@Qty)
	BEGIN
	update [dbo].[WebCartItemMaster] set OrderQty=(OrderQty-@Qty) where CartAutoId=@CartAutoId and ProductId=@ProductId 
	END
	else
	BEGIN
	SET @IsException=1                                                  
    SET @ExceptionMessage='Qty Can not be less than default qty' 
	END
	END  
   END  
   IF @Opcode=22  --Calculate Price                
   BEGIN                            
		Select @UnitType=PackingAutoId from [dbo].ProductMaster Where AutoId=@ProductId              
		Select @DefaultQty=Qty from [dbo].PackingDetails Where ProductAutoId=@ProductId and UnitType=@UnitType  
		
		Select @DefaultPrice=(select (case when temp is null then Price  when temp>=MinPrice then temp else Price end) as MainPrice from
		(
		Select PD.Price as Price,PD.minprice as MinPrice,
		(
		select top 1 CustomPrice from ProductPricingInPriceLevel as pl where pl.pricelevelAutoid=@PriceLevelAutoid
		and pl.ProductAutoid=pm.Autoid  and pl.unitAutoid=pd.unitType order by autoid desc
		) as temp
		from [dbo].ProductMaster as PM                           
		inner join [dbo].PackingDetails as PD on                                  
		PD.ProductAutoId=PM.AutoId and PD.UnitType=PM.PackingAutoId                           
		inner join [dbo].UnitMaster as UM on                    
		PD.UnitType=UM.AutoId  
		Where Pd.ProductAutoId=@ProductId and PM.ProductStatus=1                          
		) as t)   

    IF @Operation=0  --Plus              
	BEGIN              
		BEGIN TRY              
			SET @OutQty=(@Quantity+@DefaultQty)              
			SET @FindActualQty=(@OutQty/@DefaultQty)              
			SET @FindActualPrice=(@FindActualQty*@DefaultPrice)              
			SELECT @FindActualPrice as Price,@OutQty as Quantity,@AutoId
			UPDATE WebCartItemMaster SET OrderQty=ISNULL(@OutQty,0)  Where AutoId=@AutoId  
		END TRY              
		BEGIN CATCH              
			SET @IsException=1                              
			SET @ExceptionMessage=ERROR_MESSAGE()                        
		END CATCH              
	END              
    ELSE IF @Operation=1 --Minus              
    BEGIN              
	BEGIN TRY              
		SET @OutQty=(@Quantity-@DefaultQty)              
		IF(@OutQty<=@DefaultQty)              
		BEGIN              
			SET @FindActualPrice=@DefaultPrice              
			SET @OutQty=@DefaultQty              
		END              
		ELSE BEGIN              
			SET @FindActualQty=(@OutQty%@DefaultQty)              
			IF(@FindActualQty!=0)              
			BEGIN              
				SET @OutQty=@DefaultQty              
				SET @FindActualPrice=@DefaultPrice              
			END              
			ELSE              
			BEGIN              
				SET @FindActualPrice=((@OutQty*@DefaultPrice)/@DefaultQty)              
			END              
		END              
		SELECT Cast(@FindActualPrice as decimal(10,2)) as Price,@OutQty as Quantity,@AutoId   
		UPDATE WebCartItemMaster SET OrderQty=ISNULL(@OutQty,0)  Where AutoId=@AutoId  
		END TRY              
		BEGIN CATCH              
			SET @IsException=1                              
			SET @ExceptionMessage=ERROR_MESSAGE()                        
	END CATCH              
	END              
	ELSE IF @Operation=2 -- On Quantity text change              
	BEGIN              
		BEGIN TRY              
			SET @FindActualQty=@Quantity%@DefaultQty   
			IF @Quantity=0   
			BEGIN  
				SET @OutQty=@DefaultQty              
				SET @FindActualPrice=@DefaultPrice    
			END  
			ELSE IF(@FindActualQty!=0)              
			BEGIN              
				SET @OutQty=@DefaultQty              
				SET @FindActualPrice=@DefaultPrice              
			END              
			ELSE BEGIN      
				SET @OutQty=@Quantity                 
				SET @FindActualPrice=ISNULL((@Quantity/@DefaultQty),0)*@DefaultPrice              
			END              
			SELECT @FindActualPrice as Price,@OutQty as Quantity,@AutoId       --Updated on 10/17/2019 22:31   
			UPDATE WebCartItemMaster SET OrderQty=ISNULL(@OutQty,0)  Where AutoId=@AutoId  
		END TRY              
		BEGIN CATCH              
			SET @IsException=1                              
			SET @ExceptionMessage=ERROR_MESSAGE()                        
		END CATCH              
  END       
	ELSE IF @Operation=3 -- On Quantity text change on cart master page             
	BEGIN              
	BEGIN TRY            
		SET @FindActualQty=@Quantity%@DefaultQty   
		IF @Quantity=0   
		BEGIN  
			SET @OutQty=@DefaultQty              
			SET @FindActualPrice=@DefaultPrice    
		END  
		ELSE IF(@FindActualQty!=0)              
		BEGIN              
			SET @OutQty=@DefaultQty              
			SET @FindActualPrice=@DefaultPrice              
		END              
		ELSE BEGIN      
			SET @OutQty=@Quantity                 
			SET @FindActualPrice=ISNULL((@Quantity/@DefaultQty),0)*@DefaultPrice 
		END            
		SELECT @FindActualPrice as Price,@OutQty as Quantity ,@AutoId   
		UPDATE WebCartItemMaster SET OrderQty=ISNULL(@OutQty,0)  Where AutoId=@AutoId  
	END TRY            
	BEGIN CATCH            
		SET @IsException=1                            
		SET @ExceptionMessage=ERROR_MESSAGE()                      
	END CATCH              
	END 	  
	
  END    
 
 END TRY                              
 BEGIN CATCH                              
		SET @IsException=1                              
		SET @ExceptionMessage='Oops, something went wrong.Please try later'                              
 END CATCH                            
END   
