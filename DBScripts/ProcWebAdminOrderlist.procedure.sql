USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcOrderMaster]    Script Date: 07/13/2020 22:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProcWebAdminOrderMaster]                                                                                                                                                    
 @Opcode INT=NULL,                                                                                                                                                    
 @OrderAutoId INT=NULL,  
 @CustomerAutoId int =NULL,
 @OrderNo VARCHAR(15)=NULL,                                                                                                                                                                                                              
 @OrderStatus INT=NULL,  
 @SalesPersonAutoId INT=NULL,
 @customertype int= NULL,
 @ShippingType INT=NULL,                                                                                                                                                                                  
 @Todate DATETIME=NULL,                                                                                                                                                    
 @Fromdate DATETIME=NULL,                                                                                    
       
 @PageIndex INT = 1,                
 @PageSize INT = 10,                                                                                                                            
 @RecordCount INT =null,                                                                                                                                                          
 @isException bit out,                                           
 @exceptionMessage varchar(max) out                                                                                      
AS                                                                                                                                                    
BEGIN                                                                                                                               
  BEGIN TRY                                                                                                                                            
   Set @isException=0                                                                                                                           
   Set @exceptionMessage='Success'                                                                                                         
   IF @Opcode=41
   BEGIN
  
  SELECT ROW_NUMBER() OVER(ORDER BY [AutoId] desc) AS RowNumber, * INTO #Results30 from                                                                    
    (                                                                       
    SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                                                                    
    SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],                                                                    
    (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems  ,                                    
 st.ShippingType                                                                   
    FROM [dbo].[OrderMaster] As OM                                                         
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                      
    INNER JOIN [dbo].ShippingType AS st ON st.AutoId = OM.ShippingType                                          
    INNER JOIN [dbo].[StatusMaster] AS  SM ON OM.Status=SM.AutoId and sm.Category=(case when OM.Status=0 then  'Website' else 'OrderMaster' end)                                                      
    WHERE                                                         
    OM.OrderType=4                                                                    
    and (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                                    
    and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                                                    
    and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (Convert(date,[OrderDate]) between @FromDate and @Todate))                                                                                                                                           
	and (@OrderStatus is null or @OrderStatus=0 or OM.Status=@OrderStatus)                                         
	and (@ShippingType is null or @ShippingType=0 or OM.ShippingType=@ShippingType)                                                                  
                                 
    )as t order by [AutoId] desc                                                   
                                                                    
    SELECT CASE WHEN ISNULL(@PageSize,0)=0 then 0 else COUNT(*)  end as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results30                                                                    
    SELECT * FROM #Results30                                                                    
    WHERE ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                          

   END
    IF @Opcode=42                                      
   BEGIN                                         
                                        
	SET @OrderAutoId = (SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE AutoId=@OrderAutoId)    
	
	SELECT * FROM (                                                                                                                                                        
	SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                      
	inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                                                                  
	where om.AutoId=@OrderAutoId   and ISNULL(DrvRemarks,'')!=''                                                        
	UNION                                                                                             
	SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om  
	inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                                      
	and ISNULL(AcctRemarks,'')!=''                                                                                                                            
	UNION                                                                                                                                                    
	SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om  
	inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                          
	om.AutoId=@OrderAutoId            and ISNULL(OrderRemarks,'')!=''                                          
	UNION                                                                                                               
	SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                          
	inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                        
	where om.AutoId=@OrderAutoId          and ISNULL(PackerRemarks,'')!=''                                                                                                                                   
	UNION                                                          
	SELECT 'Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                         
	inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                                                          
	where om.AutoId=@OrderAutoId     and ISNULL(ManagerRemarks,'')!=''                                                                                                                       
	UNION                                                                                                                                                    
	SELECT 'Warehouse' as EmpType,WarehouseRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName as EmpName  from OrderMaster as om                                      
	inner join EmployeeMaster as emp on emp.AutoId=om.WarehouseAutoId                                                                                                                                                     
	where om.AutoId=@OrderAutoId             and ISNULL(WarehouseRemarks,'')!=''                                                                                                                                      
	) AS T                          
                                                                                                                                           
	SET @CustomerAutoId= (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE AutoId=@OrderAutoId)                                                                                                                                                    
	SET @OrderStatus = (SELECT [Status] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                    
                                                                                                       
	SELECT PackerAutoId,OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,ISNULL((CONVERT(VARCHAR(20), OM.[DeliveryDate], 101)),                                                         
	(CONVERT(VARCHAR(20), OM.DelDate,101) + ' ' + convert(varchar(10), DelDate, 108))                                                                                                                                                    
	) AS DeliveryDate,TaxType,                                                                                                                                                    
	OM.[CustomerAutoId],CT.[TermsDesc],OM.[BillAddrAutoId],BA.[Address] As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,                                                                                           
	OM.[ShipAddrAutoId],SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,DO.[AmtPaid],DO.[AmtDue],                                                             
	SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],ST.ShippingType, OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],CommentType,Comment,                                                          
	OM.[Driver],EM.[FirstName]+ ' ' + EM.[LastName] AS DrvName,OM.[ShippingType],ST.ShippingType as ShippingTypeName,CONVERT(VARCHAR(20),OM.[AssignDate],101) AS AssignDate,[DrvRemarks],isnull(DeductionAmount,0.00) as DeductionAmount,ManagerRemarks,                  
	isnull(OM.CreditAmount,0.00)as CreditAmount,isnull(OM.PayableAmount,0.00) as PayableAmount,                                                                                                                                          
	ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit,                                                                                                                           
	PackerRemarks,OrderRemarks,ISNULL(Times,0) as Times,ISNULL(om.MLQty,0) AS MLQty ,ISNULL(om.MLTax ,0) AS MLTax ,                                                                                                                   
	WarehouseRemarks,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,isnull(Weigth_OZQty,0) as Weigth_OZQty,
	isnull(OM.Weigth_OZTax,0) as Weigth_OZTax,isnull(OM.Weigth_OZTaxAmount,0) as  Weigth_OZTaxAmount,CM.CustomerName,CM.CustomerType                                                       
	FROM [dbo].[OrderMaster] As OM                                                                             
	LEFT JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 
	CASE WHEN OM.Status= 0 THEN 'Website' ELSE 'OrderMaster' END                                                                                                                 
	INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                                                   
	INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                           
	LEFT JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
	LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                                                                      
	LEFT JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                                                                     
	LEFT JOIN [CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                    
	LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[Driver]                                               
	left JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]  
	inner join ShippingType as ST on ST.AutoId=OM.ShippingType                       
	WHERE OM.AutoId = @OrderAutoId                                                                                                                                                  
	---------------                                                                                                
	SET @customerType=(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)  
                                                   
		SELECT OIM.AutoId as ItemAutoId, PM.[AutoId] AS ProductAutoId,PM.[ProductId],PM.[ProductName],UM.AutoId AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],                                                                                                        
		OIM.[UnitPrice],convert(decimal(10,2),UnitPrice/QtyPerUnit) as PerpiecePrice,                                                                                                                            
		ISNULL(OIM.OM_MinPrice,0) as [MinPrice],PD.[Price],OIM.[RequiredQty],ISNULL(OIM.OM_CostPrice,0) as OM_CostPrice,ISNULL(OIM.OM_MinPrice,0) as OM_MinPrice,                                                                               
		[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],                                                                                                                                   
		OIM.[RemainQty],IsExchange,isFreeItem,UnitMLQty,TotalMLQty,isnull(OIM.Weight_Oz,0) as WeightOz FROM [dbo].[OrderItemMaster] AS OIM                                                                           
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                                           
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                                                                                                     
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                                                                  
		WHERE [OrderAutoId]=@OrderAutoId                                                              
	
	--select  distinct pm.AutoId,ProductName,UnitTypeAutoId from OrderItemMaster as oim                                                                                                                                                    
	--inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId                                                                                                                                           
	--left join ItemBarcode as ib on ib.ProductAutoId=oim.ProductAutoId                                                                                        
	--and ib.UnitAutoId=oim.UnitTypeAutoId                                                                                                                                                    
	--where OrderAutoId=@OrderAutoId              
	
	SET @OrderStatus=(SELECT Status FROM OrderMaster WHERE AutoId=@OrderAutoId)    
 END    
   ELSE IF @Opcode=43                                                                    
	BEGIN   
		SELECT
		(
			SELECT cm.[AutoId] as  A,[CustomerId] + ' - ' + [CustomerName] +' ['+ct.CustomerType+']' As C FROM [dbo].[CustomerMaster] as cm                                            
			inner join CustomerType as ct on cm.CustomerType=ct.AutoId   where status=1 
			order by C
			for json path
		) as CustomerList,		
		(
			SELECT AutoId, ShippingType as ST,EnabledTax from shippingType where EnabledTax=1
			order by ST
			for json path
		) as ShippingType
		for json path
	END  
  END TRY                                                                      
  BEGIN CATCH                               
  Set @isException=1                                          
  Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                                    
  END CATCH                                                                                       
END 