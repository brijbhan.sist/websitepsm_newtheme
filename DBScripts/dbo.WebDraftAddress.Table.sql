USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebDraftAddress]    Script Date: 01/13/2020 21:27:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebDraftAddress](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAutoId] [int] NULL,
	[Address] [varchar](200) NULL,
	[State] [int] NULL,
	[Zipcode] [varchar](20) NULL,
	[IsDefault] [int] NULL,
	[BCityAutoId] [int] NULL,
	[City] [varchar](80) NULL,
	[ZipcodeAutoid] [int] NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[WebDraftAddress] ON 

INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (1, 1, N'Test Address 2', 21, N'06053', 1, 986, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (2, 1, N'Test Addess', 21, N'06053', 1, 986, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (3, 2, N'Babu Hostel near masjid', 24, N'33025', 1, 988, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (4, 2, N'1263 Broad Street', 21, N'06468', 1, 985, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (5, 3, N'Babu Hostel near masjid', 21, N'06516', 1, 987, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (6, 3, N'1263 Broad Street', 21, N'07737', 1, 984, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (7, 4, N'Addr2', 21, N'06053', 1, 986, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (9, 5, N'Third Ship Addr', 37, N'48317', 1, 993, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (10, 5, N'Third Bill Addr', 21, N'07737', 1, 984, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (11, 6, N'1263 Broad Street', 21, N'06468', 1, 985, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (12, 7, N'Babu Hostel near masjid', 21, N'06516', 1, 987, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (13, 7, N'1263 Broad Street', 21, N'06468', 1, 985, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (15, 9, N'Babu Hostel near masjid', 37, N'48326', 1, 992, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (17, 11, N'19 Church Street', 21, N'06468', 1, 985, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (18, 11, N'Babu Hostel near masjid', 21, N'06053', 1, 986, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (8, 4, N'Addr1', 21, N'07737', 1, 984, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (14, 8, N'1263 Broad Street', 21, N'06516', 1, 987, NULL, NULL)
INSERT [dbo].[WebDraftAddress] ([AutoId], [CustomerAutoId], [Address], [State], [Zipcode], [IsDefault], [BCityAutoId], [City], [ZipcodeAutoid]) VALUES (16, 10, N'Babu Hostel near masjid', 37, N'48317', 1, 993, NULL, NULL)
SET IDENTITY_INSERT [dbo].[WebDraftAddress] OFF
