USE [PriceSmart]
GO

create table contactpersontypemaster
(
Autoid int Identity(1,1),
TypeName varchar(50)
)
insert into contactpersontypemaster(TypeName) values('Store')
insert into contactpersontypemaster(TypeName) values('Manager')
insert into contactpersontypemaster(TypeName) values('Employee')
select * from contactpersontypemaster