
ALTER procedure [dbo].[ProcWeb_ContactUs]
@Opcode int=NULL,   
@Name varchar(130)=null,
@Email varchar(130)=null,
@Query varchar(max)=null,
@PhoneNo varchar(12)=null,
@QueryType varchar(50)=NULL,
@Date datetime=null,
@isException bit out,                        
@exceptionMessage varchar(max) out                        
AS
BEGIN
     SET @isException=0
	 SET @exceptionMessage=''
	 IF @Opcode=11
	 BEGIN	      
          Insert into WebContactUs (Name,Email,Query,Status,Date,QueryType,PhoneNo) VALUES (@Name,@Email,@Query,1,@Date,@QueryType,@PhoneNo)
		  
		   DECLARE @FromEmailId varchar(150)=null,@FromName varchar(150)='Pricesmart',@smtp_userName varchar(150)=null,@PSW varchar(150)=null,@SMTPServer varchar(20)=null,
		   @Port INT=null,@SSL varchar(20)=null,@ToEmailId varchar(150)=null,@CCEmailId varchar(150)=null,@BCCEmailId varchar(150)=null,@Subject varchar(100)='Contact Us-Pricesmart'
		   ,@EmailBody varchar(max)=null,@SourceApp varchar(100)=null,@SubUrl varchar(100)='mypsm.net',@Password varchar(100)

		   SET @EmailBody='Name: '+ @Name+'		
			<br/>Phone No:'+@PhoneNo+'
			<br/>Email:'+@Email+'
			<br/>Query Type:'+@QueryType+'
			<br/>Query:'+@Query+'				
			<br>Thanks and Regards,
			<br>PriceSmart'
		   select @BCCEmailId=BCCEmail,@CCEmailId=CCCEmail,@ToEmailId = TOEmail from PriceSmart.dbo.EmailCategory where Type='CantactUs'
		   SELECT @FromEmailId=EmailId,@Port=port,@SMTPServer=server,@Password=convert(varchar(100),DecryptBypassphrase('WHM',Pass)),@SSL=ssl FROM [PriceSmart].[dbo].EmailServerMaster
		  
		   EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] @Opcode=11,@FromEmailId=@FromEmailId,@FromName=@FromName,@smtp_userName=@FromEmailId,@Password=@Password,@SMTPServer=@SMTPServer,
		   @Port=@Port,@SSL=@SSL,@ToEmailId=@ToEmailId,@CCEmailId=@CCEmailId,@BCCEmailId=@BCCEmailId,@Subject=@Subject,@EmailBody=@EmailBody,@SourceApp=@SourceApp,@SubUrl=@SubUrl,@IsException=0,@ExceptionMessage=''
	 END
END