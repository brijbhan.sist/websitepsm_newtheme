USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[WebCartMaster]    Script Date: 06/03/2020 05:30:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE [dbo].[WebCartMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[StorId] [int] NULL,
	[ShippAddrId] [int] NULL,
	[BillAddrId] [int] NULL,
	[TotalAmount]  AS ([dbo].[FN_Web_Cart_TotalAmount]([AutoId])),
	[MltaxRate]  AS ([dbo].[FN_Web_Cart_MLtaxApply]([AutoId])),
	[TotalMLqty]  AS ([dbo].[FN_Web_Cart_TotalMLqty]([AutoId])),
	[Mltax]  AS ([dbo].[FN_Web_Cart_Mltax]([AutoId])),
	[WeightTaxRate] [decimal](18, 2) NULL,
	[TotalWeightqty]  AS ([dbo].[FN_Web_Cart_TotalWeightqty]([AutoId])),
	[WeightTax]  AS ([dbo].[FN_Web_Cart_WeightTax]([AutoId])),
	[PaybleAmount]  AS ([dbo].[FN_Web_Cart_PaybleAmount]([AutoId])),
	[CartDate] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[WebCartMaster] ADD  DEFAULT (getdate()) FOR [CartDate]
GO


