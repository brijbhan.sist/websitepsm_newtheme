USE [PriceSmart]
GO

/****** Object:  Table [dbo].[ForgotPasswordOTPMaster]    Script Date: 10/10/2020 23:47:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ForgotPasswordOTPMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[OTP] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[OtpDateTime] [datetime] NULL,
 CONSTRAINT [PK_ForgotPasswordOTPMaster] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


