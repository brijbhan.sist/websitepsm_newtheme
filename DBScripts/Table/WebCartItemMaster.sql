USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[WebCartItemMaster]    Script Date: 06/03/2020 05:31:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO 
CREATE TABLE [dbo].[WebCartItemMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CartAutoId] [int] NOT NULL,
	[ProductId] [int] NULL,
	[UnitAutoId] [varchar](50) NULL,
	[OrderQty] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[NetPrice]  AS ([unitprice]*([orderQty]/[Qtyperunit])),
	[MLPerQty] [decimal](18, 2) NULL,
	[MLQty]  AS ([OrderQty]*[MLPerQty]),
	[WeightPerQty] [decimal](18, 2) NULL,
	[WeightQty]  AS ([OrderQty]*[WeightPerQty])
) ON [PRIMARY]
GO


