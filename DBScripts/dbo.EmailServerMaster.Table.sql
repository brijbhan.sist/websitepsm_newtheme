USE [PriceSmart]
GO  


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailServerMaster](
	[SendTo] [varchar](50) NULL,
	[EmailId] [varchar](100) NULL,
	[port] [int] NULL,
	[server] [varchar](50) NULL,
	[Pass] [varchar](100) NULL,
	[ssl] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
