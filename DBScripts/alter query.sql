
---10/23/2020--------------
insert into [dbo].[EmailCategory]([Type],[BCCEmail],[CCCEmail])
values('PlaceOrder','brijbhan.sist@gmail.com','uneel913@gmail.com')
-------------------------------------
insert into [dbo].[EmailCategory]([Type],[BCCEmail],[CCCEmail])
values('CantactUs','brijbhan.sist@gmail.com','uneel913@gmail.com')
---10/10/2020--------------
insert into [dbo].[EmailCategory]([Type],[BCCEmail],[CCCEmail])
values('ForgotPassword','brijbhan.sist@gmail.com','uneel913@gmail.com')
---07/18/2020--------------
alter table [PriceSmart].[dbo].WebDratStoreContactDetails add IsDefault bit
ALTER TABLE [PriceSmart].[dbo].WebDraftAddress drop column Address
alter table [PriceSmart].[dbo].WebDraftAddress add Address1 varchar(200)
alter table [PriceSmart].[dbo].WebDraftAddress add Address2 varchar(200)
---07/08/2020--------------
Alter table WebsiteProductCategory drop column Location
---07/05/2020---------------
create table WebsiteProductCategory
(
AutoId int identity(1,1),
ProductId int, 
Location varchar(10),
Type int 
)

create table WebsiteProductCategoryType
(
AutoId int identity(1,1),
Type char(50)
)

INSERT INTO WebsiteProductCategoryType VALUES ('Special')
INSERT INTO WebsiteProductCategoryType VALUES ('Best Seller')

----6/24/2020-----------
Alter table ShippingType add WebsiteDisplayName varchar(150)
Update ShippingType SET WebsiteDisplayName=ShippingType

----6/24/2020-----------
USE [PriceSmart]
GO
CREATE TABLE [dbo].[WebTempUserRegistration](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Mobile] [varchar](10) NULL,
	[Email] [varchar](50) NULL,
	[Password] [varbinary](max) NULL,
	[Status] [bit] NULL,
	[RegistrationDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER table WebTempUserRegistration add OTP INT
----6/21/2020----------
alter table WebCustomerShippingType add MinimumOrderAmount decimal(18,2),ExtraShippingCharge decimal(18,2),WebsiteDisplayName varchar(150)
Update WebCustomerShippingType SET MinimumOrderAmount=350,ExtraShippingCharge=0
----6/20/2020----------
use PriceSmart
go
alter table CompanyDetails Add MinimumOrderAmount decimal(18,2)
go
Update CompanyDetails SET MinimumOrderAmount=350
----6/13/2020----
Insert into StatusMaster Values (0,'Pending','Website','#AAAF67')

----6/12/2020-----
 ALTER TABLE WebCustomerPaymentMode ADD Location varchar(25)
 ALTER TABLE WebCustomerShippingType ADD Location varchar(25)

 ------6/11/2020----------
ALTER table WebCartItemMaster ADD DefaultUnitQty AS ([OrderQty]/[QtyPerUnit])
alter table [dbo].[WebCartMaster] 
	add  PaymentId  int,
	ShippingId int

-----07/02/2020--------
alter table [dbo].[WebCartMaster]  drop column  [MltaxRate]
alter table [dbo].[WebCartMaster] add [MltaxRate]  AS ([dbo].[FN_Web_Cart_MLtaxApply]([AutoId]))
-----07/03/2020--------
update ManageBanner  set Title='Medicine 2 Pack',Subtitle='Medicine 2 Pack',ImageUrl='images/h1.png' where AutoId=1
update ManageBanner  set ImageUrl='images/Axe.png' where AutoId=2
update ManageBanner  set ImageUrl='images/EnergyDrink.png' where AutoId=2