USE [SMS_DB]
GO
CREATE TABLE [dbo].[SMSLog](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[accountSid] [varchar](100) NULL,
	[authToken] [varchar](100) NULL,
	[Fromno] [varchar](15) NULL,
	[Tono] [varchar](200) NULL,
	[SMSBody] [varchar](max) NULL,
	[CreationDate] [datetime] NULL,
	[Status] [int] NULL,
	[SentDate] [datetime] NULL,
	[SourceApp] [varchar](50) NULL,
	[SubUrl] [varchar](50) NULL,
	[ErrorDescription] [varchar](max) NULL,
	[Attemptcount] [int] NULL,
	[LastAttemptDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[ProcSMS]    Script Date: 6/23/2020 7:00:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Proc [dbo].[ProcSMS]
(
 @accountSid varchar(100)=null, 
 @authToken varchar(100)=null, 
 @fromno varchar(100)=null,
 @tono varchar(max)=null, 
 @SMSBody  varchar(max)=null, 
 @Status int=null, 
 @SentDate datetime=null, 
 @SourceApp  varchar(20)=null, 
 @SubUrl  varchar(100)=null, 
 @ErrorDescription varchar(max)=null,
 @AutoId int = null,
 @Opcode int = null,
 @PageIndex INT=1,
 @PageSize INT=10,
 @RecordCount INT=null,
 @isException BIT OUT,
 @exceptionMessage VARCHAR(max) OUT
)
as begin
BEGIN TRY
	SET @isException=0
	SET @exceptionMessage='Success!!'
  if @Opcode = 11
  begin
	  if not exists (select 1 from SMSLog where SMSBody = @SMSBody )
		begin
			insert into SMSLog(accountSid, authToken,fromno,Tono ,SMSBody, CreationDate, Status ,SourceApp, SubUrl)
			values (@accountSid, @authToken,@fromno,@Tono ,@SMSBody, getdate(),0 ,@SourceApp, @SubUrl)
		end
	end
else if @Opcode = 41 -- EXE will find with this 
begin
	select AutoId, accountSid,authToken,fromno,Tono ,SMSBody  
	from SMSLog where Status = 0 and (isnull(Attemptcount,0)=0 or LastAttemptDate<DATEADD(minute, -1, getdate())) and convert(date,CreationDate) = convert(date,getdate())
	order by isnull(Attemptcount,0) asc, CreationDate asc
end
else if @Opcode = 21
begin
	update SMSLog set Status = case when @ErrorDescription = 'Successful' then 1 else 0 end, SentDate = GETDATE(), ErrorDescription = @ErrorDescription, Attemptcount= isnull(Attemptcount,0)+1, LastAttemptDate=GETDATE() where AutoId = @AutoId 
end

End TRY
BEGIN CATCH
		SET @isException=1
		SET @exceptionMessage=ERROR_MESSAGE()
END CATCH
end 
 
GO
USE [master]
GO
ALTER DATABASE [SMS_DB] SET  READ_WRITE 
GO
