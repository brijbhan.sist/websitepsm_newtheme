USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Web_Cart_WeightTax]    Script Date: 06/02/2020 21:04:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_Web_Cart_WeightTax]
(   
  @AutoId int   
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
 DECLARE @WeightTax  decimal(18,2) 
 set @WeightTax =isnull((select (TotalWeightqty*WeightTaxRate)  from WebCartMaster
 where AutoId=@AutoId ),0.00)  
 RETURN @WeightTax  
END 

