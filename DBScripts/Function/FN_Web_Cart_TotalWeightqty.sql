USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Web_Cart_TotalWeightqty]    Script Date: 06/02/2020 21:05:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_Web_Cart_TotalWeightqty]
(   
  @AutoId int   
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
 DECLARE @TotalWeightqty  decimal(18,2) 
 set @TotalWeightqty =isnull((select sum(WeightQty)  from WebCartItemMaster
 where CartAutoId=@AutoId ),0.00)  
 RETURN @TotalWeightqty  
END 