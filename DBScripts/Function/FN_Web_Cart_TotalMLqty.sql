USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Web_Cart_TotalMLqty]    Script Date: 06/02/2020 21:05:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_Web_Cart_TotalMLqty] 
(   
  @AutoId int   
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
 DECLARE @TotalMLqty  decimal(18,2) 
 set @TotalMLqty =isnull((select sum(MLQty )  from WebCartItemMaster  
 where CartAutoId=@AutoId ),0.00)  
 RETURN @TotalMLqty   
END 
