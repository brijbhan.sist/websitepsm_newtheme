USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Web_Cart_MLtaxApply]    Script Date: 06/02/2020 21:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_Web_Cart_MLtaxApply]  
(   
  @CartAutoId int   
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
 DECLARE @TaxRate decimal(18,2) 
 set @TaxRate=isnull((select TaxRate  from MLTaxMaster where TaxState in (select State from  BillingAddress where AutoId in (select BillAddrId from WebCartMaster where AutoId=@CartAutoId))),0.00) 
 RETURN @TaxRate  
END  
