USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Web_Cart_TotalAmount]    Script Date: 06/02/2020 21:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_Web_Cart_TotalAmount] 
(   
  @AutoId int   
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
 DECLARE @TotalAmount decimal(18,2) 
 set @TotalAmount=isnull((select sum(NetPrice) from WebCartItemMaster 
 where CartAutoId=@AutoId ),0.00)
 RETURN @TotalAmount  
END 