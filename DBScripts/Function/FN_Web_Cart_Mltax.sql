USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Web_Cart_Mltax]    Script Date: 06/02/2020 21:09:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_Web_Cart_Mltax]
(   
  @AutoId int   
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
 DECLARE @Mltax  decimal(18,2) 
 set @Mltax =isnull((select (TotalMLqty*MltaxRate)  from WebCartMaster
 where AutoId=@AutoId ),0.00)  
 RETURN @Mltax  
END 

