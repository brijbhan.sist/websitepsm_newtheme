USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_Netmount]    Script Date: 01/01/2021 01:37:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
go
ALTER TABLE SubCategoryMaster DROP Column SubCategory_ShowonWebsite 
go
ALTER FUNCTION  [dbo].[FN_SubCategory_WebsiteView]
(
	 @AutoId int
)
RETURNS int
AS
BEGIN
	DECLARE @bool int=0
	
	IF EXISTS(select * from ProductMaster as pm inner join PackingDetails as pd on pm.AutoId=pd.ProductAutoId
				where pm.ShowOnWebsite=1 and SubcategoryAutoId=@AutoId AND productstatus=1 and
				pd.unittype=pm.packingautoid and PM.ImageUrl not like '%default_pic.png%')
	BEGIN
	  
	   IF EXISTS(select AutoId from SubCategoryMaster where AutoId=@AutoId and Status=1 and IsShow=1)
	   BEGIN
			SET @bool=1
	   END
	END 
	RETURN @bool
END

go
ALTER TABLE SubCategoryMaster add SubCategory_ShowonWebsite as [dbo].[FN_SubCategory_WebsiteView](AutoId)

