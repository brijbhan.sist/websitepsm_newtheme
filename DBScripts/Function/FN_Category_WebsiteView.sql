USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_Netmount]    Script Date: 01/01/2021 01:37:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
go
--ALTER TABLE CategoryMaster DROP Column Category_ShowonWebsite 
go
CREATE FUNCTION  [dbo].[FN_Category_WebsiteView]
(
	 @AutoId int
)
RETURNS int
AS
BEGIN
	DECLARE @bool int=0
	
	IF EXISTS(select * from SubCategoryMaster as sm where sm.CategoryAutoId=@AutoId and sm.SubCategory_ShowonWebsite=1)
	BEGIN
	  
	   IF EXISTS(select AutoId from CategoryMaster where AutoId=@AutoId and Status=1 and IsShow=1)
	   BEGIN
			SET @bool=1
	   END
	END 
	RETURN @bool
END

go
ALTER TABLE CategoryMaster add Category_ShowonWebsite as [dbo].[FN_Category_WebsiteView](AutoId)

