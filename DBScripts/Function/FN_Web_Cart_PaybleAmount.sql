USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Web_Cart_PaybleAmount]    Script Date: 06/02/2020 21:08:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_Web_Cart_PaybleAmount]
(   
  @AutoId int   
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
 DECLARE @PaybleAmount  decimal(18,2) 
 set @PaybleAmount =isnull((select (TotalAmount+Mltax+WeightTax)  from WebCartMaster
 where AutoId=@AutoId ),0.00)  
 RETURN @PaybleAmount  
END 

