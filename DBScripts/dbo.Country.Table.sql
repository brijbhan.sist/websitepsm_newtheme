USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 01/03/2020 23:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [varchar](4) NULL,
	[Country] [varchar](20) NULL,
 CONSTRAINT [PK__Country__6B232905AB29BFA3] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
