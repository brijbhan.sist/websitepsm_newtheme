USE [PriceSmart]
GO
/****** Object:  StoredProcedure [dbo].[ProcWeb_CustomerMaster]    Script Date: 01/13/2020 21:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE  [dbo].[ProcWeb_CustomerMaster]                                                                   
@Opcode INT=Null,                                                                  
@CustomerAutoId VARCHAR(15)=NULL,                               
@CustomerId VARCHAR(15)=NULL,                                                               
@CustomerName VARCHAR(50)=NULL,                                                                  
@CustomerType INT=NULL,                                                                   
@Email VARCHAR(50)=NULL,                                                               
@AltEmail VARCHAR(50)=NULL,                                                              
@Contact1 VARCHAR(30)=NULL,                                                                  
@Contact2 VARCHAR(30)=NULL,                                                                  
@SalesPersonAutoId INT=NULL,                                                                   
@Status INT=NULL,                                                                
@Terms INT=NULL,                                                              
@MobileNo VARCHAR(20)=NULL,                                                                
@FaxNo VARCHAR(20)=NULL,                                                                 
@TaxId VARCHAR(50)=NULL,                                                                
@ContactPersonName VARCHAR(100)=NULL,                                                                  
@BusinessName VARCHAR(100)=NULL,                                                                  
@OPTLicence VARCHAR(100)=NULL,                                
@EmpAutoId INT=NULL,  
@UserId INT=NULL,                                                           
@isException BIT OUT,                                                                  
@exceptionMessage VARCHAR(max) OUT                                      
AS                                                                  
BEGIN                                                
 SET @isException=0                                                                  
 SET @exceptionMessage='success'                                            
 IF @Opcode=11                                                                  
  BEGIN                                                                            
     BEGIN TRY                                                          
     BEGIN TRAN tr1  
         SET @SalesPersonAutoId=(Select top 1 SalesPersonAutoId from [psmnj.a1whm.com].[dbo].CustomerMaster WHERE CustomerType=2 and Status=1)     
  
		 SET @CustomerId=(SELECT [dbo].[SequenceCodeGenerator]('CustomerId'))             
                                   
		 INSERT INTO  [psmnj.a1whm.com].[dbo].CustomerMaster ( CustomerId , CustomerName , CustomerType ,Terms, Email , AltEmail , Contact1 , Contact2 ,                 
		  SalesPersonAutoId , Status , MobileNo , TaxId , BusinessName,          
		 CreatedBy,CreatedOn,UpdateOn,UpdatedBy,WebStatus)                                                                   
		 VALUES (@CustomerId,@CustomerName,1,1,@Email,@AltEmail,@Contact1,@Contact2,1187,0,                                                                  
		 @MobileNo,@TaxId,@BusinessName,null,getdate(),getdate(),null,1) 

		 DECLARE @StoreId INT
		 SET @StoreId=Scope_Identity()

		 INSERT INTO WebUserStoreRelMaster (StoreId,UserId) VALUES (@StoreId,@UserId)		   
    
     COMMIT TRAN tr1                                                                 
     END TRY                                               
     BEGIN CATCH                                                                  
     ROLLBACK TRAN   tr1                           
    SET @isException=1                                                                  
    SET @exceptionMessage=ERROR_MESSAGE()                                                                  
     END CATCH    
  END                                                    
END 
GO
