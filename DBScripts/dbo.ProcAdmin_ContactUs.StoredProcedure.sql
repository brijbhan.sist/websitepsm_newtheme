USE [PriceSmart]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ProcAdmin_ContactUs]  
@Opcode int=NULL,     
@Name varchar(50)=null,  
@Email varchar(50)=null,  
@Query varchar(max)=null,  

@PageIndex INT=1,                                                                                        
@PageSize INT=10,                                                                                        
@RecordCount INT=null,     
@isException bit out,                          
@exceptionMessage varchar(max) out                          
  
AS  
BEGIN  
  SET @isException=0  
  SET @exceptionMessage=''  
  IF @Opcode=41 
  BEGIN    
		SELECT ROW_NUMBER() OVER(ORDER BY AutoId desc) AS RowNumber, * INTO #Results FROM                                                                                        
		(  
		Select AutoId,Name,Email,Query,Status FROM WebContactUs 

		) AS t  ORDER BY AutoId desc       
		SELECT COUNT(RowNumber) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results                      
		SELECT * FROM #Results                                                                                        
		WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1  
  END  
END

GO
