USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[WebCustomerRegistration]    Script Date: 01/03/2020 23:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebCustomerRegistration](
	[Autoid] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](50) NULL,
	[CustomerName] [varchar](50) NULL,
	[Address1] [varchar](max) NULL,
	[Address2] [varchar](max) NULL,
	[Zipcode] [varchar](6) NULL,
	[City] [varchar](250) NULL,
	[State] [varchar](250) NULL,
	[Country] [varchar](250) NULL,
	[Email] [varchar](50) NULL,
	[Phone] [varchar](12) NULL,
	[Website] [varchar](50) NULL,
	[Stores] [varchar](50) NULL,
	[Distributer] [varchar](50) NULL,
	[TaxId] [varchar](50) NULL,
	[Comments] [varchar](max) NULL,
	[StoreStatus] [int] NULL,
	[DocumentSatus] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Compa__68A8708A]  DEFAULT (NULL) FOR [CompanyName]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Custo__699C94C3]  DEFAULT ('') FOR [CustomerName]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Addre__6A90B8FC]  DEFAULT ('') FOR [Address1]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Addre__6B84DD35]  DEFAULT ('') FOR [Address2]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Zipco__6C79016E]  DEFAULT ('') FOR [Zipcode]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustome__City__6D6D25A7]  DEFAULT ('') FOR [City]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__State__6E6149E0]  DEFAULT ('') FOR [State]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Count__6F556E19]  DEFAULT ('') FOR [Country]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Email__70499252]  DEFAULT ('') FOR [Email]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Phone__713DB68B]  DEFAULT ('') FOR [Phone]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Websi__7231DAC4]  DEFAULT ('') FOR [Website]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Store__7325FEFD]  DEFAULT ((0)) FOR [Stores]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Distr__741A2336]  DEFAULT ('') FOR [Distributer]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__TaxId__750E476F]  DEFAULT ((0)) FOR [TaxId]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Comme__76026BA8]  DEFAULT ('') FOR [Comments]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Store__74D93D45]  DEFAULT ((0)) FOR [StoreStatus]
GO
ALTER TABLE [dbo].[WebCustomerRegistration] ADD  CONSTRAINT [DF__WebCustom__Docum__75CD617E]  DEFAULT ((0)) FOR [DocumentSatus]
GO
