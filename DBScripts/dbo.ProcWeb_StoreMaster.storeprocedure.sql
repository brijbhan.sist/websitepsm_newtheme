alter Procedure [dbo].[ProcWeb_StoreMaster]                                
  @Opcode INT=NULL,                                
  @Autoid int =null,                               
  @FirstName varchar(50)=null,   
  @UserName varchar(50)=null,
  @LastName varchar(50)=null,                               
  @CompanyName varchar(50)=null,                                
  @CustomerName varchar(50)=null,                          
  @OPTLicense varchar(50)=null,                              
  @TaxId varchar(50)=null,                                
  @BillAddress1 varchar(MAX)=null, 
  @BillAddress2 varchar(MAX)=null, 
  @BillCity varchar(50)=null,                            
  @BillZipCode varchar(50)=null,                           
  @BillState varchar(50)=null,                           
  @ShippingAddress1  varchar(50)=null,        
  @ShippingAddress2  varchar(50)=null,   
  @ShipCity  varchar(50)=null,                          
  @ShippingZipCode  varchar(50)=null,                     
  @IsDefault int=null,                    
  @Type int=null,       
  @StoreType int=null,
  @ShipState  varchar(50)=null,                          
  @ContactPerson varchar(50)=null,                                
  @Email varchar(200)=null,
  @Mobile varchar(10)=null,
  @AlternateEmail  varchar(200)=null,
  @Landlineno1 varchar(12)=null,
  @FaxNo varchar(25)=null,
  @Landlineno2 varchar(12)=null,
  @ContactType int=null,                          
  @UserId varchar(50)=null,                           
  @Password varchar(MAX)=null,                     
  @StoreId int=null,  
  @XmlContactDetail  xml=null,
  @Location varchar(50)=null,  
  @StoreOpenTime time(7)=null,  
  @StoreCloseTime time(7)=null,  
  @Document int=null,            
  @DocumentUrl varchar(120)=null,    
  @StoreLocation varchar(10)=null,   
  @BillingLatitude varchar(25)=null,
  @BillingLongitude varchar(25)=null,
  @ShippingLatitude varchar(25)=null,
  @ShippingLongitude varchar(25)=null,
  @IsException bit out,                                                                      
  @ExceptionMessage varchar(max) out                                      
AS                                
BEGIN                                
  SET @IsException=0                                
  SET @ExceptionMessage=''            
  IF @Opcode=11  
  IF EXISTS(select StoreName from [PriceSmart].[dbo].WebStoreDraft as WSD
INNER JOIN [PriceSmart].[dbo].WebUserStoreRelMaster as WUSD on WSD.AutoId=WUSD.StoreId
where UserId=@Autoid and  StoreName=@CustomerName)  
	BEGIN
		SET @IsException=1                                
		SET @ExceptionMessage='Store is already exists'
	END		   
ELSE
  BEGIN                            
  BEGIN TRY                                
    BEGIN TRANSACTION tr2                                
		DECLARE @CustomerId varchar(15),@StorId int,@BillAutoId int,@ShipAutoId int                          
                      
		INSERT INTO  [PriceSmart].[dbo].WebStoreDraft ( BusinessName, StoreName, OPTLicense, TaxID,Status,StoreOpenTime,StoreCloseTime,CreatedDate)                  
		VALUES (@CompanyName,@CustomerName,@OPTLicense,@TaxId,0,@StoreOpenTime,@StoreCloseTime,getdate())                          
        
		SET @StorId=SCOPE_IDENTITY()  

		insert into [PriceSmart].[dbo].WebDratStoreContactDetails(IsDefault,ContactPerson,Email,Mobile,AlternateEmail,Landlineno1,FaxNo,Landlineno2,ContactType,StoreAutoId)
		select tr.td.value('IsDefault[1]','bit') as IsDefault,
		tr.td.value('CPerson[1]','varchar(50)') as ContactPerson,
		tr.td.value('Email[1]','varchar(50)') as Email,
	    tr.td.value('MobileNo[1]','varchar(12)') as Mobile,
		tr.td.value('AEmail[1]','varchar(50)') as AlternateEmail,
		tr.td.value('Landlineno1[1]','varchar(12)') as Landlineno1,
		tr.td.value('FaxNo[1]','varchar(25)') as FaxNo,
		tr.td.value('Landlineno2[1]','varchar(12)') as Landlineno1,
		tr.td.value('ContactType[1]','int') as ContactType,
		@StorId
		from  @XmlContactDetail.nodes('/MainData/XmlContactDetail') tr(td)    

		IF @ShippingZipCode=''  --Updated on 10/18/2019 02:14 AM                    
		BEGIN            
		INSERT INTO [PriceSmart].[dbo].WebDraftAddress (CustomerAutoId,Address,Address2,State,Zipcode,IsDefault,City,Lat,Long)               
		VALUES (@StorId,@BillAddress1,@BillAddress2,@BillState,@BillZipCode,1,@BillCity,@BillingLatitude,@BillingLongitude)                          
		SET @BillAutoId=SCOPE_IDENTITY()                    
		SET @ShipAutoId=@BillAutoId                    
    END                    
    ELSE                    
    BEGIN                    
		INSERT INTO [PriceSmart].[dbo].WebDraftAddress (CustomerAutoId,Address,Address2,State,Zipcode,IsDefault,City,Lat,Long)                          
		values (@StorId,@ShippingAddress1,@ShippingAddress2,@ShipState,@ShippingZipCode,1,@ShipCity,@ShippingLatitude,@ShippingLongitude)                          
		SET @ShipAutoId=SCOPE_IDENTITY()                          
                          
		INSERT INTO [PriceSmart].[dbo].WebDraftAddress (CustomerAutoId,Address,Address2,State,Zipcode,IsDefault,City,Lat,Long)                          
		VALUES (@StorId,@BillAddress1,@BillAddress2,@BillState,@BillZipCode,1,@BillCity,@BillingLatitude,@BillingLongitude)                          
		SET @BillAutoId=SCOPE_IDENTITY()                     
    END                         
                          
		UPDATE [PriceSmart].[dbo].WebStoreDraft SET BillAutoId=@BillAutoId,ShipAutoId=@ShipAutoId Where AutoId=@StorId                          
                          
		-- Select * from WebUserStoreRelMaster                         
                          
		INSERT INTO [PriceSmart].[dbo].WebUserStoreRelMaster (StoreId,UserId,StoreStatus,LocationStatus) VALUES (@StorId,@Autoid,0,0)             
            
		Select StoreId,DBO.UDFStoreList(StoreLocation,StoreId) AS StoreName,StoreStatus AS Status,ISNull((Select COUNT(StoreId) from [PriceSmart].[dbo].WebUserStoreRelMaster             
		where UserId=WUR.AutoId),0) as TotalStore from [PriceSmart].[dbo].WebUserStoreRelMaster AS WUSRM INNER JOIN [PriceSmart].[dbo].WebUserRegistration as WUR           
		ON WUSRM.UserId=WUR.AutoId             
		where WUR.Email=@UserName AND WUSRM.StoreId=@StorId  for json path,include_null_values                                 
                                   
   COMMIT TRANSACTION tr2                                
     END TRY                                
     BEGIN CATCH                                
   ROLLBACK TRANSACTION tr2                                
   SET @IsException=1                                
   SET @ExceptionMessage=ERROR_MESSAGE()                                
  END CATCH                                
    END                                
                                             
else IF @Opcode=15   
	IF EXISTS(select StoreName from [PriceSmart].[dbo].WebStoreDraft as WSD
INNER JOIN [PriceSmart].[dbo].WebUserStoreRelMaster as WUSD on WSD.AutoId=WUSD.StoreId
where UserId=@UserId and  StoreName=@CustomerName and WSD.AutoId!=@Autoid)  
		BEGIN
			SET @IsException=1                                
			SET @ExceptionMessage='Store is already exists'
	END
ELSE
  BEGIN                     
  BEGIN TRY                                
  BEGIN TRANSACTION tr3                                        
    UPDATE  [PriceSmart].[dbo].WebStoreDraft SET BusinessName=@CompanyName, StoreName=@CustomerName, 
	OPTLicense=@OPTLicense,TaxID=@TaxId,
	StoreOpenTime=@StoreOpenTime,StoreCloseTime=@StoreCloseTime  
	WHERE AutoId=@Autoid                             
                           
    SET @StorId=@Autoid 
	delete from [PriceSmart].[dbo].WebDratStoreContactDetails where StoreAutoId=@Autoid
    insert into [PriceSmart].[dbo].WebDratStoreContactDetails(IsDefault,ContactPerson,Email,Mobile,AlternateEmail,Landlineno1,FaxNo,Landlineno2,ContactType,StoreAutoId)
		select tr.td.value('IsDefault[1]','bit') as IsDefault,
		tr.td.value('CPerson[1]','varchar(50)') as ContactPerson,
		tr.td.value('Email[1]','varchar(50)') as Email,
	    tr.td.value('MobileNo[1]','varchar(12)') as Mobile,
		tr.td.value('AEmail[1]','varchar(50)') as AlternateEmail,
		tr.td.value('Landlineno1[1]','varchar(12)') as Landlineno1,
		tr.td.value('FaxNo[1]','varchar(25)') as FaxNo,
		tr.td.value('Landlineno2[1]','varchar(12)') as Landlineno1,
		tr.td.value('ContactType[1]','int') as ContactType,
		@StorId
		from  @XmlContactDetail.nodes('/MainData/XmlContactDetail') tr(td)    
		
        Delete FROM [PriceSmart].[dbo].WebDraftAddress Where CustomerAutoId=@StoreId                  
	
   IF @ShippingZipCode=''  --Updated on 10/18/2019 02:14 AM                    
		BEGIN            
		INSERT INTO [PriceSmart].[dbo].WebDraftAddress (CustomerAutoId,Address,Address2,State,Zipcode,IsDefault,City,Lat,Long)               
		VALUES (@StorId,@BillAddress1,@BillAddress2,@BillState,@BillZipCode,1,@BillCity,@BillingLatitude,@BillingLongitude)                          
		SET @BillAutoId=SCOPE_IDENTITY()                    
		SET @ShipAutoId=@BillAutoId                    
    END                    
    ELSE                    
    BEGIN                    
		INSERT INTO [PriceSmart].[dbo].WebDraftAddress (CustomerAutoId,Address,Address2,State,Zipcode,IsDefault,City,Lat,Long)                          
		values (@StorId,@ShippingAddress1,@ShippingAddress2,@ShipState,@ShippingZipCode,1,@ShipCity,@ShippingLatitude,@ShippingLongitude)                          
		SET @ShipAutoId=SCOPE_IDENTITY()                          
                          
		INSERT INTO [PriceSmart].[dbo].WebDraftAddress (CustomerAutoId,Address,Address2,State,Zipcode,IsDefault,City,Lat,Long)                          
		VALUES (@StorId,@BillAddress1,@BillAddress2,@BillState,@BillZipCode,1,@BillCity,@BillingLatitude,@BillingLongitude)                          
		SET @BillAutoId=SCOPE_IDENTITY()                     
    END                                         
         UPDATE [PriceSmart].[dbo].WebStoreDraft SET BillAutoId=@BillAutoId,ShipAutoId=@ShipAutoId Where AutoId=@StorId                     
                             
 COMMIT TRANSACTION tr3                                
  END TRY                                
  BEGIN CATCH                                
 ROLLBACK TRANSACTION tr3                                
  SET @IsException=1                                
  SET @ExceptionMessage=ERROR_MESSAGE()                                
 END CATCH                         
   END                              
ELSE IF @Opcode=16
BEGIN 
Update [PriceSmart].[dbo].WebDratStoreContactDetails set ContactType=@ContactType,ContactPerson=@ContactPerson,Email=@Email
,AlternateEmail=@AlternateEmail,Mobile=@Mobile,Landlineno1=@Landlineno1,Landlineno2=@Landlineno2,FaxNo=@FaxNo 
where AutoId=@Autoid
END 
                        
IF @Opcode=46  --User Draft Store List                  
	BEGIN     
		SELECT WSD.AutoId,StoreName,WSR.StoreId,SM.StatusType as Status,SM.ColorCode,WCD.Email,WCD.Mobile,Landlineno1,
		SA.Address+', '+(case when ISNULL(SA.Address2,'')='' then '' else SA.Address2+' ,' end)+SA.City+', '+(case when ISNULL((Select StateCode FROM State as s where s.StateName=trim(SA.State)),'')='' 
		then SA.State else ISNULL((Select StateCode FROM State as s where s.StateName=trim(SA.State)),'') end)+' - '+SA.Zipcode as ShippingAddress,                  
		BA.Address+', '+(case when ISNULL(BA.Address2,'')='' then '' else BA.Address2+' ,' end)+BA.City+', '+(case when ISNULL((Select StateCode FROM State as s where s.StateName=trim(BA.State)),'')='' 
		then BA.State else ISNULL((Select StateCode FROM State as s where s.StateName=trim(BA.State)),'') end)+' - ' +BA.Zipcode as BillingAddress 
		FROM [PriceSmart].[dbo].WebStoreDraft  as WSD                   
		INNER JOIN [PriceSmart].[dbo].WebUserStoreRelMaster as WSR on                    
		WSD.AutoId=WSR.StoreId                    
		INNER JOIN [PriceSmart].[dbo].WebDraftAddress as SA on WSD.ShipAutoId=SA.AutoId                    
		INNER JOIN [PriceSmart].[dbo].WebDraftAddress as BA on WSD.BillAutoId=BA.AutoId  
		INNER JOIN [PriceSmart].[dbo].WebDratStoreContactDetails as WCD on WCD.StoreAutoId=WSR.StoreId AND WCD.ISDefault=1 
		INNER JOIN [psmnj.a1whm.com].[dbo].StatusMaster as SM on                    
		WSD.Status=SM.AutoId and SM.Category='Store' 
		where WSR.UserId=@UserId
		union 
		SELECT CM.AutoId,CustomerName as StoreName,WSR.StoreId,SM.StatusType as Status,SM.ColorCode,CP.Email,CP.MobileNo,Landline as Landlineno1                   
		,SA.Address+', '+(case when ISNULL(SA.Address2,'')='' then '' else SA.Address2+' ,' end)+(Select top 1 CityName from  [dbo].CityMaster Where AutoId=SA.SCityAutoId)+', '+(Select top 1 StateCode from  [dbo].State Where AutoId=SA.State)+' - '+SA.Zipcode as ShippingAddress,                  
		BA.Address+', '+(case when ISNULL(BA.Address2,'')='' then '' else BA.Address2+' ,' end)+(Select top 1 CityName from   [dbo].CityMaster Where AutoId=BA.BCityAutoId)+', '+(Select top 1 StateCode from  [dbo].State Where AutoId=BA.State)+' - '+BA.Zipcode as BillingAddress 
		FROM   [dbo].CustomerMaster  as cm 
		LEFT JOIN [PriceSmart].[dbo].WebUserStoreRelMaster as WSR on cm.AutoId=WSR.StoreId                  
		LEFT JOIN   [dbo].ShippingAddress as SA on cm.DefaultShipAdd=SA.AutoId                    
		INNER JOIN  [dbo].BillingAddress as BA on cm.DefaultBillAdd=BA.AutoId 
		left JOIN   [dbo].CustomerContactPerson as CP on CP.CustomerAutoId=WSR.StoreId AND CP.ISDefault=1
		left JOIN   [psmnj.a1whm.com].[dbo].StatusMaster as SM on                    
		cm.Status=SM.AutoId and SM.Category='Store'                       
		where WSR.UserId=@UserId
		for Json path ,include_null_values                          
END             
IF @Opcode=47                    
   BEGIN  
   select 
   (
		SELECT WSD.AutoId,StoreName,OPTLicense,TaxID,BillAutoId,ShipAutoId                
		,SM.StatusType as Status,SM.ColorCode,BA.Address as BAddress1,BA.Address2 as BAddress2,BA.City as BCity,
		BA.Zipcode as BZipcode,BA.State as BState,SA.Address as SAddress1,SA.Address2 as SAddress2,SA.City as SCity,  
		SA.Zipcode as SZipcode,SA.State as SState,Convert(varchar(10),left(WSD.StoreOpenTime,5)) as StoreOpenTime,Convert(varchar(10),
		left(WSD.StoreCloseTime,5)) as StoreCloseTime,BA.Lat as BillingLatitude,BA.Long as BillingLongitude,
		SA.Lat as ShippingLatitude,SA.Long as ShippingLongitude,WSD.BusinessName
		FROM [PriceSmart].[dbo].WebStoreDraft  as WSD                   
		INNER JOIN [PriceSmart].[dbo].WebUserStoreRelMaster as WSR on                    
		WSD.AutoId=WSR.StoreId                    
		INNER JOIN [PriceSmart].[dbo].WebDraftAddress as SA on                    
		WSD.ShipAutoId=SA.AutoId                    
		INNER JOIN [PriceSmart].[dbo].WebDraftAddress as BA on                    
		WSD.BillAutoId=BA.AutoId                    
		INNER JOIN  [psmnj.a1whm.com].[dbo].StatusMaster as SM on                    
		WSD.Status=SM.AutoId and SM.Category='Store'                    
		Where WSR.StoreId=@Autoid                    
		for Json path,include_null_values 
		) as DStoredetails,
		(
		select CCP.AutoId,IsDefault,StoreAutoId,ContactPerson,Email,Mobile,AlternateEmail,Landlineno1,FaxNo,Landlineno2,
		CPT.TypeName,ContactType from [PriceSmart].[dbo].WebDratStoreContactDetails as CCP
		INNER JOIN [psmnj.a1whm.com].[dbo].[contactpersontypemaster] as CPT on CPT.AutoId=CCP.ContactType
		where StoreAutoId=@Autoid 
		for json path 
		) as DCSDetails
		for json path

END

IF @Opcode=49 --Approved Store List                  
	BEGIN                  
		SELECT CM.AutoId,CustomerId,WSR.StoreId ,CustomerName as StoreName,SM.StatusType as Status,SM.ColorCode,BusinessName,OPTLicence                    
		,SA.Address+' ,'+(case when ISNULL(SA.Address2,'')='' then '' else SA.Address2+' ,' end)+' ,'+(Select top 1 CityName from  [psmnj.a1whm.com].[dbo].CityMaster Where AutoId=SA.SCityAutoId)+' ,'+SA.Zipcode as ShippingAddress,                  
		BA.Address+' ,'+(case when ISNULL(BA.Address2,'')='' then '' else BA.Address2+' ,' end)+' ,'+(Select top 1 CityName from   [psmnj.a1whm.com].[dbo].CityMaster Where AutoId=BA.BCityAutoId)+' ,'+BA.Zipcode as BillingAddress 
		FROM   [psmnj.a1whm.com].[dbo].CustomerMaster  as cm 
		LEFT JOIN [PriceSmart].[dbo].WebUserStoreRelMaster as WSR on                  
		cm.AutoId=WSR.StoreId                  
		LEFT JOIN   [psmnj.a1whm.com].[dbo].ShippingAddress as SA on                    
		cm.DefaultShipAdd=SA.AutoId                    
		INNER JOIN   [psmnj.a1whm.com].[dbo].BillingAddress as BA on                    
		cm.DefaultBillAdd=BA.AutoId                    
		INNER JOIN   [psmnj.a1whm.com].[dbo].StatusMaster as SM on                    
		cm.Status=SM.AutoId and SM.Category='Store'                       
		where WSR.UserId=@UserId --Updated on 10/18/2019 01:35 AM                    
		for Json path                    
END                 
IF @Opcode=50 --Approved Store details                  
	BEGIN   
	select
	  (
		SELECT CM.AutoId,CustomerId,CustomerName as StoreName,isnull(BusinessName,'') as BusinessName,isnull(OPTLicence,'') as OPTLicence,
		isnull(TaxId,'') as TaxId,SA.Address as SAddress,isnull(SA.Address2,'') as SAddress2,(Select top 1 CityName from   [dbo].CityMaster Where AutoId=SA.SCityAutoId) as SCity,SA.Zipcode as SZipcode,s.StateName as SState,                  
		BA.Address as BAddress,isnull(BA.Address2,'') as BAddress2,(Select top 1 CityName from   [dbo].CityMaster Where AutoId=BA.BCityAutoId) as BCity,BA.Zipcode as BZipcode,Bs.StateName as BState 
		,CONVERT(varchar(15),CAST(OptimotwFrom AS TIME),100) as OptimotwFrom, CONVERT(varchar(15),CAST(OptimotwTo AS TIME),100) as OptimotwTo FROM   [dbo].CustomerMaster  as cm                                                                      
		INNER JOIN [PriceSmart].[dbo].WebUserStoreRelMaster as WSR on cm.AutoId=WSR.StoreId                  
		LEFT  JOIN   [dbo].ShippingAddress as SA on cm.DefaultShipAdd=SA.AutoId                    
		LEFT JOIN   [dbo].BillingAddress as BA on cm.DefaultBillAdd=BA.AutoId      
		LEFT JOIN   [dbo].State as s on SA.State=s.AutoId 
		INNER JOIN   [dbo].State as Bs on BA.State=Bs.AutoId 		                   
		where WSR.StoreId=@StoreId                  
		for Json path,include_null_values  
		) as StoreDeatils,
		(
		select ISDefault,ContactPerson,MobileNo,Landline,Landline2,Fax,Email,AlternateEmail,TypeName from [dbo].[CustomerContactPerson] as CCP
        INNER JOIN [dbo].[contactpersontypemaster] as CPT on CPT.AutoId=CCP.Type
		where CustomerAutoId=@StoreId                  
		for Json path,include_null_values  
		) as CPDeatils
		for Json path
END 
IF @Opcode=51
BEGIN
 select *   FROM [psmnj.a1whm.com].[dbo].[contactpersontypemaster]
 for Json path,include_null_values  
END
IF @Opcode=52
BEGIN
 select * FROM [PriceSmart].[dbo].WebDratStoreContactDetails where AutoId=@Autoid
 for Json path,include_null_values  
END		
 IF @opcode=55 --Time Window 
 BEGIN          
		select convert(varchar(10),left(TimeFrom,5)) as TimeFrom,Convert(varchar(10),left(TimeTo,5)) as TimeTo,  
		CONVERT(varchar(15),CAST(TimeFrom AS TIME),100) as FromTime,CONVERT(varchar(15),CAST(TimeTo AS TIME),100)   
		as ToTime from [PriceSmart].[dbo].TimeWindow  for json path,include_null_values    
 END 
 IF @Opcode=56
 BEGIN
        SELECT FirstName+' '+isnull(LastName,'') AS Name,Mobile,Email FROM [PriceSmart].[dbo].WebUserRegistration Where AutoId=@Autoid
		for json path,include_null_values  
 END
END   