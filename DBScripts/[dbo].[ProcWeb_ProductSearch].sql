USE [psmnj.a1whm.com]
GO
ALTER PROCEDURE [dbo].[ProcWeb_ProductSearch]            
 @AutoId INT=NULL,                                  
 @Opcode INT=NULL,                                     
 @timeStamp datetime =null,     
 @ProductName varchar(max)=null,                                                                
 @IsException bit out,                                          
 @PageIndex INT =1,                                        
 @PageSize INT=28,                                        
 @RecordCount INT=null,                                                           
 @ExceptionMessage varchar(max) out                                               
AS                                    
BEGIN                                    
	SET @IsException=0                                                        
	SET @ExceptionMessage=''                                    
	BEGIN TRY                                    
		IF @Opcode=41                                
		BEGIN                           
			Select PM.AutoId, PM.ProductId, PM.ProductName from ProductMaster as PM   
			INNER JOIN BrandMaster AS BM ON PM.BrandAutoId=BM.AutoId
			Where (@ProductName IS NULL OR @ProductName='' OR PM.ProductName LIKE '%' + @ProductName + '%' 
			OR PM.ProductId LIKE '%' + @ProductName + '%' OR BM.BrandName LIKE '%' + @ProductName + '%')
			and PM.ProductStatus=1 AND PM.ShowOnWebsite=1 AND BM.Status=1 AND PM.Product_ShowOnWebsite=1
			
		END                                
	END TRY                                    
	BEGIN CATCH                                    
		SET @IsException=1                                    
		SET @ExceptionMessage=ERROR_MESSAGE()                                    
	END CATCH                                  
END   