USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 01/03/2020 23:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [varchar](12) NULL,
	[CategoryName] [varchar](50) NULL,
	[Description] [varchar](250) NULL,
	[Status] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdateBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[SeqNo] [int] NULL,
	[IsShow] [int] NULL,
 CONSTRAINT [PK__Category__6B232905A2DA7A60] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CategoryMaster] ADD  DEFAULT ((0)) FOR [SeqNo]
GO
ALTER TABLE [dbo].[CategoryMaster] ADD  DEFAULT ((1)) FOR [IsShow]
GO
