USE [PriceSmart]
GO
/****** Object:  StoredProcedure [dbo].[ProcWeb_Registration]    Script Date: 10/07/2020 01:00:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[ProcResetPasswordMaster]                                
  @Opcode INT=NULL,                                
  @Autoid int =null,      
  @Email varchar(100) = null,
  @OldPassword varchar(MAX)=null, 
  @NewPassword varchar(MAX)=null,
  @ForgotEmailId varchar(50) =null,
  @OTP varchar(20) = null,
  @FirstName varchar(25) = null,
  @LastName varchar(25) = null,
  @Password varchar(30) = null,
  @IsException bit out,                                                                      
  @ExceptionMessage varchar(max) out                                      
AS                                
BEGIN		
  SET @IsException=0                                
  SET @ExceptionMessage='' 

  IF @Opcode=21    
	 BEGIN                                                                                      
		IF EXISTS(SELECT * FROM [PriceSmart].[dbo].WebUserRegistration WHERE Email=@ForgotEmailId)      
		    BEGIN   
			  Update  [PriceSmart].[dbo].WebUserRegistration set Password = EncryptByPassPhrase('WHM',@NewPassword) Where Email=@ForgotEmailId                             
			END  
		ELSE
			BEGIN    
				SET @isException=1    
				SET @exceptionMessage= 'Password not reset successfully. !'    
			END                      		                            
     END  

  IF @Opcode=41    
      BEGIN                              
		  BEGIN TRY                              
		   BEGIN TRAN                                                            
			  set @Email=(select Email from WebUserRegistration where AutoId=@Autoid)
		   IF EXISTS(SELECT * FROM [PriceSmart].[dbo].WebUserRegistration WHERE Email=@Email and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@OldPassword)       
			   BEGIN   
			   Update  [PriceSmart].[dbo].WebUserRegistration set Password = EncryptByPassPhrase('WHM',@NewPassword) Where Email=@Email                             
				END  
		   ELSE
				BEGIN    
				SET @isException=1    
				SET @exceptionMessage= 'Old password not matached!'     
				END
		   COMMIT TRAN                              
		  END TRY                              
		  BEGIN CATCH                              
		  ROLLBACK TRANSACTION                               
		   SET @IsException=1                                
		   SET @ExceptionMessage=ERROR_MESSAGE()                                
		  END CATCH                              
     END  
  IF @Opcode=42    
		BEGIN    

		IF EXISTS(SELECT * from [PriceSmart].[dbo].WebUserRegistration where Email=@ForgotEmailId and Status=1)                                                        
			 BEGIN TRY         
			 ----------------------Send OTP-----------------					 
		             IF EXISTS(SELECT * from [PriceSmart].[dbo].WebUserRegistration where Email=@ForgotEmailId and Status=1)  
					
					 BEGIN
					   SELECT @FirstName = FirstName, @LastName = LastName from [PriceSmart].[dbo].WebUserRegistration where Email=@ForgotEmailId
					   SET @OTP=(SELECT LEFT(CAST(RAND()*1000000000+999999 AS INT),6))
					   
					   DECLARE @FromEmailId varchar(100)=null,@FromName varchar(100)='Pricesmart',@smtp_userName varchar(100)=null,@PSW varchar(20)=null,@SMTPServer varchar(20)=null,
					   @Port INT=null,@SSL varchar(20)=null,@ToEmailId varchar(100)=null,@CCEmailId varchar(100)=null,@BCCEmailId varchar(100)=null,@Subject varchar(100)='Forgotten Password Verification-Pricesmart'
					   ,@EmailBody varchar(500)=null,@SourceApp varchar(100)=null,@SubUrl varchar(100)='mypsm.net'

						SET @EmailBody='Hi '+ @FirstName+' '+@LastName+','+'
						<br/><br/>Your OTP of forgotten password is:'+' '+convert(varchar(10), @OTP)+'	
						<br/></br>This is system generated email. Please do not reply
						<br>Thanks and Regards,
						<br>PriceSmart'
					   select @BCCEmailId=BCCEmail,@CCEmailId=CCCEmail from PriceSmart.dbo.EmailCategory where Type='ForgotPassword'
					   SELECT @FromEmailId=EmailId,@Port=port,@SMTPServer=server,@Password=convert(varchar(100),DecryptBypassphrase('WHM',Pass)),@SSL=ssl,@Autoid=CreatedBy FROM [PriceSmart].[dbo].EmailServerMaster
		  
					   EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] @Opcode=11,@FromEmailId=@FromEmailId,@FromName=@FromName,@smtp_userName=@FromEmailId,@Password=@Password,@SMTPServer=@SMTPServer,
					   @Port=@Port,@SSL=@SSL,@ToEmailId=@ForgotEmailId,@CCEmailId=@CCEmailId,@BCCEmailId=@BCCEmailId,@Subject=@Subject,@EmailBody=@EmailBody,@SourceApp=@SourceApp,@SubUrl=@SubUrl,@IsException=0,@ExceptionMessage=''
					   
					   insert into  [PriceSmart].[dbo].[ForgotPasswordOTPMaster] ([OTP],[Email],[OtpDateTime])  VALUES (@OTP,@ForgotEmailId,GETDATE())
					    

					 END
					 END TRY
					 BEGIN CATCH
						  SET @IsException=1
						  SET @ExceptionMessage=ERROR_MESSAGE()
					 END CATCH                                    
	     	ELSE                                
			BEGIN                                
				SET @IsException=1                                                       
				SET @ExceptionMessage='Email does not exist. !'                                 
			END                        
 END    
  IF @Opcode=43    
	 BEGIN
		 BEGIN TRY
			   IF EXISTS(SELECT top 1 OTP FROM [PriceSmart].[dbo].[ForgotPasswordOTPMaster] WHERE OTP=@OTP AND Email=@ForgotEmailId ORDER BY OtpDateTime DESC)
			   BEGIN
					SET @IsException=0
					SET @ExceptionMessage='Valid'
			   END
			   ELSE
			   BEGIN
					SET @IsException=1
					SET @ExceptionMessage='Invalid security code'
			   END
		 END TRY
		 BEGIN CATCH
			  SET @IsException=1
			  SET @ExceptionMessage=ERROR_MESSAGE()
		 END CATCH
	    END
                                                                                                        
END   