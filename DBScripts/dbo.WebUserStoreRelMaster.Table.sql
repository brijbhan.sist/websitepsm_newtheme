USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebUserStoreRelMaster]    Script Date: 01/13/2020 21:27:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebUserStoreRelMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[StoreLocation] [varchar](20) NULL,
	[StoreStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[WebUserStoreRelMaster] ON 

INSERT [dbo].[WebUserStoreRelMaster] ([AutoId], [StoreId], [UserId], [StoreLocation], [StoreStatus]) VALUES (10, 4015, 23, N'psmnj', 1)
SET IDENTITY_INSERT [dbo].[WebUserStoreRelMaster] OFF
