USE [psmnj.a1whm.com]
go
--exec ProcGetTop20Product
Alter procedure ProcGetTop20Product
AS
BEGIN
    IF OBJECT_ID('tempdb..#Results') IS NOT NULL
	BEGIN
         DROP TABLE #Results
	END
	DELETE FROM WebsiteProductCategory Where Type IN (1,2)

	create table #Results (
    Id int identity(1,1) primary key clustered,
    ProductAutoId int,
    QtyDel int
    )

	SET STATISTICS time ON
	SELECT num_of_reads, num_of_bytes_read,
	num_of_writes, num_of_bytes_written
	FROM sys.dm_io_virtual_file_stats(DB_ID('tempdb'), 1)	

	INSERT INTO #Results (ProductAutoId,QtyDel)
	select top 20 ProductAutoId, count(1) as QtyDel from Delivered_Order_Items as DOI
	inner join ProductMaster as PM on PM.AutoId=DOI.ProductAutoId where QtyDel!=0
	and pm.ImageUrl not like '%default%'
	group by DOI.ProductAutoId,QtyDel
	order by count(1) desc

	insert into WebsiteProductCategory(ProductId,Type)
	select top 10 ProductAutoId,1 from #Results

	insert into WebsiteProductCategory(ProductId,Type)
	select top 10 ProductAutoId,2 from #Results as rt order by rt.Id desc
	
	SELECT num_of_reads, num_of_bytes_read,
	num_of_writes, num_of_bytes_written
	FROM sys.dm_io_virtual_file_stats(DB_ID('tempdb'), 1)	
END

--Select * from WebsiteProductCategory


