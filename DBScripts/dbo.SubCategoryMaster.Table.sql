USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[SubCategoryMaster]    Script Date: 01/03/2020 23:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategoryMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[SubcategoryId] [varchar](12) NULL,
	[SubcategoryName] [varchar](50) NULL,
	[Description] [varchar](250) NULL,
	[Status] [int] NULL,
	[CategoryAutoId] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdateBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[SeqNo] [int] NULL,
	[IsShow] [int] NULL,
 CONSTRAINT [PK_SubCategoryMaster] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD  DEFAULT (getdate()) FOR [UpdateDate]
GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD  DEFAULT ((0)) FOR [SeqNo]
GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD  DEFAULT ((1)) FOR [IsShow]
GO
ALTER TABLE [dbo].[SubCategoryMaster]  WITH NOCHECK ADD  CONSTRAINT [FK_SubCategoryMaster_CategoryMaster1] FOREIGN KEY([CategoryAutoId])
REFERENCES [dbo].[CategoryMaster] ([AutoId])
GO
ALTER TABLE [dbo].[SubCategoryMaster] CHECK CONSTRAINT [FK_SubCategoryMaster_CategoryMaster1]
GO
