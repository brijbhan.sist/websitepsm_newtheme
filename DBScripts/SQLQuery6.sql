CREATE TABLE [dbo].[WebCartMaster](
[AutoId] [int] IDENTITY(1,1) NOT NULL,
[UserId] [int] NULL,
[StorId] [int] NULL,
[ShippAddrId] [int] NULL,
[BillAddrId] [int] NULL,

[CartDate] [datetime] NULL  DEFAULT (getdate())
) ON [PRIMARY]
GO

   alter table [dbo].[WebCartMaster] add [TotalAmount]  AS ([dbo].[FN_Web_Cart_TotalAmount]([AutoId]))
 alter table [dbo].[WebCartMaster] add [MltaxRate]  AS ([dbo].[FN_Web_Cart_MLtaxApply]([AutoId]))
 alter table [dbo].[WebCartMaster] add [TotalMLqty]  AS ([dbo].[FN_Web_Cart_TotalMLqty]([AutoId]))
 alter table [dbo].[WebCartMaster] add [Mltax]  AS ([dbo].[FN_Web_Cart_Mltax]([AutoId]))
 alter table [dbo].[WebCartMaster] add [TotalWeightqty]  AS ([dbo].[FN_Web_Cart_TotalWeightqty]([AutoId]))
 alter table [dbo].[WebCartMaster] add [WeightTax]  AS ([dbo].[FN_Web_Cart_WeightTax]([AutoId]))
 alter table [dbo].[WebCartMaster] add [PaybleAmount]  AS ([dbo].[FN_Web_Cart_PaybleAmount]([AutoId]))