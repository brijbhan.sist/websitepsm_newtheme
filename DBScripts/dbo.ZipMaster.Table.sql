USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[ZipMaster]    Script Date: 01/03/2020 23:26:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZipMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CityId] [int] NULL,
	[ZoneId] [int] NULL,
	[Zipcode] [nvarchar](20) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_ZipMaster] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZipMaster]  WITH CHECK ADD  CONSTRAINT [FK_ZipMaster_CityMaster] FOREIGN KEY([CityId])
REFERENCES [dbo].[CityMaster] ([AutoId])
GO
ALTER TABLE [dbo].[ZipMaster] CHECK CONSTRAINT [FK_ZipMaster_CityMaster]
GO
