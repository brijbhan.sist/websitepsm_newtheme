USE [PriceSmart]
GO
/****** Object:  Table [dbo].[WebDraftCustomerPaymentMode]    Script Date: 01/13/2020 21:27:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebDraftCustomerPaymentMode](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAutoId] [int] NULL,
	[PaymentAutoId] [int] NULL,
	[IsDefault] [int] NULL
) ON [PRIMARY]
GO
