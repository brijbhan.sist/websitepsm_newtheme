Alter Procedure [dbo].[ProcWeb_PlaceOrder]  
 @UserId INT=NULL,  
 @StoreId INT=NULL,  
 @Opcode INT=NULL,  
 @OrderAutoId int=null,    
 @OrderId INT=NULL,   
 @OrderNumber varchar(50)=null,
 @PageIndex INT =1,                                  
 @PageSize INT=5,      
 @RecordCount INT=null, 
 @isException bit out,                 
 @exceptionMessage varchar(max) out    
AS  
BEGIN  
	SET @isException=0  
	SET @exceptionMessage=''  
	Declare @OrderNo varchar(15),@Terms int,@BillAutoId int,@ShipAutoId int,@State int,@ShippingCharges decimal(18,2),--@PaymentMethod int,@ShippingMethod int,  
	@SalesPersonAutoId Int,@TaxType int,@IsTaxApply INT=0,@CartId int,@PriceLevelAutoId int,@LogRemark varchar(Max)   
If @Opcode=101   
BEGIN
if exists(select ProductId from WebCartMaster as WCM inner join  WebCartItemMaster as WCIM on WCIM.CartAutoId=WCM.AutoId where WCM.StorId=@StoreId) 
     BEGIN                                                                                        
             SET @OrderNo = (SELECT [dbo].SequenceCodeGenerator('OrderNumber')) 
			 SELECT @BillAutoId=BillAddrId FROM WebCartMaster WHERE StorId=@StoreId       
			 SELECT @ShipAutoId=ShippAddrId FROM WebCartMaster WHERE StorId=@StoreId  

		BEGIN TRY                                                                                                                          
        BEGIN TRAN    
			 SELECT @ShippingCharges=case when WCM.PaybleAmount>WCS.MinimumOrderAmount then ISNULL(WCS.ShippingCharge,0) 
			 else ISNULL(WCS.ShippingCharge+WCS.ExtraShippingCharge,0) end from [PriceSmart].[dbo].WebCustomerShippingType  as WCS  
			 inner join WebCartMaster as WCM on  
			 WCS.ShippingAutoId=WCM.ShippingId and WCS.CustomerAutoId=WCM.StorId  Where  WCM.StorId=@StoreId   
                                                                                                      
			 SET @State=(SELECT TOP 1 State FROM [dbo].BillingAddress WHERE AutoId =@BillAutoId)   
			 SELECT @TaxType= AutoId FROM [dbo].TaxTypeMaster WHERE  State=@State                                            
			 declare @TaxValue decimal(10,2)=isnull((select Value from  [dbo].TaxTypeMaster where AutoId =@TaxType),0.00),
			 @CustomerType int=(SELECT cm.CustomerType  FROM  [dbo].CustomerMaster as cm WHERE AutoId=@StoreId)
			 SET @Terms=ISNUll((SELECT Terms  FROM  [dbo].CustomerMaster WHERE AutoId=@StoreId),0)  
			 SET @SalesPersonAutoId=(SELECT SalesPersonAutoId  FROM  [dbo].CustomerMaster WHERE AutoId=@StoreId)   
			 Select @CartId=AutoId from WebCartMaster Where StorId=@StoreId   
  
			IF EXISTS(SELECT top 1 * FROM  [dbo].TaxTypeMaster WHERE  State=@State)                                                                                        
			BEGIN    
				 SET @IsTaxApply=1                                                                                    
			END   
			ELSE  
			BEGIN  
				SET @IsTaxApply=0        
			END                                                                       
			INSERT INTO  [dbo].[OrderMaster] ([OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],                                                                                            
			[SalesPersonAutoId],OverallDiscAmt,[ShippingCharges],[Status],[ShippingType],                                                                                                                        
			TaxType,OrderRemarks,OrderType,TaxValue,mlTaxPer,IsTaxApply,Weigth_OZTax)      
                                                                                                                          
			SELECT @OrderNo,getdate(),null,@StoreId,@Terms,@BillAutoId,@ShipAutoId,@SalesPersonAutoId,0,@ShippingCharges,0,1,                             
			@TaxType,'',4,@TaxValue,ISNULL((SELECT TaxRate FROM  [dbo].MLTaxMaster where TaxState=@STATE),0.00),                                                                                        
			@IsTaxApply,isnull((select Value from  [dbo].Tax_Weigth_OZ),0)   
    
			SET @OrderAutoId=SCOPE_IDENTITY()  
                  
			INSERT INTO  [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],                                    
			[SRP],[GP],[Tax],IsExchange,TaxValue,UnitMLQty,isFreeItem,Weight_Oz,OM_CostPrice,OM_MinPrice,BasePrice)    
                                                                                                                          
			SELECT @OrderAutoId,wcm.ProductId,wcm.UnitAutoId,wcm.QtyPerUnit,wcm.UnitPrice,wcm.DefaultUnitQty,pm.P_SRP,0,
			0,0,@TaxValue,  
			wcm.mlPerQty,0,wcm.WeightperQty,PKD.CostPrice,  
			CASE WHEN @CustomerType=3 then PKD.CostPrice 
			     WHEN @CustomerType=2 then WHminPrice
				 else PKD.MinPrice end,PKD.Price
			FROM WebCartItemMaster as wcm 
			INNER JOIN  [dbo].ProductMaster as pm on  
			wcm.ProductId=pm.AutoId  
			INNER JOIN PackingDetails as PKD on
			pm.AutoId=PKD.ProductAutoId and pkd.UnitType=pm.PackingAutoId
			Where  CartAutoId=@CartId    
                  
				  
			INSERT INTO  [dbo].[Order_Original] ([AutoId],[OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],                                        
			[SalesPersonAutoId],[TotalAmount],[OverallDisc],[OverallDiscAmt],                                                          
			[ShippingCharges],[TotalTax],[GrandTotal],[ShippingType],                                                                                                                        
			TaxType,MLQty,MLTax)   
                                                                                                                    
			SELECT [AutoId],[OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],                                        
			[SalesPersonAutoId],[TotalAmount],[OverallDisc],[OverallDiscAmt],                                                          
			[ShippingCharges],[TotalTax],[GrandTotal],[ShippingType],                                                                                                                        
			TaxType,MLQty,MLTax from  
			 [dbo].[OrderMaster] WHERE AutoId=@OrderAutoId                                                                                                                        
                                                     
			UPDATE  [dbo].SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='OrderNumber'   
                                                                           
			INSERT INTO  [dbo].[OrderItems_Original]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],[TotalPieces],  
			[SRP],[GP],[Tax],[NetPrice],IsExchange,isFreeItem,UnitMLQty,TotalMLQty)   
                                          
			SELECT [OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],[TotalPieces],  
			[SRP],[GP],[Tax],[NetPrice],IsExchange,isFreeItem,UnitMLQty,TotalMLQty  
			FROM  [dbo].OrderItemMaster AS tb                                                                        
			inner join  [dbo].ProductMaster as pm on pm.AutoId=tb.ProductAutoId WHERE OrderAutoId=@OrderAutoId   
			SET @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM  [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @StoreId)   
  
			exec  [dbo].UPDTAE_PRICELEVEL                                                                                                                         
			@OrderAutoId=@OrderAutoId,                                                                                                         
			@PriceLevelAutoId=@PriceLevelAutoId          
                                         
			SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM  [dbo].[tbl_ActionMaster] where [AutoId]=1), '[OrderNo]', @OrderNo)     
                                                                                                                         
			INSERT INTO  [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                   
			VALUES(1,(Select SalesPersonAutoId from CustomerMaster  where AutoId=@StoreId),getdate(),@LogRemark,@OrderAutoId)  
                                                 
			DELETE FROM WebCartItemMaster WHERE CartAutoId=@CartId   
			DELETE FROM WebCartMaster WHERE StorId=@StoreId     
          
			update  [dbo].CustomerMaster set LastOrderDate = GETDATE() where AutoId =@StoreId  

			EXEC [dbo].[ProcWeb_PlaceOrder] @OrderAutoId=@OrderAutoId,@Opcode=102,@isException=0,@exceptionMessage=''  

			EXEC [dbo].[ProcWebPlaceOrderEmail] @OrderAutoId=@OrderAutoId,@StoreId=@StoreId
  
		COMMIT TRANSACTION                                                                                                                          
		END TRY                                                                                                                          
		BEGIN CATCH                                                                                                      
				ROLLBACK TRAN                                                                                               
					Set @isException=1                                                                         
					Set @exceptionMessage=ERROR_MESSAGE()                                                                     
        End Catch    		
     END    
ELSE
BEGIN
Set @isException=1                                                                         
Set @exceptionMessage='No item'   
END
END
IF @Opcode=102  
 BEGIN  
    declare @Url varchar(200)=null;
	set @Url=(select Url from  APIUrlDetails)

	Select  OM.OrderNo,OM.ShippingCharges,OM.Totaltax,OM.TotalAmount as SubTotal,
	case when OM.AdjustmentAmt>0 then ('<span>&#36;</span>'+Convert(varchar,OM.AdjustmentAmt)) when OM.AdjustmentAmt=0.00 then Convert(varchar,OM.AdjustmentAmt) else ('-<span>&#36;</span>'+Convert(varchar,ABS(OM.AdjustmentAmt))) end as AdjustmentAmt,
	TotalTax,PayableAmount as TotalAmount, MLTax,Weigth_OZTaxAmount as WeightTax,
	(  
	Select ((Select Url from APIUrlDetails)+ (case when PM.ThirdImage='' 
	then PM.ImageUrl else PM.ThirdImage end) )      as ImageUrl,PM.ProductId,PM.ProductName,UM.UnitType,OIM.RequiredQty,UnitPrice,NetPrice as Total,
	case when UnitTypeAutoId=3 then Convert(varchar,(TotalPieces/QtyPerUnit))+' '+(SELECT UnitType FROM UnitMaster Where AutoId=UnitTypeAutoId)
	else
	Convert(varchar,(TotalPieces/QtyPerUnit))+' '+(SELECT UnitType FROM UnitMaster Where AutoId=UnitTypeAutoId)+'<br/>('+Convert(varchar,TotalPieces)+' pieces)' end as DefQty
	from  [dbo].OrderItemMaster  as OIM  INNER JOIN  [dbo].ProductMaster as PM on  
	OIM.ProductAutoId=PM.AutoId  
	INNER JOIN  [dbo].UNitMaster as UM on  
	UM.AutoId=OIM.UnitTypeAutoId  
	Where OrderAutoid=OM.AutoId  
	for json path,include_null_values  
	) as ItemList from  [dbo].OrderMaster as OM  
	Where OM.AutoId=@OrderAutoId
	for json path,include_null_values 

 END  
IF @Opcode=103  
  BEGIN  
	SELECT ROW_NUMBER() OVER(ORDER BY OrderDate desc) AS RowNumber, OrderNo,FORMAt(OrderDate,'MM/dd/yyyy') as OrderDate,
	DeliveryDate,isnull(PayableAmount,0) as PayableAmount,TotalNOI as TotalItem,
	EM.FirstName+' '+EM.LastName as SalesPerson,CM.CustomerName,ST.ShippingType,SM.StatusType,SM.ColorCode,
	BA.Address+', '+BA.City+',<br/> '+S.StateName+', '+BA.Zipcode as Address into #Results FROM  OrderMaster AS OM   
	INNER JOIN  EmployeeMaster as EM on OM.SalesPersonAutoId=EM.AutoId AND EM.EmpType=2  
	INNER JOIN  CustomerMaster AS CM ON  OM.CustomerAutoId=CM.AutoId  
	INNER JOIN  ShippingType AS ST ON OM.ShippingType=ST.AutoId 
	INNER JOIN  StatusMaster as SM ON OM.Status=SM.AutoId and sm.Category=(case when OM.Status=0 then  'Website' else 'OrderMaster' end)
	INNER JOIN  BillingAddress AS BA ON  OM.BillAddrAutoId=BA.AutoId   
	INNER JOIN  State as S ON BA.State=S.AutoId  
	Where  OM.OrderType=4  and OM.customerAutoId=@StoreId order by OrderDate desc
  
  SELECT COUNT(OrderNo) AS RecordCount, case when @PageSize=0 then COUNT(OrderNo) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results                            
  
  SELECT *  FROM #Results                              
  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                            

 END  
IF @Opcode=104  
  BEGIN 
	select om.OrderNo,FORMAT(OrderDate,'MM/dd/yyyy') as OrderDate,OM.TotalAmount as SubTotal,TotalNOI as TotalItem,ST.ShippingType,
	EM.FirstName+' '+EM.LastName as SalesPerson,sa.Address+', '+sa.City+', '+S.StateName+' ,'+sa.Zipcode as Address,
	GrandTotal,SM.StatusType,SM.ColorCode,isnull(FORMAT(DeliveryDate,'MM/dd/yyyy'),'') as DeliveryDate
	From  OrderMaster as om
	INNER JOIN ShippingAddress as sa on sa.AutoId=om.shipAddrAutoId
	INNER JOIN  EmployeeMaster as EM on OM.SalesPersonAutoId=EM.AutoId AND EM.EmpType=2  
	INNER JOIN  StatusMaster as SM ON OM.Status=SM.AutoId and sm.Category=(case when OM.Status=0 then  'Website' else 'OrderMaster' end)
	INNER JOIN  State as S ON sa.State=S.AutoId  
	INNER JOIN  ShippingType AS ST ON OM.ShippingType=ST.AutoId 
	where  OrderNo=@OrderNumber order by OrderDate desc
	for json path,include_null_values   
 END

IF @Opcode=105  
  BEGIN  
	select (Select Url from APIUrlDetails)+PM.ThumbnailImageUrl as ImageUrl,PM.ProductName,PM.ProductId,UnitPrice,
	convert(varchar(20),RequiredQty)+' '+UM.UnitType+' '+case when UM.AutoId!=3 then '<br/>('+convert(varchar(20),TotalPieces)+' '+'Pieces'+')' else '' end
	as Qty,RequiredQty,TotalPieces,NetPrice,OM.TotalAmount as SubTotal,
	case when OM.AdjustmentAmt>0 then '<span>&#36;</span>'+Convert(varchar,OM.AdjustmentAmt) 
	else '-<span>&#36;</span>'+Convert(varchar,ABS(OM.AdjustmentAmt)) end as AdjustmentAmt,OM.OverallDiscAmt,
	TM.PrintLabel,OM.TotalTax,OM.MLTax,
	OM.Weigth_OZTaxAmount as WeigthTax,OM.ShippingCharges,OM.GrandTotal,oim.isFreeItem as IsFreeItem,
	oim.IsExchange,ISNULL(oim.Tax,0) as Tax
	from OrderItemMaster  as OIM 
	inner join OrderMaster as OM on OM.AutoId=OIM.OrderAutoId
	inner join ProductMaster as PM on PM.AutoId=OIM.ProductAutoId
	inner join PackingDetails as PD on PD.UnitType= PM.PackingAutoId and OIM.ProductAutoId=PD.ProductAutoId
	inner join UnitMaster as UM on UM.AutoId=PD.UnitType
	left JOIN BillingAddress as BA on BA.AutoId=OM.BillAddrAutoId
	left JOIN TaxTypeMaster as TM on BA.State=TM.State
	where  orderno=@OrderNumber 
  for json path,include_null_values  
 END
END  
GO
