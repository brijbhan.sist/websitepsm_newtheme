﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="TermsnCondition.aspx.cs" Inherits="TermsnCondition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="/Themes/css/about.css" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <main role="main" class="main">
        <div>
            <div id="aboutcontainer">
                <div class="container">
                    <div class="blocks-title" style="width: 230px">Terms and condition</div>
                    <div class="aboutus-content">
                        <div>
                            <p>
                                We are here for you to satisfy with the range of product mix of General Merchandised Products like, 
	Cosmetics, Toiletries, Novelties, house hold accessories etc. 
	Our aim is to give you the full range of products with smart price tag with the best after sales services.
	PRICING AND PACKAGING 
	All items are sold either in full case packs or when available, in the smaller inner case packs, at the indicated prices.
                            </p>
                            <p>
                                We do not open inner/case packs to repackage into smaller quantities to sell or provide item samples. 
	There are absolutely no exceptions! /Prices listed on our catalog generally match those in our warehouse, 
	but may vary. Price-Smart reserves the right to make changes to the selling prices at any time without prior notice.
                                
                            </p>
                            <p>
                                Please note that our prices on the website do not include taxes and delivery/shipping charges.
	Please refer to these abbreviations in our catalog. 
	(1)PK-Pack (2)DZ-Dozen (3)CS-Case (4)EA-Each (5)CD-card display ITEM DISPLAY/DESCRIPTION/COLORS
	our catalog always attempts to present and describe each item as accurately as possible. 
	However we cannot guarantee that the color and size you see will match the actual product's color.
	TYPOGRAPHICAL ERRORS Price-Smart always strives to provide product information as accurately as possible, 
	However typographical errors may occur. Price-Smart reserves the right to revoke any stated offer and to correct any errors, 
	inaccuracies or comissions after an order has been submitted, regardless of whether or not the order has been confirmed.
                            </p>
                            <p>
                                <b>ORDERING TERMS </b>
                                <br />
                                Minimum Order Total amount of the order must be at least a minimum of US $250 (excluding freight).
	Order Placement/ Acceptance payment method/ Please call 973-440-8499, 732-589-6972, 914-356-7627 to place an order 
	and all orders must be paid via COD. Method of delivery/ We do free shipping on delivery for order of $250 or more. 
	We do free on hand delivery in some area of NJ, please ask sales person for this option.
	Order Limitations/ Price-Smart reserves the right to limit quantities or reject any order you place with us without notice.
                            </p>
                            Processing Time/ Price-Smart is committed to processing orders promptly based on shipping method, order size and ship-to point.
                            <p></p>
                            <p>
                                <b>Other Terms </b>
                                <br />
                                Valid business license and/or tobacco license are required to open an account. You should be at least 18 years of age to do business with us. 
                           
                                If intended for anything other than legal use your sale will be denied.
                                    <br />
                                All customers are responsible for their local and state tobacco and sales taxes.
                                    <br />
                            </p>
                            <p>
                                <b>Disclaimer:</b> The above trademarks or registered trademarks of different products are their respective companies and the products images are sourced 
	by its top distribution channel. Price Smart LCC NJ–USA has no concern with its trademark or product photo itself because we sale products of behalf 
	of them and all the information and images are provided by them. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

</asp:Content>

