﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
 [System.Web.Script.Services.ScriptService]
public class Session : System.Web.Services.WebService {
    public Session () {
    }
    [WebMethod(EnableSession = true)]
    public  string session(string dataValue)
    {
        try
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            Session["StoreID"] = Convert.ToInt32(jdv["StoreID"]);
            Session["UserId"] = jdv["UserId"];
            Session["EmailId"] = jdv["Email"];
            Session["CustomerName"] = jdv["CustomerName"];
            Session["StoreName"] = jdv["StoreName"];
            Session["StoreStatus"] = Convert.ToInt32(jdv["StoreStatus"]);
            Session["TotalStore"] = Convert.ToInt32(jdv["TotalStore"]);
            Session["Location"] = jdv["Location"];
            Session["LocationStatus"] = Convert.ToInt32(jdv["LocationStatus"]); 
        }
        catch (Exception)
        {
            return "false";
        }
            return "success";
    }
    [WebMethod(EnableSession = true)]
    public string logout()
    {
        Session.Abandon();
        return "success";
    }


    [WebMethod(EnableSession = true)]
    public string AfterAddStore(string dataValue)
    {
        try
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            Session["StoreID"] = Convert.ToInt32(jdv["StoreID"]);
            Session["StoreName"] = jdv["StoreName"];
            Session["StoreStatus"] = Convert.ToInt32(jdv["StoreStatus"]);
            Session["TotalStore"] = Convert.ToInt32(jdv["TotalStore"]);
            Session["Location"] = jdv["Location"];
            Session["LocationStatus"] = Convert.ToInt32(jdv["LocationStatus"]); 
        }
        catch (Exception)
        {
            return "false";
        }
        return "success";
    }
}
