﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="StoreRegistration.aspx.cs" Inherits="StoreRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <span><a href="/Manage_Store">Home</a> &gt; </span>
    <span>Add Store</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <span class="cart-title"><strong>Add Store</strong></span>

    <div class="add-store clear-div">
        <div class="col clear-div">
            <div class="title"><strong>Store Details</strong></div>
            <label id="lblAutoId" style="display: none;"></label>
            <div class="col-info">
                <div class="form-row">
                    <label>Business Name</label>
                    <input type="text" id="txtBusinessName" maxlength="50" name="CompanyName" placeholder="Business Name" />
                </div>
                <div class="form-row">
                    <label>Store Name <span class="required">*</span></label>
                    <input type="text" id="txtStoreName" name="txtStoreName" maxlength="50" placeholder="Store Name" class="req" />
                </div>
                <div class="form-row" id="divStoreOpenTime">
                    <label>Store Open Time <span class="required">*</span></label>
                </div>
            </div>
            <div class="col-info">
                <div class="form-row">
                    <label>OTP License</label>
                    <input type="text" id="txtOTPLicense" name="txtOTPLicense" maxlength="15" placeholder="OTP License">
                </div>
                <div class="form-row">
                    <label>TAX ID</label>
                    <input type="text" id="txtTaxID" name="txtTaxID" maxlength="20" placeholder="TAX ID">
                </div>
                <div class="form-row" id="divStoreCloseTime">
                    <label>Store Close Time <span class="required">*</span></label>
                </div>
            </div>

        </div>

        <div class="hiddenShipAddress" style="display: none;"></div>
        <div class="hiddenBillAddress" style="display: none;"></div>
        <div class="col">
            <div class="title"><strong>Billing Address</strong></div>
            <div class="col-info">
                <div class="form-row">
                    <label>Address 1 <span class="required">*</span></label>
                    <input type="text" id="txtBillAddress1" name="txtAddress" placeholder="Address" class="req">
                </div>
                <div class="form-row">
                    <label>City <span class="required">*</span></label>
                    <input type="text" id="txtBillCity" name="txtCityName" placeholder="City Name" class="req" readonly="readonly">
                </div>

            </div>
            <div class="col-info">
                <div class="form-row">
                    <label>Address 2</label>
                    <input type="text" id="txtBillAddress2" name="Address2"  placeholder=" Address 2" />
                </div>

                <div class="form-row">
                    <label>Zip Code <span class="required">*</span></label>
                    <input type="text" id="txtBillZipcode" name="txtZIPCode" class="req" placeholder="Zip Code" readonly="readonly">
                </div>

            </div>
            <div class="col-info">
                <div class="form-row">
                    <label>State <span class="required">*</span></label>
                    <input type="text" id="txtBillState" name="txtState" class="req" placeholder="State" readonly="readonly">
                </div>
            </div>
            <div class="col-info" style="display: none">
                <div class="row">
                    <label>Latitude </label>
                    <input type="text" disabled id="txtBLatitude" name="Latitude" placeholder="Latitude" />
                </div>
                <div class="row">
                    <label>Longitude  </label>
                    <input type="text" disabled id="txtBLongitude" name="Longitude" placeholder="Longitude" />
                </div>
            </div>
        </div>

        <div class="ship-to-different-address">
            <input type="checkbox" id="rbSameAsBilling" name="ship_to_different_address">
            <label>Shipping address same as billing address</label>
        </div>

        <div class="col" id="divShippingDetails">
            <div class="title"><strong>Shipping Address</strong></div>
            <div class="col-info">
                <div class="form-row">
                    <label>Address 1 <span class="required">*</span></label>
                    <input type="text" id="txtShippingAddress1" name="Address1" placeholder="Address" class="req" />
                </div>
                <div class="form-row">
                    <label>City <span class="required">*</span></label>
                    <input type="text" id="txtShipCity" name="City" class="req" placeholder="City" readonly="readonly" />
                </div>


            </div>
            <div class="col-info">
                <div class="form-row">
                    <label>Address 2 </label>
                    <input type="text" id="txtShippingAddress2" name="Address2" placeholder="Address 2" />
                </div>
                <div class="form-row">
                    <label>Zip Code <span class="required">*</span></label>
                    <input type="text" id="txtShipZipCode" name="txtZIPCode1" class="req" placeholder="Zip Code" readonly="readonly">
                </div>

            </div>
            <div class="col-info">
                <div class="form-row">
                    <label>State <span class="required">*</span></label>
                    <input type="text" id="txtShipState" name="txtState1" class="req" placeholder="State" readonly="readonly">
                </div>
            </div>
            <div class="col-info" style="display: none;">
                <div class="form-row">
                    <label>Latitude </label>
                    <input type="text" disabled="disabled" id="txtSLatitude" name="Latitude" placeholder="Latitude" />
                </div>
                <div class="form-row">
                    <label>Longitude </label>
                    <input type="text" disabled="disabled" id="txtSLongitude" name="Longitude" placeholder="Longitude" />
                </div>
            </div>
        </div>

        <div class="col">
            <div class="title"><strong>Contact Details</strong></div>
            <button type="button" id="btnAddConatctdetails" class="button white" style="float: right;" data-toggle="modal" data-target="#modalContactDetails">Add</button>
            <br />
            <br />
            <table class="table store-list" id="tblContactDetails">
                <thead>
                    <tr class="table-heading">
                        <td class="Action">Contact Person</td>
                        <td class="SetAsDefault">Default</td>
                        <td class="Emails">Email ID</td>
                        <td class="Numbers">Mobile</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <input type="hidden" class="HContactperson" contactperson="'++'" />
        <div class="form-row" style="text-align: right; padding: 10px">
            <button type="button" id="btnSubmit" class="button white" onclick="AddStore()">Submit</button>
            <button type="button" id="btnUpdate" class="button white" onclick="UpdateStore()" style="display: none">Update</button>
        </div>
    </div>

    <div class="modal fade" id="messagePopup" style="display: none" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body" style="text-align: center; height: 177px; width: 305px;">
                    <i class="fa fa-check" aria-hidden="true" style="padding: 9px; border: 1px solid #17bbd6; border-radius: 56px; font-size: 25px; color: #17bbd6;"></i>
                    <br />
                    <label id="lblPopUpMessage" style="margin: 14px; font-size: 18px;"></label>
                    <br>
                    <a href="#" onclick="RestoreUserSession()" class="btn btn-md btn-info" data-dismiss="modal">OK</a>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8Y7FRVyPLlIEvF11qdFiD-ZWsf5OVIjs&libraries=places"></sc' + 'ript>');
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document.getElementById('txtBillAddress1'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.A;
                var longitude = place.geometry.location.F;
                var mesg = "Address: " + address;
                mesg += "\nLatitude: " + latitude;
                mesg += "\nLongitude: " + longitude;
                setBillingData(place);

            });
        });
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document.getElementById('txtShippingAddress1'));
            google.maps.event.addListener(places, 'place_changed', function () {

                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.A;
                var longitude = place.geometry.location.F;
                var mesg = "Address: " + address;
                mesg += "\nLatitude: " + latitude;
                mesg += "\nLongitude: " + longitude;
                // $("#txtShippingAddress").val(address);
                setshipingData(place);
            });
        });
    </script>
    <div class="modal fade" id="modalContactDetails" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="display: block; background: #d60000; color: white;">
                    <h4 class="modal-title">Contact Details</h4>
                    <label id="lblCAutoId" style="display: none;"></label>
                </div>
                <div class="modal-body">
                    <div class="add-contact clear-div">
                        <div class="col clear-div">
                            <div class="col-info">
                                <div class="form-row" id="divstoretype">
                                </div>
                                <div class="form-row">
                                    <label>Contact Person<span class="required">*&nbsp&nbsp</span></label>
                                    <input type="text" id="txtContactPerson" maxlength="50" name="Address1" placeholder=" Contact Person" class="creq" />
                                </div>
                                <div class="form-row">
                                    <label>Email ID<span class="required">*&nbsp&nbsp</span></label>
                                    <input type="text" name="email" id="txtEmail"  onblur="validateEmail(this)" placeholder="Email" class="creq"/>
                                </div>
                                <div class="form-row">
                                    <label>Alternate Email</label>
                                    <input type="text" name="email" id="txtAlternateEmail"  onblur="validateEmail(this)" placeholder="Alternative Email" />
                                </div>
                                <div class="form-row">
                                    <label>Set as default</label>
                                    <input type="radio" id="Default" name="Default" />
                                </div>
                            </div>
                            <div class="col-info">
                                <div class="form-row">
                                    <label>Mobile No <span class="required">*&nbsp&nbsp</span></label>
                                    <input type="text" name="Phone" id="txtPhone" maxlength="10" placeholder="Ex - 9999999999" class="creq" onkeypress='return isNumberKey(event)' />

                                </div>
                                <div class="form-row">
                                    <label>Landline No 1 <span class="required">*&nbsp&nbsp</span></label>
                                    <input type="text" name="Phone" id="txtLandlineno1" maxlength="10" placeholder="Ex - 9999999999" class="creq" onkeypress='return isNumberKey(event)' />
                                </div>
                                <div class="form-row">
                                    <label>Landline No 2    </label>
                                    <input type="text" name="Phone" id="txtLandlineno2" maxlength="10" onkeypress='return isNumberKey(event)' placeholder="Ex - 9999999999" class="textprop" />
                                </div>
                                <div class="form-row">
                                    <label>Fax No</label>
                                    <input type="text" name="Phone" id="txtFaxNo" maxlength="10" placeholder="Fax No" class="textprop" onkeypress='return isNumberKey(event)' />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnAdd" class="button white"  onclick="addcontactdetail();">Add</button>
                    <button type="button" id="btnUpdatecontact" class="button white"  onclick="UpdateContactdetails();" style="display: none">Update</button>
                    <button type="button" class="button white" title="close" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

