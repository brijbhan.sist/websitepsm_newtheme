﻿$(document).ready(function () {
    DeskMenu();
});
function DeskMenu() {
  
    var location = GetLocation(1);
    if ($("#hfLocation").val() == location) {
        location = "";
    }
    else {
        location = $("#hfLocation").val()
    }
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.Location = location
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CategoryList",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            $(".sub-menu").html('');
            var Category = response.d.responseData;
            var html = ''; var catid = 0;
            $.each(Category, function (i, item) {
                if (item.AutoId == catid && i > 0) {
                    html += '<li><a href="/ProductList.aspx?subCatAutoId=' + item.subcatAutoId + '">' + item.SubcategoryName + '</a></li>';
                }
                else {
                    if (i != 0) {                      
                        html += '</ul>';
                        html += '</li>';
                    }
                    html += '<li class="has-submenu"><a href="#">' + item.CategoryName + '</a>';
                    html += '<ul class="submenu">';
                    html += '<li><a href="/ProductList.aspx?subCatAutoId=' + item.subcatAutoId + '">' + item.SubcategoryName + '</a></li>';
                    catid = item.AutoId;
                }
            });
            $("#submenu").append(html);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}

function Mobilemenu() {
    var location = GetLocation(1);
    if ($("#hfLocation").val() == location) {
        location = "";
    }
    else {
        location = $("#hfLocation").val()
    }
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.Location = location
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CategoryList",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            $(".sub-menu").html('');
            var Category = response.d.responseData;
            var html = ''; var catid = 0;
            $.each(Category, function (i, item) {
                if (item.AutoId == catid && i > 0) {
                    html += '<li><a href="/ProductList.aspx?subCatAutoId=' + item.subcatAutoId + '">' + item.SubcategoryName + '</a></li>';
                }
                else {
                    if (i != 0) {
                        html += '</ul>';
                        html += '</li>';
                    }
                html += '<li class="has-submenu" onclick="jQuery()">';
                html += '<span href="javascipt:void(0)">' + item.CategoryName+'</span>';
                html += '<ul class="submenu">';
                html += '<li><a href="/ProductList.aspx?subCatAutoId=' + item.subcatAutoId + '">' + item.SubcategoryName + '</a></li>';
                html += '</li></ul>';
            });
            $(".submenu menus").append(html);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}