﻿var catLoadStatus = 0; prdtName = "", prdtId = "";
$(document).ready(function () {
    getCategoryList();
    if ($("#hfStoreId").val() != "0") {
        GetCartDetails();
    }

});
$(window).load(function () {
    $(".pageloader").fadeOut("slow");
});
function simplifyUrl(Name) {

    var str = "";
    str = Name.replace(/ /g, '-');
    str = str.replace('%', 'percent');
    str = str.replace('&', 'and');
    str = str.replace('+', 'plus');
    str = str.replace('/', '-');
    str = str.replace("'", '');
    str = str.replace('.', '-');
    str = str.replace('--', '-');
    str = str.replace('---', '-');
    str = str.replace('----', '-');
    str = str.replace('----', '-');
    str = str.replace("'s", '-s');
    str = str.replace("'S", '-s');
    str = str.replace("'", '-');
    return str.toLowerCase();;
}
function getUrlVariable(pagename) {

    var domain = window.location.host;
    var query = window.location.href, pageid = "";
    pageid = query.replace(domain + pagename + '/', '');
    pageid = pageid.split('-');
    pageid = pageid[pageid.length - 1];
    return pageid;
}
function getCategoryList() {

    var location = GetLocation(1);
    if ($("#hfLocation").val() == location) {
        location = GetLocation(2);
    }
    else {
        location = $("#hfLocation").val()
    }
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.Location = location
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CategoryList",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var Category = response.d.responseNewData;
            var html = ''; var catid = 0;
            var html1 = ''; var catid = 0, strCat = "", strSubCat = ""; 
            $.each(Category, function (i, item) {
                strCat = simplifyUrl(item.CategoryName);
                html += '<li class="has-submenu"><a href="#">' + item.CategoryName + '</a>';
                html += '<ul class="submenu">';
                $.each(item.SubcategoryName, function (i, item1) {
                    strSubCat = simplifyUrl(item1.SubcategoryName);
                    html += '<li><a href="/category/' + strCat + '-' + strSubCat + '-' + item1.subCatAutoId + '">' + item1.SubcategoryName + '</a></li>';
                });
                html += '</ul>';
                html += '</li>';
            })
            $.each(Category, function (i, item) {
                strCat = simplifyUrl(item.CategoryName);
                html1 += "<li class='has-submenu' onclick='getList(this)'>";
                html1 += "<span href='javascipt:void(0)'>" + item.CategoryName + "</span>";
                html1 += "<ul class='submenu'>";
                $.each(item.SubcategoryName, function (i, item1) {
                    strSubCat = simplifyUrl(item1.SubcategoryName);
                    html1 += "<li><a href='/category/" + strCat + "-" + strSubCat + "-" + item1.subCatAutoId + "'>" + item1.SubcategoryName + "</a></li>";
                });
                html1 += "</ul>";
                html1 += "</li>"; 
            })
            $("#submenu").append(html);
            $("#mob-menu-1").append(html1);
            $("#hfCatLoadStatus").val(1);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}
function getList(e,) {
    $("#mob-menu-1").attr('status', 1);
    var $this = $(e);
    $this.toggleClass('show').children('ul.submenu').slideToggle();
    $this.siblings('.has-submenu').removeClass('show').children('ul.submenu').slideUp();
    return false;
}
function getopenList(e,type) {
    var $this = $(e);
    if ($("#mob-menu-1").attr('status') == '0') {
        $this.toggleClass('show').children('ul.submenu').slideToggle();
        return false;
    }

    $this.siblings('.has-submenu').removeClass('show').children('ul.submenu').slideUp();
    $("#mob-menu-1").attr('status', type);
}
$("#anchorCat").click(function () {
    showCategory();
});
function showCategory() {
    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var data = {
        AccessToken: "a2d8fjhsdkfhsbd" + UserAutoId + "gxzn3d8xy7jewbc7x"
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CategoryList",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            $("#categoryListPopup").html('');
            var Category = response.d.responseData;
            var html = ''; var catid = 0;
            $.each(Category, function (i, item) {
                if (item.AutoId == catid && i > 0) {
                    //html += ' <li style="display:none;"><a href="/ProductList.aspx?subCatAutoId=' + item.subcatAutoId + '"><p id="subcatlist" >' + item.SubcategoryName + '</p></a></li>';
                    html += ' <li style="display:none;"><a href=/' + item.CategoryName + '-' + item.SubcategoryName + '-ProductList-' + subcatAutoId + '><p id="subcatlist" >' + item.SubcategoryName + '</p></a></li>';
                }
                else {
                    if (i != 0) {
                        html += '</ul>';
                    }
                    html += '<ul>';
                    html += '  <li><a href="#">' + item.CategoryName + '<i class="fa fa-plus"></i></a></li>';
                    //html += ' <li style="display:none;"><a href="/ProductList.aspx?subCatAutoId=' + item.subcatAutoId + '"><p id="subcatlist" >' + item.SubcategoryName + '</p></a></li>';
                    html += ' <li style="display:none;"><a href=/' + item.CategoryName + '-' + item.SubcategoryName + '-ProductList-' + subcatAutoId + '><p id="subcatlist" >' + item.SubcategoryName + '</p></a></li>';
                    catid = item.AutoId;
                }
            })
            $("#categoryListPopup").append(html);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}
function getRecentProduct(PageId) {
    var apiUrl = $("#ApiAppKey").html();
    var html = "";
    var StoreStatus = $("#hfStoreId").val();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var rc = new Object();
    rc.ProductId = PageId;
    rc.UserAutoId = UserAutoId;
    var data = {
        AccessToken: "a2d8fjhsdkfhsbd" + UserAutoId + "gxzn3d8xy7jewbc7x",
        requestContainer: rc
    }
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/ProductDescription",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                var ProductDetail = response.d.responseData;
                var InStock; var OutStock;
                html += '<div class="col-sm-12 col-lg-5">';
                html += ' <a href="/ProductDetails.aspx?ProductId=' + ProductDetail[0].ProductId + '">';
                html += "<img src='" + ProductDetail[0].ImageUrl + "' alt='viewimg'>";
                html += '</a></div>';
                html += ' <div class="col-sm-12 col-lg-7 detail">';
                html += '<span style="color: #95C3EF;font-weight: 700;">#' + ProductDetail[0].ProductId + '</span>';
                html += ' <h5>' + ProductDetail[0].ProductName + '</h5>';
                if (StoreStatus == "True") {
                    html += ' <h4>$<span id="spViewPrice">' + parseFloat(ProductDetail[0].Price).toFixed(2) + '</span></h4>';
                }
                html += '<label id="lblProductId" style="display:none">' + ProductDetail[0].AutoId + '</label>'
                html += '<ul class="brand">';
                html += '<li><span class="boltext">Sold in Lots of: ' + ProductDetail[0].UnitType + '(' + ProductDetail[0].Quantity + ')</span></li>';
                if (ProductDetail[0].BrandName != "") {
                    html += '<li>Brand: <span class="boltext">' + ProductDetail[0].BrandName + '</span></li>';
                }
                if (ProductDetail[0].AvailableInStock == 1) {
                    InStock = 'In Stock';
                    html += ' <li>Availability: <i class="fa fa-check-square-o"></i> <span class="boltext"  style="color:green">' + InStock + '</span></li>';
                }
                else {
                    OutStock = 'Out of Stock';
                    html += ' <li>Availability: <span class="boltext" style="color:red">' + OutStock + '</span></li>';
                }
                html += ' </ul>';
                html += '<h6>OverView</h6>'
                html += '<p>' + ProductDetail[0].Description + '</p>'
                if (StoreStatus == "True") {
                    html += '<ul class="incrtevalue">';
                    html += ' <li>';
                    html += '<input type="button" value="-" onclick="calculateMasterQty(this,1)" class="qtyminus button"  />';
                    html += '<input type="text" name="quantity" onchange="calculateMasterQty(this,2)" value=' + ProductDetail[0].Quantity + ' class="qty" />';
                    html += '<input type="button" value="+" onclick="calculateMasterQty(this,0)" class="qtyplus button"  />';
                    html += '</li>';
                    html += ' <li><a href="#" title="Add to Cart" class="button"><i class="fa fa-shopping-cart"></i>ADD TO CART</a></li>';
                    html += '</ul>';
                }
                html += '</div>';
                $("#RecentProduct").append(html);
            }
        }
    })
}
function calculateMasterQty(e, i) {
    var fieldName = $(e).closest('li').find('.qty');
    var enterQty = parseInt(fieldName.val(), 10);
    var apiUrl = $("#ApiAppKey").html();
    var html = "";
    var rc = new Object();
    rc.ProductId = $("#lblProductId").html();
    rc.Qauntity = enterQty;
    rc.Operation = i;
    rc.Location = $("#hfLocation").val()
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CalculatePrice",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                $('#spViewPrice').html(parseFloat(response.d.responseData[0].Price).toFixed(2));
                fieldName.val(response.d.responseData[0].Qauntity);
            }
        }
    })
}
function GetCartDetails() {

    var apiUrl = $("#ApiAppKey").html();
    var StoreId = parseInt($("#hfStoreId").val());
    var rc = new Object();
    rc.AutoId = parseInt($("#UserAutoId").val());
    rc.StoreId = StoreId;
    rc.Location = $("#hfLocation").val();
    rc.UserName = $("#hfEmail").val();
    rc.Token = getToken();
    var data = {
        rc: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CartDetails",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {

            if (response.d.response == "success") {
                var html = "";
                $(".cartfull").html('');
                if (response.d.responseData.length > 0) {
                    var List = response.d.responseData;
                    html += '<table>';
                    var count = 0;
                    $.each(List, function (i, item) {
                        count = Number(i) + 1;
                        html += '<tr>'
                        html += '<td style="width: 68px;"><a href="#">'
                        html += "<img src='" + item.ImageUrl + "' alt='imgname' style='height: 50px;width:50px'></a></td>"
                        html += '<td><a href="">' + item.ProductName + '</a></td>'
                        html += '<td>X'
                        html += ' <br>'
                        html += '' + item.OrderQty + '</td>'
                        html += '<td>$ ' + parseFloat(item.Price).toFixed(2) + '</td>'
                        html += ' <td> <button type="button" class="button" style="height:8px;width:8px" onclick="DeleteCartItem(' + item.AutoId + ')"><i class="fa fa-trash"></i></button></td>';
                        html += '</tr>'
                    })
                    html += '<tr>'
                    html += '<td style="text-align:right" colspan="3"><h5>Sub Total :</h5></td><td>';

                    if (count > 0) {
                        html += ' <h5>$' + parseFloat(List[0].TotalPrice).toFixed(2) + '</h5>'
                    }
                    html += '</td'
                    html += '</tr>'
                    html += '</table>'
                    $(".cartfull").append(html);
                    if (count > 0) {
                        $("#ViewCart").show();
                        $("#MobViewCart").show();
                        $("#deks-cart-count").html(List.length);
                        $("#mob-cart-count").html(List.length);
                        $(".cartfull").show();
                        $("#liCheckOut").show();
                        $("#search").removeClass('col-xs-12 col-sm-12 col-md-12');
                        $("#search").addClass('col-xs-8 col-sm-9 col-md-6');
                    }
                    else {
                        $("#ViewCart").hide();
                        $("#MobViewCart").hide();
                        $(".cartfull").hide();
                    }
                    $(".cartempty").hide();
                }
                else if (response.d.response == "Token") {
                    $("#search").removeClass('col-xs-8 col-sm-9 col-md-6');
                    $("#search").addClass('col-xs-12 col-sm-12 col-md-12');
                    $("#ViewCart").hide();
                    $("#MobViewCart").hide();
                    abandonSession();
                }
                else {
                    $("#ViewCart").hide();
                    $("#MobViewCart").hide();
                    $("#search").removeClass('col-xs-8 col-sm-9 col-md-6');
                    $("#search").addClass('col-xs-12 col-sm-12 col-md-12');
                }
            }
        }
    })
}
function DeleteCartItem(AutoId) {
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.AutoId = AutoId;
    rc.UserName = $("#hfEmail").val();
    rc.Token = getToken();
    rc.Location = $("#hfLocation").val();
    var data = {
        rc: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/DeleteCartDetails",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {

            if (response.d.response == "success") {
                GetCartDetails();
                toastr.success('Product has been deleted successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

            }
            else if (response.d.response == "Token") {
                toastr.success('Oops something went wrong.Please try later.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        },
        error: function (result) {
            console.log("", response.d.response, "error");
        },
        failure: function (result) {
            console.log("", response.d.response, "error");
        }
    });
}
function ChangeStore() {
    closeModal()
    var html = '';
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.Autoid = parseInt($("#UserAutoId").val()) || 0;
    rc.Location = GetLocation(1);
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Registration.asmx/ChangeStore",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {

            localStorage.setItem("response", JSON.stringify(response));
            if (response.d.response == "success") {
                if (response.d.responseNewData.length == 0) {
                    RestoreSession(response, 0, 0, null, 0, GetLocation(2), 0);
                }
                else if (response.d.responseNewData.length >= 1) {
                    if (response.d.responseNewData[0].StoreId == 0) {
                        RestoreSession(response, 0, 0, null, 0, GetLocation(2), 0);
                    }
                    else {
                        var StoreList = response.d.responseNewData;
                        html += '<ul class="list-group" id="tblChangeStoreList" style="margin-bottom: 0px !important;">'
                        $.each(StoreList, function (i, list) {
                            if (Number(list.StoreId) == Number($("#hfStoreId").val())) {
                                html += ' <li class="list-group-item"><input type="radio" checked LocationStatus=' + list.LocationStatus + ' Location=' + list.StoreLocation + ' StoreStatus=' + list.StoreStatus + ' alt=' + list.StoreId + ' name="flat" class="textprop" style="position: relative;top: 3px;opacity:1 !important">  <span class="spStoreName">' + list.StoreName + '</span></li>'
                            }
                            else {
                                html += ' <li class="list-group-item"><input type="radio" LocationStatus=' + list.LocationStatus + ' Location=' + list.StoreLocation + ' StoreStatus=' + list.StoreStatus + ' alt=' + list.StoreId + ' name="flat" class="textprop" style="position: relative;top: 3px;opacity:1 !important">  <span class="spStoreName">' + list.StoreName + '</span></li>'
                            }
                        })
                        html += '</ul>'
                        html += '<div class="form-row" style = "text-align: center;" >'
                        html += '<button name="login" class="button white" type="button" onclick="SelectStore()">Continue</button>'
                        html += '</div><br/>'
                        $("#divPopUpChangeStoreList").html(html);
                        $("#modalChangeStoreList").modal('show');
                    }
                }
            }
            else {
                $("#ErrorMessage").show();
                $("#ErrorMessage").html(response.d.responseMessage);
                setTimeout(function () {
                    var selectedEffect = 'blind';
                    var options = {};
                    $("#ErrorMessage").hide();
                }, 3000);
            }
        }
    })
}
function SelectStore() {
    var storeId = 0, storeName = '', Location = null, LocationStatus = 0;
    $("#tblChangeStoreList li").each(function () {
        if ($(this).find('.textprop').prop('checked') == true) {
            storeId = $(this).find('.textprop').attr('alt');
            storeName = $(this).find('.spStoreName').html();
            StoreStatus = $(this).find('.textprop').attr('StoreStatus');
            Location = $(this).find('.textprop').attr('Location');
            if (Location == "null") {
                Location = GetLocation(2);
            }
            LocationStatus = $(this).find('.textprop').attr('LocationStatus');
            return;
        }
    });
    if (storeId != 0) {
        var resp = JSON.parse(localStorage.getItem('response'));
        var length = resp.d.responseNewData.length;
        RestoreSession(storeId, length, storeName, StoreStatus, Location, LocationStatus);
        $("#modalChangeStoreList").modal('hide');
    }
    else {
        $("#ErrorMessage").show();
        $("#ErrorMessage").html("Please select store.");
        setTimeout(function () {
            var selectedEffect = 'blind';
            var options = {};
            $("#ErrorMessage").hide();
        }, 3000);
    }
}
function RestoreSession(storeId, TotalStore, storeName, StoreStatus, Location, LocationStatus) {
    var data = {
        StoreID: storeId,
        StoreName: storeName,
        StoreStatus: StoreStatus,
        TotalStore: TotalStore,
        Location: Location,
        LocationStatus: LocationStatus
    }
    $.ajax({
        type: "POST",
        url: "/Session.asmx/AfterAddStore",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (result) {
            if (result.d == "success") {
                if (TotalStore != 0) {
                    location.href = "Manage_Store";
                }
                else {
                    location.href = "Store_Registration";
                }
            }
            else {
                location.href = "/home";
            }
        }
    })
}
function abandonSession() {
    var apiUrl = $("#ApiAppKey").html();
    var Token = getToken();
    var rc = new Object();
    rc.Location = GetLocation(1);
    rc.UserName = $("#hfEmail").val();
    rc.Token = Token;
    var data = {
        cr: rc
    };
    debugger;
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Login.asmx/CustomerLogout",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (result) {
            if (result.d.response == "Logout") {
                $.ajax({
                    type: "POST",
                    url: "/Session.asmx/logout",
                    data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    beforeSend: function () {
                    },
                    complete: function () {
                    },
                    success: function (result) {
                        if (result.d == "success") {
                            $("#usrStng").hide();
                            localStorage.clear();
                            window.location.href = '/login';
                        }
                    }
                })
            }
        }
    })
}
function getToken() {
    var Token = "";
    if (localStorage.getItem('Token') != null) {
        Token = localStorage.getItem('Token');
    }
    return Token
}
function setToken(Token) {
    localStorage.setItem('Token', Token);
}
function Implement() {
    var domain = "";
    domain = $(location).attr('host');
    domain = domain.split(':', 0);
    if (domain == "locahost") {

    }
}
function GetLocation(num) {
    var location = "";
    if (num == 1) {
        location = "pricesmart";
    }
    else if (num == 2) {
        location = $('#hfLocation').val();
    }
    return location;
}
function WriteConsoleLog(e) {
    print(e);
}

$('#filter_keyword').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});

function SerachProductNameByBtn() {
    debugger
    var ProductName = $("#filter_keyword").val();
    if (ProductName == '') {
        return false;
    }
    if (prdtName != "" && prdtId!="") {
        var productN = simplifyUrl(prdtName);
        window.location.href = "/product/" + productN + "-" + prdtId;
    }
    else {
        toastr.error('Invalid search keyword minimum 3 digit required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function SerachProductName() {
    debugger
    var location = GetLocation(1);
    if ($("#hfLocation").val() == location) {
        location = GetLocation(2);
    }
    else {
        location = $("#hfLocation").val()
    }
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.Location = location,
        rc.ProductName = $("#filter_keyword").val();
    var data = {
        cr: rc
    };
    $("#filter_keyword").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: apiUrl + "/WebApi/SerachProduct.asmx/SearcProduct",
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json",
                type: "POST",
                success: function (data) {
                    if (data.d.responseData.length != 0) {
                        response($.map(data.d.responseData, function (item) {
                            prdtId = data.d.responseData[0].ProductId;
                            prdtName = data.d.responseData[0].ProductName;
                            return {
                                label: item.ProductId + '-' + item.ProductName,
                                val: item.ProductId
                            }
                        }))
                    } else {
                        var blankArr = [];
                        var Blankdata = {
                            "AutoId": 0,
                            "Location": null,
                            "ProductId": 30101,
                            "ProductName": "JUUL Pods 4PK Cool Mint (5%)",
                            "ProductName": "JUUL Pods 4PK Cool Mint (5%)",
                            "__type": "DllProductsearch.Productsearch"
                        }
                        blankArr.push(Blankdata);
                        response($.map(blankArr, function (item) {
                            return {
                                label: "Product not found",
                                val: ""
                            }
                        }))
                    }
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        }, open: function () {
            $(this).autocomplete('widget').zIndex(1000);
        },
        select: function (e, i) {

            if (i.item.val != "") {
                var text = this.value.split(/,\s*/);
                this.value = text.join("");
                var value = $("#hfProducId").val().split(/,\s*/);
                value.pop();
                value.push(i.item.val);
                $("#hfProducId")[0].value = value.join("");

                var productN = simplifyUrl(i.item.label);

                productN = productN.replace(i.item.val + "-", "");

                // html += ' <a href="/product/' + productN + '-' + i.item.val + '" class="proimg">';
                window.location.href = "/product/" + productN + '-' + i.item.val;
                return false;
            } else {
                $("#filter_keyword").val('');
                return false;
            }


        },
        minLength: 3
    });
    $('.ui-autocomplete').css('max-height', '200px');
    $('.ui-autocomplete').css('overflow', 'auto');
}


$('#Mfilter_keyword').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});

function SerachProductNameByBtn1() {
    var ProductName = $("#Mfilter_keyword").val();
    if (ProductName == '') {
        return false;
    }
    window.location.href = "/product/" + ProductName
}
function SerachProductName1() {

    var location = GetLocation(1);
    if ($("#hfLocation").val() == location) {
        location = GetLocation(2);
    }
    else {
        location = $("#hfLocation").val()
    }
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.Location = location,
        rc.ProductName = $("#Mfilter_keyword").val();
    var data = {
        cr: rc
    };
    $("#Mfilter_keyword").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: apiUrl + "/WebApi/SerachProduct.asmx/SearcProduct",
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json",
                type: "POST",
                success: function (data) {
                    if (data.d.responseData.length != 0) {
                        response($.map(data.d.responseData, function (item) {
                            return {
                                label: item.ProductId + '-' + item.ProductName,
                                val: item.ProductId
                            }
                        }))
                    } else {
                        var blankArr = [];
                        var Blankdata = {
                            "AutoId": 0,
                            "Location": null,
                            "ProductId": 30101,
                            "ProductName": "JUUL Pods 4PK Cool Mint (5%)",
                            "ProductName": "JUUL Pods 4PK Cool Mint (5%)",
                            "__type": "DllProductsearch.Productsearch"
                        }
                        blankArr.push(Blankdata);
                        response($.map(blankArr, function (item) {
                            return {
                                label: "Product not found",
                                val: ""
                            }
                        }))
                    }
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        }, open: function () {
            $(this).autocomplete('widget').zIndex(1000);
        },
        select: function (e, i) {

            if (i.item.val != "") {
                var text = this.value.split(/,\s*/);
                this.value = text.join("");
                var value = $("#hfProducId").val().split(/,\s*/);
                value.pop();
                value.push(i.item.val);
                $("#hfProducId")[0].value = value.join("");

                var productN = simplifyUrl(i.item.label);

                productN = productN.replace(i.item.val + "-", "");

                // html += ' <a href="/product/' + productN + '-' + i.item.val + '" class="proimg">';
                window.location.href = "/product/" + productN + '-' + i.item.val;
                return false;
            } else {
                $("#filter_keyword").val('');
                return false;
            }


        },
        minLength: 3
    });
    $('.ui-autocomplete').css('max-height', '200px');
    $('.ui-autocomplete').css('overflow', 'auto');
}

function calculateQty(e, i) {
    var fieldName = $(e).closest('div').find('.qty');
    var enterQty = parseInt(fieldName.val(), 10)|| 0;
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.ProductId = $(e).closest('.item').find('.add_to_cart').attr('alt');
    console.log($(e).closest('.item').find('.add_to_cart').attr('alt'))
    rc.Qauntity = enterQty;
    rc.Operation = i;
    rc.UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    rc.Location = $("#hfLocation").val();
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CalculatePrice",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                $(e).closest('.item').find('.itemprice').html('<span>&#36;</span>' + parseFloat(response.d.responseData[0].Price).toFixed(2));
                fieldName.val(response.d.responseData[0].Qauntity);
            }
        }
    })
}
function AddToCart(e, i) {
    var fieldName;
    var rc = new Object();
    if (i == 0) {
        fieldName = $(e).closest('.item').find('.qty');
        rc.ProductId = $(e).closest('.item').find('.add_to_cart').attr('alt');
    }
    else {
        fieldName = $(e).closest('.item').find('.qty');
        rc.ProductId = $(e).closest('.item').find('.qty').attr('alt');
    }
    var enterQty = parseInt(fieldName.val(), 10);
    var apiUrl = $("#ApiAppKey").html();
    var StoreId = parseInt($("#hfStoreId").val());
    rc.Qauntity = enterQty;
    rc.AutoId = parseInt($("#UserAutoId").val()) || 0;
    rc.StoreId = StoreId;
    rc.Location = $("#hfLocation").val();
    rc.UserName = $("#hfEmail").val();
    rc.Token = getToken();
    var data = {
        cl: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/AddToCart",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
            else if (response.d.response == "success") {
                GetCartDetails();
                toastr.success('Product has been added successfully.', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
            else
                toastr.error(response.d.responseMessage, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    })
}