﻿$(document).ready(function () {

    maxDateget();

});


function validateEmail(elementValue) {
    var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var check = EmailCodePattern.test($(elementValue).val());
    if (!check) {
        toastr.error('Invalid Email ID.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $(elementValue).val('');
    }
}

function SendMail() {
    if (checkRequiredField()) {
        var apiUrl = $("#ApiAppKey").html();
        var rc = new Object();
        rc.Name = $("#txtName").val();
        rc.Email = $("#txtEmail").val();
        rc.Query = $("#txtQuery").val();
        rc.QueryType = $("#ddltype").val();
        rc.Date = $("#birth_date").val();
        rc.PhoneNo = $("#txtPhoneNo").val();
        rc.Location = GetLocation(1);
        var data = {
            cr: rc
        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/Contact.asmx/ContactQuery",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                if (response.d.response == "success") {
                    swal("", response.d.responseMessage, "success")                    
                    $("#txtName").val('');
                    $("#txtEmail").val('');
                    $("#txtQuery").val('');
                    $("#ddltype").val('0')
                    $("#birth_date").val('');
                    $("#txtPhoneNo").val('');
                }
                else {
                    swal("", response.d.responseMessage, "success") 
                }
            }
        })
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


function maxDateget() {
    debugger;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear() + 20;
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("birth_date").setAttribute("max", today);

}