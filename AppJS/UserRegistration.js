﻿$(document).on('keypress', '#txtFirstName', function (event) {
    var regex = new RegExp("^[a-zA-Z ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});
$(document).on('keypress', '#txtLastName', function (event) {
    var regex = new RegExp("^[a-zA-Z ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});
function TempUserRegistration() {
    var elementValue = $("#txtEmail").val();
    var Phone = $("#txtMobile").val();
    if (checkRequiredFieldreg()) {
        if (Phone.length != 10) {
            toastr.error('Please enter 10  digit mobile number.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return false
        }
        if (elementValue != "") {
            var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var check = EmailCodePattern.test(elementValue);
            if (!check) {
                toastr.error('Please enter a valid Email ID.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return false
            }
        }
        if ($("#txtPassword").val() != $("#txtConfirmPassword").val()) {//condition added on 11/30/2019 By Rizwan Ahmad
            toastr.error('Password do not match.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return false
        }
        var apiUrl = $("#ApiAppKey").html();
        var rc = new Object();
        rc.FirstName = $("#txtFirstName").val(),
            rc.LastName = $("#txtLastName").val(),
            rc.Phone = $("#txtMobile").val(),
            rc.Email = $("#txtEmail").val(),
            rc.Password = $("#txtPassword").val(),
            rc.ConfirmPassword = $("#txtConfirmPassword").val(),
            rc.Location = GetLocation(2);
        var data = {
            cr: rc
        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/Registration.asmx/TempUserRegistration",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                if (response.d.response == "success") {
                    if (response.d.responseMessage == "EmailIdExist") {
                        swal("", "Email ID already exists (" + $("#txtEmail").val() + ") Try another.", "warning");
                    }
                    else {
                        $("#pMessage").html('Please enter 6 digit otp which is sent on your register <span style="font-weight: 600;margin-right: 5px;">' + $("#txtEmail").val() + '</span>Email ID');
                        $("#VerifyOTP").show();
                        $("#userinformation").hide();
                    }
                }
                else {
                    swal("", "Oops, Something went wrong.Please try later.", "error");
                }
            }
        })
    }
    else {
        toastr.error('All fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function AddUser() {
    var elementValue = $("#txtEmail").val();
    var Phone = $("#txtMobile").val();
    $("#BtnAddUser").attr('disabled', true);
    if (checkRequiredFieldreg()) {
        if ($("#txtPassword").val() != $("#txtConfirmPassword").val()) {//condition added on 11/30/2019 By Rizwan Ahmad
            toastr.error('Password do not match.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#BtnAddUser").attr('disabled', false);
            return false
        }
        if (Phone.length != 10) {
            $("#txtMobile").addClass('border-warning');
            toastr.error('Please enter 10  digit mobile number.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#BtnAddUser").attr('disabled', false);
            return false
        }
        if (elementValue != "") {
            var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var check = EmailCodePattern.test(elementValue);
            if (!check) {
                toastr.error('Please enter a valid Email ID.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#txtEmail").addClass('border-warning');
                $("#BtnAddUser").attr('disabled', false);
                return false
            }
        }

        var apiUrl = $("#ApiAppKey").html();
        var rc = new Object();
        rc.FirstName = $("#txtFirstName").val(),
            rc.LastName = $("#txtLastName").val(),
            rc.Phone = $("#txtMobile").val(),
            rc.Email = $("#txtEmail").val(),
            rc.Password = $("#txtPassword").val(),
            rc.ConfirmPassword = $("#txtConfirmPassword").val(),
            rc.Location = GetLocation(2);
        var data = {
            cr: rc
        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/Registration.asmx/UserRegistration",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                if (response.d.response == "success") {
                    swal({
                        title: "",
                        text: "Register Successfully.",
                        icon: "success",
                        showCancelButton: false,
                        buttons: {
                            confirm: {
                                text: "Ok",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        }
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            DeirectLogin(rc.Email, rc.Password);
                        }
                    });
                }
                else {
                    swal("", response.d.responseMessage, "error");
                    $("#BtnAddUser").attr('disabled', false);
                }
            }
        })
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function reset() {
    $('input[type="text"]').removeClass('border-warning');
    $("#txtEmail").removeClass('border-warning');
    $("#ddlStores").removeClass('border-warning');
    $('input[type="text"]').val('');
    $('input[type="password"]').val('');
    $("#txtEmail").val('');
    $("#BtnAddUser").attr('disabled', false);
}
function validateEmail() {
    var elementValue = $("#txtEmail").val();
    if (elementValue != "") {
        var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var check = EmailCodePattern.test(elementValue);
        if (!check) {
            toastr.error('Please enter a valid Email ID.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        return (elementValue).toLowerCase().toString();
    }
}
function DeirectLogin(Email, Password) {
    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = Email;
    var rc = new Object();
    rc.UserName = Email,
        rc.Password = Password,
        rc.Location = GetLocation(1);
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Registration.asmx/CustomerLogin",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                var Token = response.d.responseNewData[0].Token;
                setToken(Token);
                storeUserSession(response, 0, 0, null, 0, GetLocation(2), 0);
            }
            else {
                $("#ErrorMessage").show();
                $("#ErrorMessage").html(response.d.responseMessage);
                setTimeout(function () {
                    var selectedEffect = 'blind';
                    var options = {};
                    $("#ErrorMessage").hide();
                }, 3000);
            }
        }
    })
}
function storeUserSession(response, storeId, TotalStore, storeName, StoreStatus, Location, LocationStatus) {
    var data = {
        StoreID: storeId,
        UserId: response.d.responseNewData[0].UserId,
        Email: response.d.responseNewData[0].Email,
        StoreName: storeName,
        StoreStatus: StoreStatus,
        TotalStore: TotalStore,
        Location: Location,
        LocationStatus: LocationStatus,
        CustomerName: response.d.responseNewData[0].UserName
    }
    $.ajax({
        type: "POST",
        url: "/Session.asmx/session",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (result) {
            if (result.d == "success") {
                if (TotalStore != 0) {
                    location.href = "/User/Manage_Store";
                }
                else {
                    location.href = "/Store_Registration";
                }
            }
            else {
                $("#ErrorMessage").show();
                $("#ErrorMessage").html("Oops something went wrong.Please try later");
                setTimeout(function () {
                    var selectedEffect = 'blind';
                    var options = {};
                    $("#ErrorMessage").hide();
                }, 3000);
            }
        }
    })
}
function checkRequiredFieldreg() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '0.00') {
            boolcheck = false;
            $(this).addClass('border-warning');
            $("#BtnAddUser").attr('disabled', false);
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
function VerifyOTP() {
    if ($("#txtSecurityCode").val() == "") {
        swal("", "Please enter OTP.", "error");
    }
    else {
        var apiUrl = $("#ApiAppKey").html();
        var rc = new Object();
        rc.Email = $("#txtEmail").val(),
            rc.OTP = $("#txtSecurityCode").val()
        rc.Location = GetLocation(2);
        var data = {
            cr: rc
        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/Registration.asmx/VerifyOTP",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                if (response.d.response == "success") {
                    if (response.d.responseMessage == "Valid") {
                        AddUser();
                    }
                }
                else {
                    swal("", response.d.responseMessage, "error");
                    $("#txtSecurityCode").val() == "";
                }
            }
        })
    }
}