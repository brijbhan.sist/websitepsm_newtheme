﻿
var PageId = 0;
$(document).ready(function () {
    PageId = getUrlVariable('product');
    getProductDetails(PageId);
    getProductList();
    localStorage.setItem("ProductId", PageId);
});
function getProductDetails(PageId) {
    var location = GetLocation(1);
    if ($("#hfLocation").val() == location) {
        location = "";
    }
    else {
        location = $("#hfLocation").val()
    }
    var StoreId = parseInt($("#hfStoreId").val())||0;
    var apiUrl = $("#ApiAppKey").html();
    var html = "", html1 = "", html2 = "";
    var StoreStatus = $("#hfStoreStatus").val() || 0;
    var rc = new Object();
    rc.StoreId = StoreId;
    rc.ProductId = PageId;
    rc.Location = location;
    rc.StoreStatus = StoreStatus;
    var data = {
        requestContainer: rc
    }
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/ProductDescription",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        async: false,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                $("#productDetails").html('');
                var ProductDetail = response.d.responseNewData;
                var imagedetails = ProductDetail[0].ImageList;
                if (ProductDetail.length != 0) {
                    localStorage.setItem("subcatid", ProductDetail[0].SubcategoryAutoId);
                    $.each(ProductDetail, function (i, item) {
                        html1 += '<div class="breadcrumbs">'
                        html1 += '<span> <a href="/">Home</a> &gt; </span>'
                        html1 += '<span><a href="" id="spCategory">' + item.CategoryName + '</a> &gt;</span>'
                        html1 += '<span><a href="/category/' + item.CategoryName + '-' + item.SubcategoryName + '-' + item.SubcategoryAutoId + '"" id="spSubCategory">' + item.SubcategoryName + '</a> &gt;</span>'
                        html1 += '<span id="spProductName">' + item.ProductName + '</span>'
                        html1 += '</div>'

                        html += '<div class="item-left">';
                        html += '<ul id="item_slider">';
                        html += '</ul>';
                        html += '</div>';
                        html += '<div class="item-right">'
                        html += '<span class="itemid" prdtid=' + item.AutoId + '>#' + item.ProductId + '</span>';
                        html += '<p class="maintitle" style="padding:22px 0 22px">' + item.ProductName + '</p>';
                        html += '<div>';
                        if (item.UnitType == "Piece") {
                            html += '<div class="subtitle"><strong>Sold in: </strong><span>Peice (' + item.Quantity + ')</span></div>';
                        } else {
                            html += '<div class="subtitle"><strong>Sold in: </strong><span>' + item.UnitType + ' (' + item.Quantity + ' Pieces)</span></div>';
                        }
                        html += '<div class="subtitle"><strong>Brand: </strong><span>' + item.BrandName + '</span></div>';
                        if (item.AvailableInStock == 1) {

                            html += '<div class="subtitle"><strong>Stock: </strong><span style="color:#ff0000">Out of Stock</span></div>';
                        }

                        html += '</div>';
                        if (StoreStatus > 0) {
                            html += '<div id="spPrice" class="itemprice">$' + parseFloat(item.MainPrice).toFixed(2) + '</div>';
                            if (item.AvailableInStock == 0) {
                                html += '<div class="clear-div quantity ">';
                                html += '<div class="input-quantity input-quantity-large">';
                                html += '<button type="button" onclick="QtycalculateQty(this,1,0)" class="minus">--</button>';
                                html += '<div>';
                                html += '<input type="number" onchange="QtycalculateQty(this,2,0)" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="qty" max=' + (item.UnitType == "Piece" ? 99999 : (item.Quantity * 500)) + ' min="' + item.Quantity + '" step="' + item.Quantity + '" name="quantity" value="' + item.Quantity + '" inputmode="numeric"  maxlength="6"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>';
                                html += '</div>';
                                html += '<button type="button" onclick="QtycalculateQty(this,0,0)" class="plus">+</button>';
                                html += '</div>';
                                html += '<div class="add_to_cart" alt=' + item.AutoId + '><button type="button" class="button added_to_cart" onclick="AddToCart(this,0)">Add to Cart</button></div>';
                                html += '</div>';
                            }

                        }
                        if (item.Description != null && item.Description.trim()!='') {
                            html += ' <div class="description">';
                            html += ' <div class="description-title">Product Description:</div>';
                            html += ' <div class="productdescription">'
                            html += item.Description;
                            html += ' </div>';
                            html += '</div>';
                        }
                        html += '</div>'
                        html2 += '<li><a href="#"><img src="' + item.ImageUrl + '" OnError="this.src=\'https://image.mypsm.net/Attachments/default_pic.png\'" /></a></li>';
                        if (item.ImageList != null) {
                            $.each(item.ImageList, function (J, item1) {
                                html2 += '<li><a href="#"><img src="' + item1.ImageUrl + '" OnError="this.src=\'https://image.mypsm.net/Attachments/default_pic.png\'" /></a></li>';
                            });
                        }
                    });


                    $("#breadcrumbsdata").html(html1);
                    $("#productDetails").html(html);
                    $("#item_slider").html(html2);
                    $("#item_slider").lightSlider({
                        gallery: false,
                        item: 1,
                        slideMargin: 0,
                        speed: 700,
                        cssEasing: "ease-in",
                        loop: true,
                        vertical: false,
                        controls: true,
                        auto: false,
                        pause: 4000,
                        pager: false,
                        prevHtml: '<div class="slider-left">&nbsp;</div>',
                        nextHtml: '<div class="slider-right">&nbsp;</div>',
                    });
                }
                else {
                    $("#noDataFound").show();
                    $("#proDetails").hide();
                    $("#EmptyTable").show();
                    $("#proDetails").hide();
                    $(".breadcum").hide();
                }
            }
        },
        error: function (response) {
            $("#noDataFound").show();
            $("#proDetails").hide();
            $(".breadcum").hide();
        },
        failure: function (response) {
            ;
        }
    })
}
function getProductList() {
    var subcatid = localStorage.getItem("subcatid");
    var apiUrl = $("#ApiAppKey").html();
    var StoreStatus = $("#hfStoreStatus").val() || 0;
    var StoreId = parseInt($("#hfStoreId").val())||0;
    var rc = new Object();
    rc.StoreId = StoreId;
    rc.CategoryAutoId = 0;
    rc.SubcategoryAutoId = subcatid;
    rc.PageIndex = 1;
    rc.PageSize = 0;
    rc.StoreStatus = StoreStatus;
    rc.Location = $("#hfLocation").val()
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/ProductListing",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                var ProductDetail = response.d.responseData;
                var List = ProductDetail[0].ProductListDetails;
                var html = '';
                html = ' <ul class="clear-div itemdiv">';
                $.each(List, function (i, item) {
                    var ProductN = simplifyUrl(item.ProductName);
                    if (item.AvailableInStock == 1) {
                        html += '<li class="item outofstock" alt=' + item.Quantity + '">';
                    }
                    else {
                        html += '<li class="item" alt=' + item.Quantity + '">';
                    }
                    html += '<div>';
                    html += "<a href='/product/" + ProductN + "-" + item.ProductId + "'>";
                    html += "<center><div style='height:234px;display: table-cell;vertical-align: middle;'>";
                    html += "<img src=\'" + item.ImageUrl + "' style='height: auto;max-height:225px' alt='" + item.ProductName + "' OnError='this.src =\"https://image.mypsm.net/Attachments/default_pic.png\"' />";
                    html += '</div></center>';
                    html += '<span>' + item.ProductName + '</span>';
                    html += '</a>';
                    if (StoreStatus > 0) {
                        html += ' <div class="itemprice"><span>&#36;</span>' + parseFloat(item.Price).toFixed(2) + '</div>';
                        html += '<div class="clear-div quantity">';
                        if (item.AvailableInStock == 0) {
                            html += '<div class="input-quantity">';
                            html += '<button type="button" onclick="calculateQty(this,1,1)" class="minus">--</button>';
                            html += ' <div>';
                            html += '<input type="number"  alt=' + item.AutoId + ' class="qty" onchange="calculateQty(this,2,1)" name="quantity" step="10"  value="' + item.Quantity + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" inputmode="numeric" maxlength="6"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>';
                            html += '</div>';
                            html += '<button type="button" onclick="calculateQty(this,0,1)" class="plus">+</button>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="add_to_cart" alt=' + item.AutoId + '>';
                            html += '<button type="button" onclick="AddToCart(this,0)" class="button white">Add To Cart</button>';
                        }
                        else {
                            html += '<div class="input-quantity">';
                            html += '<button type="button" disabled="disabled" class="minus" style="cursor: not-allowed;">--</button>';
                            html += ' <div>';
                            html += '<input type="number" disabled="disabled" style="cursor: not-allowed;" alt=' + item.AutoId + ' class="qty" name="quantity" step="10" maxlength="3" value="' + item.Quantity + '" inputmode="numeric" />';
                            html += '</div>';
                            html += '<button type="button" disabled="disabled" style="cursor: not-allowed;" class="plus">+</button>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="add_to_cart" alt=' + item.AutoId + '>';
                            html += "<a href='/product/" + ProductN + "-" + item.ProductId + "' title='View' class='button white'>View Details</a>";
                        }
                        html += '</div>';
                    }
                    else {
                        html += "<a href='/product/" + ProductN + "-" + item.ProductId + "' title='View' class='button white'>View Details</a>";
                    }
                    html += '</div>';
                    html += '</li>';
                });
                html += '</u>';
                $("#item").append(html);
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}
function QtycalculateQty(e, i, j) {
    var fieldName = 0, enterQty = 0, apiUrl = "";
    var rc = new Object();
    if (j == 0) {
        fieldName = $(e).closest('.iteminfo').find('.qty');
        enterQty = parseInt(fieldName.val(), 10);
        rc.ProductId = $(".itemid").attr('prdtid');
        rc.Qauntity = enterQty;
    }
    else {
        fieldName = $(e).closest('.clear-div').find('.qty');
        enterQty = parseInt(fieldName.val(), 10);
        rc.ProductId = $(e).closest('.clear-div').find('.qty').attr('alt');
        rc.Qauntity = enterQty;
    }
    apiUrl = $("#ApiAppKey").html();
    rc.Location = $("#hfLocation").val()
    rc.Operation = i;
    rc.StoreId = parseInt($("#hfStoreId").val()) || 0;
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CalculatePrice",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (j == 0) {
                    $('#spPrice').html('$' +parseFloat(response.d.responseData[0].Price).toFixed(2));
                }
                else {
                    $(e).closest('.item').find('.itemprice').html('$'+parseFloat(response.d.responseData[0].Price).toFixed(2));
                }
                fieldName.val(response.d.responseData[0].Qauntity);
            }
        }
    })
}

