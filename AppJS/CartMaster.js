﻿$(document).ready(function () {
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    if (StoreId == 0) {
        $("#cart-page").hide();
        $("#EmptyTable").show();
        $("#ViewCart").hide();
        $("#MobViewCart").hide();
        $(".cartfull").hide();
    }
    else {
        CartDetails();
    }
})
function CartDetails() {
    var apiUrl = $("#ApiAppKey").html();
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var rc = new Object();
    rc.AutoId = UserAutoId;
    rc.UserName = $("#hfEmail").val();
    rc.Location = $("#hfLocation").val();
    rc.StoreId = StoreId;
    rc.Token = getToken();
    var data = {
        rc: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CartDetails",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                var html = "",total=0;
                $("#tblCart").html('');
                var List1 = response.d.responseData;
                if (List1.length > 0) {
                    html += '   <table class="order_table" cellspacing="0">'
                    html += '<thead>'
                    html += '<tr class="head"><th class="product-remove">&nbsp;</th><th colspan="2">Product</th><th class="product-price"> Price</th><th class="product-quantity">Quantity</th><th class="product-subtotal">Sub Total</th></tr >'
                    html += ' </thead>'
                    html += '<tbody>'
                    $.each(List1, function (i, item) {
                        var productN = simplifyUrl(item.ProductName);
                        html += '<tr>'
                        html += '<td class="product-remove"><a href="#"  alt=' + item.ProductAutoId + ' onclick="DeleteItemFromCarts(' + item.AutoId + ')" class="remove">&nbsp;</a></td>'
                        html += '<td class="product-thumbnail">'
                        html += '<a href="#">'
                        html += '<img src="' + item.ImageUrl + '" OnError="this.src =\'https://image.mypsm.net/Attachments/default_pic.png\'"  style="height:96px;"/>'
                        html += '</a>'
                        html += '<td class="product-name" data-title="Product">'
                        html += '<a href="/product/' + productN + '-' + item.ProductId + '" >' + item.ProductName + '</a>'
                        html += '<div class="product-unit">'
                        html += '<div><span>Qty : </span>' + item.DefaultQty + '</div>'
                        html += '</div>'
                        html += '</td>'
                        html += ' <td class="product-price" data-title="Price">'
                        html += '<span><bdi><span>&#36;</span>' + parseFloat(item.Price).toFixed(2) + '</bdi></span>'
                        html += '</td>'
                        html += '<td class="product-quantity" data-title="Quantity">'
                        html += '<div class="quantity input-quantity input-quantity-small">'
                        html += '<button type="button"  onclick="calQty(this,1)" class="minus">--</button>'
                        html += '<div class="quantity">'
                        html += '<input type="number"  onchange="changeManualQunatity(this,' + item.ProductAutoId + ',' + item.AutoId + ')" step="1" min="0" max="" value="' + item.Qty + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" inputmode="numeric" class="input-text qty text" />'
                        html += '</div>'
                        html += '<button type="button" onclick="calQty(this,0)" class="plus">+</button>'
                        html += '</div>'
                        html += '</td>'
                        html += ' <td class="product-subtotal" data-title="Subtotal">'
                        html += '<span><bdi><span>&#36;</span>' + parseFloat(item.DefaultPrice).toFixed(2) + '</bdi></span>'
                        html += '</td>'
                        html += ' </tr>'
                        total += parseFloat(item.DefaultPrice);
                    })
                    html += '</tbody>'
                    html += '</table>'
                    $("#tblCart").append(html);
                    $("#spSubTotal").html(parseFloat(total).toFixed(2));
                    $("#spTotalAmount").html(parseFloat(total).toFixed(2));
                }
                else {                    
                    $("#cart-page").hide();
                    $("#EmptyTable").show();
                    $("#ViewCart").hide();
                    $("#MobViewCart").hide();
                    $(".cartfull").hide();
                    
                }
            }
            else {
                abandonSession();
            }
        }
    })
}
function DeleteItemFromCart(AutoId) {
    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var rc = new Object();
    rc.UserName = $("#hfEmail").val();
    rc.Location = $("#hfLocation").val();
    rc.UserAutoId = UserAutoId;
    rc.AutoId = AutoId;
    rc.Token = getToken();
    var data = {
        rc: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/DeleteCartDetails",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                CartDetails();
                GetCartDetails();
                swal("", "Product has been deleted successfully", "success");
            }
        }
    })
}
function DeleteItemFromCarts(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete Cart Item.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            DeleteItemFromCart(AutoId);
        }
    })
}
function calQty(e, i) {
    var row = $(e).closest('tr');
    var PID = $(row).find(".product-remove a").attr('alt') || 0;
    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var rc = new Object();
    rc.ProductId = PID;
    rc.Operation = i;
    rc.UserAutoId = UserAutoId;
    rc.StoreId = StoreId
    rc.Location = $("#hfLocation").val()
    rc.Token = getToken();
    var data = {
        requestContainer: rc
    };

    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CalculateCartPrice",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                CartDetails();
            }
            if (response.d.response == "failed") {
                toastr.error('Qty Can not be less than default qty.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                CartDetails();
            }
        }
    })
}
function calculate() {
    var sum = 0;
    $('#tblCartMaster > tbody  > tr').each(function () {
        var price = parseFloat($(this).find('.calPrice').text());
        sum += parseFloat(price).toFixed(2);
    });
    $('#subTotal').text(sum);
}
function changeManualQunatity(e, i, j) {
    var fieldName = $(e).val();
    var enterQty = parseInt(fieldName, 10);
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.ProductId = i;
    rc.Qauntity = enterQty;
    rc.Operation = 2;
    rc.UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    rc.Location = $("#hfLocation").val();
    rc.AutoId = j;
    rc.StoreId = StoreId;
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CalculateCartMasterPrice",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                CartDetails();
            }
        }
    })
}

function checkount() {
    $("#btncheckout").prop('disabled', true);
    //if (parseFloat($("#subtotalamount").val()) < 350) {
    //    swal("", "minimum order of $350.00", "warning");
    //} else {
        location.href='/Checkout';
    //}
}

