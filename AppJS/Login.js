﻿$(function () {
    var body = document.getElementsByTagName('body')[0];
    body.onkeypress = function (e) {
        if (e.keyCode === 13) {
            
            if ($("#txtUserId").val() == '') {
                $("#txtUserId").focus();
                return;
            }
            if ($("#txtPassword").val() == '') {
                $("#txtPassword").focus();
            }
            
            NewLogin(e);
            e.preventDefault();
            return false;
        }
    }
    
});
function NewLogin(e) {
    var html = '';
    if ($("#txtUserId").val() == "" && $("#txtPassword").val() == "") {
        toastr.error('Email ID and Password is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        var apiUrl = $("#ApiAppKey").html();
        var UserAutoId = $("#txtUserId").val();
        var rc = new Object();
        rc.UserName = $("#txtUserId").val(),
            rc.Password = $("#txtPassword").val(),
            rc.Location = GetLocation(1)
        var data = {
            cr: rc
        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/Login.asmx/CustomerLogin",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                localStorage.setItem("response", JSON.stringify(response));
                if (response.d.response == "success") {
                    var Token = response.d.responseNewData[0].Token;
                    setToken(Token);
                    if (response.d.responseNewData.length == 0) {
                        storeSession(response, 0, 0, null, 0, GetLocation(2), 0);
                    }
                    else if (response.d.responseNewData.length == 1) {

                        storeSession(response, response.d.responseNewData[0].StoreId, 1, response.d.responseNewData[0].StoreName, response.d.responseNewData[0].StoreStatus, response.d.responseNewData[0].StoreLocation, response.d.responseNewData[0].LocationStatus);
                    }
                    else if (response.d.responseNewData.length > 1) {
                        var StoreList = response.d.responseNewData;
                        if (response.d.responseNewData[0].StoreId == 0) {
                            storeSession(response, 0, 0, null, 0, GetLocation(2), 0);
                        }
                        else {
                            html += '<ul class="list-group" id="tblStoreList" style="margin-bottom: 0px !important;">'                            
                            $.each(StoreList, function (i, list) {
                                html += ' <li class="list-group-item"><input type="radio" LocationStatus=' + list.LocationStatus + ' Location=' + list.StoreLocation + '  StoreStatus=' + list.StoreStatus + ' alt=' + list.StoreId + ' name="flat" class="textprop" style="position: relative;top: 3px;">  <span class="spStoreName">' + list.StoreName + '</span></li>'
                            })
                            html += '</ul>'
                            html += '<div class="form-row" style = "text-align: center;" >'
                            html += '<button name="login" class="button white" type="button" onclick="verifyStore()">Continue</button>'
                            html += '</div><br/>'
                            $("#divPopUpStoreList").html(html);
                            window.$("#modalStoreList").modal('show');
                        }
                    }
                }
                else {
                    toastr.error(response.d.responseMessage, 'error', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            }
        })
    }
}
function login() {
    
}
function verifyStore() {
   
    var storeId = 0, storeName = '',Location=null,LocationStatus=0;
    $("#tblStoreList li").each(function () {
        if ($(this).find('.textprop').prop('checked') == true) {
            storeId = $(this).find('.textprop').attr('alt');
            storeName = $(this).find('.spStoreName').html();
            StoreStatus = $(this).find('.textprop').attr('StoreStatus');
            if ($(this).find('.textprop').attr('Location') == "null") {
                Location = GetLocation(2);
            }
            else
            {
                Location = $(this).find('.textprop').attr('Location');
            }
            LocationStatus = $(this).find('.textprop').attr('LocationStatus');
            return;
        }
    });
    if (storeId != 0) {
        var resp = JSON.parse(localStorage.getItem('response'));
        var length = resp.d.responseNewData.length;
        storeSession(resp, storeId, length, storeName, StoreStatus, Location, LocationStatus);
        $("#modalStoreList").modal('hide');
    }
    else {
        toastr.error('Please select store.', 'Error', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function storeSession(response, storeId, TotalStore, storeName, StoreStatus, Location, LocationStatus) {
    if (Location == null) {
        Location = GetLocation(2);
    }
    var data = {
        StoreID: storeId,
        UserId: response.d.responseNewData[0].UserId,
        Email: response.d.responseNewData[0].Email,
        StoreName: storeName,
        StoreStatus: StoreStatus,
        TotalStore: TotalStore,
        Location: Location,
        LocationStatus:LocationStatus,
        CustomerName: response.d.responseNewData[0].UserName 
    }
    $.ajax({
        type: "POST",
        url: "/Session.asmx/session",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (result) {
            if (result.d == "success") {
                if (TotalStore != 0) {
                    window.location.href = "/Manage_Store";
                }
                else {
                    window.location.href = "/Store_Registration";
                }
            }
            else {
                toastr.error('Oops something went wrong.Please try later.', 'error', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
    })
}
