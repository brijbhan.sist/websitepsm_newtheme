﻿var TimeFrom = "", TimeTo = "", Type = "";
$(document).ready(function () {
    debugger
    GetTimeWindow();
    GetUserInfo();
    BindDropdown();
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var PageId = getQueryString('PageId');
    if (PageId != null) {
        EditStore(PageId);
    }
    $('input[type="checkbox"]').click(function () {
        if ($(this).is(':checked')) {
            if ($("#txtBillAddress1").val() != "") {
                $("#txtShippingAddress1").removeClass("req");
                $("#divShippingDetails").hide();
            }
            if ($("#txtBillCity").val() != "") {
                $("#txtShipCity").removeClass("req");
            }
            if ($("#txtBillZipcode").val() != "") {
                $("#txtShipZipCode").removeClass("req");
            }
            if ($("#txtBillState").val() != "") {
                $("#txtShipState").removeClass("req");
            }
        }
        else {
            $("#divShippingDetails").show();
        }
    });
});

function setBillingData(place) {
    debugger;
    var m = 0, check = 0;
    $(".hiddenBillAddress").html(place.adr_address);
    $("#txtBillAddress1").val($(".hiddenBillAddress").find(".street-address").text());
    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {

            if (place.address_components[i].types[j] == "postal_code") {
                var zcode = place.address_components[i].long_name
                $("#txtBillZipcode").val(place.address_components[i].long_name);

            }
            else if (place.address_components[i].types[j] == "administrative_area_level_1") {
                $("#txtBillState").val(place.address_components[i].long_name);
            }
            if (place.address_components[i].types[j] == "neighborhood") {
                $("#txtBillCity").val(place.address_components[i].long_name);
                check = 1;
            }
            if (check == 0) {
                if (place.address_components[i].types[j] == "locality") {
                    $("#txtBillCity").val(place.address_components[i].long_name);
                }
                else if (place.address_components[i].types[j] == "administrative_area_level_3") {
                    $("#txtBillCity").val(place.address_components[i].long_name);
                }
            }

        }
        m++;
    }
    if (m > 4) {
        $("#txtBLatitude").val(place.geometry.location.lat);
        $("#txtBLongitude").val(place.geometry.location.lng);
    }
    else {
        swal("", "Invalid address !", "error");

    }

}
function setshipingData(place) {
    $(".hiddenShipAddress").html(place.adr_address);
    $("#txtShippingAddress1").val($(".hiddenShipAddress").find(".street-address").text());
    var m = 0, check=0;
    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {

            if (place.address_components[i].types[j] == "postal_code") {
                var zcode = place.address_components[i].long_name
                $("#txtShipZipCode").val(place.address_components[i].long_name);

            }
            else if (place.address_components[i].types[j] == "administrative_area_level_1") {
                $("#txtShipState").val(place.address_components[i].long_name);
            }
            if (place.address_components[i].types[j] == "neighborhood") {
                $("#txtShipCity").val(place.address_components[i].long_name);
                check = 1;
            }
            if (check == 0) {
                if (place.address_components[i].types[j] == "locality") {
                    $("#txtShipCity").val(place.address_components[i].long_name);
                }
                else if (place.address_components[i].types[j] == "administrative_area_level_3") {
                    $("#txtShipCity").val(place.address_components[i].long_name);
                }
            }
        }
        m++;
    }
    if (m > 4) {
        $("#txtSLatitude").val(place.geometry.location.lat);
        $("#txtSLongitude").val(place.geometry.location.lng);
    }
    else {
        swal("", "Invalid address !", "error");

    }
}

function AddStore() {
    if (checkRequiredField()) {
        var check = validateTime();

        if (check == 1) {
            var flage = false;
            if ($('#tblContactDetails tbody tr').length > 0) {
                $("#tblContactDetails tbody tr").each(function () {
                    if ($(this).find('.SetAsDefault input').prop('checked') == true) {
                        flage = true;
                    }
                })
                if (!flage) {
                    toastr.error('Atleast one contact person default.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else {


                    var XmlContactDetail = "<MainData>";
                    $("#tblContactDetails tbody tr").each(function () {

                        XmlContactDetail += '<XmlContactDetail>';
                        XmlContactDetail += '<IsDefault><![CDATA[' + $(this).find('.SetAsDefault input').prop('checked') + ']]></IsDefault>';
                        XmlContactDetail += '<CPerson><![CDATA[' + $(this).find('.HContactperson').attr('Contactperson') + ']]></CPerson>';
                        XmlContactDetail += '<ContactType><![CDATA[' + $(this).find('.HTypeNo').attr('Typeno') + ']]></ContactType>';
                        XmlContactDetail += '<Email><![CDATA[' + $(this).find('.HEmail').attr('Email') + ']]></Email>';
                        XmlContactDetail += '<AEmail><![CDATA[' + $(this).find('.HAEmail').attr('AEmail') + ']]></AEmail>';
                        XmlContactDetail += '<MobileNo><![CDATA[' + $(this).find('.HMobileNo').attr('MobileNo') + ']]></MobileNo>';
                        XmlContactDetail += '<FaxNo><![CDATA[' + $(this).find('.HFaxNo').attr('FaxNo') + ']]></FaxNo>';
                        XmlContactDetail += '<Landlineno1><![CDATA[' + $(this).find('.HLandlineno1').attr('Landlineno1') + ']]></Landlineno1>';
                        XmlContactDetail += '<Landlineno2><![CDATA[' + $(this).find('.HLandlineno2').attr('Landlineno2') + ']]></Landlineno2>';

                        XmlContactDetail += '</XmlContactDetail>';

                    });
                    XmlContactDetail += "</MainData>";

                    var Token = getToken();
                    var apiUrl = $("#ApiAppKey").html();
                    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;


                    var rc = new Object();
                    rc.CompanyName = $("#txtBusinessName").val();
                    rc.CustomerName = $("#txtStoreName").val();
                    rc.OPTLicense = $("#txtOTPLicense").val();
                    rc.TaxId = $("#txtTaxID").val();
                    rc.XmlContactDetail = XmlContactDetail;
                    rc.BillAddress1 = $("#txtBillAddress1").val();
                    rc.BillAddress2 = $("#txtBillAddress2").val();
                    rc.BillCity = $("#txtBillCity").val();
                    rc.BillZipCode = $("#txtBillZipcode").val();
                    rc.BillState = $("#txtBillState").val();

                    rc.ShippingAddress1 = $("#txtShippingAddress1").val();
                    rc.ShippingAddress2 = $("#txtShippingAddress2").val();
                    rc.ShipCity = $("#txtShipCity").val();
                    rc.ShippingZipCode = $("#txtShipZipCode").val();
                    rc.ShipState = $("#txtShipState").val();

                    rc.ContactPerson = $("#txtContactPerson").val();
                    rc.Email = $("#txtEmail").val();
                    rc.AlternateEmail = $("#txtAlternateEmail").val();
                    rc.FaxNo = $("#txtFaxNo").val();
                    rc.MainPhone = $("#txtMainPhone").val();
                    rc.WorkPhone = $("#txtWorkPhone").val();
                    rc.Phone = $("#txtPhone").val();
                    rc.StoreOpenTime = $("#fromTime").val();
                    rc.StoreCloseTime = $("#toTime").val();
                    rc.BillingLatitude = $("#txtBLatitude").val();
                    rc.BillingLongitude = $("#txtBLongitude").val();
                    rc.ShippingLatitude = $("#txtSLatitude").val();
                    rc.ShippingLongitude = $("#txtSLongitude").val();
                    rc.UserAutoId = UserAutoId;
                    rc.Autoid = UserAutoId;
                    rc.Location = GetLocation(1);
                    rc.UserName = $("#hfEmail").val();
                    rc.Token = Token;

                    var data = {
                        cr: rc
                    };
                    $.ajax({
                        type: "POST",
                        url: apiUrl + "/WebApi/WsStoreMaster.asmx/StoreRegistration",
                        data: JSON.stringify(data),
                        contentType: "application/json",
                        datatype: "json",
                        beforeSend: function () {
                        },
                        complete: function () {
                        },
                        success: function (response) {
                            if (response.d.response == "success") {
                                localStorage.setItem('SetSession', JSON.stringify(response));
                                reset();
                                swal({
                                    title: "",
                                    text: "Store registered successfully.",
                                    icon: "success",
                                    showCancelButton: true,
                                    buttons: {
                                        confirm: {
                                            text: "Ok",
                                            value: true,
                                            visible: true,
                                            className: "",
                                            closeModal: true
                                        }
                                    }
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        RestoreUserSession();
                                    }
                                });
                            }
                            else if (response.d.response == "Token") {
                                $("#modalTokenMessage").modal('show');
                            }
                            else {
                                swal("", response.d.responseMessage, "error");
                            }
                        }
                    })
                }
            }
            else {
                toastr.error('At least one contact person details is mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }

    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function validateEmail(e) {
    var elementValue = $(e).val();
    if (elementValue != "") {
        var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var check = EmailCodePattern.test(elementValue);
        if (!check) {
            $(e).val('');
            toastr.error('Please enter a valid Email ID.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        if ($("#txtEmail").val() == $("#txtAlternateEmail").val()) {
            $(e).val('');
            toastr.error("Email and Alternate Email can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        return (elementValue).toLowerCase().toString();
    }
}

function addcontactdetail() {
    var Phone = $("#txtPhone").val();
    var MPhone = $("#txtLandlineno1").val();
    var SPhone = $("#txtLandlineno2").val();
    var Fax = $("#txtFaxNo").val();
    var Email = $("#txtEmail").val();
    var AEmail = $("#txtAlternateEmail").val();
    if (checkRequiredFieldstore()) {
        if (Phone != "") {
            if (Phone.length != 10) {
                $("#txtPhone").addClass('border-warning');
                toastr.error('Please enter 10  digit mobile number.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return false
            }

        }
        if (MPhone != "") {
            if (MPhone.length != 10) {
                toastr.error('Please enter 10  digit landline no. 1', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#txtLandlineno1").addClass('border-warning');
                return false;
            }
        }
        if (SPhone != "") {
            if (SPhone.length != 10) {
                toastr.error('Please enter 10  digit landline no. 2', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#txtWorkPhone").addClass('border-warning');
                return false
            }
        }
        if (Fax != "") {
            if (Fax.length != 10) {
                toastr.error('Please enter 10  digit Fax number.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#txtFaxNo").addClass('border-warning');
                return false
            }
        }

        if (Email != "") {
            var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var check = EmailCodePattern.test(Email);
            var check1 = EmailCodePattern.test(AEmail);
            if (!check) {
                toastr.error('Please enter a valid Email ID.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#txtEmail").addClass('border-warning');
                return false
            }
        }
        if (AEmail != "") {
            var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var check1 = EmailCodePattern.test(AEmail);
            if (!check1) {
                toastr.error('Please enter a valid Email ID.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#txtAlternateEmail").addClass('border-warning');
                return false
            }
            if ($("#txtEmail").val() == $("#txtAlternateEmail").val()) {
                toastr.error("Email and Alternate Email can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#txtAlternateEmail").addClass('border-warning');
                return false
            }
        }
        var Type = "";
        var CPerson = $("#txtContactPerson").val();
        var Email = $("#txtEmail").val();
        var AEmail = $("#txtAlternateEmail").val();
        var MobileNo = $("#txtPhone").val();
        var Landlineno1 = $("#txtLandlineno1").val();
        var Landlineno2 = $("#txtLandlineno2").val();
        var FaxNo = $("#txtFaxNo").val();
        var ContactType = $("#ContactType").val();
        if (ContactType == 1) {
            Type += "Store"
        }
        if (ContactType == 2) {
            Type += "Manager";
        }
        if (ContactType == 3) {
            Type = "Employee";
        }

        var row = $("#tblContactDetails thead tr").clone(true);
        $(".Action", row).html('<input type="hidden" class="HTypeNo" TypeNo="' + ContactType + '" /><input type ="hidden" class="HContactperson" Contactperson = "' + CPerson + '" /><input type="hidden" class="HEmail" Email="' + Email + '" /><input type = "hidden" class= "HAEmail" AEmail = "' + AEmail + '" /><input type="hidden" class="HMobileNo" MobileNo="' + MobileNo + '" /><input type="hidden" class="HFaxNo" FaxNo="' + FaxNo + '" /><input type="hidden" class="HLandlineno1" Landlineno1="' + Landlineno1 + '" /><input type="hidden" class="HLandlineno2" Landlineno2="' + Landlineno2 + '" /><div class="btnaction"><div><span class="label">Action :</span><button  type="button" class="button blue small"  onclick ="deleteItemrecord(this)">Delete</button></div ></div ><div><div><span class="label">Person Name :</span>' + CPerson + '</div><div><span class="label">Type :</span> ' + Type + '</div></div>');
        if (($("#Default").prop("checked"))) {
            $(".SetAsDefault", row).html('<div><span class="label">Default :</span><input type="radio"  name="Default" checked="true"/></div>');
        }
        else {
            $(".SetAsDefault", row).html('<div><span class="label">Default :</span><input type="radio"  name="Default" /></div>');
        }
        $(".Emails", row).html('<div><span class="label">Email :</span><a href="#">' + Email + '</a></div><div> <span class="label">Alternate Email :</span><a href="#">' + AEmail + '</a></div>');

        $(".Numbers", row).html('<div class="mobile"><div><span class="label">Mobile :</span>' + MobileNo + '</div ><div><span class="label">Fax No :</span>' + FaxNo + '</div></div><div class="mobile"><div><span class="label">Landline No 1 :</span>' + Landlineno1 + '</div><div><span class="label">Landline No 2 :</span>' + Landlineno2 + '</div></div>');
        if ($('#tblContactDetails tbody tr').length > 0) {
            $('#tblContactDetails tbody tr:first').before(row);
        }
        else {
            $('#tblContactDetails tbody').append(row);
        }
        $('#tblContactDetails tbody tr').removeClass("table-heading")
        $("#ContactType").val(0);
        $("#txtAlternateEmail").val('');
        $("#txtLandlineno1").val('');
        $("#txtFaxNo").val('');
        $("#txtLandlineno2").val('');
        toastr.success('Contact Details added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#modalContactDetails").modal('hide');
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }

}

function reset() {
    $('input[type="text"]').removeClass('border-warning');
    $(".select2-selection__rendered").removeAttr('style');
    $("#txtEmail").removeClass('border-warning');
    $("#ddlStores").removeClass('border-warning');
    $('input[type="text"]').val('');
    $("#btnSubmit").show();
    $("#btnUpdate").hide();
    $("#txtEmail").val('');
    $("#ddlStores").val('');

    $("#fromTime").val('0');
    $("#toTime").val('0');

    $("#txtBillZipcode").val(''); $("#txtShipZipCode").val('');
    $('input[type="checkbox"]').prop('checked', false);
    $('#tblContactDetails tbody tr').remove(); 
}
function GetTimeWindow() {
    var apiUrl = $("#ApiAppKey").html();
    var Token = getToken();
    var rc = new Object();
    rc.Location = GetLocation(1);
    rc.UserName = $("#hfEmail").val();
    rc.Token = Token;
    var data = {
        rc: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsStoreMaster.asmx/GetTimeWindow",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {

            if (response.d.response == "success") {
                var TimeList = response.d.responseNewData;
                if (response.d.responseNewData.length > 0) {
                    TimeFrom = '<label>Store Open Time <span class="required">*</span></label>';
                    TimeFrom += '<select id="fromTime" name="txtStoreOpen">';
                    TimeFrom += "<option value='0'>--Select--</option>";
                    TimeTo = '<label>Store Close Time <span class="required">*</span></label>';
                    TimeTo += '<select id="toTime" class="textprop req">';
                    TimeTo += "<option value='0'>--Select--</option>";
                    $.each(TimeList, function (i, item) {
                        TimeFrom += "<option value='" + item.TimeFrom + "'>" + item.FromTime + "</option>"
                        TimeTo += "<option value='" + item.TimeFrom + "'>" + item.FromTime + "</option>"
                    })
                    TimeFrom += '</select>';
                    TimeTo += '</select>';
                    $("#divStoreOpenTime").html(TimeFrom);
                    $("#divStoreCloseTime").html(TimeTo);
                    $("#fromTime").val('09:00');
                    $("#toTime").val('18:00');
                }
            }
            else if (response.d.response == "Token") {
                abandonSession();
            }
        }
    })
}

$("#rbSameAsBilling").change(function () {
    if ($("#rbSameAsBilling").prop('checked') == true) {
        if ($("#txtBillAddress1").val() != "") {
            $("#txtShippingAddress1").val('');
            $("#divShippingDetails").hide();
        }
        if ($("#txtBillCity").val() != "") {
            $("#txtShipCity").val('');
        }
        if ($("#txtBillZipcode").val() != "") {
            $("#txtShipZipCode").val('');
        }
        if ($("#txtBillState").val() != "") {
            $("#txtShipState").val('');
        }
        $("#txtShippingAddress").removeClass('req');
        $("#txtShipZipCode").removeClass('req');
        $("#txtShipCity").removeClass('req');
        $("#txtShipState").removeClass('req');
    }
    else {
        $("#txtShippingAddress").val('');
        $("#txtShipCity").val('');
        $("#txtShipState").val('');
        $("#divShippingDetails").show();
        $("#txtShippingAddress").addClass('req');
        $("#txtShipZipCode").addClass('req');
        $("#txtShipCity").addClass('req');
        $("#txtShipState").addClass('req');
    }
})
function EditStore(PageId) {
    debugger
    var apiUrl = $("#ApiAppKey").html();
    var location = GetLocation(2);
    var Token = getToken();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var rc = new Object();
    rc.UserAutoId = UserAutoId;
    rc.Autoid = PageId;
    rc.Location = location;
    rc.UserName = $("#hfEmail").val();
    rc.Token = Token;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsStoreMaster.asmx/EditStore",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },

        success: function (response) {
            var store = "", Contact = "";
            if (response.d.response == "success") {

                var store = response.d.responseNewData[0].DStoredetails;
                var Contact = response.d.responseNewData[0].DCSDetails;
                $("#lblAutoId").text(store[0].AutoId);
                $("#lblBillingAutoId").text(store[0].BillAutoId);
                $("#lblShippingAutoId").text(store[0].ShipAutoId);
                $("#txtBusinessName").val(store[0].BusinessName);
                $("#txtStoreName").val(store[0].StoreName);
                $("#txtOTPLicense").val(store[0].OPTLicense);
                $("#txtTaxID").val(store[0].TaxID);

                $("#txtBillAddress1").val(store[0].BAddress1);
                $("#txtBillAddress2").val(store[0].BAddress2);
                $("#txtBillCity").val(store[0].BCity);
                $("#txtBillZipcode").val(store[0].BZipcode);
                $("#txtBillState").val(store[0].BState);

                $("#txtShippingAddress1").val(store[0].SAddress1);
                $("#txtShippingAddress2").val(store[0].SAddress2);
                $("#txtShipCity").val(store[0].SCity);
                $("#txtShipZipCode").val(store[0].SZipcode);
                $("#txtShipState").val(store[0].SState);
                $("#fromTime").val(store[0].StoreOpenTime);
                $("#toTime").val(store[0].StoreCloseTime);

                $("#txtBLatitude").val(store[0].BillingLatitude);
                $("#txtBLongitude").val(store[0].BillingLongitude);
                $("#txtSLatitude").val(store.ShippingLatitude);
                $("#txtSLongitude").val(store.ShippingLongitude);

                $("#hfAutoId").val(store[0].AutoId);
                $("#btnSubmit").hide();
                $("#btnUpdate").show();
                $("#hRegistrationLabel").html('Update Store');

                var row = $("#tblContactDetails thead tr").clone(true);
                if ($('#tblContactDetails tbody tr').length > 0) {
                    $('#tblContactDetails tbody tr:first').before(row);
                }
                else {
                    $('#tblContactDetails tbody').append(row);
                }
                $("#tblContactDetails tbody tr").remove();
                var row = $("#tblContactDetails thead tr").clone(true);
                $.each(Contact, function (index, item) {
                    $(".Action", row).html('<input type="hidden" class="HTypeNo" TypeNo="' + item.ContactType + '" /><input type ="hidden" class="HContactperson" Contactperson = "' + item.ContactPerson + '" /><input type="hidden" class="HEmail" Email="' + item.Email + '" /><input type = "hidden" class= "HAEmail" AEmail = "' + item.AlternateEmail + '" /><input type="hidden" class="HMobileNo" MobileNo="' + item.Mobile + '" /><input type="hidden" class="HFaxNo" FaxNo="' + item.FaxNo + '" /><input type="hidden" class="HLandlineno1" Landlineno1="' + item.Landlineno1 + '" /><input type="hidden" class="HLandlineno2" Landlineno2="' + item.Landlineno2 + '" /><div class="btnaction"><div><span class="label">Action :</span><button type="button" class="button blue small"  onclick="editContactDetails(' + item.AutoId + ')">View</button><br/><br/><button type="button" class="button blue small"  onclick ="deleteItemrecord(this)">Delete</button></div ></div ><div><div><span class="label">Person Name :</span>' + item.ContactPerson + '</div><div><span class="label">Type :</span> ' + item.TypeName + '</div></div>');
                    if (item.IsDefault == true) {
                        $(".SetAsDefault", row).html('<div><span class="label">Default :</span><input type="radio"  name="Default" checked="true"/></div>');
                    }
                    else {
                        $(".SetAsDefault", row).html('<div><span class="label">Default :</span><input type="radio"  name="Default" /></div>');
                    }
                    $(".Emails", row).html('<div><span class="label">Email :</span><a href="#">' + item.Email + '</a></div><div> <span class="label">Alternate Email :</span><a href="#">' + item.AlternateEmail + '</a></div>');

                    $(".Numbers", row).html('<div class="mobile"><div><span class="label">Mobile :</span>' + item.Mobile + '</div ><div><span class="label">Fax No :</span>' + item.FaxNo + '</div></div><div class="mobile"><div><span class="label">Landline No 1 :</span>' + item.Landlineno1 + '</div><div><span class="label">Landline No 2 :</span>' + item.Landlineno2 + '</div></div>');
                    $('#tblContactDetails tbody').append(row);
                    row = $("#tblContactDetails tbody tr:last").clone(true);

                });
                $('#tblContactDetails tbody tr').removeClass("table-heading")
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    });
}

function UpdateStore() {
    if ($("#rbSameAsBilling").prop('checked') == true) {
        if ($("#txtBillAddress1").val() != "") {
            $("#txtShippingAddress1").val('');
            $("#divShippingDetails").hide();
        }
        if ($("#txtBillCity").val() != "") {
            $("#txtShipCity").val('');
        }
        if ($("#txtBillZipcode").val() != "") {
            $("#txtShipZipCode").val('');
        }
        if ($("#txtBillState").val() != "") {
            $("#txtShipState").val('');
        }
    }

    if (checkRequiredField()) {
        var check = validateTime();
      
        if (check == 1) {
            var flage = false;
            if ($('#tblContactDetails tbody tr').length > 0) {
                $("#tblContactDetails tbody tr").each(function () {
                    if ($(this).find('.SetAsDefault input').prop('checked') == true) {
                        flage = true;
                    }
                })
                if (!flage) {
                    toastr.error('Atleast one contact person default.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else {

                    var XmlContactDetail = "<MainData>";
                    $("#tblContactDetails tbody tr").each(function () {
                        XmlContactDetail += '<XmlContactDetail>';
                        XmlContactDetail += '<IsDefault><![CDATA[' + $(this).find('.SetAsDefault input').prop('checked') + ']]></IsDefault>';
                        XmlContactDetail += '<CPerson><![CDATA[' + $(this).find('.HContactperson').attr('Contactperson') + ']]></CPerson>';
                        XmlContactDetail += '<ContactType><![CDATA[' + $(this).find('.HTypeNo').attr('Typeno') + ']]></ContactType>';
                        XmlContactDetail += '<Email><![CDATA[' + $(this).find('.HEmail').attr('Email') + ']]></Email>';
                        XmlContactDetail += '<AEmail><![CDATA[' + $(this).find('.HAEmail').attr('AEmail') + ']]></AEmail>';
                        XmlContactDetail += '<MobileNo><![CDATA[' + $(this).find('.HMobileNo').attr('MobileNo') + ']]></MobileNo>';
                        XmlContactDetail += '<FaxNo><![CDATA[' + $(this).find('.HFaxNo').attr('FaxNo') + ']]></FaxNo>';
                        XmlContactDetail += '<Landlineno1><![CDATA[' + $(this).find('.HLandlineno1').attr('Landlineno1') + ']]></Landlineno1>';
                        XmlContactDetail += '<Landlineno2><![CDATA[' + $(this).find('.HLandlineno2').attr('Landlineno2') + ']]></Landlineno2>';
                        XmlContactDetail += '</XmlContactDetail>';

                    });
                    XmlContactDetail += "</MainData>";

                    var Token = getToken();
                    var apiUrl = $("#ApiAppKey").html();
                    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;

                    var rc = new Object();
                    rc.Autoid = $("#lblAutoId").text();
                    rc.UserAutoId = UserAutoId;
                    rc.CompanyName = $("#txtBusinessName").val();
                    rc.CustomerName = $("#txtStoreName").val();
                    rc.OPTLicense = $("#txtOTPLicense").val();
                    rc.TaxId = $("#txtTaxID").val();
                    rc.XmlContactDetail = XmlContactDetail;

                    rc.BillAddress1 = $("#txtBillAddress1").val();
                    rc.BillAddress2 = $("#txtBillAddress2").val();
                    rc.BillCity = $("#txtBillCity").val();
                    rc.BillZipCode = $("#txtBillZipcode").val();
                    rc.BillState = $("#txtBillState").val();

                    rc.ShippingAddress1 = $("#txtShippingAddress1").val();
                    rc.ShippingAddress2 = $("#txtShippingAddress2").val();
                    rc.ShipCity = $("#txtShipCity").val();
                    rc.ShippingZipCode = $("#txtShipZipCode").val();
                    rc.ShipState = $("#txtShipState").val();

                    rc.StoreOpenTime = $("#fromTime").val();
                    rc.StoreCloseTime = $("#toTime").val();
                    rc.BillingLatitude = $("#txtBLatitude").val();
                    rc.BillingLongitude = $("#txtBLongitude").val();
                    rc.ShippingLatitude = $("#txtSLatitude").val();
                    rc.ShippingLongitude = $("#txtSLongitude").val();
                    rc.Location = GetLocation(1);
                    rc.UserName = $("#hfEmail").val();
                    rc.Token = Token;

                    var data = {
                        cr: rc
                    };
                    $.ajax({
                        type: "POST",
                        url: apiUrl + "/WebApi/WsStoreMaster.asmx/UpdateStore",
                        data: JSON.stringify(data),
                        contentType: "application/json",
                        datatype: "json",
                        beforeSend: function () {
                        },
                        complete: function () {
                        },
                        success: function (response) {

                            if (response.d.response == "success") {
                                swal({
                                    title: "",
                                    text: "Store details has been updated.",
                                    icon: "success",
                                    showCancelButton: false,
                                    buttons: {
                                        confirm: {
                                            text: "Ok",
                                            value: true,
                                            visible: true,
                                            className: "",
                                            closeModal: true
                                        }
                                    }
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        reset();
                                        $("#hRegistrationLabel").html('Add Store');
                                        window.location.href = "/Upload_Document";
                                    }
                                });
                            }
                            else if (response.d.response == "Token") {
                                $("#modalTokenMessage").modal('show');
                            }
                            else {
                                swal("", response.d.responseMessage, "error")
                            }
                        }
                    })
                }
            }
            else {
                toastr.error('At least one contact person details is mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }

        }
    }
        

    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function UpdateContactdetails() {
    if (checkRequiredFieldstore()) {
        var Token = getToken();
        var apiUrl = $("#ApiAppKey").html();
        var rc = new Object();
        rc.Autoid = $("#lblCAutoId").text();

        rc.ContactPerson = $("#txtContactPerson").val();
        rc.Email = $("#txtEmail").val();
        rc.AlternateEmail = $("#txtAlternateEmail").val();
        rc.Landlineno1 = $("#txtLandlineno1").val();
        rc.Landlineno2 = $("#txtLandlineno2").val();
        rc.Mobile = $("#txtPhone").val();
        rc.ContactType = $("#ContactType").val();
        rc.FaxNo = $("#txtFaxNo").val();
        rc.Location = GetLocation(1);
        rc.UserName = $("#hfEmail").val();
        rc.Token = Token;

        var data = {
            cr: rc
        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/WsStoreMaster.asmx/UpdateContactDetails",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {

                if (response.d.response == "success") {
                    swal({
                        title: "",
                        text: "Contact details has been updated.",
                        icon: "success",
                        showCancelButton: false,
                        buttons: {
                            confirm: {
                                text: "Ok",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        }
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $("#modalContactDetails").modal('hide');
                            location.reload();
                        }
                    });

                }
                else if (response.d.response == "Token") {
                    $("#modalTokenMessage").modal('show');
                }
                else {
                    swal("", response.d.responseMessage, "error")
                }
            }
        })
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


function RestoreUserSession() {
    var response = '';
    var StoreId = 0, TotalStore = 0, StoreName = null, StoreStatus = 0;
    if (localStorage.getItem('SetSession') != null) {
        response = JSON.parse(localStorage.getItem('SetSession'));
        var Token = response.d.responseNewData[0].Token;
        setToken(Token);
        StoreId = response.d.responseNewData[0].StoreId;
        TotalStore = response.d.responseNewData[0].TotalStore;
        StoreName = response.d.responseNewData[0].StoreName;
        StoreStatus = response.d.responseNewData[0].Status;
    }
    var data = {
        StoreID: StoreId,
        StoreName: StoreName,
        StoreStatus: StoreStatus,
        TotalStore: TotalStore,
        Location: GetLocation(2),
        LocationStatus: 0

    }
    $.ajax({
        type: "POST",
        url: "/Session.asmx/AfterAddStore",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (result) {
            if (result.d == "success") {
                if (TotalStore != 0) {
                    window.location.href = "/Upload_Document";
                }
                else {
                    location.href = "/Store_Registration";
                }
            }
            else {
                location.href = "/Index.aspx";
            }
        }
    })
}
function validateTime() {
    var check = 1;
    if ($("#fromTime").val() != "0" && $("#toTime").val() != "0") {
        From = $("#fromTime").val();
        From = From.slice(0, 2);
        From1 = $("#fromTime").val();
        From1 = From1.slice(3, 5);
        To = $("#toTime").val();
        To = To.slice(0, 2);
        To1 = $("#toTime").val();
        To1 = To1 = To1.slice(3, 5);
        if (Number(From) > Number(To)) {
            check = 0;
            $("#fromTime").attr('style', 'border:1px solid red !important');
            $("#toTime").attr('style', 'border:1px solid red !important');
            toastr.error('Invalid time.Please select valid time', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if (Number(From) == Number(To)) {
            if (Number(From1) > Number(To1) || Number(From1) == Number(To1)) {
                check = 0;
                $("#fromTime").attr('style', 'border:1px solid red !important');
                $("#toTime").attr('style', 'border:1px solid red !important');
                toastr.error('Invalid time.Please select valid time', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
    }
    return check;
}
function GetUserInfo() {
    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = parseInt($("#UserAutoId").val());
    var Token = getToken();
    var rc = new Object();
    rc.Location = GetLocation(1);
    rc.UserAutoId = UserAutoId;
    rc.UserName = $("#hfEmail").val();
    rc.Token = Token;
    var data = {
        rc: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsStoreMaster.asmx/GetUserInformation",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                var UserInfo = response.d.responseNewData;
                if (response.d.responseNewData.length > 0) {
                    $("#txtContactPerson").val(UserInfo[0].Name);
                    $("#txtPhone").val(UserInfo[0].Mobile);
                    $("#txtEmail").val(UserInfo[0].Email);
                }
            }
            else if (response.d.response == "Token") {
                abandonSession();
            }
        }
    })
}
function BindDropdown() {
    var apiUrl = $("#ApiAppKey").html();
    var Token = getToken();
    var rc = new Object();
    rc.Location = GetLocation(1);
    rc.UserName = $("#hfEmail").val();
    rc.Token = Token;
    var data = {
        rc: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsStoreMaster.asmx/BindDropdown",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {

            if (response.d.response == "success") {
                var Storetype = response.d.responseNewData;
                if (response.d.responseNewData.length > 0) {
                    if (response.d.responseNewData.length > 0) {

                        Type = '<label>Contact Type <span class="required">*</span></label>';
                        Type += '<select id="ContactType" class="textprop cddlreq">';
                        Type += "<option value='0'>--Select--</option>";
                        $.each(Storetype, function (i, item) {
                            Type += "<option value='" + item.AutoId + "'>" + item.TypeName + "</option>"
                        })
                        Type += '</select>';

                        $("#divstoretype").html(Type);


                    }
                }
                else if (response.d.response == "Token") {
                    abandonSession();
                }
            }
        }
    })
}

function checkRequiredFieldstore() {
    var boolcheck = true;
    $('.creq').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '0.00') {
            boolcheck = false;
            $(this).addClass('border-warning');

        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.cddlreq').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '-Select-' || $(this).val().trim() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
            $("#btnAdd").attr('disabled', false);
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}

function editContactDetails(AutoId) {
    var apiUrl = $("#ApiAppKey").html();
    var Token = getToken();
    var rc = new Object();
    rc.Location = GetLocation(1);
    rc.AutoId = AutoId;
    rc.UserName = $("#hfEmail").val();
    rc.Token = Token;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsStoreMaster.asmx/EditContactDetails",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },

        success: function (response) {
            if (response.d.response == 'success') {

                var contact = response.d.responseNewData;
                $("#lblCAutoId").text(contact[0].AutoId);
                $("#ContactType").val(contact[0].ContactType);
                $("#txtContactPerson").val(contact[0].ContactPerson);
                $("#txtEmail").val(contact[0].Email);
                $("#txtAlternateEmail").val(contact[0].AlternateEmail);
                $("#txtPhone").val(contact[0].Mobile);
                $("#txtLandlineno1").val(contact[0].Landlineno1);
                $("#txtLandlineno2").val(contact[0].Landlineno2);
                $("#txtFaxNo").val(contact[0].FaxNo);
                $("#modalContactDetails").modal('show');
                $("#btnAdd").hide();
                $("#btnUpdatecontact").show();
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    })
}

function deleteItemrecord(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Contact Detail.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            $(e).closest('tr').remove();
            swal("", "Contact person details deleted successfully.", "success");
        }
    })
}