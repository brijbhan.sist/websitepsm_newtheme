﻿$(document).ready(function () {
    if (localStorage.getItem('InvoiceDetails') != null) {
        var resp = JSON.parse(localStorage.getItem('InvoiceDetails'));
        GetInvoiceDetails(resp);
    }
    else {
        location.href = '/Cart';
    }
})
function GetInvoiceDetails(response) {
    var item = response.d.responseNewData[0];
    var itemList = response.d.responseNewData[0].ItemList;
    var html = '';
    var html1 = '';
    html += '<table class="order_table" cellspacing="0">'
    html += '<thead>'
    html += '<tr class="head"><th colspan="2">Product</th><th class="product-price" > Unit Price</th><th class="product-quantity">Quantity</th><th class="product-subtotal">Total</th></tr >'
    html += '</thead >'
    html += '<tbody>'
    $("#bOrderNo").html(item.OrderNo);
    $.each(itemList, function (i, item) {
        var productN = simplifyUrl(item.ProductName);
        html += '<tr>'
        html += '<td class="product-thumbnail"><a href="#" style="height:100px;width:100px"><img OnError="this.src =\'https://image.mypsm.net/Attachments/default_pic.png\'" src="' + item.ImageUrl + '" style="max-height:100px;max-width:100px"/></a></td>'
        html += '<td class="product-name" data-title="Product"><a href="/product/' + productN + '-' + item.ProductId + '">' + item.ProductName + '</a><div class="product-unit"></div></td>'
        html += '<td class="product-price" data-title="Price"><span><bdi><span>&#36;</span>' + parseFloat(item.UnitPrice).toFixed(2) + '</bdi></span></td>'
        html += '<td class="product-quantity" data-title="Quantity"><span>' + item.DefQty + '</span></td>'
        html += '<td class="product-subtotal" data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(item.Total).toFixed(2) + '</bdi></span></td>'
        html += ' </tr>'

    })
    html += '</tbody>'
    html += ' </table>'

    html1 += ' <table cellspacing="0" class="order_table">'
    html1 += '<tr class="cart-subtotal">'
    html1 += ' <th>Subtotal</th>'
    html1 += '<td data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(item.SubTotal).toFixed(2) + '</bdi></span></td>'
    html1 += '</tr>'

    html1 += ' <tr class="cart-subtotal">'
    html1 += '<th>Shipping Charge</th>'
    html1 += '<td data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(item.ShippingCharges).toFixed(2) || 0 + '</bdi></span></td>'
    html1 += '</tr>'
    if (item.MLTax != 0) {
        html1 += '<tr class="cart-subtotal">'
        html1 += '<th>ML Tax</th>'
        html1 += '<td data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(item.MLTax).toFixed(2) + '</bdi></span></td>'
        html1 += ' </tr>'
    }
    if (item.WeightTax != 0) {
        html1 += '<tr class="cart-subtotal">'
        html1 += '<th>Weight Tax</th>'
        html1 += '<td data-title="Subtotal"><span><bdi><span>&#36;</span>' + parseFloat(item.WeightTax).toFixed(2) + '</bdi></span></td>'
        html1 += ' </tr>'
    }
    if (item.AdjustmentAmt != 0) {
        html1 += '<tr class="cart-subtotal">'
        html1 += '<th>Adjustment Amount</th>'
        html1 += '<td data-title="Subtotal"><span><bdi>' + item.AdjustmentAmt + '</bdi></span></td>'
        html1 += '</tr>'
    }
    html1 += ' <tr class="order-total">'
    html1 += '<th>Total</th>'
    html1 += '<td data-title="Total"><strong><span><bdi>' + parseFloat(item.TotalAmount).toFixed(2) + '</bdi></span></strong></td>'
    html1 += '</tr>'
    $("#tblCart").html(html);
    $("#priceSection").html(html1);
    //  localStorage.removeItem("InvoiceDetails");
}

