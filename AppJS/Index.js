﻿$(document).ready(function () {
    ManageBanner();
    ManageSlider();
    newProduct();
    SpecialProduct();
    TopsellingProduct();
});

function newProduct() {
    var apiUrl = $("#ApiAppKey").html();
    var StoreStatus = $("#hfStoreStatus").val() || 0;
    var rc = new Object();
    rc.StoreStatus = StoreStatus;
    rc.Location = $("#hfLocation").val();
    rc.StoreId = parseInt($("#hfStoreId").val())||0;
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/LatestProduct",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        async: false,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (response.d.responseData.length > 0) {
                    var ProductDetail = response.d.responseData;
                    var List = ProductDetail[0].ProductListDetails;
                    var html = '';
                    $.each(List, function (i, item) {

                        if (item.AvailableInStock == 1) {
                            html += '<li class="item outofstock">';
                        }
                        else {
                            html += '<li class="item">';
                        }
                        html += '<div>';
                        var productN = simplifyUrl(item.ProductName);
                        html += "<a href='/product/" + productN + "-" + item.ProductId + "'>";
                        html += "<center><div style='height:234px;display: table-cell;vertical-align: middle;'>";
                        html += "<img src=\'" + item.ImageUrl + "' style='height: auto;max-height:225px' alt='" + item.ProductName + "' OnError='this.src =\"https://image.mypsm.net/Attachments/default_pic.png\"' />";
                        html += '</div></center>';
                        html += '<span>' + item.ProductName + '</span>';
                        html += '</a>';
                        if (StoreStatus > 0) {

                            html += '<div class="itemprice"><span>&#36;</span>' + parseFloat(item.Price).toFixed(2) + '</div>';
                            html += '<div class="clear-div quantity">';
                            if (item.AvailableInStock == 0) {
                                html += '<div class="input-quantity">'
                                html += '<button type="button" class="minus" onclick="calculateQty(this,1)">--</button>';
                                html += '<div>';
                                html += '<input type="number" class="qty" name="quantity"  onchange="calculateQty(this,2)" step="' + item.Quantity + '"  value="' + item.Quantity + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" inputmode="numeric" maxlength="6"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">';
                                html += '</div>';
                                html += '<button type="button" onclick="calculateQty(this,0)" class="plus">+</button></div>';
                                html += '</div>';
                                html += '<div class="add_to_cart" alt=' + item.AutoId + '>';
                                html += '<button type="button" onclick="AddToCart(this,0)" class="button white" >ADD TO CART</button>';
                            }
                            else {
                                html += '<div class="input-quantity">'
                                html += '<button type="button" disabled="disabled" style="cursor: not-allowed;" class="minus" ">--</button>';
                                html += '<div>';
                                html += '<input type="number" disabled="disabled" style="cursor: not-allowed;" class="qty" name="quantity"   step="1" maxlength="3" value="' + item.Quantity + '" inputmode="numeric">';
                                html += '</div>';
                                html += '<button type="button" disabled="disabled" style="cursor: not-allowed;" class="plus">+</button></div>';
                                html += '</div>';
                                html += '<div class="add_to_cart" alt=' + item.AutoId + '>';
                                html += "<a href='/product/" + productN + "-" + item.ProductId + "' title='View' class='button white'>View Details</a>";
                            }
                            html += '</div>';
                        }
                        else {
                            html += "<a href='/product/" + productN + "-" + item.ProductId + "' title='View' class='button white'>View Details</a>";
                        }
                        html += '</div>';
                        html += '</li>';
                        $("#Sec_latest_products").show();
                    });
                    $("#latest_products").append(html);

                }
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}

function SpecialProduct() {
    var apiUrl = $("#ApiAppKey").html();
    var StoreStatus = $("#hfStoreStatus").val() || 0;
    var rc = new Object();
    rc.StoreStatus = StoreStatus;
    rc.Location = $("#hfLocation").val();
    rc.StoreId = parseInt($("#hfStoreId").val())||0;
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/SpecialProduct",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        async: false,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (response.d.responseData.length > 0) {

                    var ProductDetail = response.d.responseData;
                    var List = ProductDetail[0].ProductListDetails; 
                    var html = '';
                    $.each(List, function (i, item) {
                        if (item.AvailableInStock == 1) {
                            html += '<li class="item outofstock">';
                        }
                        else {
                            html += '<li class="item">';
                        }
                        html += '<div>';

                        var productN = simplifyUrl(item.ProductName);
                        html += "<a href='/product/" + productN + "-" + item.ProductId + "'>";

                        html += "<center><div style='height:234px;display: table-cell;vertical-align: middle;'>";
                        html += "<img src=\'" + item.ImageUrl + "' style='height: auto;max-height:225px' alt='" + item.ProductName + "' OnError='this.src =\"https://image.mypsm.net/Attachments/default_pic.png\"' />";
                        html += '</div></center>';
                        
                        html += '<span>' + item.ProductName + '</span>';
                        html += '</a>';
                        if (StoreStatus > 0) {
                            html += '<div class="itemprice"><span>&#36;</span>' + parseFloat(item.Price).toFixed(2) + '</div>';
                            html += '<div class="clear-div quantity">';
                            if (item.AvailableInStock == 0) {
                                html += '<div class="input-quantity">'
                                html += '<button type="button" class="minus" onclick="calculateQty(this,1)">--</button>';
                                html += '<div>';
                                html += '<input type="number" class="qty" name="quantity"  onchange="calculateQty(this,2)" step="' + item.Quantity + '" value="' + item.Quantity + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" inputmode="numeric" maxlength="6"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">';
                                html += '</div>';
                                html += '<button type="button" onclick="calculateQty(this,0)" class="plus">+</button></div>';
                                html += '</div>';
                                html += '<div class="add_to_cart" alt=' + item.AutoId + '>';
                                html += '<button type="button" onclick="AddToCart(this,0)" class="button white">ADD TO CART</button>';
                            }
                            else {
                                html += '<div class="input-quantity">'
                                html += '<button type="button" disabled="disabled" style="cursor: not-allowed;" class="minus" ">--</button>';
                                html += '<div>';
                                html += '<input type="number" disabled="disabled" style="cursor: not-allowed;" class="qty" name="quantity"   step="1" maxlength="3" value="' + item.Quantity + '" inputmode="numeric">';
                                html += '</div>';
                                html += '<button type="button" disabled="disabled" style="cursor: not-allowed;" class="plus">+</button></div>';
                                html += '</div>';
                                html += '<div class="add_to_cart" alt=' + item.AutoId + '>';
                                html += "<a href='/product/" + productN + "-" + item.ProductId + "' title='View' class='button white'>View Details</a>";
                            }
                            html += '</div>';
                        }
                        else {
                            html += "<a href='/product/" + productN + "-" + item.ProductId + "' title='View' class='button white'>View Details</a>";
                        }
                        html += '</div>';
                        html += '</li>';
                        $("#Sec_special_products").show();
                    });
                    $("#special_products").append(html);

                }
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}

function TopsellingProduct() {
    var apiUrl = $("#ApiAppKey").html();
    var StoreStatus = $("#hfStoreStatus").val() || 0;
    var rc = new Object();
    rc.StoreStatus = StoreStatus;
    rc.Location = $("#hfLocation").val();
    rc.StoreId = parseInt($("#hfStoreId").val())||0;
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/TopsellingProduct",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        async: false,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (response.d.responseData.length > 0) {
                    var ProductDetail = response.d.responseData;
                    var List = ProductDetail[0].ProductListDetails;
                    var html = '';
                    $.each(List, function (i, item) {

                        if (item.AvailableInStock == 1) {
                            html += '<li class="item outofstock">';
                        }
                        else {
                            html += '<li class="item">';
                        }
                        html += '<div>';

                        var productN = simplifyUrl(item.ProductName);
                        html += "<a href='/product/" + productN + "-" + item.ProductId + "'>";

                        html += "<center><div style='height:234px;display: table-cell;vertical-align: middle;'>";
                        html += "<img src=\'" + item.ImageUrl + "' style='height: auto;max-height:225px' alt='" + item.ProductName + "' OnError='this.src =\"https://image.mypsm.net/Attachments/default_pic.png\"' />";
                        html += '</div></center>';

                        html += '<span>' + item.ProductName + '</span>';
                        html += '</a>';

                        if (StoreStatus > 0) {

                            html += '<div class="itemprice"><span>&#36;</span>' + parseFloat(item.Price).toFixed(2) + '</div>';
                            html += '<div class="clear-div quantity">';
                            if (item.AvailableInStock == 0) {
                                html += '<div class="input-quantity">'
                                html += '<button type="button" class="minus" onclick="calculateQty(this,1)">--</button>';
                                html += '<div>';
                                html += '<input type="number" class="qty" name="quantity"  onchange="calculateQty(this,2)" step="' + item.Quantity + '"  value="' + item.Quantity + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" inputmode="numeric" maxlength="6"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">';
                                html += '</div>';
                                html += '<button type="button" onclick="calculateQty(this,0)" class="plus">+</button></div>';
                                html += '</div>';
                                html += '<div class="add_to_cart" alt=' + item.AutoId + '>';
                                html += '<button type="button" onclick="AddToCart(this,0)" class="button white">ADD TO CART</button>';
                            }
                            else {
                                html += '<div class="input-quantity">'
                                html += '<button type="button" disabled="disabled" style="cursor: not-allowed;" class="minus" ">--</button>';
                                html += '<div>';
                                html += '<input type="number" disabled="disabled" style="cursor: not-allowed;" class="qty" name="quantity"   step="1" maxlength="3" value="' + item.Quantity + '" inputmode="numeric">';
                                html += '</div>';
                                html += '<button type="button" disabled="disabled" style="cursor: not-allowed;" class="plus">+</button></div>';
                                html += '</div>';
                                html += '<div class="add_to_cart" alt=' + item.AutoId + '>';
                                html += "<a href='/product/" + productN + "-" + item.ProductId + "' title='View' class='button white'>View Details</a>";
                            }
                            html += '</div>';
                        }

                        else {
                            html += "<a href='/product/" + productN + "-" + item.ProductId + "' title='View' class='button white'>View Details</a>";
                        }
                        html += '</div>';
                        html += '</li>';
                        $("#Sec_selling_products").show();

                    });
                    $("#selling_products").append(html);
                }
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}
function ManageBanner() {
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.Location = GetLocation(1);
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/ManageBanner.asmx/GetBanner",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        async: false,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var boxhtml = ``;
            var i = 0;
            var bannerData = JSON.parse(response.d);
            $.each(bannerData, function () { 
                boxhtml += '<div class="product-img" ><a href="' + bannerData[i].Link + '">';
                boxhtml += '<img src="' + bannerData[i].ImageUrl + '" alt="' + bannerData[i].Title + '"  OnError="this.src=\'https://image.mypsm.net/Attachments/default_pic.png\'">';
                boxhtml += '</div></a>';
                i++;
            });
            $("#bannerbox").html(boxhtml);
        }
    });
}

function ManageSlider() {
    var apiUrl = $("#ApiAppKey").html();
    var rc = new Object();
    rc.Location = GetLocation(1);
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/ManageBanner.asmx/GetSlider",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        async: false,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var sliderhtml = '';
            var sliderData = JSON.parse(response.d);
            $.each(sliderData, function (index, item) {
                sliderhtml += '<li><a href="' + sliderData[index].Link + '"><img class="slide-bg" src="' + sliderData[index].ImageUrl + '" data-src="' + sliderData[index].ImageUrl + '" /></a></li>';
            });
            $("#home_slider_inner").html(sliderhtml);
        }
    });
}
