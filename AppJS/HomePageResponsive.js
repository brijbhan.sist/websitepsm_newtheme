﻿var lastChar = "";
$(document).ready(function () {
     
    var x = $(window).width();
    var y = $(window).height();
    var a = x / 2;
    var b = y / 2;

    $(".open").css('width', a);
    $(".open").css('height', b);
    var url = $(location).attr('pathname');

     lastChar = url.split('/');

    if (lastChar[lastChar.length - 1] == "Upload_Document") {
        $(".list-group a").removeClass("active");
        $(".Upload_Document").addClass("active");
    }
    if (lastChar[lastChar.length - 1] == "Store_Registration") {
        $(".list-group a").removeClass("active");
        $(".Store_Registration").addClass("active");
    }
    if (lastChar[lastChar.length - 1] == "Store_List") {
        $(".list-group a").removeClass("active");
        $(".Store_List").addClass("active");
        $(".DraftListitem").addClass("active");
    }
    if (lastChar[lastChar.length - 1] == "My_Orders") {
        $(".list-group a").removeClass("active");
        $(".My_Orders").addClass("active");
    }
    if (lastChar[lastChar.length - 1] == "My_Address") {
        $(".list-group a").removeClass("active");
        $(".My_Address").addClass("active");
    }
    myFunction()
});

function showMenu() {
   
    $(".submenus").toggle();
    $(".submenus").scroll();
}
function userSetting() {   
    $(".usermenu").toggle();
    $(".usermenu").scroll();
}


function myFunction() {
     
    var url = $(location).attr('pathname');
    lastChar = url.split('/');
   
    if ($(window).width() <= 768) {
        if (lastChar[lastChar.length - 1] == "Manage_Store") {
            $("#manageStore").hide();
            $(".hideRespo").hide();
        }
        else if (lastChar[lastChar.length - 1] == "Store_Registration")
        {
            $("#userMenuID").hide(); 
            $(".hideRespo").hide(); 
        }
        else {
            $("#manageStore").show();
            $(".hideRespo").show();
        }
    }  
    else {
        $("#manageStore").show();
        $(".hideRespo").show();
    }
}
function backLink() {
     
    $(".hideRespo").hide(); 
    $("#userMenuID").show();
    $("#manageStore").hide();
    $("#ApprovedStoreList").hide(); 
    $("#changePassword").hide(); 
    $("#MyOrders").hide(); 
    $("#OrderDetails").hide();     
    $("#StoreList").hide(); 
    $("#documentList").hide();
}


