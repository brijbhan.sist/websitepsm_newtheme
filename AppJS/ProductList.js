﻿var PageId = 0;
$(document).ready(function () {
    PageId = getUrlVariable('category');
    getProductList(1);
    // $("#inputID").maxlength(6);
});
function Pagevalue(e) {
    getProductList(parseInt($(e).attr("page")));
}
function getProductList(PageIndex) { 
    var location = GetLocation(1);
    if ($("#hfLocation").val() == location) {
        location = "";
    }
    else {
        location = $("#hfLocation").val()
    }
    var apiUrl = $("#ApiAppKey").html();
    var html = "";
    var StoreStatus = $("#hfStoreStatus").val() || 0;//for to show price or not
    var rc = new Object();
    rc.CategoryAutoId = 0;
    rc.SubcategoryAutoId = parseInt(PageId);
    rc.PageIndex = PageIndex;
    rc.PageSize = 30;
    rc.StoreStatus = StoreStatus;
    rc.Location = location;
    rc.StoreId = parseInt($("#hfStoreId").val())||0;
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/ProductListing",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                $("#product").html('');
                if (response.d.responseData.length > 0) {
                    var ProductDetail = response.d.responseData;
                    var List = ProductDetail[0].ProductListDetails;
                    $("#lblCategory").html(ProductDetail[0].CategoryName);
                    $("#lblSubCategory").html(ProductDetail[0].SubcategoryName);
                    $("#lblSubCategory2").html(ProductDetail[0].SubcategoryName);
                    $("#spCategoryTitle").html(ProductDetail[0].CategoryName);
                    $.each(List, function (i, item) {
                        var productN = simplifyUrl(item.ProductName);
                        if (item.AvailableInStock == 1) {
                            html += '<li class="item outofstock">';
                        }
                        else {
                            html += '<li class="item">';
                        }
                        html += '<div>';
                        html += "<a href='/product/" + productN + "-" + item.ProductId + "'>";

                        html += "<center><div style='height:234px;display: table-cell;vertical-align: middle;'>";
                        html += "<img src=\'" + item.ImageUrl + "' style='height: auto;max-height:225px' alt='" + item.ProductName + "' OnError='this.src =\"https://image.mypsm.net/Attachments/default_pic.png\"' />";
                        html += '</div></center>';
                       
                        html += '<span>' + item.ProductName + '</span>';
                        html += ' </a>';
                        if (StoreStatus > 0) {

                            html += '<div class="itemprice"><span>&#36;</span>' + parseFloat(item.Price).toFixed(2) + '</div>';
                            html += ' <div class="clear-div quantity">';
                            if (item.AvailableInStock == 0) {
                                html += '<div class="input-quantity">';
                                html += ' <button type="button" onclick="calculateQty(this,1)" class="minus">--</button>';
                                html += '<div>';
                                html += '<input type="number" class="qty" name="quantity"  onchange="calculateQty(this,2)"   value="' + item.Quantity + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" inputmode="numeric"  maxlength="6"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" />';
                                html += ' </div>';
                                html += '<button type="button" onclick="calculateQty(this,0)" class="plus">+</button>';
                                html += ' </div>';
                                html += ' </div>';
                                html += ' <div class="add_to_cart" alt=' + item.AutoId + '>';
                                html += '<button type="button" onclick="AddToCart(this,0)" class="button white">ADD TO CART</button>';
                            }
                            else {
                                html += '<div class="input-quantity">';
                                html += ' <button type="button" disabled="disabled" class="minus" style="cursor: not-allowed;">--</button>';
                                html += '<div class="item">';
                                html += '<input type="number" disabled="disabled" class="qty" style="cursor: not-allowed;" name="quantity"  maxlength="3"  value="' + item.Quantity + '" inputmode="numeric" />';
                                html += ' </div>';
                                html += '<button type="button" disabled="disabled" style="cursor: not-allowed;" class="plus">+</button>';
                                html += ' </div>';
                                html += ' </div>';

                                html += ' <div class="add_to_cart" alt=' + item.AutoId + '>';
                                html += "<a href='/product/" + productN + "-" + item.ProductId + "' title='View' class='button white'>View Details</a>";
                            }
                            html += '</div>';
                        }
                        else {
                            html += "<a href='/product/" + productN + "-" + item.ProductId + "' title='View' class='button white'>View Details</a>";
                        }
                        html += '</div>';
                        html += '</li >';
                    });
                    $("#product").append(html);
                    $(".page").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(ProductDetail[0].PageIndex),
                        PageSize: parseInt(ProductDetail[0].PageSize),
                        RecordCount: parseInt(ProductDetail[0].RecordCount)
                    });
                    var j = (ProductDetail[0].PageIndex - 1) * ProductDetail[0].PageSize + 1;
                    var k = j + ProductDetail[0].PageSize - 1;
                    if (k > ProductDetail[0].RecordCount) {
                        k = ProductDetail[0].RecordCount
                    }
                    $("#totalofResult").html("Showing " + (ProductDetail[0].PageIndex) + " - " + k + " of " + ProductDetail[0].RecordCount + " Results");
                }
                else {
                    $("#lblCategory").html(ProductDetail[0].CategoryName);
                    $("#lblSubCategory").html(ProductDetail[0].SubcategoryName);
                    $("#lblSubCategory2").html(ProductDetail[0].SubcategoryName);
                    $("#ProductListEmptyTable").show();
                    $(".iconbar").hide();
                }
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}

function ASPSnippetsPager(a, b) {

    var c = '<li><a onclick="Pagevalue(this)" style="cursor:pointer" class="Active" page="{1}">{0}</a></li>';
    var d = '<li><a class="spn">{0}</a></li>';
    var e, f, g; var g = 5; var h = Math.ceil(b.RecordCount / b.PageSize);
    if (b.PageIndex > h) { b.PageIndex = h } var i = "";
    if (h > 1) {
        f = h > g ? g : h; e = b.PageIndex > 1 && b.PageIndex + g - 1 < g ? b.PageIndex : 1;
        if (b.PageIndex > g % 2) {
            if (b.PageIndex == 2) f = 5;
            else f = b.PageIndex + 2
        } else { f = g - b.PageIndex + 1 }
        if (f - (g - 1) > e) { e = f - (g - 1) }
        if (f > h) { f = h; e = f - g + 1 > 0 ? f - g + 1 : 1 }
        var j = (b.PageIndex - 1) * b.PageSize + 1;
        var k = j + b.PageSize - 1;
        if (k > b.RecordCount) {
            k = b.RecordCount
        }
        i = "<span>Records " + (j == 0 ? 1 : j) + " - " + k + " of " + b.RecordCount + "</span>&nbsp;&nbsp;&nbsp;&nbsp; ";
        if (b.PageIndex > 1) {
            i += c.replace("{0}", "First").replace("{1}", "1");
            i += c.replace("{0}", "<<").replace("{1}", b.PageIndex - 1)
        }
        for (var l = e; l <= f; l++) {
            if (l == b.PageIndex) {
                i += d.replace("{0}", l)
            }
            else {
                i += c.replace("{0}", l).replace("{1}", l)
            }
        }
        if (b.PageIndex < h) {
            i += c.replace("{0}", ">>").replace("{1}", b.PageIndex + 1); i += c.replace("{0}", "Last").replace("{1}", h)
        }
    }
    a.html(i);
    try {
        a[0].disabled = false
    }
    catch (m) {
    }
}
(function (a) {
    a.fn.ASPSnippets_Pager = function (b) {
        var c = {};
        var b = a.extend(c, b);
        return this.each(function () { ASPSnippetsPager(a(this), b) })
    }
})(jQuery);







