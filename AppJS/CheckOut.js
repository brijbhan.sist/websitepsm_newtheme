﻿$(document).ready(function () {
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    if (StoreId == 0) {
        location.href = "/Cart.aspx";
    }
    else {
        getBillingAddress();
        CheckOutDetails();
    }
})
function getBillingAddress() {

    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var UserName = $("#hfEmail").val();
    var rc = new Object();
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.StoreId = StoreId;
    rc.Location = $("#hfLocation").val()
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Registration.asmx/GetBilling",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var defaultShip = 0, defaultBill = 0;
            var html = '', html1 = '';
            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    defaultBill = response.d.responseNewData[0].DefaultBillAdd;
                    defaultShip = response.d.responseNewData[0].DefaultShipAdd;
                    var Billing = response.d.responseNewData[0].BillingList;
                    var Shipping = response.d.responseNewData[0].ShippingList;
                    $.each(Billing, function (i, item) {
                        if (defaultBill == item.AutoId) {
                            html += ' <div class="col address">'
                            html += ' <div class="title"><strong>Billing Address</strong></div>'
                            html += ' <div class="col-info">'
                            html += '<div>' + item.Address + ', </div>'
                            html += '<div><span>' + item.City + '</span>, <span>' + item.StateName + '</span>, <span>' + item.Zipcode + '</span></div>'
                            html += '</div>'
                        }
                    });
                    $("#divBillingAddress").html(html);
                    $.each(Shipping, function (i, item) {
                        if (defaultShip == item.AutoId) {
                            html1 += '<div class="col address">'
                            html1 += '<div class="title"><strong>Shipping Address</strong></div>'
                            html1 += '<div class="col-info">'
                            html1 += '<div>' + item.Address + ', </div>'
                            html1 += '<div><span>' + item.City + '</span>, <span>' + item.StateName + '</span>, <span>' + item.Zipcode + '</span></div>'
                            html1 += '</div>'
                        }
                    });
                    $("#divShippingAddress").html(html1);
                }
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    })
}

function OpenBilling() {
    PopUpBilling();
    $("#Billing").modal('show');
}

function OpenShipping() {
    PopUpShipping();
    $("#Shipping").modal('show');
}

function AddBilling() {
    if (billRequiredField()) {
        var apiUrl = $("#ApiAppKey").html();
        var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
        var UserName = $("#hfEmail").val();
        var StoreId = parseInt($("#hfStoreId").val()) || 0;
        var rc = new Object();
        rc.BillAddress = $("#txtBillAddress").val();
        rc.BillZipCode = $("#ddlBillZipCode").val();
        rc.UserAutoId = UserAutoId;
        rc.Type = 0;
        rc.UserName = UserName;
        rc.Token = getToken();
        rc.StoreId = StoreId;
        var data = {
            cr: rc
        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/Registration.asmx/AddShippingBilling",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {

                if (response.d.response == "success") {
                    swal("", "Billing details added successfully", "success");
                    PopUpBilling();
                    $("#btnBilling").hide();
                    $("#btnShowBilling").show();
                    $("#BillEntry").hide();
                    $("#divPopUpBillingAddress").show();
                    $("#txtBillAddress").val('');
                    $("#ddlBillZipCode").val(0).change();
                    $("#txtBillCity").val('');
                    $("#txtBillState").val('');
                }
                else {
                    swal("", response.d.responseMessage, "error");
                }
            }
        })
    }
    else {
        toastr.error("All * fields are mandatory.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function AddShipping() {
    if (shipRequiredField()) {
        var apiUrl = $("#ApiAppKey").html();
        var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
        var UserName = $("#hfEmail").val();
        var StoreId = parseInt($("#hfStoreId").val()) || 0;
        var rc = new Object();
        rc.ShippingAddress = $("#txtShippingAddress").val();
        rc.ShippingZipCode = $("#ddlShippingZipCode").val();
        rc.UserAutoId = UserAutoId;
        rc.UserName = UserName;
        rc.Token = getToken();
        rc.Type = 1;
        rc.StoreId = StoreId;
        var data = {
            cr: rc
        };
        $.ajax({
            type: "POST",
            url: apiUrl + "/WebApi/Registration.asmx/AddShippingBilling",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                if (response.d.response == "success") {
                    swal("", "Shipping details added successfully", "success");
                    PopUpShipping();
                    $("#btnAddShipping").hide();
                    $("#btnShowShipping").show();
                    $("#ShipEntry").hide();
                    $("#divPopUpShippingAddress").show();
                    $("#txtShippingAddress").val('');
                    $("#ddlShippingZipCode").val(0).change();
                    $("#txtShipCity").val('');
                    $("#txtShipState").val('');
                }
                else {
                    swal("", response.d.responseMessage, "error");
                }
            }
        })
    }
    else {
        toastr.error("All * fields are mandatory.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function billRequiredField() {
    var boolcheck = true;
    $('.reqb').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning !important');
        } else {
            $(this).removeClass('border-warning !important');
        }
    });

    $('.ddlreqb').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).addClass('border-warning !important');
        } else {
            $(this).removeClass('border-warning !important');
        }
    });

    if ($('#ddlZipCode').val() == '0') {
        $('#ddlZipCode').closest('div').find('.select2-selection__rendered').attr('style', 'border:1px solid #FF9149  !important');
    } else {
        $('#ddlZipCode').closest('div').find('.select2-selection__rendered').removeAttr('style');
    }
    return boolcheck;
}

function shipRequiredField() {
    var boolcheck = true;
    $('.reqs').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning !important');
        } else {
            $(this).removeClass('border-warning !important');
        }
    });

    $('.ddlreqs').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).addClass('border-warning !important');
        } else {
            $(this).removeClass('border-warning !important');
        }
    });

    if ($('#ddlZipCode').val() == '0') {
        $('#ddlZipCode').closest('div').find('.select2-selection__rendered').attr('style', 'border:1px solid #FF9149  !important');
    } else {
        $('#ddlZipCode').closest('div').find('.select2-selection__rendered').removeAttr('style');
    }
    return boolcheck;
}

function showBilling() {
    $("#btnBilling").show();
    $("#btnShowBilling").hide();
    $("#BillEntry").show();
    $("#divPopUpBillingAddress").hide();
}

function showShipping() {
    $("#btnAddShipping").show();
    $("#btnShowShipping").hide();
    $("#ShipEntry").show();
    $("#divPopUpShippingAddress").hide();
}

function PopUpBilling() {
    var UserName = $("#hfEmail").val();
    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var rc = new Object();
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.StoreId = StoreId;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Registration.asmx/GetBilling",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        async: true,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var htmlBill = '', htmlShip = '';
            var defBill = 0, defShip = 0;
            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    defBill = response.d.responseNewData[0].DefaultBillAdd;
                    var list = response.d.responseNewData[0].BillingShippingList;
                    $.each(list, function (i, item) {
                        htmlBill += '<div class="col-md-4">'
                        htmlBill += '<div class="legend disable" style="min-height:125px">'
                        htmlBill += '<div class="col-md-12" style=" padding: 2px; margin: 13px;min-height:80px ">'
                        htmlBill += '<p>' + item.Address + ' , </p>'
                        htmlBill += '<p><span style="font-weight:600">' + item.City + '</span>,<span style="font-weight:600">' + item.StateName + '</span> , <span style="font-weight:700">' + item.Zipcode + '</span><br/>'
                        htmlBill += '</div>'
                        htmlBill += '<div style="position: relative;bottom: 0;text-align: center;">'
                        if (defBill == item.AutoId) {
                            htmlBill += '<button type="button" disabled="disabled" class="btn btn-xs btn-warning" style="position:unset;bottom: 0;text-align: center;"><i class="fa fa-check" aria-hidden="true"></i> Deliver here</button></div>'
                        }
                        else {
                            htmlBill += '<input type="button" value="Deliver here" onclick="BillingDeliver(' + item.AutoId + ')" class="btn btn-xs btn-warning" style="position:unset;bottom: 0;text-align: center;"></div>'
                        }
                        htmlBill += '</div>'
                        htmlBill += '</div>'
                    });
                    $("#divPopUpBillingAddress").html(htmlBill);
                }
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    })
}

function PopUpShipping() {
    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var UserName = $("#hfEmail").val();
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var rc = new Object();
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.StoreId = StoreId;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Registration.asmx/GetBilling",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        async: true,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var htmlBill = '', htmlShip = '';
            var defBill = 0, defShip = 0;
            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    defShip = response.d.responseNewData[0].DefaultShipAdd;
                    var list = response.d.responseNewData[0].BillingShippingList; 
                    $.each(list, function (i, item) {
                        htmlShip += '<div class="col-md-4">'
                        htmlShip += '<div class="legend disable" style="min-height:125px">'
                        htmlShip += '<div class="col-md-12" style=" padding: 2px; margin: 13px;min-height:80px ">'
                        htmlShip += '<p>' + item.Address + ' , </p>'
                        htmlShip += '<p><span style="font-weight:600">' + item.City + '</span>,<span style="font-weight:600">' + item.StateName + '</span> , <span style="font-weight:700">' + item.Zipcode + '</span><br/>'
                        htmlShip += '</div>'
                        htmlShip += '<div style="position: relative;bottom: 0;text-align: center;">'
                        if (defShip == item.AutoId) {
                            htmlShip += '<button type="button" disabled="disabled"  class="btn btn-xs btn-warning" style="position:unset;bottom: 0;text-align: center;"><i class="fa fa-check" aria-hidden="true"></i> Deliver Here</button></div>'
                        }
                        else {
                            htmlShip += '<input type="button" value="Deliver here" onclick="ShippingDeliver(' + item.AutoId + ')" class="btn btn-xs btn-warning" style="position:unset;bottom: 0;text-align: center;"></div>'
                        }
                        htmlShip += '</div>'
                        htmlShip += '</div>'
                    });
                    $("#divPopUpShippingAddress").html(htmlShip);
                }
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    })
}

function BillingDeliver(AID)  //Change Billing Details
{
    var UserName = $("#hfEmail").val();
    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var rc = new Object();
    rc.Autoid = AID;
    rc.Type = 0;
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.StoreId = StoreId;
    var data = {       
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Registration.asmx/UpdateShippingBilling",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                swal("", "Billing updated successfully.", "success");                       
                getBillingAddress();
                $("#Billing").modal('hide');
            }
            else {
                swal("", "Oops something went wrong.", "error");               
            }
        }
    })
}

function ShippingDeliver(AID) //Change Shipping Details 
{
    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var UserName = $("#hfEmail").val();
    var rc = new Object();
    rc.Autoid = AID;
    rc.Type = 1;
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.StoreId = StoreId;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/Registration.asmx/UpdateShippingBilling",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                swal("", "Shipping updated successfully.", "success");                     
                getBillingAddress();
                $("#Shipping").modal('hide');
            }
            else {
                swal("", "Oops something went wrong.", "error");                    
            }
        }
    })
}

function CheckOutDetails() {
    var ShipHtml = '';
    var PayHtml = '';
    var apiUrl = $("#ApiAppKey").html();
    var UserAutoId = parseInt($("#UserAutoId").val()) || 0;
    var UserName = $("#hfEmail").val();
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var rc = new Object();
    rc.UserAutoId = UserAutoId;
    rc.StoreId = StoreId;
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.Location = $("#hfLocation").val()
    rc.Token = getToken();
    var data = {
        requestContainer: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/CheckOutDetails",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (response.d.responseNewData.length > 0) {
                    var PriceList = response.d.responseNewData;
                    var ShipList = response.d.responseNewData[0].ShippingList;
                    var PayList = response.d.responseNewData[0].PaymentList;
                    if (parseFloat(PriceList[0].WeightTax) > 0) {
                        $("#trWeightTax").show();
                        $("#spWeightTax").html(parseFloat(PriceList[0].WeightTax).toFixed(2));
                    }
                    else {
                        $("#trWeightTax").hide();
                    }
                    if (parseFloat(PriceList[0].Mltax) > 0) {
                        $("#trMLTax").show();
                        $("#spMLTax").html(parseFloat(PriceList[0].Mltax).toFixed(2));
                    }
                    else {
                        $("#trMLTax").hide();
                    }
                    var ship = 0; 
                    $.each(ShipList, function (i, ShipItem) {
                        if (ShipItem.ShippingCharge == 0) {
                            ship = 'Free';
                        }
                        else {
                            ship = '$' + (parseFloat(ShipItem.ShippingCharge).toFixed(2));
                        }
                        ShipHtml += '<div>'
                        if (PriceList[0].ShippingId != null) {
                            if (ShipItem.ShippingAutoId == PriceList[0].ShippingId) {
                                ShipHtml += '<input type="radio"  name="shipmethod" id="ups-regular" checked="checked">'
                            }
                            else {
                                ShipHtml += '<input type="radio" onclick="changeShipping(' + ShipItem.ShippingAutoId + ',1)" name="shipmethod" id="ups-regular">'
                            }
                        }
                        else if (ShipItem.IsDefault == '1') {
                            ShipHtml += '<input type="radio"  name="shipmethod" id="ups-regular"  checked="checked">'
                        }
                        else {
                            ShipHtml += '<input type="radio" onclick="changeShipping(' + ShipItem.ShippingAutoId + ',1)" name="shipmethod" id="ups-regular" >'
                        }
                        ShipHtml += ' <span>' + ShipItem.ShippingType + ' - <span class="btn btn-xs btn-success">' + ship + '</span></span> '
                        ShipHtml += '</div>'
                    });

                    $("#spTotalItem").html(PriceList[0].TotalItem);
                    $("#shipDIV").html(ShipHtml);
                    $.each(PayList, function (j, PayItem) {
                        PayHtml += '<div>'
                        if (PriceList[0].PaymentId != null) {
                            if (PayItem.PaymentAutoId == PriceList[0].PaymentId) {
                                PayHtml += '<input type="radio"  name="paymethod" id="cash" checked="checked">'
                            }
                            else {
                                PayHtml += '<input type="radio" onclick="changeShipping(' + PayItem.PaymentAutoId + ',2)" name="paymethod" id="cash">'
                            }
                        }
                        else if (PayItem.IsDefault == '1') {
                            PayHtml += '<input type="radio"  name="paymethod" id="cash"  checked="checked">'
                        }
                        else {
                            PayHtml += '<input type="radio" onclick="changeShipping(' + PayItem.PaymentAutoId + ',2)" name="paymethod" id="cash">'
                        }
                        PayHtml += '<span>' + PayItem.PaymentMode + '</label>'
                        PayHtml += '</div>'
                    });
                    $("#PayDIV").html(PayHtml);
                    $("#spTotalCount").html(parseFloat(PriceList[0].TotalItem));
                    $("#spSubTotal").html(parseFloat(PriceList[0].SubTotal).toFixed(2));
                    $("#spShipping").html(parseFloat(PriceList[0].Shipping).toFixed(2));
                    $("#spTotal").html(parseFloat(PriceList[0].PaybleAmount).toFixed(2));
                }
                else {
                    abandonSession();
                }
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    })
}

function changeShipping(AutoId, i) {    
    var apiUrl = $("#ApiAppKey").html();
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var UserName = $("#hfEmail").val();
    var rc = new Object();
    rc.Autoid = AutoId;
    rc.StoreId = StoreId;
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.Location = $("#hfLocation").val();
    rc.Type = i;
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/WsProductList.asmx/ChangeShipMethod",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                if (i == 1) {
                    toastr.success('Shipping method changed successfully.', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else {
                    toastr.success('Payment method changed successfully.', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                }
                CheckOutDetails();
            }
            else if (response.d.response == "Token") {
                $("#modalTokenMessage").modal('show');
            }
        }
    })
}

function PlaceOrder() {
    $("#btnPlaceOrder").prop('disabled', true);
    var apiUrl = $("#ApiAppKey").html();
    var StoreId = parseInt($("#hfStoreId").val()) || 0;
    var UserName = $("#hfEmail").val();
    var rc = new Object();
    rc.StoreId = StoreId;
    rc.UserName = UserName;
    rc.Token = getToken();
    rc.Location = $("#hfLocation").val();   
    var data = {
        cr: rc
    };
    $.ajax({
        type: "POST",
        url: apiUrl + "/WebApi/PlaceOrder.asmx/OrderPlace",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (response.d.response == "success") {
                    swal({
                        title: "",
                        text: "Order has been placed successfully.",
                        icon: "success",
                        showCancelButton: true,
                        allowOutsideClick: false,
                        closeOnClickOutside: false,
                        buttons: {
                            confirm: {
                                text: "Ok",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        }
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            localStorage.setItem('InvoiceDetails', JSON.stringify(response));
                            location.href = "/ThankYou";
                        }
                    });                                
            }
            else 
                swal({
                    title: "",
                    text: response.d.responseMessage,
                    icon: "warning",
                    showCancelButton: false,
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    buttons: {
                        confirm: {
                            text: "Ok",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        location.href ="/Cart"
                    }
                });            
        }
    })
}